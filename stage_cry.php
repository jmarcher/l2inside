<style type="text/css">
<!--
.Estilo1 {color: #000000}
-->
</style>
<h1 align="center">Weapons Special Abilities Information</h1>
<table align="center" border="1" cellpadding="2" cellspacing="0" width="100%">
  <tbody>
    <tr align="center">
      <th width="15%">Type </th>
      <th width="5%">Grade </th>
      <th width="35%">Weapon Name (Crystal Stage) </th>
      <th width="15%">Red Crystal </th>
      <th width="15%">Green Crystal </th>
      <th width="15%">Blue Crystal </th>
    </tr>
    <tr>
      <td rowspan="19" align="center">Blunt</td>
      <td align="center">C</td>
      <td align="center">Stick of Faith ( 5 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Mana Up</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Magic Hold</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Magic Shield</span></td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td align="center">Battle Axe ( 5 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Anger</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Rsk.Focus</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Haste</span></td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td align="center">Big Hammer ( 5 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Health</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Rsk.Focus</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Haste</span></td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td align="center">Silver Axe ( 5 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Anger</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Rsk.Focus</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Haste</span></td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td align="center">Skull Graver ( 5 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Anger</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Health</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Rsk.Focus</span></td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td align="center">Dwarven War Hammer ( 6 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Anger</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Health</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Haste</span></td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td align="center">Club of Nature ( 7 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Magic Hold</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Magic Mental Shield</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Acumen</span></td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td align="center">Mace of The Underworld ( 7 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Mana Up</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Magic Silence</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Conversion</span></td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td align="center">Nirvana Axe ( 7 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Magic Poison</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Magic Power</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Magic Weakness</span></td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td align="center">Stick of Eternity ( 7 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Empower</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Rsk., Evasion</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Bless the Body</span></td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td align="center">War Axe ( 7 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Anger</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Health</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Haste</span></td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td align="center">Yaksa Mace ( 8 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Anger</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Health</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Rsk. Focus</span></td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td align="center">Heavy War Axe ( 9 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Anger</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Health</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Rsk. Focus</span></td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td align="center">Art of Battle Axe ( 10 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Health</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Rsk. Focus</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Haste</span></td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td align="center">Deadman's Glory ( 10 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Anger</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Health</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Haste</span></td>
    </tr>
    <tr>
      <td align="center">A</td>
      <td align="center">Meteor Shower ( 11 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Focus</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Critical Bleed</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Rsk. Haste</span></td>
    </tr>
    <tr>
      <td align="center">A</td>
      <td align="center">Elysian Axe ( 12 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Health</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Anger</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Critical Drain</span></td>
    </tr>
    <tr>
      <td align="center">S</td>
      <td align="center">Arcana Mace ( 13 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Acumen</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">MP Regeneration</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Mana Up</span></td>
    </tr>
    <tr>
      <td align="center">S</td>
      <td align="center">Basalt Battlehammer ( 13 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">HP Drain</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Health</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">HP Regeneration</span></td>
    </tr>
    <tr>
      <td rowspan="18" align="center">Two-Handed Blunts</td>
      <td align="center">C</td>
      <td align="center">Crystal Staff ( 5 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Rsk. Evasion</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Mana Up</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Bodily Blessing</span></td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td align="center">Heavy Doom Axe ( 5 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Magic Poison</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Magic Weakness</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Magic Chaos</span></td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td align="center">Heavy Doom Hammer ( 5 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Magic Regeneration</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Mental Shield</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Magic Hold</span></td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td align="center">Cursed Staff ( 6 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Magic Hold</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Magic Poison</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Magic Weakness</span></td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td align="center">Inferno Staff ( 7 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Acumen</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Magic Silence</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Magic Paralyze</span></td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td align="center">Paagrio Hammer ( 7 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Rsk. Evasion</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Magic Poison</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Magic Weakness</span></td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td align="center">Paradia Staff ( 7 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Magic Regeneration</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Mental Shield</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Magic Hold</span></td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td align="center">Sage's Staff ( 7 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Magic Hold</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Magic Poison</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Magic Weakness</span></td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td align="center">Paagrio Axe ( 7 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Mana Up</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Magic Weakness</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Magic Chaos</span></td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td align="center">Deadman's Staff ( 8 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Magic Regeneration</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Mental Shield</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Magic Hold</span></td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td align="center">Demon's Staff ( 8 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Magic Poison</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Magic Weakness</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Magic Chaos</span></td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td align="center">Ghoul's Staff ( 8 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Rsk. Evasion</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Mana Up</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Bodily Blessing</span></td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td align="center">Sprite's Staff ( 9 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Magic Regeneration</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Mental Shield</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Magic Hold</span></td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td align="center">Staff of Evil Spirit ( 10 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Magic Focus</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Bodily Blessing</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Magic Poison</span></td>
    </tr>
    <tr>
      <td align="center">A</td>
      <td align="center">Dasparion's Staff ( 11 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Mana Up</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Conversion</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Acumen</span></td>
    </tr>
    <tr>
      <td align="center">A</td>
      <td align="center">Branch of The Mother Tree ( 12 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Conversion</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Magic Damage</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Acumen</span></td>
    </tr>
    <tr>
      <td align="center">S</td>
      <td align="center">Imperial Staff ( 13 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Empower</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">MP Regeneration</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Magic Hold</span></td>
    </tr>
    <tr>
      <td align="center">S</td>
      <td align="center">Dragon Hunter Axe ( 13 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">HP Regeneration</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Health</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">HP Drain</span></td>
    </tr>
    <tr>
      <td rowspan="10" align="center">Bow</td>
      <td align="center">C</td>
      <td align="center">Crystallized Ice Bow ( 5 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Guidance</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Evasion</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Quick Recovery</span></td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td align="center">Elemental Bow ( 5 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Guidance</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Miser</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Quick Recovery</span></td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td align="center">Noble Elven Bow ( 6 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Evasion</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Miser</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Cheap Shot</span></td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td align="center">Akat Long Bow ( 7 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Guidance</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Evasion</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Miser</span></td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td align="center">Eminence Bow ( 8 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Guidance</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Miser</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Cheap Shot</span></td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td align="center">Dark Elven Long Bow ( 9 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Evasion</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Critical Bleed</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Miser</span></td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td align="center">Bow of Peril ( 10 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Guidance</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Quick Recovery</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Cheap Shot</span></td>
    </tr>
    <tr>
      <td align="center">A</td>
      <td align="center">Carnage Bow ( 11 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Light</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Critical Bleed</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Mana Up</span></td>
    </tr>
    <tr>
      <td align="center">A</td>
      <td align="center">Soul Bow ( 12 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Cheap Shot</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Quick Recovery</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Critical Poison</span></td>
    </tr>
    <tr>
      <td align="center">S</td>
      <td align="center">Draconic Bow ( 13 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Cheap Shot</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Focus</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Critical Slow</span></td>
    </tr>
    <tr>
      <td rowspan="13" align="center">Dagger</td>
      <td align="center">C</td>
      <td align="center">Cursed Dagger ( 5 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Critical Bleed</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Critical Poison</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Rsk. Haste</span></td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td align="center">Dark Elven Dagger ( 5 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Focus</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Back Blow</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Rsk. Haste</span></td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td align="center">Soulfire Dirk ( 6 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Magic Hold</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Magic Silence</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Mana Up</span></td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td align="center">Stiletto ( 6 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Critical Bleed</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Critical Poison</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Rsk. Haste</span></td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td align="center">Dark Screamer ( 7 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Evasion</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Focus</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Critical Bleed</span></td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td align="center">Grace Dagger ( 7 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Evasion</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Focus</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Back Blow</span></td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td align="center">Crystal Dagger ( 8 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Critical Bleed</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Critical Poison</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Critical Damage</span></td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td align="center">Hell Knife ( 9 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Mental Shield</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Magic Weakness</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Magic Regeneration</span></td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td align="center">Kris ( 9 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Evasion</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Focus</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Back Blow</span></td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td align="center">Demon's Sword ( 10 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Critical Bleed</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Critical Poison</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Critical Damage</span></td>
    </tr>
    <tr>
      <td align="center">A</td>
      <td align="center">Bloody Orchid ( 11 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Focus</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Back Blow</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Critical Bleed</span></td>
    </tr>
    <tr>
      <td align="center">A</td>
      <td align="center">Soul Separator ( 12 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Guidance</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Critical Damage</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Rsk. Haste</span></td>
    </tr>
    <tr>
      <td align="center">S</td>
      <td align="center">Angel Slayer ( 13 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Crt. Damage</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">HP Drain</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Haste</span></td>
    </tr>
    <tr>
      <td rowspan="9" align="center">Fists</td>
      <td align="center">C</td>
      <td align="center">Chakram ( 5 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Critical Drain</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Critical Poison</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Rsk. Haste</span></td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td align="center">Fist Blade ( 6 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Rsk. Evasion</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Rsk. Haste</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Haste</span></td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td align="center">Knuckle Duster ( 8 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Rsk. Evasion</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Rsk. Haste</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Haste</span></td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td align="center">Great Pata ( 8 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Critical Drain</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Critical Poison</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Rsk. Haste</span></td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td align="center">Arthro Nail ( 9 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Critical Poison</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Rsk. Evasion</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Rsk. Haste</span></td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td align="center">Bellion Cestus ( 10 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Critical Drain</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Critical Poison</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Rsk. Haste</span></td>
    </tr>
    <tr>
      <td align="center">A</td>
      <td align="center">Blood Tornado ( 11 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Haste</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Focus</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Anger</span></td>
    </tr>
    <tr>
      <td align="center">A</td>
      <td align="center">Dragon Grinder ( 12 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Rsk. Evasion</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Guidance</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Health</span></td>
    </tr>
    <tr>
      <td align="center">S</td>
      <td align="center">Demon Splinter ( 13 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Focus</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Health</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Crt. Stun</span></td>
    </tr>
    <tr>
      <td rowspan="13" align="center">Pole</td>
      <td align="center">C</td>
      <td align="center">Body Slasher ( 5 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Critical Stun</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Long Blow</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Wide Blow</span></td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td align="center">Orcish Glaive ( 5 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Anger</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Critical Stun</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Long Blow</span></td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td align="center">Scythe ( 5 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Anger</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Critical Stun</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Light</span></td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td align="center">Bec de Corbin ( 6 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Critical Stun</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Long Blow</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Light</span></td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td align="center">Poleaxe ( 7 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Wide Blow</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Long Blow</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Critical Stun</span></td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td align="center">Scorpion ( 7 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Anger</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Critical Stun</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Long Blow</span></td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td align="center">Widow Maker ( 7 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Critical Stun</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Long Blow</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Wide Blow</span></td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td align="center">Orcish Poleaxe ( 8 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Critical Stun</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Long Blow</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Wide Blow</span></td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td align="center">Great Axe ( 9 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Anger</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Critical Stun</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Light</span></td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td align="center">Lance ( 10 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Anger</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Critical Stun</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Long Blow</span></td>
    </tr>
    <tr>
      <td align="center">A</td>
      <td align="center">Halberd ( 11 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Haste</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Critical Stun</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Wide Blow</span></td>
    </tr>
    <tr>
      <td align="center">A</td>
      <td align="center">Tallum Glaive ( 12 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Guidance</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Health</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Wide Blow</span></td>
    </tr>
    <tr>
      <td align="center">S</td>
      <td align="center">Saint Spear ( 13 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Health</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Guidance</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Haste</span></td>
    </tr>
    <tr>
      <td rowspan="21" align="center">Sword</td>
      <td align="center">C</td>
      <td align="center">Stormbringer ( 5 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Critical Anger</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Focus</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Light</span></td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td align="center">Katana ( 6 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Focus</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Critical Damage</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Haste</span></td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td align="center">Raid Sword ( 6 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Focus</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Critical Drain</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Critical Poison</span></td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td align="center">Shamshir ( 6 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Guidance</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Back Blow</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Rsk. Evasion</span></td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td align="center">Spirit Sword ( 6 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Critical Damage</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Critical Poison</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Haste</span></td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td align="center">Homunkulus's Sword ( 7 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Acumen</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Conversion</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Magic Paralyze</span></td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td align="center">Sword of Whispering Death ( 7 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Empower</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Magic Power</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Magic Silence</span></td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td align="center">Caliburs ( 7 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Guidance</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Focus</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Critical Damage</span></td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td align="center">Sword of Delusion ( 7 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Focus</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Health</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Rsk. Haste</span></td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td align="center">Sword of Limit ( 7 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Guidance</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Critical Drain</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Health</span></td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td align="center">Sword of Nightmare ( 7 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Health</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Focus</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Light</span></td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td align="center">Tsurugi ( 7 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Focus</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Critical Damage</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Haste</span></td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td align="center">Samurai Longsword ( 8 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Focus</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Critical Damage</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Haste</span></td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td align="center">Sword of Valhalla ( 9 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Acumen</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Magic Weakness</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Magic Regeneration</span></td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td align="center">Keshanberk ( 9 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Guidance</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Focus</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Back Blow</span></td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td align="center">Sword of Damascus ( 10 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Focus</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Critical Damage</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Haste</span></td>
    </tr>
    <tr>
      <td align="center">A</td>
      <td align="center">Elemental Sword ( 11 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Magic Power</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Magic Paralyze</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Empower</span></td>
    </tr>
    <tr>
      <td align="center">A</td>
      <td align="center">Tallum Blade ( 11 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Critical Poison</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Haste</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Anger</span></td>
    </tr>
    <tr>
      <td align="center">A</td>
      <td align="center">Sword of Miracles ( 12 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Magic Power</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Magic Silence</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Acumen</span></td>
    </tr>
    <tr>
      <td align="center">A</td>
      <td align="center">Dark Legion's Edge ( 12 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Critical Damage</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Health</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Rsk. Focus</span></td>
    </tr>
    <tr>
      <td align="center">S</td>
      <td align="center">Forgotten Blade ( 13 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Haste</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Health</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Focus</span></td>
    </tr>
    <tr>
      <td rowspan="5" align="center">Two-Handed Swords</td>
      <td align="center">C</td>
      <td align="center">Flamberge ( 5 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Critical Damage</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Focus</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Light</span></td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td align="center">Berserker Blade ( 8 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Focus</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Critical Damage</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Haste</span></td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td align="center">Great Sword ( 9 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Health</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Critical Damage</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Focus</span></td>
    </tr>
    <tr>
      <td align="center">A</td>
      <td align="center">Dragon Slayer ( 12 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Health</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Critical Bleed</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Critical Drain</span></td>
    </tr>
    <tr>
      <td align="center">S</td>
      <td align="center">Heaven's Divider ( 13 )</td>
      <td align="center" bgcolor="#ff9999"><span class="Estilo1">Haste</span></td>
      <td align="center" bgcolor="#99ff99"><span class="Estilo1">Health</span></td>
      <td align="center" bgcolor="#99ccff"><span class="Estilo1">Focus</span></td>
    </tr>
  </tbody>
</table>
<h1 align="center">Dual Sword Special Abilities</h1>
<div align="center">The dual sword SA's are automatically applied when the weapon is enchanted to +4 or more.</div>
<table align="center" border="1" cellpadding="2" cellspacing="0" width="100%">
  <tbody>
    <tr>
      <th width="5%">Grade</th>
      <th width="30%">Dual Weapon Name</th>
      <th width="65%">Description</th>
    </tr>
    <tr>
      <td align="center">C</td>
      <td>Crimson Sword*Elven Long Sword</td>
      <td>Accuracy will increase by 2.</td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td>Elven Sword*Elven Long Sword</td>
      <td>P. Atk will increase by 61 during a critical attack.</td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td>Sword of Revolution*Sword of Revolution</td>
      <td>Maximum HP will increase by 25%.</td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td>Sword of Revolution*Elven Long Sword</td>
      <td>Critical Attack will increase by 50.</td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td>Elven Long Sword*Elven Long Sword</td>
      <td>Atk. Spd. will increase by 4%.</td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td>Stormbringer*Stormbringer</td>
      <td>Accuracy will increase by 2.</td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td>Stormbringer*Katana</td>
      <td>Maximum HP will increase by 25%.</td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td>Stormbringer*Raid Sword</td>
      <td>Atk. Spd. will increase by 2%.</td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td>Stormbringer*Shamshir</td>
      <td>P. Atk. will increase by 49 during a critical attack.</td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td>Stormbringer*Spirit Sword</td>
      <td>Critical attack will increase by 24.</td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td>Katana*Katana</td>
      <td>Atk. Spd. will increase by 4%.</td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td>Katana*Raid Sword</td>
      <td>P. Atk. will increase by 84 during a critical attack.</td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td>Katana*Spirit Sword</td>
      <td>Accuracy will increase by 2.</td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td>Raid Sword*Raid Sword</td>
      <td>Atk. Spd. will increase by 4%.</td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td>Shamshir*Katana</td>
      <td>P. Atk. will increase by 84 during a critical attack.</td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td>Shamshir*Raid Sword</td>
      <td>Critical will increase by 40.</td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td>Shamshir*Shamshir</td>
      <td>Accuracy will increase by 2.</td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td>Shamshir*Spirit Sword</td>
      <td>Maximum HP will increase by 25%.</td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td>Spirit Sword*Raid Sword</td>
      <td>Critical will increase by 40.</td>
    </tr>
    <tr>
      <td align="center">C</td>
      <td>Spirit Sword*Spirit Sword</td>
      <td>Maximum HP will increase by 25%.</td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td>Stormbringer*Caliburs</td>
      <td>Accuracy will increase by 5.</td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td>Stormbringer*Sword of Delusion</td>
      <td>Maximum HP will increase by 25%.</td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td>Stormbringer*Sword of Limit</td>
      <td>P. Atk will increase by 166 during a critical attack.</td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td>Stormbringer*Sword of Nightmare</td>
      <td>Critical will increase by 76.</td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td>Stormbringer*Tsurugi</td>
      <td>Atk. Spd. will increase by 7%.</td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td>Katana*Caliburs</td>
      <td>Accuracy will increase by 4.</td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td>Katana*Sword of Delusion</td>
      <td>Maximum HP will increase by 25%.</td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td>Katana*Sword of Limit</td>
      <td>P. Atk. will increase by 146 during a critical attack.</td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td>Katana*Sword of Nightmare</td>
      <td>Critical will increase by 65.</td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td>Katana*Tsurugi</td>
      <td>Atk. Spd. will increase by 6%.</td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td>Raid Sword*Caliburs</td>
      <td>Accuracy will increase by 4.</td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td>Raid Sword*Sword of Delusion</td>
      <td>Maximum HP will increase by 25%.</td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td>Raid Sword*Sword of Limit</td>
      <td>P. Atk. will increase by 146 during a critical attack.</td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td>Raid Sword*Sword of Nightmare</td>
      <td>Critical will increase by 65.</td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td>Raid Sword*Tsurugi</td>
      <td>Atk. Spd. will increase by 6%.</td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td>Shamshir*Caliburs</td>
      <td>Accuracy will increase by 4.</td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td>Shamshir*Sword of Delusion</td>
      <td>Maximum HP will increase by 25%.</td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td>Shamshir*Sword of Limit</td>
      <td>P. Atk. will increase by 146 during a critical attack.</td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td>Shamshir*Sword of Nightmare</td>
      <td>Critical will increase by 65.</td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td>Shamshir*Tsurugi</td>
      <td>Atk. Spd. will increase by 6%.</td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td>Spirit Sword*Caliburs</td>
      <td>Accuracy will increase by 4.</td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td>Spirit Sword*Sword of Delusion</td>
      <td>Maximum HP will increase by 25%.</td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td>Spirit Sword*Sword of Limit</td>
      <td>P. Atk. will increase by 146 during a critical attack.</td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td>Spirit Sword*Sword of Nightmare</td>
      <td>Critical will increase by 65.</td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td>Spirit Sword*Tsurugi</td>
      <td>Atk. Spd. will increase by 6%.</td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td>Caliburs*Caliburs</td>
      <td>P. Atk. will increase by 127 during a critical attack.</td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td>Caliburs*Sword of Delusion</td>
      <td>Critical will increase by 54.</td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td>Caliburs*Sword of Limit</td>
      <td>Maximum HP will increase by 25%.</td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td>Caliburs*Sword of Nightmare</td>
      <td>Atk. Spd. will increase by 5%.</td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td>Caliburs*Tsurugi</td>
      <td>Accuracy will increase by 4.</td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td>Stormbringer*Samurai Long sword</td>
      <td>Accuracy will increase by 4.</td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td>Sword of Delusion*Sword of Delusion</td>
      <td>Accuracy will increase by 4.</td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td>Sword of Delusion*Sword of Nightmare</td>
      <td>P. Atk. will increase by 127 during a critical attack.</td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td>Sword of Delusion*Tsurugi</td>
      <td>Maximum HP will increase by 25%.</td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td>Sword of Limit*Sword of Delusion</td>
      <td>Maximum HP will increase by 25%.</td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td>Sword of Limit*Sword of Limit</td>
      <td>P. Atk. will increase by 127 during a critical attack.</td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td>Sword of Limit*Sword of Nightmare</td>
      <td>Critical will increase by 54.</td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td>Sword of Limit*Tsurugi</td>
      <td>Atk. Spd. will increase by 5%.</td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td>Sword of Nightmare*Sword of Nightmare</td>
      <td>Critical will increase by 54.</td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td>Sword of Nightmare*Tsurugi</td>
      <td>Atk. Spd. will increase by 5%.</td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td>Tsurugi*Tsurugi</td>
      <td>Critical will increase by 54.</td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td>Katana*Samurai Long Sword</td>
      <td>P. Atk. will increase by 106 during a critical attack.</td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td>Raid Sword*Samurai Long Sword</td>
      <td>Critical will increase by 43.</td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td>Shamshir*Samurai Long Sword</td>
      <td>Accuracy will increase by 3.</td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td>Spirit Sword*Samurai Long Sword</td>
      <td>Maximum HP will increase by 25%.</td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td>Caliburs*Samurai Long Sword</td>
      <td>Atk. Spd. will increase by 5%.</td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td>Sword of Delusion*Samurai Long Sword</td>
      <td>P. Atk. will increase by 133 during a critical attack.</td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td>Sword of Limit*Samurai Long Sword</td>
      <td>Accuracy will increase by 3.</td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td>Sword of Nightmare*Samurai Long Sword</td>
      <td>Maximum HP will increase by 25%.</td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td>Tsurugi*Samurai Long Sword</td>
      <td>Critical will increase by 52.</td>
    </tr>
    <tr>
      <td align="center">B</td>
      <td>Samurai Long Sword*Samurai Long Sword</td>
      <td>Atk. Spd. will increase by 4%.</td>
    </tr>
    <tr>
      <td align="center">A</td>
      <td>Keshanberk*Keshanberk</td>
      <td>Atk. Spd. will increase by 8%. Inflicts additional damage to target during PvP.</td>
    </tr>
    <tr>
      <td align="center">A</td>
      <td>Keshanberk*Damascus</td>
      <td>Maximum HP will increase by 25% as well. Enhances damage to target during PvP.</td>
    </tr>
    <tr>
      <td align="center">A</td>
      <td>Damascus*Damascus</td>
      <td>Accuracy will increase by 6. Enhances damage to target during PvP.</td>
    </tr>
    <tr>
      <td align="center">S</td>
      <td>Tallum Blade*Dark Legion's Edge</td>
      <td>MaxHP +15%, MaxMP +20%, MaxCP +30% when enchanted by 4 or more. Also enhances damage to target during PvP.</td>
    </tr>
  </tbody>
</table>
<h1 align="center">Special Abilities List</h1>
<table align="center" border="1" cellpadding="2" cellspacing="0" width="100%">
  <tbody>
    <tr align="center">
      <th width="20%">Name</th>
      <th width="80%">Description</th>
    </tr>
    <tr>
      <td>Acumen</td>
      <td>Increases Casting Speed by 15%.</td>
    </tr>
    <tr>
      <td>Anger</td>
      <td>Decreases the maximum amount of HP by 15% and increases attack power.</td>
    </tr>
    <tr>
      <td>Back Blow</td>
      <td>Critical Rate increases when attacking from behind.</td>
    </tr>
    <tr>
      <td>Cheap Shot</td>
      <td>Greatly reduces the amount of MP used when attacking with a bow, for every shot.</td>
    </tr>
    <tr>
      <td>Conversion</td>
      <td>Increases maximum MP by 60% and decreases maximum HP by 40%</td>
    </tr>
    <tr>
      <td>Crt. Anger</td>
      <td>When crit, own HP decreases and gives extra damage to target.</td>
    </tr>
    <tr>
      <td>Crt. Bleed</td>
      <td>When crit, high chance of bleed effect.</td>
    </tr>
    <tr>
      <td>Crt. Damage</td>
      <td>When crit, gives extra damage.</td>
    </tr>
    <tr>
      <td>Crt. Drain</td>
      <td>When crit, drains some of HP off target.</td>
    </tr>
    <tr>
      <td>Crt. Poison</td>
      <td>When crit, high chance of poison.</td>
    </tr>
    <tr>
      <td>Crt. Stun</td>
      <td>When crit, high chance of stun.</td>
    </tr>
    <tr>
      <td>Empower</td>
      <td> Increases magic power.</td>
    </tr>
    <tr>
      <td>Evasion</td>
      <td>Evasion increases permanently +4.</td>
    </tr>
    <tr>
      <td>Focus</td>
      <td>Critical Rate increases permanently +15%.</td>
    </tr>
    <tr>
      <td>Guidance</td>
      <td>Accuracy increases permanently +4.</td>
    </tr>
    <tr>
      <td>Haste</td>
      <td>Attack Speed increases permanently +7%.</td>
    </tr>
    <tr>
      <td>Health</td>
      <td>Increases maximum HP by 25%.</td>
    </tr>
    <tr>
      <td>Light</td>
      <td>Lowers the weapon's weight by 70%.</td>
    </tr>
    <tr>
      <td>Long Blow</td>
      <td>Polearm range increases permanently.</td>
    </tr>
    <tr>
      <td>Magic Bless Body</td>
      <td>When using a &quot;good&quot; spell on target, chance of Bless the Body buff.</td>
    </tr>
    <tr>
      <td>Magic Chaos</td>
      <td>When using a &quot;bad&quot; spell on target, chance of Chaos.</td>
    </tr>
    <tr>
      <td>Magic Damage</td>
      <td> When using harmful magic on a target, it delivers additional magic damage with a fixed percentage.</td>
    </tr>
    <tr>
      <td>Magic Focus</td>
      <td>When using a &quot;good&quot; spell on target, chance of Focus buff.</td>
    </tr>
    <tr>
      <td>Magic Hold</td>
      <td>When using a &quot;bad&quot; spell on target, chance of Hold.</td>
    </tr>
    <tr>
      <td>Magic Mental Shield</td>
      <td>When using a &quot;good&quot; spell on target, chance of Mental Shield buff.</td>
    </tr>
    <tr>
      <td>Magic Might</td>
      <td>When using a &quot;good&quot; spell on target, chance of Might buff.</td>
    </tr>
    <tr>
      <td>Magic Paralyze</td>
      <td> When using harmful magic on a target, it will paralyze the target with a fixed percentage.</td>
    </tr>
    <tr>
      <td>Magic Poison</td>
      <td>When using a &quot;bad&quot; spell on target, chance of Poison.</td>
    </tr>
    <tr>
      <td>Magic Power</td>
      <td> When attacking with magic, the amount of MP used increases with a fixed percentage and magic power also increases.</td>
    </tr>
    <tr>
      <td>Magic Regeneration</td>
      <td>When using a &quot;good&quot; spell on target, chance of Regeneration buff.</td>
    </tr>
    <tr>
      <td>Magic Shield</td>
      <td>When using a &quot;good&quot; spell on target, chance of Shield buff.</td>
    </tr>
    <tr>
      <td>Magic Silence</td>
      <td>When using harmful magic, 10 % chance to cast Silence against target. Effect 1.</td>
    </tr>
    <tr>
      <td>Magic Weakness</td>
      <td>When using a &quot;bad&quot; spell on target, chance of Weakness.</td>
    </tr>
    <tr>
      <td>Mana Up</td>
      <td>Increases maximum MP by 30%</td>
    </tr>
    <tr>
      <td>Mental Shield </td>
      <td>When using a &quot;good&quot; spell on target, chance of Mental Shield buff.</td>
    </tr>
    <tr>
      <td>Might Mortal</td>
      <td>Permanently  increases chance of Mortal Blow/Deadly Blow (type of styles) success  rate. (Note: This SA can no longer be imbued on a weapon. It was  formerly available on Demon Sword, Crystal Dagger, Stilletto, Dark  Elven Dagger)</td>
    </tr>
    <tr>
      <td>Miser</td>
      <td>Chance of shoulshot consumption decreasing by 0 to 4 shots per attack.</td>
    </tr>
    <tr>
      <td>Quick Recovery</td>
      <td>Reuse/Cast delay decreases permanently.</td>
    </tr>
    <tr>
      <td>Rsk. Evasion</td>
      <td>When HP is under 60%, evasion increases.</td>
    </tr>
    <tr>
      <td>Rsk. Focus</td>
      <td>When HP is under 60%, crit chance increases.</td>
    </tr>
    <tr>
      <td>Rsk. Haste</td>
      <td>When HP is under 60%, attack speed increases.</td>
    </tr>
    <tr>
      <td>Wide Blow</td>
      <td>Polearm effective angle increases permanently.</td>
    </tr>
  </tbody>
</table>
<table cellspacing="0" cellpadding="0">
  <tr>
    <td><table align="center" border="0" cellpadding="0" cellspacing="0" width="790">
      <tbody>
        <tr>
          <td align="center"><h1 align="center">Special Ability (SA) Guide</h1>
                <p>Weapon Special Abilities, also known as SA's, are available on a wide variety of C, B, A and S   grade weapons in the game. Each weapon, except dual weapons, can have a choice of 1 out of 3   special abilities and you can never have more than 1 special ability on a weapon. Dual Swords   get 1 set special ability and do not get to chose Special Ability. Weapon SA's are <strong>non-transferable</strong>, so if you want to exchange or upgrade your weapon at   Blacksmith of Mammon you will have to remove your SA from the weapon first. (This can be   done by the Black Marketer of Mammon found in most major towns).</p>
            <strong>How to get special ability (SA) on your weapon?</strong>
                <p><strong>Weapons not listed above:</strong> These weapons cannot receive special abilities in   any way.</p>
            <p><strong>Dual Swords:</strong> Dual Swords gain their special ability by enchanting them to +4,   then they will receive the assigned special ability automatically. <u>Be warned</u> enchanting a weapon from +3 to +4 has a chance of destroying the weapon, leaving you with crystals   of the grade type of the dual swords.</p>
            <p><strong>Any other weapon listed:</strong> Any other weapon can receive their SA regardless   of enchanted status. To apply SA to a weapon, you will need a crystal of appropriate stage (level)   and color, then go to a blacksmith and pay for having the SA applied to the weapon, payment will   be in appropriate grade gemstones + money. Example: If you want to have the Haste Special Ability   on an Art of Battle Axe (highest B-grade blunt), you will have to acquire a stage 10 blue crystal,   339 b-grade gemstones and money, then go to a blacksmith, tell him you want to enhance your weapon,   select the Special Ability from the list (the weapon has to be in your inventory) and click ok,   you will now get the weapon with SA in your inventory. Enchantments on a weapon are not lost when   you apply your SA, and a weapon with an SA can be further enhanced without losing the SA (unless   you fail the enchant and shatter the actual weapon). Its worth it to note that if you fail a blessed   enchant weapon (rare enchant weapon scrolls available from high level raid bosses),   you do not lose the weapon or the SA.</p></td>
        </tr>
      </tbody>
    </table></td>
  </tr>
  <tr>
    <td width="730"></td>
  </tr>
  <tr>
    <td height="0" style="visibility:hidden;"> this is <em>online mmorpg game</em> 3d game multiplayer game internet game server adventure game fantasy mmorpg <em>massive multiplayer online graphic design</em> </td>
  </tr>
  <tr>
    <td><br /></td>
  </tr>
</table>
<p>&nbsp;</p>
