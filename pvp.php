<table align="center" border="0" cellpadding="0" cellspacing="1" width="100%">
  <tbody>
    <tr>
      <td><strong>PvP &amp; Karma</strong></td>
    </tr>

    <tr>
      <td colspan="3"><ul><li>A  chaotic character with a PK count of five or more will be transported  to Floran Village either upon clicking &ldquo;Return to the Nearest Village&rdquo;  or use of a Scroll of Escape.</li>
        <li>Hold the Ctrl key to attack a party member in the arena.</li>
        <li>Guards will not attack chaotic players who are using Silent Move.</li>
        <li>Killing a purple party member will not cause you to go red.</li>
        <li>Town  safe zones do not go into effect until you are actually inside the town  boundaries. For example, the tunnel descending into Dark Elf Village is  not a safe zone.</li>
        <li>Using the skill <em>Fake Death</em> to train agro mobs onto other players is not an exploit.</li>
        <li>You will not gain karma if you kill your summoned creature.</li>
        <li>Many special attack skills do not work on clan members, party members or white players.</li>
        <li>If you are killed by your opponent outside the PvP arena after fleeing from a duel, normal death rules will apply.</li>
      </ul></td>
    </tr>
  </tbody>
</table>
