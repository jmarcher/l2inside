<table align="center" border="0" cellpadding="0" cellspacing="1" width="100%">
  <tbody>
    <tr>
      <td><strong>Commerce &amp; Trade</strong></td>
    </tr>
    <tr>
      <td colspan="3"><ul>
        <li>The manor system opens for business every day at 8:00 PM CST (8:00 GMT+1 on Teon).</li>
        <li>To  ensure the security of your items, only trade items away that you are  willing to lose forever, including to friends. When trading items,  confirm every time that the item you are receiving is what you  expected, as some items have the same inventory icon. Do not drop any  items on the ground, especially when grouped. Always verify the exact  name of the player you are trading with or that requested the trade.</li>
        <li>Stores that contribute to blocking hallways, arches or passages in peace zones are subject to being relocated by GMs.</li>
        <li>Always type the exact price of items you want to sell/buy in the private store window. For example, '100000'</li>
        <li>Be  wary of trusting other players during item transactions. For example,  when exchanging items with other players who repeatedly cancel a trade,  they may seek to replace one of the offered items with one of lesser  value.</li>
        <li>
          <p>Always verify the identity of any player with whom  you would like to trade, as many names can look similar.For example, do  not auto-reply to private messages. Be sure to type their name out  manually.</p>
        </li>
        <li>Never drop an item unless you are willing to risk losing it forever.</li>
        <li>Always check the color of private store messages. Purple is 'Sell'; yellow is 'Buy'.</li>
      </ul></td>
    </tr>
  </tbody>
</table>
