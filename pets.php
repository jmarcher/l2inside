<? if (isset($_GET['file'])){?><table align="center" border="0" cellpadding="0" cellspacing="1" width="100%">
  <tbody>
    <tr>
      <td><strong>Pets</strong></td>
    </tr>

    <tr>
      <td colspan="3"><ul>
        <li>If  a pet dies and the owner&rsquo;s inventory is full, the items from the pet&rsquo;s  inventory will drop on the ground as there is no space to accommodate  the extra items.</li>
        <li>If pet food is in the pet's inventory and its  hunger gauge falls below 55%, it will automatically consume food to  increase its hunger gauge. But don&rsquo;t forget to continually give your  pet food, or it will die! Always keep an eye on your pet&rsquo;s hunger gauge.</li>
        <li>Only  castle lords and clan hall owners of Town of Aden clan halls, the  Fortress of the Dead, and the Devastated Castle clan hall may ride  Wyverns. </li>
        <li>Pets cannot pick up arrows, soulshots, spiritshots, blessed spiritshots, or compressed spiritshots and soulshots.</li>
        <li>You can check a pet's level by pointing your mouse at the collar icon.</li>
        <li>You may only summon one pet at a time.</li>
        <li>Your pet cannot return to its collar if its hunger drops below 40%.</li>
        <li>Certain resurrection spells will partially restore a dead pet's experience.</li>
        <li>If your pet equips a weapon or piece of armor once, it will automatically re-equip those items every time you summon the pet.</li>
        <li>You will gain karma if your pet PKs a player.</li>
        <li>To feed your pet, drag the pet food into the pet's inventory and then double-click on it.</li>
        <li>Hold the shift key when targeting a dead pet's name for resurrection.</li>
      </ul></td>
    </tr>
  </tbody>
</table>
<? }else{
	include "error.php";
}?>
