<? if (isset($_GET['file'])){?>
<table align="center" border="0" cellpadding="0" cellspacing="1" width="100%">
  <tbody>
    <tr>
      <td><strong>Interface</strong></td>
    </tr>
    <tr>
      <td colspan="3"><ul>
        <li>You  can hide dropped items on the ground by selecting the option in the  Game Options window. If you turn this function on, you can  significantly reduce lag arising from the client. You cannot obtain  hidden items on the ground by mouse, but you can use the Pick Up button.</li>
        <li>Press Alt + Enter while running Lineage II to switch between Window mode and full-screen.</li>
        <li>When  in a party, the party leader can delegate authority to a member of that  party through the Change of Party Leader button or the  /changepartyleader command.</li>
        <li>Town safe zones do not go into  effect until you are actually inside the town boundaries. For example,  the tunnel descending into Dark Elf Village is not a safe zone.</li>
        <li>You can find nearby NPCs using the /target command.</li>
        <li>Clicking the small triangle in the target window with a player selected will reveal clan and alliance affiliation.</li>
        <li>Double right-clicking on a party member will let you assist that party member by attacking the same mob.</li>
        <li>When  joining a party you will be able to see what the Party Leader has set  for the Drop Distribution. There is Random (goes to a random player  within the party) and Finders Keepers (whoever picks up the item gets  it in their inventory).</li>
        <li>When receiving private messages from  many players, you can toggle through their names by typing a quotation  mark, then using the up or down arrow.</li>
        <li>The &quot;Random Loot&quot; option is not influenced by level, party leadership, or any other statistical element. It is literally random.</li>
        <li>If you press the mousewheel, the camera angle will rotate 180 degrees.</li>
        <li>If  your character appears to &quot;fall through the world&quot;, this can be  corrected immediately by restarting to the character selection screen  and logging back in.</li>
        <li>When entering heavily populated regions  such as towns or siege areas, using the &quot;Lower Detail&quot; option (Alt+P)  can improve your overall game performance.</li>
      </ul></td>
    </tr>
  </tbody>
</table>
<?
}else{
	include "error.php";
}
?>
