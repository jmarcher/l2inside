<? if (isset($_GET['file'])){?><table align="center" border="0" cellpadding="0" cellspacing="1" width="100%">
  <tbody>
    <tr>
      <td><strong>Items</strong> </td>
    </tr>
    <tr>
      <td colspan="3"><ul>
        <li>Humans,  Elves, Dark Elves, and Orcs can sell up to three items when creating a  private store. Dwarves can sell up to four items in a private store.  This number can be increased by acquiring common skills, learned from  Fisherman&rsquo;s Guild Members in every port and town.</li>
        <li>Almost all  weapons and armor can be safely enchanted to +3, but beyond that,  success is based on a pre-defined rate of probability. One-piece type  armor can be safely enchanted up to +4.</li>
        <li>Armor sets that affect weight increase the amount at which your weight penalty is applied.</li>
        <li>Weapons with enhancements cannot be used in the creation of dual swords.</li>
        <li>Place  soulshots and spiritshots in the shortcut bar and right-click the  related shortcut to automatically use soulshots and spiritshots.  Right-click it again to turn off automatic use.</li>
        <li>Scrolls of  Escape may be interrupted if you are hit while attempting to teleport.  If this happens you will not teleport and your scroll will be gone.</li>
        <li>Use  /target [name] to target the player for resurrection with a spell or  scroll. The player must accept the resurrection attempt.</li>
        <li>If you crystalize an enchanted item you will receive additional crystals according to the level of enchantment.            For example, if you crystalize an item enchanted +4 or higher, you will receive more bonus crystals than items            enchanted to +3 or less.</li>
        <li>Combining swords of a certain grade can create dual swords of the next grade level.</li>
        <li>Characters  can carry up to 80 items on their person; this includes equipped items,  quest items and items in your regular inventory. Dwarves can carry up  to 100 items.</li>
        <li>When making dual swords, enchantments do not carry over from the original weapons.</li>
        <li>If  you have three swords, one of which is enchanted, the blacksmith will  randomly combine any two of them when making dual swords.</li>
        <li>When  you enchant items beyond +3, you run the risk of destroying the item  forever. When this occurs, you will receive a lesser amount of crystals  than if you had crystallized it.</li>
      </ul></td>
    </tr>
  </tbody>
</table>
<?
}else{
	include "error.php";
}
?>
