<table align="center" border="0" cellpadding="0" cellspacing="1" width="100%">
  <tbody>
    <tr>
      <td><strong>Quests</strong></td>
    </tr>
    <tr>
      <td colspan="3"><ul>
        <li>If  you exceed 80% of your weight capacity or if your inventory slots are  80% or more full (80 items for Dwarves and 64 items for other races),  you will not be able to have a conversation with an NPC regarding a  quest.</li>
        <li>When you complete your profession change quests, you  must deliver the final quest item(s) to the proper NPC before the  change will take effect. Do not delete these items, as the quests are  non-repeatable.</li>
        <li>If, in the middle of a quest, an NPC instructs you to <em>train more</em>, this means you need to level your character before you can continue with your quest.</li>
        <li>If  you are in the middle of a promotion quest, but change your mind about  the class you want to become, you may cancel this quest and start a  different one. Once you complete your transfer quest(s), you cannot  change your path.</li>
        <li>Some quest monsters must be killed solo to  get the item they drop. This means that other players may not attack or  even debuff the quest mob.</li>
        <li>Before you decide an item or NPC has  not spawned correctly, search the area thoroughly. Many quest monsters  will wander away from their spawn location.</li>
        <li>If you ever want to manually remove a quest item, open the regular inventory, click on                                    the &quot;Quest&quot; tab, then drag the item into the trash.</li>
        <li>If  you are instructed to use only a certain weapon to kill a quest  monster, this means you cannot use any alternate weapons or spells on  that monster.</li>
        <li>Deviating from in-game quest instructions by following unofficial guides may prevent you from continuing your quest.</li>
        <li>Players can have up to twenty-five quests at once.</li>
      </ul></td>
    </tr>
  </tbody>
</table>
