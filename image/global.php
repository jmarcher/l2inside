<?php

    /* Utilidades varias para el phpBB */
    /* Por Karnage : karnage@frag.cl */
    
    // url de tu phpBB     
    
    $url = "http://forum.l2inside.org";
    
    // path del config.php de tu phpBB
    
    $conf = "/home/l2inside/public_html/forum/config.php";
    
    // 
    
    $max = mktime(date("G"),date("i")+5,date("s"),date("m"),date("d"),date("Y"));
    
    // fin de configuracion 

    require_once ($conf);        
    
    function mysql_query ($sql) {
        global $dbhost,$dbuser,$dbpasswd,$dbname;
        
        mysql_connect ($dbhost,$dbuser,$dbpasswd) or die ("No se puede conectar a la BD!"); 
        mysql_select_db ($dbname);
            
        $res = mysql_query ($sql) or die ("Consulta inv�lida : $sql");
        return $res;    
    }
    
    function show_online_users () {
        global $table_prefix,$url,$max;
        
        $sql = "SELECT * FROM ".$table_prefix."sessions, ".$table_prefix."users 
                     WHERE session_viewonline = 1 AND session_user_id = user_id";
        $res = mysql_query ($sql);        
        echo "<ul>";
        
        while ($row = mysql_fetch_array($res)) {
            if ($row['session_time'] >= $row['session_start'] && $row['session_time'] <= $row['session_time']+300) :
                //echo $row['session_time']." <= ".$max;
                echo "<li><a href=\"".$url."/profile.php?mode=viewprofile&u=".$row['session_user_id']."\">".$row['username']."</a>";
            endif;        
        }
        echo "</ul>";    
    }

    function count_online_reg_users () {
        global $table_prefix;
        
        $sql = "SELECT count(session_id) AS counter FROM ".$table_prefix."sessions WHERE session_user_id > 0";
        $res = mysql_query ($sql);
        $row = mysql_fetch_array ($res);
        
        return $row['counter'];
    }
    
    
    function count_online_anom_users () {
        global $table_prefix;
        
        $sql = "SELECT count(session_id) AS counter FROM ".$table_prefix."sessions WHERE session_user_id = -1";
        $res = mysql_query ($sql);
        $row = mysql_fetch_array ($res);
        
        return $row['counter'];
    }
    
    function last_user_registered () {
            global $table_prefix,$url;
            
            $sql = "SELECT user_id,username FROM ".$table_prefix."users ORDER BY user_id DESC LIMIT 1";
            $res = mysql_query ($sql);
            
            while ($row = mysql_fetch_array($res)) {
                echo "<a href=\"".$url."/profile.php?mode=viewprofile&u=".$row['user_id']."\">".$row['username']."</a>";
            
            }
    }

    function count_total_registered_users () {
        global $table_prefix;
        
        $sql = "SELECT count(user_id) AS counter FROM ".$table_prefix."users WHERE user_id <> -1";
        $res = mysql_query ($sql);
        $row = mysql_fetch_array ($res);
        
        return $row['counter'];
    } 

?> 