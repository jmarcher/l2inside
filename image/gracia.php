<?php
if(isset($_GET['file'])){
?>
<p><span id="intelliTXT"><strong>The Chaotic Throne: The 2nd Throne / GRACIA &ndash; part 1 Patch Notes Translation </strong><br />
  <br />
    <strong>You are not allowed to use this translation in any commercial uses. </strong><br />
  <br />
    <strong>Source: <a href="http://lineage2.gametsg.com/index.php?view=article&amp;k1=GRACIA&amp;articleid=4416" target="_blank">http://lineage2.gametsg.com/index.ph...articleid=4416</a></strong></span></p>
<p><span><br />
  This information comes from: <br />
  <a href="http://www.l2guru.com/forum/showthread.php?t=103274" target="_blank">http://www.l2guru.com/forum/showthread.php?t=103274</a><br />
  <br />
  <strong>Vitality System </strong>
</span></p>
<span>
<ul>
  <li>Vitality System allows players to gain extra exp when hunting.</li>
  <li>When players go off line / enter safe zone / killed RAID/BOSS, they will receive Vitality Points (VP)</li>
  <li>When vp is cumulated, players will be able to level up their Vitality Levels.</li>
  <li>When players are hunting normal mobs, vp will be used in exchange for higher exp.</li>
  <li>Players&rsquo; total vp/ vitality level can be viewed from the status bar</li>
  <li>There are a total of 4 levels of vitality levels, at level 4 players will get 4 times the exp, at level 3, 3 times the exp.</li>
  <li>VP is exhausted based on exp gain, not based on hunting time.</li>
  <li>When players are in a safe zone for approx 8 &ndash; 9 hours, they will  gain 1 vitality level. (Unfortunately a lot of us will exhaust our  CPU/RAM thanks for this feature.)</li>
  <li>After killing a RAID / BOSS, the system will notify the players how many vp they have earned.</li>
  <li>During the PTS, there was a testing of killing 4 (not sure what mob), gaining 1 vitality level.</li>
  <li>Vitality Level 0 --&gt;&#61664; 1, exp will increase by 50%</li>
  <li>Vitality Level 1 --&gt;&#61664; 2, exp will increase by 33%</li>
  <li>Vitality Level 2 --&gt;&#61664; 3, exp will increase by 25%</li>
  <li>Vitality Level 3 --&gt;&#61664; 4, exp will increase by 20%</li>
  <li>In other words, from level 0 --&gt; 4, the exp will be 3 times the original</li>
</ul>
<strong>General Battle System </strong><br />
(Diego&rsquo;s translation)
<ul>
  <li>Song and Dance buffs now have their own buff bar.</li>
  <li>Player and Monster melee range increased slightly.</li>
  <li>Target location and the relationship with the attacker now affects damage bonus.</li>
  <li>Long range physical attacks now have a distance differential to damage.</li>
  <li>The poison and the bleeding damage has been increased.</li>
</ul>
<strong>(Addition) </strong>
<ul>
  <li>Songs and Dances buff bar has a total of 12 slots.</li>
  <li>Increase the effectiveness in the gap in between of Accuracy and  Evasion (Daggers will find their evasion being way more effective than  before)</li>
  <li>Each character&rsquo;s def to debuff will effect debuff duration.</li>
</ul>
<strong>Skills </strong><br />
(As much as I love to translate all of the names, I won&rsquo;t be able to do  so as most of you know that already. I will just translate what each  skill does instead. However if you know the skill name, please let me  know I shall add it here. Thanks) <br />
<br />
<strong>Knight Class</strong>
<ul>
  <li>Knight Class will have a new root skill (someone please fill in the name for me), level 43</li>
  <li>After switching to this new root skill, increases bonus dmg to 2-hand sword and dual sword</li>
  <li>After switching to this new root skill, will be able to use new attack skills</li>
  <li>Total of 3 levels, levels the amount of skills can be used might be affected by level</li>
</ul>
<u><img src="http://www.l2guru.com/forum/attachment.php?attachmentid=42&amp;stc=1&amp;d=1209668295" alt="" border="0" /></u>
<ul>
  <li>When using 2-handed sword, area atk to nearby enemies, shield block ineffective to this skill</li>
  <li>Can be used with the root skill mentioned above</li>
</ul>
<img src="http://www.l2guru.com/forum/attachment.php?attachmentid=43&amp;stc=1&amp;d=1209668343" alt="" border="0" /><br />
<ul>
  <li>When using dual sword, area atk to nearby enemies, shield block ineffective to this skill.</li>
  <li>Can be used with the root skill mentioned above</li>
</ul>
<img src="http://www.l2guru.com/forum/attachment.php?attachmentid=44&amp;stc=1&amp;d=1209668343" alt="" border="0" />
<ul>
  <li>When using dual sword, double attack to target, shield block ineffective to this skill</li>
  <li>Can be used with the root skill mentioned above.</li>
</ul>
<strong>Healers</strong>
<ul>
  <li>Healers will get a new skill (level 44) which will allow them to use magic attack skills.</li>
  <li>Healers: EE/SE/Bishop (Prophet not included)</li>
  <li>3 levels to this root skill (similar to the knight class root skill)</li>
</ul>
<img src="http://www.l2guru.com/forum/attachment.php?attachmentid=45&amp;stc=1&amp;d=1209668343" alt="" border="0" />
<ul>
  <li>Divine Punishment (assuming this might be the skill name)</li>
  <li>Holy based attack, power 57</li>
  <li>Can be used under the root skill mentioned above</li>
</ul>
<img src="http://www.l2guru.com/forum/attachment.php?attachmentid=46&amp;stc=1&amp;d=1209668343" alt="" border="0" />
<ul>
  <li>Surrender to Holy: lowers target def to holy atks, also increases target&rsquo;s dark resist</li>
  <li>Can be used under the root skill mentioned above</li>
</ul>
<img src="http://www.l2guru.com/forum/attachment.php?attachmentid=47&amp;stc=1&amp;d=1209668343" alt="" border="0" />
<ul>
  <li>Holy Curse: prevents target getting healed at the same time target will continuously reducing hp.</li>
  <li>Can be used under the root skill mentioned above</li>
</ul>
<img src="http://www.l2guru.com/forum/attachment.php?attachmentid=48&amp;stc=1&amp;d=1209668343" alt="" border="0" />
<ul>
  <li>Holy Storm: Holy based area atk, power 90</li>
  <li>Can be used under the root skill mentioned above</li>
</ul>
<strong>Skills to be learned after clicking twice </strong>
<ul>
  <li><strong>AW</strong>: Evasion Counter (LV 74) &ndash; when evades, probability of putting target into shock status</li>
  <li><strong>PW</strong>: Evasion Chance (LV 74) &ndash; when evades, probability of increasing critical chance</li>
  <li><strong>TH</strong>: Evasion Speed (Level 74) &ndash; when evades, probability of increasing attack speed</li>
  <li><strong>All 3 dagger</strong>: Shadow Staff (level 72) &ndash; cancels area opponents&rsquo; targets</li>
</ul>
<strong>Normal Type Skills </strong><br />
(Learn from skill masters) <br />
<br />
(Names copied from Diego's translation) <br />
<strong>Warsmith</strong>:
<ul>
  <li>Repair Golem (L40) &ndash; summon Repair Golem</li>
  <li>Power Golem Might (L43) &ndash; Increases golem p.atk + atk speed</li>
  <li>Golem Armor Plating (L40) &ndash; increases golem def and max hp capacity</li>
  <li>Golem Sharpen Edges (L49) &ndash; increases sword type of weapons p.atk  &amp; critical chance, 10 minutes party buff. (only 1 type of the 3  weapon buffs can exist at once)</li>
  <li>Golem Spikes (L49) &ndash; increases hammer type weapon p.atk, 10 minutes  party buff (only 1 type of the 3 weapon buffs can exist at once)</li>
  <li>Golem Bow String (L49) &ndash; increases bow type distance + p.atk, 10  minutes party buff (only 1 type of the 3 weapon buffs can exist at once)</li>
  <li>Strengthen Heavy Armor (L46) &ndash; increases heavy army p.def, 10  minutes party buff (only 1 type of the 3 armor buffs can exist at once)</li>
  <li>Strengthen Light Armor (L46) &ndash; increases light armor p.def +  evasion, 10 minutes party buff (only 1 type of the 3 armor buffs can  exist at once)</li>
  <li>Strengthen Robe Power (L46) &ndash; increases MP regen rate, 10 minutes  party buff (only 1 type of the 3 armor buffs can exist at once)</li>
  <li>Summon Merchant Golem (L62) &ndash; uses 5 D crystals, will not follow  the WS running around, will just stay at one spot, will be selling  normal ss, etc.</li>
</ul>
<strong>Prophet</strong>: Mana Steal &ndash; when atk, probability of regen mp. <br />
<br />
<strong>SE</strong>: Bless of Blood (LV 40)<br />
<br />
<strong>Kamael All</strong>: (v 43) &ndash; increases hp/mp/cp regen rate<br />
<br />
<strong>Berserker:</strong>
<ul>
  <li>(lv 52) &ndash; heals self hp completely, uses 5 souls</li>
  <li>(lv 55) &ndash; increases party hp capacity + hp regen rate, uses 10 souls</li>
</ul>
<strong>Soulbreaker </strong>
<ul>
  <li>(lv 62) &ndash; lowers area opponents&rsquo; moving speed, uses 5 souls</li>
</ul>
<strong>Arbalester </strong>
<ul>
  <li>(lv 68) &ndash; stops opponent from using skills, uses 1 dark seed (that name is probably wrong)</li>
  <li>(lv 64) &ndash; when opponent activates the trap, within a certain period of time with probability of cancelling opponents&rsquo; targets</li>
</ul>
<strong>Soulbreaker &amp; Inspector</strong>:
<ul>
  <li>(lv 58) &ndash; instant stops pain (invincible), but after the effect is gone, will be out of strength</li>
  <li>(lv 40) &ndash; increases casting speed, lowers cooldown time</li>
</ul>
<strong>Existing Skills added to Existing Classes </strong>
<ul>
  <li>WS: Fatal Strike </li>
  <li>BH: Fatal Strike </li>
  <li>AW: ??</li>
  <li>Phanton Summoner: Death Spike, Summon Cursed Bone, Poisonous Curse</li>
  <li>EE: Party Greater Heal</li>
  <li>BH: Crystalization</li>
</ul>
<strong>Changes to Existing Skills</strong><br />
(Diego&rsquo;s Translation)
<ul>
  <li>Shield  mastery: Increase the effectiveness of shield pdef and if wearing heavy  armor also increases main pdef while using a shield.</li>
  <li>Armor Mastery (Warrior &amp; Kamael): MP regeneration increased.</li>
  <li>Soul collection: Kamaels now can hold 40 souls.</li>
  <li>Protection of Rune,Alignment,Elements: increased defensive properties for both passive and active effects.</li>
  <li>Cleanse: Raid curse can no longer be removed.</li>
</ul>
(Addition)
<ul>
  <li>Bleed: added effect &#61664; lowers moving speed</li>
  <li>Light Armor Mastery &#61664; added effect: increased mp regen rate</li>
  <li>Summon Blue Eye Unicorn &#61664; limited amount of target when casting area atk</li>
  <li>Cure Poison: casting speed decreased</li>
  <li>Cure Bleed: casting speed decreased</li>
  <li>Wild Magic: increase magic critical chance by 10 times, however decreased in critical dmg (from 4x to 3x)</li>
  <li>Siren&rsquo;s Dance: increased magic critical chance, however decreased in critical dmg (as above)</li>
  <li>Fighters/Archers Will: increased p.atk critical + skill critical rate</li>
  <li>Aggression: increase effectiveness</li>
  <li>Soul Cry: skill mp usage reduced by 50%</li>
  <li>Dance of Alignment &amp; Song of Elements: added m.def &amp; elemental def effect</li>
  <li>Blade Rush: cancelled the penalty of reduced evasion</li>
  <li>Dodge: increased skill time from 10 seconds to 15 seconds</li>
</ul>
(Thank God! That was a pain looking at 3 different sites for the right names. Phew!) <br />
<br />
<strong>MP Consumption of the following skills have increased: </strong><br />
(Diego&rsquo;s Translation)<br />
Silent Move, Guard stanace, Soul Guard, Shield Fortress, Parry stance, Riposte Stance, War Frenzy, Strike Back, and others.<br />
<br />
(Addition) <br />
Accuracy, Vicious Stance, Fortitude, Hard March <br />
<br />
<br />
<strong>Changes to all Physical Attack Skills </strong>
<ul>
  <li>Dmg to all physical attack skills have increased</li>
  <li>All physical attack skills mp usage decreased</li>
  <li>All physical attack skills contain overhit</li>
</ul>
(Diego&rsquo;s translation, I think this covered it) <br />
<strong>Changes to casting time.</strong><br />
Scroll of Escape, Cure Poison and Cure bleeding are now reduced in time.<br />
<br />
<strong>MP Consumption of the following toggles are increased:</strong><br />
Soul Cry, Silent Move, Guard stanace, Soul Guard, Shield Fortress,  Parry stance, Riposte Stance, War Frenzy, Strike Back, and others.<br />
<br />
(Diego&rsquo;s Translation)
<ul>
  <li>Changes to magic damage</li>
  <li>Magic damage now applies a random variance between minimum and maximum damage.</li>
  <li>Depending on the type of weapon the damage variance of spells changes.</li>
</ul>
<strong><br />
Changes to Kamael Spirit Consumption</strong><br />
Skills that use charged souls to enhance the skill have been changed.<br />
<br />
<strong>Changes to Magic Dmg</strong>
<ul>
  <li>Magic critical rate will increase to 10 times</li>
  <li>Magic critical dmg will go down from 4 times to 3 times</li>
  <li>magical critic rate caped to +20% when buffed (Thanks to NerZ for clarification)</li>
</ul>
&quot;My God! I just spent the whole afternoon translating skills!!&quot; said Diego </span>
<?
}else{
	include "error.php";
}
?>
