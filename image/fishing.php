<? if (isset($_GET['file'])){?><div>
  <h4><strong>Fishing</strong></h4>
  <p>Fishing  offers a different experience from combat and leveling up. Not only is  fishing a great way to relax, you can also catch ingredients for item  creation.</p>
  <p>Fishing equipment and/or  the Fishing skill can be acquired along with other newly added common  skills from the Fisherman&rsquo;s Guild Members dispatched at the grocery  store or port of each town.</p>
  <p>You will  need a Fishing Rod and lures to go fishing. These items can be bought  from the store or earned through quests. Fishing rods and Fishing Shots  exist by grade (no grade, D, C, B, A, S) and item damage will be  applied according to grade. A penalty will be applied when using a  fishing rod of a grade incompatible with your character level.</p>
  <p>You  must master the Fishing skill in order to begin fishing. You can  acquire Fishing, Reeling, Pumping, and Fishing Mastery skills by  speaking with the Fisherman&rsquo;s Guild Members stationed in each town or  port.</p>
  <p>Although not essential for  fishing, Fishing Shots will assist you with your fishing. Fishing  Shots, like spiritshots and soulshots, can be accessed by  double-clicking on them in inventory or registering with shortcuts. The  Fishing Shot grade must be compatible to that of the fishing rod, and  they can also be earned through quests.</p>
  <p>The  Fishing Rod is equipped in the weapon slot and the bait in the left  hand, like an arrow. Once you equip the rod and use the Fishing skill,  you begin to fish.</p>
  <p>Once fishing is under way, the Fishing Window will display and an enlarged image of the bobber will appear.</p>
  <p align="left"><img src="image/reeling_000.jpg" alt="a2" width="400" height="311" /></p>
  <p>The  fishing skills Pumping and Reeling determine fishing success. These  skills can also be mastered by paying adena to the Fisherman&rsquo;s Guild  Members. Use the Reeling skill while the fish struggles and resists,  and the Pumping skill while the fish is exhausted.</p>
  <p>When  fishing is under way, the Fishing Window will display. When a fish  begins to bite, the HP gauge of the fish and the time allotted to catch  it will appear in the Fishing Window. Fishing is successful only if the  HP of the fish drops to 0 before the time expires.</p>
  <p>Upon  successful fishing, fish or treasure chests will be added to the  inventory, fishing will end and the Fishing Window will close. When an  attempt at catching fish fails, fishing will end and the Fishing Window  will close. Fishing will also end if the Fishing skill is re-used  during fishing.</p>
  <p>If you double-click  on fish or treasure chests acquired while fishing, they will turn into  other items. These items can be used in common crafting, and certain  items can be exchanged for Proof of Catching a Fish with the  Fisherman&rsquo;s Guild Member.</p>
  <p>Fishing  holes are home to fish abundant enough to enable fishing. Fish usually  inhabit waters where the breathing gauge is displayed or regions with  unique water quality such as the Goddard Hot Springs. Be wary, as  monsters instead of fish may be caught with fishing!</p>
  <p>Increasing  Fishing Mastery enables catching of higher grade fish. However, higher  grade fish are stronger and brighter than lower grade fish, so advanced  Reeling and Pumping skills are required to catch them.</p>
  <p>A  Fishing Manual that provides fishing instructions can be purchased from  the Fisherman&rsquo;s Guild Member. Beginner tips will be provided when  fishing with beginner bait. Beginner bait can be bought from the  Fisherman&rsquo;s Guild Member.</p>
  <p>If Reeling/Pumping skills exceed Fishing Mastery by more than 2 levels, a penalty will be imposed.</p>
  <p><strong>Night Fishing </strong></p>
  <p align="left">Night fishing can be done from 12:00 a.m. to 06:00 a.m. (game time). Game time can be viewed by typing /time.</p>
  <p align="left">Luminous  Bait is necessary for night fishing and may be purchased through a  Fishermen Guild Member. Luminous Bait can only be used at night. When  fishing at night and using Luminous Bait, high quality (high-grade)  fish will be tempted to bite. </p>
  <p align="left">A  high-grade fish can use deceptive skills unlike ordinary fish. When a  high-grade fish uses its deceptive skills, reeling and pumping must be  used in the opposite way in order to reduce the fish&rsquo;s HP. </p>
  <p align="left">Depending  on its type, a high-grade fish may reward you with various &ldquo;Fish Scale&rdquo;  drops. If you collect enough &ldquo;Fish Scale&rdquo; of the same type, you may  bring them to a Fishermen Guild Member for additional rewards. </p>
  <p align="left">Depending on your fishing mastery level, the types of high-grade fish that can be caught and rewarded fish scales will differ. </p>
  <p align="left"><strong>Fisherman&rsquo;s Potions</strong></p>
  <p align="left">The  Fisherman&rsquo;s Potion temporarily reduces a player&rsquo;s fishing mastery level  to catch a fish at a desired level. The potion may be purchased from a  Fishermen Guild Member. If the potion's level is higher than the user&rsquo;s  fishing mastery level, the potion has no effect. The effect of using a  Fisherman&rsquo;s Potion lasts 60 minutes. </p>
  <p align="left">The  Fishing Potion restores a player&rsquo;s fishing mastery level, which was  lowered by the Fisherman&rsquo;s Potion, back to the previous level. The  Fishing Potion may be purchased from a Fishermen Guild Member. The  Fishing Potion can take effect even though the Fisherman&rsquo;s Potion was  not used previously, therefore please exercise caution when using it.</p>
  <p align="left"><strong>Fishing King Championship Tournament</strong></p>
  <p align="left"> The Fishing King Championship Tournament event gives an award to the  person who catches the biggest fish. This event is held at special  times and will be announced when it begins. </p>
  <p align="left">Source: <a href="http://www.lineage2.com">Lineage II Oficial page </a></p>

  </div> <?
}else{
include "error.php";
}
?>

