<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>L2 Inside</title>


<!--
body,td,th {
	color: #FFFFFF;
	font-style: normal;
	font-stretch: normal;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
body {
	background-color: #000000;
}
-->
<style type="text/css">
<!--
.Estilo1 {font-size: 14px}
-->
</style>
</head>

<body>
<p align="right">&nbsp;</p>

<div class="txt">

  <h3 align="center">Compare Skills</h3>
  <form action="index.php" method="GET" name="skills" id="skills">
  <input name="file" value="<?=$_GET['file']?>" type="hidden" />
  <table border="1" align="center" cellpadding="1" cellspacing="1">
<tbody><tr> 
      <td align="right">Class 1:</td>
      <td>

<select name="c1" class="show">
  <option value="">Choose</option>
  <option value="36" <? if ($_GET['c1']=="36"){ echo'selected="selected"';}?>>DarkElf AbyssWalker</option>
  <option value="35" <? if ($_GET['c1']=="35"){ echo'selected="selected"';}?>>DarkElf Assassin</option>
  <option value="34" <? if ($_GET['c1']=="34"){ echo'selected="selected"';}?>>DarkElf BladeDancer</option>
  <option value="39" <? if ($_GET['c1']=="39"){ echo'selected="selected"';}?>>DarkElf DarkWizard</option>
  <option value="31" <? if ($_GET['c1']=="31"){ echo'selected="selected"';}?>>DarkElf Fighter</option>

  <option value="108" <? if ($_GET['c1']=="108"){ echo'selected="selected"';}?>>DarkElf GhostHunter</option>
  <option value="109" <? if ($_GET['c1']=="109"){ echo'selected="selected"';}?>>DarkElf GhostSentinel</option>
  <option value="38" <? if ($_GET['c1']=="38"){ echo'selected="selected"';}?>>DarkElf Mage</option>
  <option value="32" <? if ($_GET['c1']=="32"){ echo'selected="selected"';}?>>DarkElf PaulusKnight</option>
  <option value="37" <? if ($_GET['c1']=="37"){ echo'selected="selected"';}?>>DarkElf PhantomRanger</option>
  <option value="41" <? if ($_GET['c1']=="41"){ echo'selected="selected"';}?>>DarkElf PhantomSummoner</option>

  <option value="43" <? if ($_GET['c1']=="43"){ echo'selected="selected"';}?>>DarkElf ShillienElder</option>
  <option value="33" <? if ($_GET['c1']=="33"){ echo'selected="selected"';}?>>DarkElf ShillienKnight</option>
  <option value="42" <? if ($_GET['c1']=="42"){ echo'selected="selected"';}?>>DarkElf ShillienOracle</option>
  <option value="112" <? if ($_GET['c1']=="112"){ echo'selected="selected"';}?>>DarkElf ShillienSaint</option>
  <option value="106" <? if ($_GET['c1']=="106"){ echo'selected="selected"';}?>>DarkElf ShillienTemplar</option>
  <option value="107" <? if ($_GET['c1']=="107"){ echo'selected="selected"';}?>>DarkElf SpectralDancer</option>

  <option value="111" <? if ($_GET['c1']=="111"){ echo'selected="selected"';}?>>DarkElf SpectralMaster</option>
  <option value="40" <? if ($_GET['c1']=="40"){ echo'selected="selected"';}?>>DarkElf Spellhowler</option>
  <option value="110" <? if ($_GET['c1']=="110"){ echo'selected="selected"';}?>>DarkElf StormScreamer</option>
  <option value="56" <? if ($_GET['c1']=="56"){ echo'selected="selected"';}?>>Dwarf Artisan</option>
  <option value="55" <? if ($_GET['c1']=="55"){ echo'selected="selected"';}?>>Dwarf BountyHunter</option>
  <option value="53" <? if ($_GET['c1']=="53"){ echo'selected="selected"';}?>>Dwarf Fighter</option>

  <option value="117" <? if ($_GET['c1']=="117"){ echo'selected="selected"';}?>>Dwarf FortuneSeeker</option>
  <option value="118" <? if ($_GET['c1']=="118"){ echo'selected="selected"';}?>>Dwarf Maestro</option>
  <option value="54" <? if ($_GET['c1']=="54"){ echo'selected="selected"';}?>>Dwarf Scavenger</option>
  <option value="57" <? if ($_GET['c1']=="57"){ echo'selected="selected"';}?>>Dwarf Warsmith</option>
  <option value="30" <? if ($_GET['c1']=="30"){ echo'selected="selected"';}?>>Elf Elder</option>
  <option value="104" <? if ($_GET['c1']=="104"){ echo'selected="selected"';}?>>Elf ElementalMaster</option>

  <option value="28" <? if ($_GET['c1']=="28"){ echo'selected="selected"';}?>>Elf ElementalSummoner</option>
  <option value="105" <? if ($_GET['c1']=="105"){ echo'selected="selected"';}?>>Elf EvaSaint</option>
  <option value="99" <? if ($_GET['c1']=="99"){ echo'selected="selected"';}?>>Elf EvaTemplar</option>
  <option value="18" <? if ($_GET['c1']=="18"){ echo'selected="selected"';}?>>Elf Fighter</option>
  <option value="19" <? if ($_GET['c1']=="19"){ echo'selected="selected"';}?>>Elf Knight</option>
  <option value="25" <? if ($_GET['c1']=="25"){ echo'selected="selected"';}?>>Elf Mage</option>

  <option value="102" <? if ($_GET['c1']=="102"){ echo'selected="selected"';}?>>Elf MoonlightSentinel</option>
  <option value="103" <? if ($_GET['c1']=="103"){ echo'selected="selected"';}?>>Elf MysticMuse</option>
  <option value="29" <? if ($_GET['c1']=="29"){ echo'selected="selected"';}?>>Elf Oracle</option>
  <option value="23" <? if ($_GET['c1']=="23"){ echo'selected="selected"';}?>>Elf PlainsWalker</option>
  <option value="22" <? if ($_GET['c1']=="22"){ echo'selected="selected"';}?>>Elf Scout</option>
  <option value="24" <? if ($_GET['c1']=="24"){ echo'selected="selected"';}?>>Elf SilverRanger</option>

  <option value="27" <? if ($_GET['c1']=="27"){ echo'selected="selected"';}?>>Elf SpellSinger</option>
  <option value="100" <? if ($_GET['c1']=="100"){ echo'selected="selected"';}?>>Elf SwordMuse</option>
  <option value="21" <? if ($_GET['c1']=="21"){ echo'selected="selected"';}?>>Elf SwordSinger</option>
  <option value="20" <? if ($_GET['c1']=="20"){ echo'selected="selected"';}?>>Elf TempleKnight</option>
  <option value="101" <? if ($_GET['c1']=="101"){ echo'selected="selected"';}?>>Elf WindRider</option>
  <option value="26" <? if ($_GET['c1']=="26"){ echo'selected="selected"';}?>>Elf Wizard</option>

  <option value="93" <? if ($_GET['c1']=="93"){ echo'selected="selected"';}?>>Human Adventurer</option>
  <option value="96" <? if ($_GET['c1']=="96"){ echo'selected="selected"';}?>>Human ArcanaLord</option>
  <option value="94" <? if ($_GET['c1']=="94"){ echo'selected="selected"';}?>>Human Archmage</option>
  <option value="16" <? if ($_GET['c1']=="16"){ echo'selected="selected"';}?>>Human Bishop</option>
  <option value="97" <? if ($_GET['c1']=="97"){ echo'selected="selected"';}?>>Human Cardinal</option>
  <option value="15" <? if ($_GET['c1']=="15"){ echo'selected="selected"';}?>>Human Cleric</option>

  <option value="6" <? if ($_GET['c1']=="6"){ echo'selected="selected"';}?>>Human DarkAvenger</option>
  <option value="89" <? if ($_GET['c1']=="89"){ echo'selected="selected"';}?>>Human Dreadnought</option>
  <option value="88" <? if ($_GET['c1']=="88"){ echo'selected="selected"';}?>>Human Duelist</option>
  <option value="0" <? if ($_GET['c1']=="0"){ echo'selected="selected"';}?>>Human Fighter</option>
  <option value="2" <? if ($_GET['c1']=="2"){ echo'selected="selected"';}?>>Human Gladiator</option>
  <option value="9" <? if ($_GET['c1']=="9"){ echo'selected="selected"';}?>>Human Hawkeye</option>

  <option value="91" <? if ($_GET['c1']=="91"){ echo'selected="selected"';}?>>Human HellKnight</option>
  <option value="98" <? if ($_GET['c1']=="98"){ echo'selected="selected"';}?>>Human Hierophant</option>
  <option value="4" <? if ($_GET['c1']=="4"){ echo'selected="selected"';}?>>Human Knight</option>
  <option value="10" <? if ($_GET['c1']=="10"){ echo'selected="selected"';}?>>Human Mage</option>
  <option value="13" <? if ($_GET['c1']=="13"){ echo'selected="selected"';}?>>Human Necromancer</option>
  <option value="5" <? if ($_GET['c1']=="5"){ echo'selected="selected"';}?>>Human Paladin</option>

  <option value="90" <? if ($_GET['c1']=="90"){ echo'selected="selected"';}?>>Human PhoenixKnight</option>
  <option value="17" <? if ($_GET['c1']=="17"){ echo'selected="selected"';}?>>Human Prophet</option>
  <option value="7" <? if ($_GET['c1']=="7"){ echo'selected="selected"';}?>>Human Rogue</option>
  <option value="92" <? if ($_GET['c1']=="92"){ echo'selected="selected"';}?>>Human Sagittarius</option>
  <option value="12" <? if ($_GET['c1']=="12"){ echo'selected="selected"';}?>>Human Sorcerer</option>
  <option value="95" <? if ($_GET['c1']=="95"){ echo'selected="selected"';}?>>Human Soultaker</option>

  <option value="8" <? if ($_GET['c1']=="8"){ echo'selected="selected"';}?>>Human TreasureHunter</option>
  <option value="14" <? if ($_GET['c1']=="14"){ echo'selected="selected"';}?>>Human Warlock</option>
  <option value="3" <? if ($_GET['c1']=="3"){ echo'selected="selected"';}?>>Human Warlord</option>
  <option value="1" <? if ($_GET['c1']=="1"){ echo'selected="selected"';}?>>Human Warrior</option>
  <option value="11" <? if ($_GET['c1']=="11"){ echo'selected="selected"';}?>>Human Wizard</option>
  <option value="46" <? if ($_GET['c1']=="46"){ echo'selected="selected"';}?>>Orc Destroyer</option>

  <option value="115" <? if ($_GET['c1']=="115"){ echo'selected="selected"';}?>>Orc Dominator</option>
  <option value="116" <? if ($_GET['c1']=="116"){ echo'selected="selected"';}?>>Orc Doomcryer</option>
  <option value="44" <? if ($_GET['c1']=="44"){ echo'selected="selected"';}?>>Orc Fighter</option>
  <option value="114" <? if ($_GET['c1']=="114"){ echo'selected="selected"';}?>>Orc GrandKhauatari</option>
  <option value="49" <? if ($_GET['c1']=="49"){ echo'selected="selected"';}?>>Orc Mage</option>
  <option value="47" <? if ($_GET['c1']=="47"){ echo'selected="selected"';}?>>Orc Monk</option>

  <option value="51" <? if ($_GET['c1']=="51"){ echo'selected="selected"';}?>>Orc Overlord</option>
  <option value="45" <? if ($_GET['c1']=="45"){ echo'selected="selected"';}?>>Orc Raider</option>
  <option value="50" <? if ($_GET['c1']=="50"){ echo'selected="selected"';}?>>Orc Shaman</option>
  <option value="113" <? if ($_GET['c1']=="113"){ echo'selected="selected"';}?>>Orc Titan</option>
  <option value="48" <? if ($_GET['c1']=="48"){ echo'selected="selected"';}?>>Orc Tyrant</option>
  <option value="52" <? if ($_GET['c1']=="52"){ echo'selected="selected"';}?>>Orc Warcryer</option>
</select>      </td>
      <td class="pad" align="right">Class 2:</td>
      <td>
<select name="c2" class="show">
  <option value="">Choose</option>
  <option value="36" <? if ($_GET['c2']=="36"){ echo'selected="selected"';}?>>DarkElf AbyssWalker</option>
  <option value="35" <? if ($_GET['c2']=="35"){ echo'selected="selected"';}?>>DarkElf Assassin</option>
  <option value="34" <? if ($_GET['c2']=="34"){ echo'selected="selected"';}?>>DarkElf BladeDancer</option>
  <option value="39" <? if ($_GET['c2']=="39"){ echo'selected="selected"';}?>>DarkElf DarkWizard</option>
  <option value="31" <? if ($_GET['c2']=="31"){ echo'selected="selected"';}?>>DarkElf Fighter</option>

  <option value="108" <? if ($_GET['c2']=="108"){ echo'selected="selected"';}?>>DarkElf GhostHunter</option>
  <option value="109" <? if ($_GET['c2']=="109"){ echo'selected="selected"';}?>>DarkElf GhostSentinel</option>
  <option value="38" <? if ($_GET['c2']=="38"){ echo'selected="selected"';}?>>DarkElf Mage</option>
  <option value="32" <? if ($_GET['c2']=="32"){ echo'selected="selected"';}?>>DarkElf PaulusKnight</option>
  <option value="37" <? if ($_GET['c2']=="37"){ echo'selected="selected"';}?>>DarkElf PhantomRanger</option>
  <option value="41" <? if ($_GET['c2']=="41"){ echo'selected="selected"';}?>>DarkElf PhantomSummoner</option>

  <option value="43" <? if ($_GET['c2']=="43"){ echo'selected="selected"';}?>>DarkElf ShillienElder</option>
  <option value="33" <? if ($_GET['c2']=="33"){ echo'selected="selected"';}?>>DarkElf ShillienKnight</option>
  <option value="42" <? if ($_GET['c2']=="42"){ echo'selected="selected"';}?>>DarkElf ShillienOracle</option>
  <option value="112" <? if ($_GET['c2']=="112"){ echo'selected="selected"';}?>>DarkElf ShillienSaint</option>
  <option value="106" <? if ($_GET['c2']=="106"){ echo'selected="selected"';}?>>DarkElf ShillienTemplar</option>
  <option value="107" <? if ($_GET['c2']=="107"){ echo'selected="selected"';}?>>DarkElf SpectralDancer</option>

  <option value="111" <? if ($_GET['c2']=="111"){ echo'selected="selected"';}?>>DarkElf SpectralMaster</option>
  <option value="40" <? if ($_GET['c2']=="40"){ echo'selected="selected"';}?>>DarkElf Spellhowler</option>
  <option value="110" <? if ($_GET['c2']=="110"){ echo'selected="selected"';}?>>DarkElf StormScreamer</option>
  <option value="56" <? if ($_GET['c2']=="56"){ echo'selected="selected"';}?>>Dwarf Artisan</option>
  <option value="55" <? if ($_GET['c2']=="55"){ echo'selected="selected"';}?>>Dwarf BountyHunter</option>
  <option value="53" <? if ($_GET['c2']=="53"){ echo'selected="selected"';}?>>Dwarf Fighter</option>

  <option value="117" <? if ($_GET['c2']=="117"){ echo'selected="selected"';}?>>Dwarf FortuneSeeker</option>
  <option value="118" <? if ($_GET['c2']=="118"){ echo'selected="selected"';}?>>Dwarf Maestro</option>
  <option value="54" <? if ($_GET['c2']=="54"){ echo'selected="selected"';}?>>Dwarf Scavenger</option>
  <option value="57" <? if ($_GET['c2']=="57"){ echo'selected="selected"';}?>>Dwarf Warsmith</option>
  <option value="30" <? if ($_GET['c2']=="30"){ echo'selected="selected"';}?>>Elf Elder</option>
  <option value="104" <? if ($_GET['c2']=="104"){ echo'selected="selected"';}?>>Elf ElementalMaster</option>

  <option value="28" <? if ($_GET['c2']=="28"){ echo'selected="selected"';}?>>Elf ElementalSummoner</option>
  <option value="105" <? if ($_GET['c2']=="105"){ echo'selected="selected"';}?>>Elf EvaSaint</option>
  <option value="99" <? if ($_GET['c2']=="99"){ echo'selected="selected"';}?>>Elf EvaTemplar</option>
  <option value="18" <? if ($_GET['c2']=="18"){ echo'selected="selected"';}?>>Elf Fighter</option>
  <option value="19" <? if ($_GET['c2']=="19"){ echo'selected="selected"';}?>>Elf Knight</option>
  <option value="25" <? if ($_GET['c2']=="25"){ echo'selected="selected"';}?>>Elf Mage</option>

  <option value="102" <? if ($_GET['c2']=="102"){ echo'selected="selected"';}?>>Elf MoonlightSentinel</option>
  <option value="103" <? if ($_GET['c2']=="103"){ echo'selected="selected"';}?>>Elf MysticMuse</option>
  <option value="29" <? if ($_GET['c2']=="29"){ echo'selected="selected"';}?>>Elf Oracle</option>
  <option value="23" <? if ($_GET['c2']=="23"){ echo'selected="selected"';}?>>Elf PlainsWalker</option>
  <option value="22" <? if ($_GET['c2']=="22"){ echo'selected="selected"';}?>>Elf Scout</option>
  <option value="24" <? if ($_GET['c2']=="24"){ echo'selected="selected"';}?>>Elf SilverRanger</option>

  <option value="27" <? if ($_GET['c2']=="27"){ echo'selected="selected"';}?>>Elf SpellSinger</option>
  <option value="100" <? if ($_GET['c2']=="100"){ echo'selected="selected"';}?>>Elf SwordMuse</option>
  <option value="21" <? if ($_GET['c2']=="21"){ echo'selected="selected"';}?>>Elf SwordSinger</option>
  <option value="20" <? if ($_GET['c2']=="20"){ echo'selected="selected"';}?>>Elf TempleKnight</option>
  <option value="101" <? if ($_GET['c2']=="101"){ echo'selected="selected"';}?>>Elf WindRider</option>
  <option value="26" <? if ($_GET['c2']=="26"){ echo'selected="selected"';}?>>Elf Wizard</option>

  <option value="93" <? if ($_GET['c2']=="93"){ echo'selected="selected"';}?>>Human Adventurer</option>
  <option value="96" <? if ($_GET['c2']=="96"){ echo'selected="selected"';}?>>Human ArcanaLord</option>
  <option value="94" <? if ($_GET['c2']=="94"){ echo'selected="selected"';}?>>Human Archmage</option>
  <option value="16" <? if ($_GET['c2']=="16"){ echo'selected="selected"';}?>>Human Bishop</option>
  <option value="97" <? if ($_GET['c2']=="97"){ echo'selected="selected"';}?>>Human Cardinal</option>
  <option value="15" <? if ($_GET['c2']=="15"){ echo'selected="selected"';}?>>Human Cleric</option>

  <option value="6" <? if ($_GET['c2']=="6"){ echo'selected="selected"';}?>>Human DarkAvenger</option>
  <option value="89" <? if ($_GET['c2']=="89"){ echo'selected="selected"';}?>>Human Dreadnought</option>
  <option value="88" <? if ($_GET['c2']=="88"){ echo'selected="selected"';}?>>Human Duelist</option>
  <option value="0" <? if ($_GET['c2']=="0"){ echo'selected="selected"';}?>>Human Fighter</option>
  <option value="2" <? if ($_GET['c2']=="2"){ echo'selected="selected"';}?>>Human Gladiator</option>
  <option value="9" <? if ($_GET['c2']=="9"){ echo'selected="selected"';}?>>Human Hawkeye</option>

  <option value="91" <? if ($_GET['c2']=="91"){ echo'selected="selected"';}?>>Human HellKnight</option>
  <option value="98" <? if ($_GET['c2']=="98"){ echo'selected="selected"';}?>>Human Hierophant</option>
  <option value="4" <? if ($_GET['c2']=="4"){ echo'selected="selected"';}?>>Human Knight</option>
  <option value="10" <? if ($_GET['c2']=="10"){ echo'selected="selected"';}?>>Human Mage</option>
  <option value="13" <? if ($_GET['c2']=="13"){ echo'selected="selected"';}?>>Human Necromancer</option>
  <option value="5" <? if ($_GET['c2']=="5"){ echo'selected="selected"';}?>>Human Paladin</option>

  <option value="90" <? if ($_GET['c2']=="90"){ echo'selected="selected"';}?>>Human PhoenixKnight</option>
  <option value="17" <? if ($_GET['c2']=="17"){ echo'selected="selected"';}?>>Human Prophet</option>
  <option value="7" <? if ($_GET['c2']=="7"){ echo'selected="selected"';}?>>Human Rogue</option>
  <option value="92" <? if ($_GET['c2']=="92"){ echo'selected="selected"';}?>>Human Sagittarius</option>
  <option value="12" <? if ($_GET['c2']=="12"){ echo'selected="selected"';}?>>Human Sorcerer</option>
  <option value="95" <? if ($_GET['c2']=="95"){ echo'selected="selected"';}?>>Human Soultaker</option>

  <option value="8" <? if ($_GET['c2']=="8"){ echo'selected="selected"';}?>>Human TreasureHunter</option>
  <option value="14" <? if ($_GET['c2']=="14"){ echo'selected="selected"';}?>>Human Warlock</option>
  <option value="3" <? if ($_GET['c2']=="3"){ echo'selected="selected"';}?>>Human Warlord</option>
  <option value="1" <? if ($_GET['c2']=="1"){ echo'selected="selected"';}?>>Human Warrior</option>
  <option value="11" <? if ($_GET['c2']=="11"){ echo'selected="selected"';}?>>Human Wizard</option>
  <option value="46" <? if ($_GET['c2']=="46"){ echo'selected="selected"';}?>>Orc Destroyer</option>

  <option value="115" <? if ($_GET['c2']=="115"){ echo'selected="selected"';}?>>Orc Dominator</option>
  <option value="116" <? if ($_GET['c2']=="116"){ echo'selected="selected"';}?>>Orc Doomcryer</option>
  <option value="44" <? if ($_GET['c2']=="44"){ echo'selected="selected"';}?>>Orc Fighter</option>
  <option value="114" <? if ($_GET['c2']=="114"){ echo'selected="selected"';}?>>Orc GrandKhauatari</option>
  <option value="49" <? if ($_GET['c2']=="49"){ echo'selected="selected"';}?>>Orc Mage</option>
  <option value="47" <? if ($_GET['c2']=="47"){ echo'selected="selected"';}?>>Orc Monk</option>

  <option value="51" <? if ($_GET['c2']=="51"){ echo'selected="selected"';}?>>Orc Overlord</option>
  <option value="45" <? if ($_GET['c2']=="45"){ echo'selected="selected"';}?>>Orc Raider</option>
  <option value="50" <? if ($_GET['c2']=="50"){ echo'selected="selected"';}?>>Orc Shaman</option>
  <option value="113" <? if ($_GET['c2']=="113"){ echo'selected="selected"';}?>>Orc Titan</option>
  <option value="48" <? if ($_GET['c2']=="48"){ echo'selected="selected"';}?>>Orc Tyrant</option>
  <option value="52" <? if ($_GET['c2']=="52"){ echo'selected="selected"';}?>>Orc Warcryer</option>
</select><input name="Show" type="submit" id="Show" title="Show" value="Show skills" />
</label>      </td>
</tr></tbody></table>
	  
</form>
<? if(isset($_GET['c1']) && isset($_GET['c2'])){?>
      <table width="90%" border="1" align="center">
	  	        <tr>
          <td width="3%" bgcolor="#333333"></td>
          <td width="62%" bgcolor="#333333">Skill name and description </td>
          <td width="14%" bgcolor="#333333"><?php
  if ($_GET['c1']=="36"){
echo "DarkElf AbyssWalker";
}
  if ($_GET['c1']=="35"){
echo "DarkElf Assassin";
}
  if ($_GET['c1']=="34"){
echo "DarkElf BladeDancer";
}
  if ($_GET['c1']=="39"){
echo "DarkElf DarkWizard";
}
  if ($_GET['c1']=="31"){
echo "DarkElf Fighter";
}
  if ($_GET['c1']=="108"){
echo "DarkElf GhostHunter";
}
  if ($_GET['c1']=="109"){
echo "DarkElf GhostSentinel";
}
  if ($_GET['c1']=="38"){
echo "DarkElf Mage";
}
  if ($_GET['c1']=="32"){
echo "DarkElf PaulusKnight";
}
  if ($_GET['c1']=="37"){
echo "DarkElf PhantomRanger";
}
  if ($_GET['c1']=="41"){
echo "DarkElf PhantomSummoner";
}
  if ($_GET['c1']=="43"){
echo "DarkElf ShillienElder";
}
  if ($_GET['c1']=="33"){
echo "DarkElf ShillienKnight";
}
  if ($_GET['c1']=="42"){
echo "DarkElf ShillienOracle";
}
  if ($_GET['c1']=="112"){
echo "DarkElf ShillienSaint";
}
  if ($_GET['c1']=="106"){
echo "DarkElf ShillienTemplar";
}
  if ($_GET['c1']=="107"){
echo "DarkElf SpectralDancer";
}
  if ($_GET['c1']=="111"){
echo "DarkElf SpectralMaster";
}
  if ($_GET['c1']=="40"){
echo "DarkElf Spellhowler";
}
  if ($_GET['c1']=="110"){
echo "DarkElf StormScreamer";
}
  if ($_GET['c1']=="56"){
echo "Dwarf Artisan";
}
  if ($_GET['c1']=="55"){
echo "Dwarf BountyHunter";
}
  if ($_GET['c1']=="53"){
echo "Dwarf Fighter";
}
  if ($_GET['c1']=="117"){
echo "Dwarf FortuneSeeker";
}
  if ($_GET['c1']=="118"){
echo "Dwarf Maestro";
}
  if ($_GET['c1']=="54"){
echo "Dwarf Scavenger";
}
  if ($_GET['c1']=="57"){
echo "Dwarf Warsmith";
}
  if ($_GET['c1']=="30"){
echo "Elf Elder";
}
  if ($_GET['c1']=="104"){
echo "Elf ElementalMaster";
}
  if ($_GET['c1']=="28"){
echo "Elf ElementalSummoner";
}
  if ($_GET['c1']=="105"){
echo "Elf EvaSaint";
}
  if ($_GET['c1']=="99"){
echo "Elf EvaTemplar";
}
  
  if ($_GET['c1']=="18"){
echo "Elf Fighter";
}
  if ($_GET['c1']=="19"){
echo "Elf Knight";
}
  if ($_GET['c1']=="25"){
echo "Elf Mage";
}
  if ($_GET['c1']=="102"){
echo "Elf MoonlightSentinel";
}
  if ($_GET['c1']=="103"){
echo "Elf MysticMuse";
}
  if ($_GET['c1']=="29"){
echo "Elf Oracle";
}
  if ($_GET['c1']=="23"){
echo "Elf PlainsWalker";
}
  if ($_GET['c1']=="22"){
echo "Elf Scout";
}
  if ($_GET['c1']=="24"){
echo "Elf SilverRanger";
}
  if ($_GET['c1']=="27"){
echo "Elf SpellSinger";
}
  if ($_GET['c1']=="100"){
echo "Elf SwordMuse";
}
  if ($_GET['c1']=="21"){
echo "Elf SwordSinger";
}
  if ($_GET['c1']=="20"){
echo "Elf TempleKnight";
}
  if ($_GET['c1']=="101"){
echo "Elf WindRider";
}
  if ($_GET['c1']=="26"){
echo "Elf Wizard";
}
  if ($_GET['c1']=="93"){
echo "Human Adventurer";
}
  if ($_GET['c1']=="96"){
echo "Human ArcanaLord";
}
  if ($_GET['c1']=="94"){
echo "Human Archmage";
}
  if ($_GET['c1']=="16"){
echo "Human Bishop";
}
  if ($_GET['c1']=="97"){
echo "Human Cardinal";
}
  if ($_GET['c1']=="15"){
echo "Human Cleric";
}
  if ($_GET['c1']=="6"){
echo "Human DarkAvenger";
}
  if ($_GET['c1']=="89"){
echo "Human Dreadnought";
}
  if ($_GET['c1']=="88"){
echo "Human Duelist";
}
  if ($_GET['c1']=="0"){
echo "Human Fighter";
}
  if ($_GET['c1']=="2"){
echo "Human Gladiator";
}
  if ($_GET['c1']=="9"){
echo "Human Hawkeye";
}
  if ($_GET['c1']=="91"){
echo "Human HellKnight";
}
  if ($_GET['c1']=="98"){
echo "Human Hierophant";
}
  if ($_GET['c1']=="4"){
echo "Human Knight";
}
  if ($_GET['c1']=="10"){
echo "Human Mage";
}
  if ($_GET['c1']=="13"){
echo "Human Necromancer";
}
  if ($_GET['c1']=="5"){
echo "Human Paladin";
}
  if ($_GET['c1']=="90"){
echo "Human PhoenixKnight";
}
  if ($_GET['c1']=="17"){
echo "Human Prophet";
}
  if ($_GET['c1']=="7"){
echo "Human Rogue";
}
  if ($_GET['c1']=="92"){
echo "Human Sagittarius";
}
  if ($_GET['c1']=="12"){
echo "Human Sorcerer";
}
  if ($_GET['c1']=="95"){
echo "Human Soultaker";
}
  if ($_GET['c1']=="8"){
echo "Human TreasureHunter";
}
  if ($_GET['c1']=="14"){
echo "Human Warlock";
}
  if ($_GET['c1']=="3"){
echo "Human Warlord";
}
  if ($_GET['c1']=="1"){
echo "Human Warrior";
}
  if ($_GET['c1']=="11"){
echo "Human Wizard";
}
  if ($_GET['c1']=="46"){
echo "Orc Destroyer";
}
  if ($_GET['c1']=="115"){
echo "Orc Dominator";
}
  if ($_GET['c1']=="116"){
echo "Orc Doomcryer";
}
  if ($_GET['c1']=="44"){
echo "Orc Fighter";
}
  if ($_GET['c1']=="114"){
echo "Orc GrandKhauatari";
}
  if ($_GET['c1']=="49"){
echo "Orc Mage";
}
  if ($_GET['c1']=="47"){
echo "Orc Monk";
}
  if ($_GET['c1']=="51"){
echo "Orc Overlord";
}
  if ($_GET['c1']=="45"){
echo "Orc Raider";
}
  if ($_GET['c1']=="50"){
echo "Orc Shaman";
}
  if ($_GET['c1']=="113"){
echo "Orc Titan";
}
  if ($_GET['c1']=="48"){
echo "Orc Tyrant";
}
  if ($_GET['c1']=="52"){
echo "Orc Warcryer";
}
?></td>
          <td width="14%" bgcolor="#333333"><?php
  if ($_GET['c2']=="36"){
echo "DarkElf AbyssWalker";
}
  if ($_GET['c2']=="35"){
echo "DarkElf Assassin";
}
  if ($_GET['c2']=="34"){
echo "DarkElf BladeDancer";
}
  if ($_GET['c2']=="39"){
echo "DarkElf DarkWizard";
}
  if ($_GET['c2']=="31"){
echo "DarkElf Fighter";
}
  if ($_GET['c2']=="108"){
echo "DarkElf GhostHunter";
}
  if ($_GET['c2']=="109"){
echo "DarkElf GhostSentinel";
}
  if ($_GET['c2']=="38"){
echo "DarkElf Mage";
}
  if ($_GET['c2']=="32"){
echo "DarkElf PaulusKnight";
}
  if ($_GET['c2']=="37"){
echo "DarkElf PhantomRanger";
}
  if ($_GET['c2']=="41"){
echo "DarkElf PhantomSummoner";
}
  if ($_GET['c2']=="43"){
echo "DarkElf ShillienElder";
}
  if ($_GET['c2']=="33"){
echo "DarkElf ShillienKnight";
}
  if ($_GET['c2']=="42"){
echo "DarkElf ShillienOracle";
}
  if ($_GET['c2']=="112"){
echo "DarkElf ShillienSaint";
}
  if ($_GET['c2']=="106"){
echo "DarkElf ShillienTemplar";
}
  if ($_GET['c2']=="107"){
echo "DarkElf SpectralDancer";
}
  if ($_GET['c2']=="111"){
echo "DarkElf SpectralMaster";
}
  if ($_GET['c2']=="40"){
echo "DarkElf Spellhowler";
}
  if ($_GET['c2']=="110"){
echo "DarkElf StormScreamer";
}
  if ($_GET['c2']=="56"){
echo "Dwarf Artisan";
}
  if ($_GET['c2']=="55"){
echo "Dwarf BountyHunter";
}
  if ($_GET['c2']=="53"){
echo "Dwarf Fighter";
}
  if ($_GET['c2']=="117"){
echo "Dwarf FortuneSeeker";
}
  if ($_GET['c2']=="118"){
echo "Dwarf Maestro";
}
  if ($_GET['c2']=="54"){
echo "Dwarf Scavenger";
}
  if ($_GET['c2']=="57"){
echo "Dwarf Warsmith";
}
  if ($_GET['c2']=="30"){
echo "Elf Elder";
}
  if ($_GET['c2']=="104"){
echo "Elf ElementalMaster";
}
  if ($_GET['c2']=="28"){
echo "Elf ElementalSummoner";
}
  if ($_GET['c2']=="105"){
echo "Elf EvaSaint";
}
  if ($_GET['c2']=="99"){
echo "Elf EvaTemplar";
}
  
  if ($_GET['c2']=="18"){
echo "Elf Fighter";
}
  if ($_GET['c2']=="19"){
echo "Elf Knight";
}
  if ($_GET['c2']=="25"){
echo "Elf Mage";
}
  if ($_GET['c2']=="102"){
echo "Elf MoonlightSentinel";
}
  if ($_GET['c2']=="103"){
echo "Elf MysticMuse";
}
  if ($_GET['c2']=="29"){
echo "Elf Oracle";
}
  if ($_GET['c2']=="23"){
echo "Elf PlainsWalker";
}
  if ($_GET['c2']=="22"){
echo "Elf Scout";
}
  if ($_GET['c2']=="24"){
echo "Elf SilverRanger";
}
  if ($_GET['c2']=="27"){
echo "Elf SpellSinger";
}
  if ($_GET['c2']=="100"){
echo "Elf SwordMuse";
}
  if ($_GET['c2']=="21"){
echo "Elf SwordSinger";
}
  if ($_GET['c2']=="20"){
echo "Elf TempleKnight";
}
  if ($_GET['c2']=="101"){
echo "Elf WindRider";
}
  if ($_GET['c2']=="26"){
echo "Elf Wizard";
}
  if ($_GET['c2']=="93"){
echo "Human Adventurer";
}
  if ($_GET['c2']=="96"){
echo "Human ArcanaLord";
}
  if ($_GET['c2']=="94"){
echo "Human Archmage";
}
  if ($_GET['c2']=="16"){
echo "Human Bishop";
}
  if ($_GET['c2']=="97"){
echo "Human Cardinal";
}
  if ($_GET['c2']=="15"){
echo "Human Cleric";
}
  if ($_GET['c2']=="6"){
echo "Human DarkAvenger";
}
  if ($_GET['c2']=="89"){
echo "Human Dreadnought";
}
  if ($_GET['c2']=="88"){
echo "Human Duelist";
}
  if ($_GET['c2']=="0"){
echo "Human Fighter";
}
  if ($_GET['c2']=="2"){
echo "Human Gladiator";
}
  if ($_GET['c2']=="9"){
echo "Human Hawkeye";
}
  if ($_GET['c2']=="91"){
echo "Human HellKnight";
}
  if ($_GET['c2']=="98"){
echo "Human Hierophant";
}
  if ($_GET['c2']=="4"){
echo "Human Knight";
}
  if ($_GET['c2']=="10"){
echo "Human Mage";
}
  if ($_GET['c2']=="13"){
echo "Human Necromancer";
}
  if ($_GET['c2']=="5"){
echo "Human Paladin";
}
  if ($_GET['c2']=="90"){
echo "Human PhoenixKnight";
}
  if ($_GET['c2']=="17"){
echo "Human Prophet";
}
  if ($_GET['c2']=="7"){
echo "Human Rogue";
}
  if ($_GET['c2']=="92"){
echo "Human Sagittarius";
}
  if ($_GET['c2']=="12"){
echo "Human Sorcerer";
}
  if ($_GET['c2']=="95"){
echo "Human Soultaker";
}
  if ($_GET['c2']=="8"){
echo "Human TreasureHunter";
}
  if ($_GET['c2']=="14"){
echo "Human Warlock";
}
  if ($_GET['c2']=="3"){
echo "Human Warlord";
}
  if ($_GET['c2']=="1"){
echo "Human Warrior";
}
  if ($_GET['c2']=="11"){
echo "Human Wizard";
}
  if ($_GET['c2']=="46"){
echo "Orc Destroyer";
}
  if ($_GET['c2']=="115"){
echo "Orc Dominator";
}
  if ($_GET['c2']=="116"){
echo "Orc Doomcryer";
}
  if ($_GET['c2']=="44"){
echo "Orc Fighter";
}
  if ($_GET['c2']=="114"){
echo "Orc GrandKhauatari";
}
  if ($_GET['c2']=="49"){
echo "Orc Mage";
}
  if ($_GET['c2']=="47"){
echo "Orc Monk";
}
  if ($_GET['c2']=="51"){
echo "Orc Overlord";
}
  if ($_GET['c2']=="45"){
echo "Orc Raider";
}
  if ($_GET['c2']=="50"){
echo "Orc Shaman";
}
  if ($_GET['c2']=="113"){
echo "Orc Titan";
}
  if ($_GET['c2']=="48"){
echo "Orc Tyrant";
}
  if ($_GET['c2']=="52"){
echo "Orc Warcryer";
}
?></td>
          <td width="7%" bgcolor="#333333">Type</td>
        </tr>
<?
$res=do_sql("SELECT T.`class_id` AS c_id,T.`skill_id` AS s_id, MAX( T.`level` ) AS max, T.`name` AS name
FROM `skill_trees` AS T
WHERE T.`class_id` = '".$_GET['c1']."'
OR T.`class_id` = '".$_GET['c2']."'
GROUP BY T.`skill_id`
ORDER BY T.`skill_id`");
/*$res=do_sql("SELECT DISTINCT `skill_id` FROM `skill_trees` WHERE `class_id` = '".$_GET['c1']."' OR `class_id` ='".$_GET['c2']."'");*/
while($row=mysql_fetch_assoc($res)){
?>

	        <tr>
          <td><img src="image/icons/<? 
	
		$res12=do_sql("SELECT name,description,operate_type FROM `l2jz_skills` WHERE `id`=".$row['s_id']." AND `level`='".$row['max']."'");
$row12=mysql_fetch_assoc($res12);
	if($row['s_id']==239){
			$numero=$row['s_id']+($row['max']-1);
		echo "skill0".$numero;	

	}else{
		if (strlen($row['s_id'])==1){
			$prefile= "skill000".$row['s_id'];
		}elseif (strlen($row['s_id'])==2){
			$prefile= "skill00".$row['s_id'];
		}elseif (strlen($row['s_id'])==3){
			$prefile= "skill0".$row['s_id'];
		}elseif (strlen($row['s_id'])==4){
			$prefile= "skill".$row['s_id'];
		}
		if(file_exists("./image/icons/".$prefile.".png")){
			echo $prefile;
		}elseif(file_exists("./image/icons/".$prefile."_0.png")){
			echo $prefile."_0";
		}else{
			echo "_0";
		}
	
	} ?>.png" width="32" height="32" class="Estilo1"></td>
          <td><?=$row['name']?><br />
		  <?=$row12['description']?>
</td>
          <td><?
		  $resaaaaa=do_sql("SELECT MAX(LEVEL ) AS max FROM `skill_trees`WHERE `class_id` ='".$_GET['c1']."' AND `skill_id` ='".$row['s_id']."'");
		  $rowaaaaa=mysql_fetch_assoc($resaaaaa);
		  if($rowaaaaa['max']==0){
		  	echo "-";
		  }else{
		  	echo "<a href='index.php?file=skills_p&id=".$row['s_id']."'>".$rowaaaaa['max']."</a>";
		  }
		  ?></td>
          <td><?
		  $resaaaaa=do_sql("SELECT MAX(LEVEL ) AS max FROM `skill_trees`WHERE `class_id` ='".$_GET['c2']."' AND `skill_id` ='".$row['s_id']."'");
		  $rowaaaaa=mysql_fetch_assoc($resaaaaa);
		  if($rowaaaaa['max']==0){
		  	echo "-";
		  }else{
		  	echo "<a href='index.php?file=skills_p&id=".$row['s_id']."'>".$rowaaaaa['max']."</a>";
		  }
		  ?></td>
          <td><?
				if ($row12['operate_type']=="0")
				{
					echo "Active";
				}elseif ($row12['operate_type']=="1")
				{
					echo "Buff - Debuff";
				}elseif ($row12['operate_type']=="2")
				{
					echo "Passive";
				}elseif ($row12['operate_type']=="3")
				{
					echo "Toggles";
				}elseif ($row12['operate_type']=="")
				{
					echo "-Unknown-";
				}
				?></td>
        </tr>
	<?

}
?>
</table>
<?
}
?>
