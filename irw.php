<? if(isset($_GET['file'])){?><body>
<br />
<center>
  <strong> Interlude refinements </strong>
</center>
<br />
<br />
Here follow a synthetic view of all existing Interlude refinements.<br />
<br />
Note that all combinations are not possible, click <a href="index.php?file=wrc6"><strong>here</strong></a> to see the 16,380 possible refinements combinations<br />
<br />
<br />
<center>
  <hr />
  <strong>Stats modificators</strong><br />
  <hr />
</center>
<br />
<table border="0">
  <tbody>
    <tr>
      <td id="http://l2inside.org"><li id="http://l2inside.org"><strong>Accuracy +X</strong></li></td>
      <td id="http://l2inside.org">X<strong> between +0.4 and +3.0</strong></td>
    </tr>
    <tr>
      <td id="http://l2inside.org"><li id="http://l2inside.org"><strong>CP Recovery +X</strong></li></td>
      <td id="http://l2inside.org">X<strong> between +0.1 and +0.8</strong></td>
    </tr>
    <tr>
      <td id="http://l2inside.org"><li id="http://l2inside.org"><strong>Critical +X</strong></li></td>
      <td id="http://l2inside.org">X<strong> between +4.9 and +32.7</strong></td>
    </tr>
    <tr>
      <td id="http://l2inside.org"><li id="http://l2inside.org"><strong>Dodge +X</strong></li></td>
      <td id="http://l2inside.org">X<strong> between +0.4 and +3.0</strong></td>
    </tr>
    <tr>
      <td id="http://l2inside.org"><li id="http://l2inside.org"><strong>HP Recovery +X</strong></li></td>
      <td id="http://l2inside.org">X<strong> between +0.1 and +0.8</strong></td>
    </tr>
    <tr>
      <td id="http://l2inside.org"><li id="http://l2inside.org"><strong>M. Atk. +X</strong></li></td>
      <td id="http://l2inside.org">X<strong> between +2.5 +28.2</strong></td>
    </tr>
    <tr>
      <td id="http://l2inside.org"><li id="http://l2inside.org"><strong>Maximum CP +X</strong></li></td>
      <td id="http://l2inside.org">X<strong> between +19.2 and +214.9</strong></td>
    </tr>
    <tr>
      <td id="http://l2inside.org"><li id="http://l2inside.org"><strong>Maximum HP +X</strong></li></td>
      <td id="http://l2inside.org">X<strong> between +12.7 and +155.9</strong></td>
    </tr>
    <tr>
      <td id="http://l2inside.org"><li id="http://l2inside.org"><strong>Maximum MP +X</strong></li></td>
      <td id="http://l2inside.org">X<strong> between +6.1 and +115.1</strong></td>
    </tr>
    <tr>
      <td id="http://l2inside.org"><li id="http://l2inside.org"><strong>M. Def. +X</strong></li></td>
      <td id="http://l2inside.org">X<strong> between +4 and +30.8</strong></td>
    </tr>
    <tr>
      <td id="http://l2inside.org"><li id="http://l2inside.org"><strong>MP Recovery +X&nbsp;&nbsp;&nbsp;&nbsp;</strong></li></td>
      <td id="http://l2inside.org">X<strong> between +0.1 and +0.4</strong></td>
    </tr>
    <tr>
      <td id="http://l2inside.org"><li id="http://l2inside.org"><strong>P. Atk. +X</strong></li></td>
      <td id="http://l2inside.org">X<strong> between +2.2 and +24.2</strong></td>
    </tr>
    <tr>
      <td id="http://l2inside.org"><li id="http://l2inside.org"><strong>P. Def. +X</strong></li></td>
      <td id="http://l2inside.org">X<strong> between +5.1 and +38.2</strong></td>
    </tr>
    <tr>
      <td id="http://l2inside.org"><li id="http://l2inside.org"><strong>CON +1</strong></li></td>
    </tr>
    <tr>
      <td id="http://l2inside.org"><li id="http://l2inside.org"><strong>INT +1</strong></li></td>
    </tr>
    <tr>
      <td id="http://l2inside.org"><li id="http://l2inside.org"><strong>MEN +1</strong></li></td>
    </tr>
    <tr>
      <td id="http://l2inside.org"><li id="http://l2inside.org"><strong>STR +1</strong></li></td>
    </tr>
  </tbody>
</table>
<br />
<center>
  <hr />
  <strong>Actives</strong><br />
  <hr />
</center>
<br />
<ul>
  <li id="http://l2inside.org">Active: Absorbs HP.<br />
  </li>
  <li id="http://l2inside.org">Active: Allows you to reflect some of the damage you incurred  back to the enemy for a certain amount of time. Excludes damage from  skill or remote attacks.<br />
  </li>
  <li id="http://l2inside.org">Active: Attacks by throwing a boulder.<br />
  </li>
  <li id="http://l2inside.org">Active: Burns up the enemy's MP.<br />
  </li>
  <li id="http://l2inside.org">Active: Decreases all skill MP consumption rates temporarily.<br />
  </li>
  <li id="http://l2inside.org">Active: Decreases all skill re-use times temporarily.<br />
  </li>
  <li id="http://l2inside.org">Active: Decreases a target's urge to attack.<br />
  </li>
  <li id="http://l2inside.org">Active: Decreases the magic MP consumption rate temporarily.<br />
  </li>
  <li id="http://l2inside.org">Active: Decreases the magic re-use time temporarily.<br />
  </li>
  <li id="http://l2inside.org">Active: Decreases the opponent's PVP P. Atk. temporarily.<br />
  </li>
  <li id="http://l2inside.org">Active: Decreases the skill MP consumption rate temporarily.<br />
  </li>
  <li id="http://l2inside.org">Active: Decreases the skill re-use time temporarily.<br />
  </li>
  <li id="http://l2inside.org">Active: Decreases the song/dance skill MP consumption rate temporarily.<br />
  </li>
  <li id="http://l2inside.org">Active: Decreases the song/dance skill re-use time temporarily.<br />
  </li>
  <li id="http://l2inside.org">Active: Decreases the target's urge to attack.<br />
  </li>
  <li id="http://l2inside.org">Active: Detonates a fireball by compressing the air around the caster.<br />
  </li>
  <li id="http://l2inside.org">Active: Ignites a firecracker.<br />
  </li>
  <li id="http://l2inside.org">Active: Ignites a large firecracker.<br />
  </li>
  <li id="http://l2inside.org">Active: Increases Accuracy temporarily.<br />
  </li>
  <li id="http://l2inside.org">Active: Increases Dodge temporarily.<br />
  </li>
  <li id="http://l2inside.org">Active: Increases M. Atk. temporarily.<br />
  </li>
  <li id="http://l2inside.org">Active: Increases M. Def. temporarily.<br />
  </li>
  <li id="http://l2inside.org">Active: Increases P. Atk. temporarily.<br />
  </li>
  <li id="http://l2inside.org">Active: Increases P. Def. temporarily.<br />
  </li>
  <li id="http://l2inside.org">Active: Increases PVP P. Atk. temporarily.<br />
  </li>
  <li id="http://l2inside.org">Active: Increases resistance to damage from falling temporarily.<br />
  </li>
  <li id="http://l2inside.org">Active: Increases the ability to restore some HP from the  damage inflicted on an enemy temporarily. Excludes damage by skill or  long-range attacks.<br />
  </li>
  <li id="http://l2inside.org">Active: Increases the chance of a critical attack temporarily.<br />
  </li>
  <li id="http://l2inside.org">Active: Increases the critical attack rate of magic attacks temporarily.<br />
  </li>
  <li id="http://l2inside.org">Active: Increases the effectiveness of HP recovery magic temporarily.<br />
  </li>
  <li id="http://l2inside.org">Active: Increases the maximum CP temporarily.<br />
  </li>
  <li id="http://l2inside.org">Active: Increases the maximum HP temporarily.<br />
  </li>
  <li id="http://l2inside.org">Active: Increases the maximum HP temporarily and restores HP by the increased amount.<br />
  </li>
  <li id="http://l2inside.org">Active: Increases the maximum MP temporarily.<br />
  </li>
  <li id="http://l2inside.org">Active: Increases the power of HP recovery magic temporarily.<br />
  </li>
  <li id="http://l2inside.org">Active: Increases the recharge recover rate of MP.<br />
  </li>
  <li id="http://l2inside.org">Active: Increases the recharge recovery rate of MP.<br />
  </li>
  <li id="http://l2inside.org">Active: Increases the target's urge to attack.<br />
  </li>
  <li id="http://l2inside.org">Active: Inflicts a dark attack.<br />
  </li>
  <li id="http://l2inside.org">Active: Inflicts a dark attack in the shape of a parabola.<br />
  </li>
  <li id="http://l2inside.org">Active: Inflicts a elementally challenged attack.<br />
  </li>
  <li id="http://l2inside.org">Active: Inflicts a elementally challenged attack in the shape of a parabola.<br />
  </li>
  <li id="http://l2inside.org">Active: Inflicts a flaming attack.<br />
  </li>
  <li id="http://l2inside.org">Active: Inflicts a flaming attack in the shape of a parabola.<br />
  </li>
  <li id="http://l2inside.org">Active: Inflicts a gusting attack.<br />
  </li>
  <li id="http://l2inside.org">Active: Inflicts a gusting attack in the shape of a parabola.<br />
  </li>
  <li id="http://l2inside.org">Active: Inflicts an earthen attack attack.<br />
  </li>
  <li id="http://l2inside.org">Active: Inflicts an earthen attack attack in the shape of a parabola.<br />
  </li>
  <li id="http://l2inside.org">Active: Inflicts a sacred attack.<br />
  </li>
  <li id="http://l2inside.org">Active: Inflicts a sacred attack in the shape of a parabola.<br />
  </li>
  <li id="http://l2inside.org">Active: Inflicts a water-type attack.<br />
  </li>
  <li id="http://l2inside.org">Active: Inflicts a water-type attack in the shape of a parabola.<br />
  </li>
  <li id="http://l2inside.org">Active: Inflicts damage by throwing a boulder.<br />
  </li>
  <li id="http://l2inside.org">Active: Instantly restores your HP.<br />
  </li>
  <li id="http://l2inside.org">Active: Launches a dark attack.<br />
  </li>
  <li id="http://l2inside.org">Active: Launches a sacred attack.<br />
  </li>
  <li id="http://l2inside.org">Active: Momentarily becomes invincible.<br />
  </li>
  <li id="http://l2inside.org">Active: Momentarily frightens the target, causing him to flee.<br />
  </li>
  <li id="http://l2inside.org">Active: Opens doors and treasure chests with a certain probability. Requires Keys of a Thief.<br />
  </li>
  <li id="http://l2inside.org">Active: Play music for a short duration.<br />
  </li>
  <li id="http://l2inside.org">Active: Puts the opponent's mind at peace and erases the desire to attack.<br />
  </li>
  <li id="http://l2inside.org">Active: Regenerates MP.<br />
  </li>
  <li id="http://l2inside.org">Active: Removes the target.<br />
  </li>
  <li id="http://l2inside.org">Active: Restores CP.<br />
  </li>
  <li id="http://l2inside.org">Active: Restores your own CP.<br />
  </li>
  <li id="http://l2inside.org">Active: Resurrects a corpse.<br />
  </li>
  <li id="http://l2inside.org">Active: Teleports party members to a village. Cannot be used  in a specially designated place such as the GM Consultation Service.<br />
  </li>
  <li id="http://l2inside.org">Active: Teleports the caster to a village. Cannot be used in a specially designated place such as the GM Consultation Service.<br />
  </li>
  <li id="http://l2inside.org">Active: Teleports to a village regardless of time. Cannot be  used in a specially designated place such as the GM Consultation  Service.<br />
  </li>
  <li id="http://l2inside.org">Active: Temporarily blocks all of the target's physical/magic skills.<br />
  </li>
  <li id="http://l2inside.org">Active: Temporarily blocks a monster's pre-emptive attack. Fighting ability significantly decreases while in effect.<br />
  </li>
  <li id="http://l2inside.org">Active: Temporarily blocks the target's magic skills.<br />
  </li>
  <li id="http://l2inside.org">Active: Temporarily causes a target to bleed heavily.<br />
  </li>
  <li id="http://l2inside.org">Active: Temporarily decreases a target's Atk. Spd.<br />
  </li>
  <li id="http://l2inside.org">Active: Temporarily decreases a target's speed.<br />
  </li>
  <li id="http://l2inside.org">Active: Temporarily holds the target. The target cannot be affected by any additional hold attacks while the effect lasts.<br />
  </li>
  <li id="http://l2inside.org">Active: Temporarily Increases the distance you can jump without sustaining damage.<br />
  </li>
  <li id="http://l2inside.org">Active: Temporarily Increases your head size.<br />
  </li>
  <li id="http://l2inside.org">Active: Temporarily Increases your lung capacity.<br />
  </li>
  <li id="http://l2inside.org">Active: Temporarily paralyzes a target.<br />
  </li>
  <li id="http://l2inside.org">Active: Temporarily petrifies the target.<br />
  </li>
  <li id="http://l2inside.org">Active: Temporarily poisons the target.<br />
  </li>
  <li id="http://l2inside.org">Active: Temporarily puts the target to sleep. The target  cannot be affected by any additional sleep attacks while the effect  lasts.<br />
  </li>
  <li id="http://l2inside.org">Active: Temporarily resist damage from falls.<br />
  </li>
  <li id="http://l2inside.org">Active: Temporarily stuns the target.<br />
  </li>
  <li id="http://l2inside.org">Active: Unleashes a general attack and temporarily decreases your magic attack power during PvP.<br />
      <br />
    <center>
      <hr />
      <strong>Chance</strong><br />
      <hr />
    </center>
    <br />
  </li>
  <li id="http://l2inside.org">Chance: Blocks all of the physical/magic skills of the target that inflicts damage on you with a certain probability.<br />
  </li>
  <li id="http://l2inside.org">Chance: Blocks the magic skills of the target that inflicts damage on you with a certain probability.<br />
  </li>
  <li id="http://l2inside.org">Chance: Decreases the Atk. Spd. of the target that inflicts damage on you with a certain probability.<br />
  </li>
  <li id="http://l2inside.org">Chance: Decreases the PVP P. Atk. of the target that inflicts damage on you with a certain probability.<br />
  </li>
  <li id="http://l2inside.org">Chance: Decreases the speed of the target that inflicts damage on you with a certain probability.<br />
  </li>
  <li id="http://l2inside.org">Chance: Decreases the urge to fight of the target that inflicts damage on you with a certain probability.<br />
  </li>
  <li id="http://l2inside.org">Chance: Has a chance to hold a target that damages you. The  target cannot be affected by any additional hold attacks while the  effect lasts.<br />
  </li>
  <li id="http://l2inside.org">Chance: Has a chance to paralyze a target that damages you.<br />
  </li>
  <li id="http://l2inside.org">Chance: Has a chance to petrify a target that damages you.<br />
  </li>
  <li id="http://l2inside.org">Chance: Has a chance to poison a target that damages you.<br />
  </li>
  <li id="http://l2inside.org">Chance: Has a chance to sleep a target that damages you.<br />
  </li>
  <li id="http://l2inside.org">Chance: Has a chance to sleep a target that damages you. The  target cannot be affected by any additional sleep attacks while the  effect lasts.<br />
  </li>
  <li id="http://l2inside.org">Chance: Has a chance to stun a target that damages you.<br />
  </li>
  <li id="http://l2inside.org">Chance: Increases accuracy with a certain probability when damage is incurred.<br />
  </li>
  <li id="http://l2inside.org">Chance: Increases critical attack rate of magic attacks with a certain probability when damage is incurred.<br />
  </li>
  <li id="http://l2inside.org">Chance: Increases critical attack rate with a certain probability when damage is incurred.<br />
  </li>
  <li id="http://l2inside.org">Chance: Increases evasion with a certain probability when damage is incurred.<br />
  </li>
  <li id="http://l2inside.org">Chance: Increases M. Atk. with a certain probability when damage is incurred.<br />
  </li>
  <li id="http://l2inside.org">Chance: Increases maximum CP with a certain probability when damage is incurred.<br />
  </li>
  <li id="http://l2inside.org">Chance: Increases maximum HP with a certain probability when damage is incurred.<br />
  </li>
  <li id="http://l2inside.org">Chance: Increases maximum MP with a certain probability when damage is incurred.<br />
  </li>
  <li id="http://l2inside.org">Chance: Increases M. Def. with a certain probability when damage is incurred.<br />
  </li>
  <li id="http://l2inside.org">Chance: Increases P. Atk. with a certain probability when damage is incurred.<br />
  </li>
  <li id="http://l2inside.org">Chance: Increases P. Def. with a certain probability when damage is incurred.<br />
  </li>
  <li id="http://l2inside.org">Chance: Increases PVP P. Atk. with a certain probability when damage is incurred.<br />
  </li>
  <li id="http://l2inside.org">Chance: Increases recovery rate when receiving HP recovery magic with a certain probability after damage is incurred.<br />
  </li>
  <li id="http://l2inside.org">Chance: Increases recovery rate when receiving MP recovery magic with a certain probability after damage is incurred.<br />
  </li>
  <li id="http://l2inside.org">Chance: Momentarily blocks all of the target's physical and magic skills during a critical attack.<br />
  </li>
  <li id="http://l2inside.org">Chance: Momentarily blocks all of the target's physical and magic skills during a magic attack.<br />
  </li>
  <li id="http://l2inside.org">Chance: Momentarily blocks all of the target's physical and magic skills during a physical attack.<br />
  </li>
  <li id="http://l2inside.org">Chance: Momentarily blocks the target's magic skill during a critical attack.<br />
  </li>
  <li id="http://l2inside.org">Chance: Momentarily blocks the target's magic skill during a magic attack.<br />
  </li>
  <li id="http://l2inside.org">Chance: Momentarily blocks the target's magic skill during a physical attack.<br />
  </li>
  <li id="http://l2inside.org">Chance: Momentarily burns up the target's MP during a critical attack.<br />
  </li>
  <li id="http://l2inside.org">Chance: Momentarily burns up the target's MP during a magic attack.<br />
  </li>
  <li id="http://l2inside.org">Chance: Momentarily burns up the target's MP during a physical attack.<br />
  </li>
  <li id="http://l2inside.org">Chance: Momentarily causes the target to bleed during a critical attack.<br />
  </li>
  <li id="http://l2inside.org">Chance: Momentarily causes the target to bleed during a magic attack.<br />
  </li>
  <li id="http://l2inside.org">Chance: Momentarily causes the target to bleed during a physical attack.<br />
  </li>
  <li id="http://l2inside.org">Chance: Momentarily decreases a target's Atk. Spd. during a critical attack.<br />
  </li>
  <li id="http://l2inside.org">Chance: Momentarily decreases a target's Atk. Spd. during a magic attack.<br />
  </li>
  <li id="http://l2inside.org">Chance: Momentarily decreases a target's Atk. Spd. during a physical attack.<br />
  </li>
  <li id="http://l2inside.org">Chance: Momentarily decreases a target's speed during a critical attack.<br />
  </li>
  <li id="http://l2inside.org">Chance: Momentarily decreases a target's speed during a physical attack.<br />
  </li>
  <li id="http://l2inside.org">Chance: Momentarily decreases the target's speed during a magic attack. Intensity 3.<br />
  </li>
  <li id="http://l2inside.org">Chance: Momentarily frightens the target, causing him to flee during a magic attack.<br />
  </li>
  <li id="http://l2inside.org">Chance: Momentarily frightens the target, causing him to flee during a physical attack.<br />
  </li>
  <li id="http://l2inside.org">Chance: Momentarily frightens the target during a critical attack.<br />
  </li>
  <li id="http://l2inside.org">Chance: Momentarily holds the target during a critical attack.  The target cannot be affected by any additional hold attacks while the  effect lasts.<br />
  </li>
  <li id="http://l2inside.org">Chance: Momentarily holds the target during a magic attack.  The target cannot be affected by any additional hold attacks while the  effect lasts.<br />
  </li>
  <li id="http://l2inside.org">Chance: Momentarily holds the target during a physical attack.  The target cannot be affected by any additional hold attacks while the  effect lasts.<br />
  </li>
  <li id="http://l2inside.org">Chance: Momentarily paralyzes the target during a critical attack.<br />
  </li>
  <li id="http://l2inside.org">Chance: Momentarily paralyzes the target during a magic attack.<br />
  </li>
  <li id="http://l2inside.org">Chance: Momentarily paralyzes the target during a physical attack.<br />
  </li>
  <li id="http://l2inside.org">Chance: Momentarily petrifies the target during a critical attack.<br />
  </li>
  <li id="http://l2inside.org">Chance: Momentarily petrifies the target during a magic attack.<br />
  </li>
  <li id="http://l2inside.org">Chance: Momentarily petrifies the target during a physical attack.<br />
  </li>
  <li id="http://l2inside.org">Chance: Momentarily poisons the target during a critical attack.<br />
  </li>
  <li id="http://l2inside.org">Chance: Momentarily poisons the target during a magic attack.<br />
  </li>
  <li id="http://l2inside.org">Chance: Momentarily poisons the target during a physical attack.<br />
  </li>
  <li id="http://l2inside.org">Chance: Momentarily puts the target to sleep during a critical  attack. The target cannot be affected by any additional sleep attacks  while the effect lasts.<br />
  </li>
  <li id="http://l2inside.org">Chance: Momentarily puts the target to sleep during a magic  attack. The target cannot be affected by any additional sleep attacks  while the effect lasts.<br />
  </li>
  <li id="http://l2inside.org">Chance: Momentarily puts the target to sleep during a physical  attack. The target cannot be affected by any additional sleep attacks  while the effect lasts.<br />
  </li>
  <li id="http://l2inside.org">Chance: Momentarily stuns the target during a critical attack.<br />
  </li>
  <li id="http://l2inside.org">Chance: Momentarily stuns the target during a magic attack.<br />
  </li>
  <li id="http://l2inside.org">Chance: Momentarily stuns the target during a physical attack.<br />
  </li>
  <li id="http://l2inside.org">Chance: Regenerates CP with a certain probability when damage is incurred.<br />
  </li>
  <li id="http://l2inside.org">Chance: Regenerates MP with a certain probability when damage is incurred.<br />
  </li>
  <li id="http://l2inside.org">Chance: Temporarily decreases a target's speed during a critical attack.<br />
  </li>
  <li id="http://l2inside.org">Chance: Temporarily decreases a target's speed during a physical attack.<br />
  </li>
  <li id="http://l2inside.org">Chance: Temporarily decreases a target's urge to attack during a critical attack.<br />
  </li>
  <li id="http://l2inside.org">Chance: Temporarily decreases a target's urge to attack during a physical attack.<br />
  </li>
  <li id="http://l2inside.org">Chance: Temporarily decreases the target's speed during a magic attack. Intensity 3.<br />
  </li>
  <li id="http://l2inside.org">Chance: Temporarily increases the target's urge to attack during a critical attack.<br />
  </li>
  <li id="http://l2inside.org">Chance: Temporarily increases the target's urge to attack during a physical attack.<br />
  </li>
  <li id="http://l2inside.org">Chance: Temporarily provokes a target to attack during a critical attack.<br />
  </li>
  <li id="http://l2inside.org">Chance: Temporarily provokes a target to attack during a physical attack.<br />
  </li>
  <li id="http://l2inside.org">Chance: Throw the target that inflicts damage on you into a state of fear and causes him to flee with a certain probability.<br />
      <br />
    <center>
      <hr />
      <strong>Passives</strong><br />
      <hr />
    </center>
    <br />
  </li>
  <li id="http://l2inside.org">Passive: Decreases the magic MP consumption rate.<br />
  </li>
  <li id="http://l2inside.org">Passive: Decreases the MP consumption rate for all skills.<br />
  </li>
  <li id="http://l2inside.org">Passive: Decreases the skill MP consumption rate.<br />
  </li>
  <li id="http://l2inside.org">Passive: Decreases the song/dance skill MP consumption rate.<br />
  </li>
  <li id="http://l2inside.org">Passive: Increases accuracy.<br />
  </li>
  <li id="http://l2inside.org">Passive: Increases critical attack rate.<br />
  </li>
  <li id="http://l2inside.org">Passive: Increases evasion.<br />
  </li>
  <li id="http://l2inside.org">Passive: Increases lung capacity.<br />
  </li>
  <li id="http://l2inside.org">Passive: Increases Lung Capacity.<br />
  </li>
  <li id="http://l2inside.org">Passive: Increases M. Atk.<br />
  </li>
  <li id="http://l2inside.org">Passive: Increases M. Def.<br />
  </li>
  <li id="http://l2inside.org">Passive: Increases P. Atk.<br />
  </li>
  <li id="http://l2inside.org">Passive: Increases P. Def.<br />
  </li>
  <li id="http://l2inside.org">Passive: Increases PVP P. Atk.<br />
  </li>
  <li id="http://l2inside.org">Passive: Increases the ability to reflect some of the damage  you incurred back to the enemy. Excludes damage by skill or long-range  attacks.<br />
  </li>
  <li id="http://l2inside.org">Passive: Increases the critical attack rate of magic attacks.<br />
  </li>
  <li id="http://l2inside.org">Passive: Increases the effect of HP recovery magic.<br />
  </li>
  <li id="http://l2inside.org">Passive: Increases the height from which you can jump without sustaining damage.<br />
  </li>
  <li id="http://l2inside.org">Passive: Increases the power of HP recovery magic.<br />
  </li>
  <li id="http://l2inside.org">Passive: Increases the recharge recovery rate of MP.<br />
  </li>
  <li id="http://l2inside.org">Passive: Increases the weapon weight limit by 2 times.<br />
  </li>
  <li id="http://l2inside.org">Passive: Raises resistance to damage from falling.</li>
</ul>
<p><strong><a name="new"></a>Refinement System Testing Results and In-Depth Info</strong><br>
Source: <a href="http://lineage2.gametsg.com/index.php?view=article&amp;k1=t1&amp;articleid=698" target="_blank">http://lineage2.gametsg.com/index.ph...&amp;articleid=698</a><br>
<br>
Refinement System Testing Results and In-Depth Info<br>
<br>
<strong>Refinement Town: </strong>All towns except for noob towns and Gludin <br>
<br>
<strong>Refinement Materials</strong>:<br>
Refine (Weapon, Life Orb, Gems) <br>
<br>
Weapon: <br>
&bull;	Grade C ~ S Weapon X1<br>
&bull;	Special weapons such as hero weapons, blood sword etc can not be refined<br>
&bull;	Refinement is not related to soul crystals, any weapons that&rsquo;s got SA can be refined as well<br>
&bull;	After refinement, weapon can not be traded, dropped<br>
&bull; After refinement, weapon can not be stored in guild warehouse, nor  can it be sent to your other characters by using the warehouse, this  can only be stored in the character&rsquo;s personal warehouse <br>
&bull; After refinement, the weapon can not be traded with NPC, nor can be  traded with blacksmith for a higher grade weapon, nor can be combined  into a dual sword <br>
&bull;	Refinement can be cancelled, but it requires more $$$<br>
<br>
Life Orb:<br>
&bull;	Only requires 1 <br>
&bull;	There are 4 types of life orbs (normal, medium grade, high grade, special grade) <br>
&bull;	Life Orbs can be found in 10 different levels (46, 49, 52, 55, 58, 62, 68, 70, 76) <br>
&bull;	Life Orb (type) affects the looks of the weapon after refinement<br>
&bull;	Life Orb (level) affects weapon abilities and by how much after refinement, this is also limited by the life orb level. <br>
&bull;	Life Orbs can now be obtained through mob hunting. <br>
<br>
Gemstones <br>
&bull;	S Grade Weapon &ndash; requires 25 C Grade gemstones<br>
&bull;	A Grade Weapon &ndash; requires 20 C Grade gemstones<br>
&bull;	B Grade Weapon &ndash; requires 30 D Grade gemstones<br>
&bull;	C Grade Weapon &ndash; requires 20 D Grade gemstones<br>
<br>
<br>
Refinement Results: <br>
&bull;	The ability refinement adds to a weapon depends on the Life Orb level and weapon leve<br>
&bull; Refinement results (through scarcity of the life orb) into 4  different colors. (probability depends on the Korean Testing Server +  Taiwan Testing Server)<br>
<br>
Blue: probability is high, generally increases battle ability, but will not increase basic elements (CON/STR/WIT/INT/MEN/DEX)<br>
<br>
Yellow: probably medium, generally has the effects from a blue refinement, and also might increase basic elements. <br>
<br>
Purple: probability low, weapons that are refined with this color will  have an increase in ability affects. (E.g. CON +1, magic attack: affect  of dark magic based affects.) <br>
<br>
Red: probably low, weapons that are refined with this color will  increase in ability effects, and interface will show purple (not sure  if this makes sense to me) e.g. p.def + 12.7, mp regen+0.1, reduce mp  usage<br>
<br>
From the testing results on Taiwan Testing Server (TTS), it seems like  red and purple are very similar, might be because there weren&rsquo;t that  many of them. From the results of KTS, red has less probability than  purple, not to mention it&rsquo;s got better effects than purple. However the  &ldquo;better effects depends on each character.)<br>
<br>
&bull;	Low level Life Orbs, might also be able to refined to a red/purple affect, however the refinement level is pretty low<br>
&bull; High level Life Orbs, might also be able to refined to a higher bonus  effect and higher refinement level, therefore results in better  refinement effects&hellip; (not that it makes too much sense to me either&hellip;)<br>
&bull;	Low probability Life Orbs, (high level, special level) and refinement effect level are not absolutely related<br>
&bull; Low Probability Life Orbs, (high level, special level) and refinement  amount (e.g. +20.7 vs +5.7 on a certain refinement ability) are not  absolutely related <br>
<br>
Lightening Effects: <br>
After refinement, weapons would have the following effects<br>
<br>
&bull; Normal Grade Life Orb &amp; Medium Grade Life Orb generally would not  generate lightening effects, unless it was refined to a red/purple  result. <br>
&bull; Normal Grade Life Orb &amp; Medium Grade Life Orb, if these are  refined to a red/purple result, it would generate a slight lightening  effect. <br>
&bull;	High Grade Life Orb, after refinement would generate a medium lightening effect<br>
&bull;	Special Grade Life Orb, after refinement would generate a strong lightening effect<br>
&bull; For a weapon that was enchanted to a blue color, after refinement,  the original blue color would be lighter than before, it would look  like a light blue lightening effect, the original enchant looks would  decrease in color by a small amount<br>
&bull;	If a weapon was enchanted to a red color, the original red glow would turn into a &ldquo;yellow&rdquo; or &ldquo;golden&rdquo; lightening effect. <br>
<br>
e.g. <img src="http://lineage2.gametsg.com/pic/c6p4/Shot00571.jpg" alt="L2inside" border="0"> for a weak lightening effect, this picture contains a medium life orb,  enchanted to purple, there is only a little &ldquo;glow&rsquo; in it. <br>
<br>
<img src="http://lineage2.gametsg.com/pic/c6p4/Shot00067.jpg" alt="L2inside" border="0"> medium lightening effect, high level life orb<br>
<br>
<img src="http://lineage2.gametsg.com/pic/c6p4/Shot00073.jpg" alt="L2inside" border="0"> strong lightening effect, special grade life orb. <br>
<br>
<img src="http://lineage2.gametsg.com/pic/c6p4/Shot01046.jpg" alt="L2inside" border="0"> a weapon enchanted with +4 or higher with lightening effect <br>
<br>
<img src="http://lineage2.gametsg.com/pic/c6p4/Shot00203.jpg" alt="L2inside" border="0"> a weapon enchanted +16 or up would turn into a yellow/golden lightening effect <br>
<br>
<br>
Removing Refinement Information <br>
&bull;	S grade weapon removal: 480k <br>
&bull;	A grade weapon removal: 330k (low A grade), 390k (mid A Grade), 420k (top A grade) <br>
&bull;	B grade weapon removal: 240k (low B grade), 270k (top B grade) <br>
&bull;	C grade weapon removal: 95k (Low C grade), 150k (mid C grade), 210k (top C grade) <br>
&bull;	After removal of refinement, the original SA would not be affected. <br>
&bull;	Afer removal of refinement, the weapon can be traded, sold, etc, etc <br>
<br>
Refinement Limitations<br>
&bull;	The most number of refinement can be added to a weapon is 4<br>
&bull;	Refinement ability can be categorized in 7 different types<br>
<br>
Attacking Refinement Effects Type: <br>
&bull;	General ability would appear in all color results <br>
&bull; Each kind of ability at most would appear 2 times, some that contain  big numbers (e.g. +37.7 focus) would not appear 2 times with the same  number. <br>
&bull;	The following is a list of outcomes that might appear: <br>
o	M.def, p.def<br>
o	M.atk, p.atk<br>
o	Increase in HP, MP, CP <br>
o	Regen speed in HP, MP, CP<br>
o	Focus <br>
o	Landing rate<br>
o	Agility <br>
<br>
Basic Elements Refinement Type <br>
&bull;	Would only appear in yellow, purple, red color results<br>
&bull;	The official site only indicated refinement effects in STR, CON, MEN, INT <br>
&bull;	Taking all info from the TTS/KTS, each weapon max +1 to only 1 of the above<br>
<br>
Refinement ability with a probability when attacked Type (e.g. when  making a p.atk, probability of causing a poison effect on the enemy)<br>
&bull;	Only appear in purple and red refinement results<br>
&bull;	Weakness: low chance of happening<br>
<br>
Refinement ability with a probability when under attacked Type.(e.g. when being attacked, probability of increasing self p.def)<br>
&bull;	Only appears in a purple, red refinement result<br>
&bull;	Biggest weakness: low chance of happening, even when it happened, low chance of landing <br>
&bull;	Weakness: low chance of happening <br>
<br>
Counter Attack Refinement Type<br>
&bull;	Currently test results from TTS, this only appears with a red refinement result, purple unknown <br>
&bull;	Weakness: though this has a higher chance of happening compare to the 2 above, however landing rate is very low<br>
<br>
Active Skill Type: (e.g. during a time period, lowers mp usage in p.atk) <br>
&bull;	Only appears in purple, red refinement results<br>
&bull;	This appears as a new active skill for the character<br>
&bull;	Skill casting time, cool down time are all fixed, no skills / dances/ chants/ songs/etc can decrease it.</p>
</body>
<?
}else{
	include "error.php";
}
?>
