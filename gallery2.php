<? if (isset($_GET['file'])){?>
<script>
function openWin(sImg, iWidth, iHeight)
{
	window.open('<?php echo $PHP_SELF; ?>?img=' + sImg, 'the_window', "width=" + iWidth + ",height=" + iHeight + ",resizable=yes,scrollbars=no");
}
</script>
<style>
.text {
	font-family: Verdana;
	font-size: 12px;
}
</style>

<?php
// PHP Image Gallery - by Charles Chadwick
//21DEC01
// Read it, Use it, Learn from it. But don't try and take credit for it. 
// If you use it, then keep this info intact. 

// these are the only variables you need to set.
// the images in the $imageDir and $thumbDir folders MUST BE NAMED THE SAME.

$imageDir = "images1/"; // path to the dir that contains your images with trailing / 
$thumbDir = "thumbs1/"; //path to your thumbs dir with trailing / 
$rowLimit = "5"; // how many rows of pictures you want per page
$colLimit = "5"; // how many columns of pictures you want per page
$openInNewWindow = "0"; // if you want the image to open in a new window, set this to 1, if not, then set it to 0
 
function openImage($image)
{
	// this function is used for displaying a large image.
	global $imageDir, $openInNewWindow;
	global $PHP_SELF;
	
	// this is just checking to see if we have opened the large image in a new window or the same browser window.
	// it creates either a Close Window link or Back To Gallery link based on this information.
	
	if ($openInNewWindow == 1)
	{
		$link = "<a href=\"javascript:window.close();\">Close Window</a>";
	}
	else
	{
		$link = "<a href=\"javascript:history.back();\">Back To Gallery</a>";
	}
	
	echo "<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\">\r";
	echo "<tr>\r<td>\r<img src=\"$imageDir$image\">\r</td>\r</tr>\r";
	echo "<tr>\r<td class=\"text\" align=\"center\">\r$link</td>\r</tr>\r";
	echo "</table>\r";
}

function showThumbs($start)
{
	global $imageDir, $thumbDir, $rowLimit, $colLimit, $openInNewWindow;
	global $PHP_SELF;
	
	// we find out what the max amount of images will be, and then create our stop and start variables
	$maxImages = $rowLimit * $colLimit;
	$stop = $maxImages + $start;
	
	// incrementing our start variable so we don't have duplicate images. 
	$start++;
	
	echo "<table cellpadding=\"2\" cellspacing=\"2\" border=\"0\" align=\"center\">\r";
	// opening the directory and reading in the files
	$res=do_sql("SELECT * FROM 1imagenes ORDER BY `1imagenes`.`name` DESC");
	while($row=mysql_fetch_assoc($res)) 
	{

		// just to be safe, we trim the file name, then we check to see if it's a file and not a directory
		$file = trim($row['name']);
	if ($row['valid']=="1"){
		if (is_file($thumbDir.$file))
		{
			// here we makes sure that the current file being read is between our stop and start varibles
			if ($iCount >= $start && $iCount <= $stop)
			{
				// here we check and see if we are at the begining of a new row. If so, we echo out a <tr> tag
				if ($endOfRow == 0) { echo "<tr>\r"; }
				
				// if the 'large file' exists, we gather information about it, and then create a link of the thumbnail
				if (file_exists($imageDir.$file))
				{
					if ($openInNewWindow == 1)
					{
						$imgSize = GetImageSize($imageDir.$file);
						$link = "<a href=\"javascript:openWin('$file', '$imgSize[0]', '".($imgSize[1] + 20)."');\">";
					}
					else
					{
						$link = "<a href=\"index.php?file=screen&mode=1&view=".$row['id_image']."\">";
					}
	
					$thumbNail = $thumbDir.$file;
					$imgSize = GetImageSize($thumbNail);
					echo "<td class=\"text\" align=\"center\">$link<img src=\"".$thumbDir.$file."\" border=\"0\" $imgSize[3]></a></td>\r";
				}
				else
				{
					// if the large image doesn't exist but the thumbnail does, we just echo the thumbnail without a link
					echo "<td><img src=\"".$thumbDir.$file."\" border=\"0\" $imgSize[3]></td>\r";
				}
				
				// incrementing our $endOfRow variable. This keeps track of which column we are currently on.
				$endOfRow ++;
				
				// if the $endOfRow is equal to the max amount of columns we want, then echo a "</tr>" tag. 
				if ($endOfRow == $colLimit) { $endOfRow = 0; echo "</tr>\r"; }				
			}
			// incrementing our $iCount variable, which keeps track of 
			$iCount++;
		}
		}
	}
	
	// decrementing our $start variable
	$start--;
	
	// setting our $back and $next variables for use in the navgation links
	$back = $start - $maxImages;
	$next = $stop;
	
	// this is where we check if we are on the first page. If we are, we create no link for the $back variable
	if ($start < $maxImages) 
	{ $back = "Back"; } 
	else 
	{ $back = "<a href=\"index.php?file=gallery&start=".abs($start - $maxImages)."\">Back</a>"; }
	
	// this is where we check if we are on the last page. If we are, we create no link for the $back variable
	if ($stop >= $iCount) 
	{ $next = "Next"; } 
	else 
	{ $next = "<a href=\"index.php?file=gallery&start=$stop\">Next</a>"; }

    // here we are dividing the amount of images in the directory ($iCount) by the max number of images to display on the page ($maxImages)
	// then we cycle through that amount and create a link out of each number. This then becomes our page numbers. 
	
	for ($i=0; $i<=(round(($iCount/$maxImages),0)); $i++)
	{
	 	 $num = $i * $maxImages;
		 $curPage = $i + 1;
		 
		 if ($num == $start)
		 {
		 	$pageNums .= " $curPage ";
		 }
		 else
		 {
		 	$pageNums .= " <a class=\"two\" href=\"index.php?file=gallery&start=$num\" class=\"two\">$curPage</a>\r ";
		 }
	}
	
	// this last if statement is for echoing out the number of images
	if ($stop >= $iCount) { $stop = $iCount; }
	echo "<tr><td colspan=\"$colLimit\" align=\"center\" class=\"text\">Now viewing ".$start." through ".($stop-1)." of ".($iCount-1)." total images</td></tr>";
	echo "<tr><td colspan=\"$colLimit\" align=\"center\" class=\"text\">$pageNums</td></tr>\r";
	echo "<tr><td colspan=\"$colLimit\" align=\"center\" class=\"text\">$back | <a href=\"index.php?file=gallery&start=0\">Home</a> | $next</td></tr>\r";
	echo "</table>\r";
}




// if the $img variable is set, then we know that we want to display the large image
if ($img)
{
	openImage($img);
}
else
{
	// otherwise, we know we want to display the thumbs
	if (!$start)
	{
		$start = "0";
	}
	
	showThumbs($start);
}

}else{
include "error.php";
}
?>