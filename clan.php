<? if (isset($_GET['file'])){
echo'
<table align="center" border="0" cellpadding="0" cellspacing="1" width="100%">
  <tbody>
    <tr>
      <td><strong>Clans</strong></td>
    </tr>

    <tr>
      <td colspan="3"><ul>
        <li>A  clan leader may transfer their rights to other clan members. The leader  must request the clan leader transfer from an NPC who is in charge of  clan activities. The new clan leader will assume their new role during  the next maintenance time (10:30 AM Central on Tuesdays), or during a  server downtime.</li>
        <li>To mail to all clan members, open the  Community menu and click the Clan tab. Click the link Clan Mail. This  will open a template for clan leader to mail to all clan members.</li>
        <li>In  order to keep the clan hall under the same owner, 100k adena for  rent/maintenance fee must be kept in the Clan Warehouse. The system  will automatically deduct this amount each week.</li>
        <li>When uploading your clan crest, make sure to save your 16 x 12 image in 256 color format, you can donwload in this page, a lot of crests.</li>
        <li>If an opponent in a clan war kills you using a pet or a summon, the normal death rules will still apply.</li>
        <li>Clan  names may be sixteen letters long, with no spaces, symbols, or numbers  allowed. If you dissolve your clan, the name will remain attached to  your character for seven days. After the dissolution is final, the name  will become available to others.</li>
        <li>Pressing the &quot;Recover&quot; option will immediately cancel a dissolution request, but it will not return the XP already lost.</li>
        <li>If you choose to dissolve a Clan, press the &quot;Dissolve&quot; link only once. Pressing it again will reset the dissolution timer.</li>
        <li>After  a clan leader has waited the necessary seven days for their old clan to  dissolve, they must then wait an additional ten days before creating a  new clan.</li>
        <li>Clan leaders will lose experience equal to one death for requesting a clan dissolution.</li>
        <li>Whenever  you perform a clan action, such as a dissolution or dismissal, please  write down the exact date and time of the action, for your own future  reference.</li>
        <li>Clan crests will not be displayed until your clan is level three.</li>
        <li>There is no way to transfer skill points (SP) to your clan leader, or any other player.</li>
        <li>Clan members must wait one day before joining a new clan once they are evicted or withdraw from their previous clan.</li>
        <li>Clan leaders must wait one day before inviting new clan members after any previous member has left or been removed.</li>
      </ul></td>
    </tr>
  </tbody>
</table>';
}else{
include "error.php";
}
?>
