<?
$uploadfile="Dibujo.bmp";
$i=imagecreatefromfile($uploadfile);
imagepng($i); 
function imagecreatefromfile($filename)
{
   static $image_creators;

   if (!isset($image_creators)) {
       $image_creators = array(
           1  => "imagecreatefromgif",
           2  => "imagecreatefromjpeg",
           3  => "imagecreatefrompng",
       6  => "imagecreatefrombmp",
           16 => "imagecreatefromxbm"
       );
   }

   $image_size = getimagesize($filename);
   if (is_array($image_size)) {
       $file_type = $image_size[2];
       if (isset($image_creators[$file_type])) {
           $image_creator = $image_creators[$file_type];
           if (function_exists($image_creator)) {
               return $image_creator($filename);
           }
       } else {
               echo "Unknown file type: ".$file_type;
       }
   }
  
   // "imagecreatefrom...() returns an empty string on failure"
   return "";
} 
?>