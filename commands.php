<? if (isset($_GET['file'])){?>
<style type="text/css">
<!--
.Estilo1 {font-weight: bold}
.Estilo4 {
	color: #6666CC;
	font-weight: bold;
}
.Estilo6 {font-weight: bold; color: #FFFFFF; }
.Estilo7 {
	color: #CC99CC;
	font-weight: bold;
}
.Estilo8 {
	font-weight: bold;
	color: #FF9900;
}
.Estilo9 {color: #66FF00}
.Estilo10 {font-weight: bold}
.Estilo13 {font-weight: bold; color: #99FF99; }
.Estilo15 {font-weight: bold; color: #6699FF; }
.Estilo17 {color: #FF00CC; font-weight: bold; }
.Estilo18 {color: #FFFFFF}
-->
</style>
        <table width="700" border="1" align="center" style="border-collapse:collapse">
          <tr>
            <td colspan="2"><div align="center" class="Estilo10">Channel comands </div></td>
          </tr>
          <tr>
            <td colspan="2">&nbsp;</td>
          </tr>
          <tr>
            <td width="114"><span class="Estilo6">Normal Chat </span></td>
            <td width="576">To chat with other players in 
                                                the nearby area, type what you 
                                                wish to say and press the &quot;Enter&quot; 
            key. </td>
          </tr>
          
          <tr>
            <td><span class="Estilo7">Trade Chat </span></td>
            <td>Type the &quot;+&quot; symbol 
                                                before a message to send your 
            message to the trade chat channel. </td>
          </tr>
          
          <tr>
            <td><span class="Estilo9"><strong>Party Chat </strong></span></td>
            <td>Type the &quot;#&quot; symbol 
                                                (Shift-3) and a message, and the 
                                                message will be displayed to all 
            members of your current party. </td>
          </tr>
          
          <tr>
            <td><span class="Estilo4">Clan Chat </span></td>
            <td>Type the &quot;@&quot; symbol 
                                                (Shift-2) and a message, and the 
                                                message will be displayed to other 
                                                members of your clan, no matter 
            where they are in the game world.</td>
          </tr>
          
          <tr>
            <td><span class="Estilo13">Alliance Chat </span></td>
            <td>Type the &quot;$&quot; symbol 
                                                (Shift-4) and a message, and the 
                                                message will be displayed to other 
                                                members of your alliance, no matter 
            where they are in the game world. </td>
          </tr>
          
          <tr>
            <td><span class="Estilo15">Hero </span></td>
            <td>Type
the &quot;%&quot; symbol (Shift-5) followed by a message to broadcast the message
to all players on the server. This channel can only be used by Heroes once every <strong>10</strong> seconds.</td>
          </tr>
          
          <tr>
            <td><span class="Estilo8">Shout </span></td>
            <td>Type the &quot;!&quot; symbol 
                                                (Shift-1) followed by a message 
                                                to shout the message to nearby 
            players.</td>
          </tr>
          
          <tr>
            <td><span class="Estilo17">Wishper </span></td>
            <td>Type a quotation mark (&quot;) 
                                                before the character name, then 
                                                enter your message to send a private 
                                                message to another player in the 
            game. </td>
          </tr>
          <tr>
            <td colspan="2">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="2" class="Estilo10"><div align="center">Key Commands </div></td>
          </tr>
        </table>     
		<table width="700" border="1" align="center" style="border-collapse:collapse">
		<tr>
		  <td colspan="2" bordercolor="1">&nbsp;</td>
		  </tr>
		<tr>
		
		<td width="12%" bordercolor="1">Alt+T</td>
    <td width="88%">Show Character's Info.</td>
  </tr>
  <tr>
    <td>Alt+C</td>
    <td>Show Player's Action.</td>
  </tr>
  <tr>
    <td>Alt+K</td>
    <td>Show Player's Skills.</td>
  </tr>
  <tr>
    <td>Alt+U</td>
    <td>Show Quest's Info.</td>
  </tr>
  <tr>
    <td>Alt+V</td>
    <td>Show Player's Inventory.</td>
  </tr>
  <tr>
    <td>Alt+B</td>
    <td>Show Bulletin Board.</td>
  </tr>
  <tr>
    <td>Alt+R</td>
    <td>Show The Macro Llist. </td>
  </tr>
  <tr>
    <td>Alt+X</td>
    <td>Show System Options.</td>
  </tr>
  <tr>
    <td>Alt+J</td>
    <td>Close chat window. Open only with ALT+H command.</td>
  </tr>
  <tr>
    <td>Alt+W</td>
    <td>Close Radar. Open only with ALT+H command.</td>
  </tr>
  <tr>
    <td>Alt+H</td>
    <td>Open/Close all windows on screen. Helpful for screenshots.</td>
  </tr>
  <tr>
    <td>ESC</td>
    <td>Cancels
any action of your character, targeting something. If no target is
selected, no character's action will be cancelled. You also can cancel
the usage of Scroll of Escape by selecting your character and pressing
ESC.</td>
  </tr>
  <tr>
    <td>PrtScn</td>
    <td>Taking a screenshot.</td>
  </tr>
  <tr>
    <td>Tab</td>
    <td>Same as ALT+V. Show Player's Inventory.</td>
  </tr>
  <tr>
    <td>Ctrl</td>
    <td>While pressing PvP starts. Any action is carried out forcibly.</td>
  </tr>
  <tr>
    <td>Alt+Ctrl</td>
    <td>Stat/End PvP. Any action is carried out forcibly.</td>
  </tr>
  <tr>
    <td>ALT+[F1-F10]</td>
    <td>Switch's between fact access panels. Only works if Enter Chat mark is disable.</td>
  </tr>
  <tr>
    <td>[F1-F12]</td>
    <td>Uses fast access slot of current fact access panel. Only works if Enter Chat mark is disable.</td>
  </tr>
  <tr>
    <td>ALT+[1-10]</td>
    <td>Switch's between fact access panels. Only works if Enter Chat mark is enable.</td>
  </tr>
  <tr>
    <td>[1-10,-,+]</td>
    <td>Uses fast access slot of current fact access panel. Only works if Enter Chat mark is enable.</td>
  </tr>
  <tr>
    <td>Arrows</td>
    <td>Character control - movement forward-backward and turning left-right. Strong drifting on turning.</td>
  </tr>
  <tr>
    <td>W S A D</td>
    <td>Character
control - movement forward-backward and turning left-right. Strong
drifting on turning. Only works if Enter Chat mark is enable.</td>
  </tr>
  <tr>
    <td>Home</td>
    <td>Put camera in front of you character.</td>
  </tr>
  <tr>
    <td>End</td>
    <td>Put camera behind your character.</td>
  </tr>
  <tr>
    <td>Page Up / Page Down</td>
    <td>Switching camera: first person, average and long distance from character.</td>
	</tr>
		</table>   
        <table width="700" border="1" align="center" style="border-collapse:collapse">
          <tr>
            <td width="684">&nbsp;</td>
          </tr>
          <tr>
            <td class="Estilo10"><div align="center">Player Actions </div></td>
          </tr>
          <tr>
            <td height="30" class="Estilo10"><div align="center"></div></td>
          </tr>
        </table>
		<table align="center" border="1" cellpadding="5" cellspacing="1" width="700" style="border-collapse:collapse">
                                <tbody>
                                  
                                  <tr class="np02b" align="center"  style="border-collapse:collapse"> 
                                  <td width="138" bordercolor="1"><strong><span class="Estilo18">Command</span></strong></td>
                                  <td width="108" bordercolor="1"><strong><span class="Estilo18">Parameter</span></strong></td>
                                  <td width="250" align="center" bordercolor="1"><strong><span class="Estilo18">Description</span></strong></td>
                                </tr>
                                <tr > 
                                  <td colspan="3" bordercolor="1"><div align="center"><strong><span class="Estilo18"><strong>Chat</strong></span></strong></div></td>
                                </tr>
                                <tr align="center" > 
                                  <td bordercolor="1"><strong><span class="Estilo18"><strong>!</strong></span></strong></td>
                                  <td align="center" bordercolor="1" class="Estilo18">[message]</td>
                                  <td align="center" bordercolor="1"><strong><span style="color: #FFFFFF;">Shout</span></strong></td>
                                </tr>
                                <tr align="center" > 
                                  <td bordercolor="1"><strong><span class="Estilo18"><strong>+</strong></span></strong></td>
                                  <td align="center" bordercolor="1" class="Estilo18">[message]</td>
                                  <td align="center" bordercolor="1"><strong><span class="style3 Estilo18">Trade                                    </span></strong></td>
                                </tr>
                                <tr align="center" > 
                                  <td bordercolor="1"><strong><span class="Estilo18"><strong>#</strong></span></strong></td>
                                  <td align="center" bordercolor="1" class="Estilo18">[message]</td>
                                  <td align="center" bordercolor="1"><strong><span class="style6 Estilo18">Party                                    </span></strong></td>
                                </tr>
                                <tr align="center" > 
                                  <td bordercolor="1"><strong><span class="Estilo18"><strong>@</strong></span></strong></td>
                                  <td align="center" bordercolor="1" class="Estilo18">[message]</td>
                                  <td align="center" bordercolor="1"><strong><span class="style2 Estilo18">Clan 
                                    </span></strong></td>
                                </tr>
                                <tr align="center" > 
                                  <td bordercolor="1"><strong><span class="Estilo18"><strong>$</strong></span></strong></td>
                                  <td align="center" bordercolor="1" class="Estilo18">[message]</td>
                                  <td align="center" bordercolor="1"><strong><span class="style4 Estilo18">Alliance                                    </span></strong></td>
                                </tr>
                                <tr align="center" >
                                  <td bordercolor="1" class="Estilo18">%</td>
                                  <td align="center" bordercolor="1" class="Estilo18">[message]</td>
                                  <td align="center" bordercolor="1" class="Estilo18">Hero Voice </td>
                                </tr>
                                <tr align="center" > 
                                  <td bordercolor="1" class="Estilo18"><strong>"</strong>[name]</td>
                                  <td align="center" bordercolor="1" class="Estilo18">[message]</td>
                                  <td align="center" bordercolor="1"><strong><span class="style5 Estilo18">Whisper</span></strong></td>
                                </tr>
                                <tr align="center" >
                                  <td colspan="3" bordercolor="1" class="Estilo1"><div align="left" class="Estilo18">
                                    <div align="center"><strong>Command Channel </strong></div>
                                  </div></td>
                                </tr>
                                <tr align="center" >
                                  <td bordercolor="1" class="Estilo1"><div align="center" class="Estilo18"><strong>/channelinvite</strong></div></td>
                                  <td align="center" bordercolor="1" class="Estilo18">[name]</td>
                                  <td align="center" bordercolor="1" class="Estilo18"> Invite parties to command channel</td>
                                </tr>
                                <tr align="center" >
                                  <td bordercolor="1"><strong><span class="Estilo18"><strong>/channelkick</strong></span></strong></td>
                                  <td align="center" bordercolor="1" class="Estilo18">[name]</td>
                                  <td align="center" bordercolor="1" class="Estilo18"> Expel a party from command channel</td>
                                </tr>
                                <tr align="center" >
                                  <td bordercolor="1"><strong><span class="Estilo18"><strong>/channeldelete</strong></span></strong></td>
                                  <td align="center" bordercolor="1" class="Estilo18">&nbsp;</td>
                                  <td align="center" bordercolor="1" class="Estilo18"> Disband the command channel </td>
                                </tr>
                                <tr align="center" >
                                  <td bordercolor="1"><strong><span class="Estilo18"><strong>/channelleave</strong></span></strong></td>
                                  <td align="center" bordercolor="1" class="Estilo18">&nbsp;</td>
                                  <td align="center" bordercolor="1" class="Estilo18"> Withdraw from command channel </td>
                                </tr>
                                <tr > 
                                  <td colspan="3" bordercolor="1"><div align="center"><strong><span class="Estilo18"><strong>Friend 
                                    and Ignore</strong></span></strong></div></td>
                                </tr>
                                <tr align="center" > 
                                  <td bordercolor="1"><strong><span class="Estilo18"><strong>/friendlist</strong></span></strong></td>
                                  <td align="center" bordercolor="1" class="Estilo18"></td>
                                  <td align="center" bordercolor="1"><strong><span class="Estilo18">Display 
                                    the friend list</span></strong></td>
                                </tr>
                                <tr align="center" > 
                                  <td bordercolor="1"><strong><span class="Estilo18"><strong>/friendinvite</strong></span></strong></td>
                                  <td align="center" bordercolor="1" class="Estilo18">[name]</td>
                                  <td align="center" bordercolor="1" class="Estilo18">Add a player 
                                    to the friend list</td>
                                </tr>
                                <tr align="center" > 
                                  <td bordercolor="1"><strong><span class="Estilo18"><strong>/frienddel</strong></span></strong></td>
                                  <td align="center" bordercolor="1" class="Estilo18">[name]</td>
                                  <td align="center" bordercolor="1" class="Estilo18">Remove a player 
                                    from friend list</td>
                                </tr>
                                <tr align="center" > 
                                  <td bordercolor="1"><strong><span class="Estilo18"><strong>/blocklist</strong></span></strong></td>
                                  <td align="center" bordercolor="1" class="Estilo18"></td>
                                  <td align="center" bordercolor="1" class="Estilo18">List of blocked 
                                    players</td>
                                </tr>
                                <tr align="center" > 
                                  <td bordercolor="1"><strong><span class="Estilo18"><strong>/block</strong></span></strong></td>
                                  <td align="center" bordercolor="1" class="Estilo18">[name]</td>
                                  <td align="center" bordercolor="1" class="Estilo18">Add a player 
                                    to the block list (ignore and stop trade)</td>
                                </tr>
                                <tr align="center" > 
                                  <td bordercolor="1"><strong><span class="Estilo18"><strong>/unblock</strong></span></strong></td>
                                  <td align="center" bordercolor="1" class="Estilo18">[name]</td>
                                  <td align="center" bordercolor="1" class="Estilo18">Remove a player 
                                    from the block list</td>
                                </tr>
                                <tr align="center" > 
                                  <td bordercolor="1"><strong><span class="Estilo18"><strong>/allblock</strong></span></strong></td>
                                  <td align="center" bordercolor="1" class="Estilo18">&nbsp;</td>
                                  <td align="center" bordercolor="1" class="Estilo18">Block whispers from all players </td>
                                </tr>
                                <tr align="center" > 
                                  <td bordercolor="1"><strong><span class="Estilo18"><strong>/allunblock</strong></span></strong></td>
                                  <td align="center" bordercolor="1" class="Estilo18">&nbsp;</td>
                                  <td align="center" bordercolor="1" class="Estilo18">Remove the whisper 
                                    block</td>
                                </tr>
                                <tr align="center" > 
                                  <td bordercolor="1"><strong><span class="Estilo18"><strong>/evaluate</strong></span></strong></td>
                                  <td align="center" bordercolor="1" class="Estilo18">&nbsp;</td>
                                  <td align="center" bordercolor="1" class="Estilo18">Recommend another 
                                    player </td>
                                </tr>
                                <tr > 
                                  <td colspan="3" bordercolor="1"><div align="center"><strong><span class="Estilo18"><strong>GM and Petition</strong></span></strong></div></td>
                                </tr>
                                <tr align="center" > 
                                  <td bordercolor="1"><strong><span class="Estilo18"><strong>/gm</strong></span></strong></td>
                                  <td align="center" bordercolor="1" class="Estilo18">[message]</td>
                                  <td align="center" bordercolor="1" class="Estilo18">Initiate a petition 
                                    to the GM staff</td>
                                </tr>
                                <tr align="center" > 
                                  <td bordercolor="1"><strong><span class="Estilo18"><strong>/gmcancel</strong></span></strong></td>
                                  <td align="center" bordercolor="1" class="Estilo18"></td>
                                  <td align="center" bordercolor="1" class="Estilo18">Cancel a petition 
                                    that has already been sent</td>
                                </tr>
                                <tr align="center" > 
                                  <td bordercolor="1"><strong><span class="Estilo18"><strong>/gmlist</strong></span></strong></td>
                                  <td align="center" bordercolor="1" class="Estilo18"></td>
                                  <td align="center" bordercolor="1" class="Estilo18">View a list of  visible 
                                    GMs</td>
                                </tr>
                                <tr align="center" > 
                                  <td bordercolor="1"><strong><span class="Estilo18"><strong>/unstuck</strong></span></strong></td>
                                  <td align="center" bordercolor="1" class="Estilo18"></td>
                                  <td align="center" bordercolor="1" class="Estilo18">Move a character 
                                    that may be trapped</td>
                                </tr>
                                <tr > 
                                  <td colspan="3" bordercolor="1"><div align="center"><strong><span class="Estilo18"><strong>Movement, Time, and Recording Gameplay </strong></span></strong></div></td>
                                </tr>
                                <tr align="center" > 
                                  <td bordercolor="1"><strong><span class="Estilo18"><strong>/loc</strong></span></strong></td>
                                  <td align="center" bordercolor="1" class="Estilo18"></td>
                                  <td align="center" bordercolor="1" class="Estilo18">Display current 
                                    location in x, y, z</td>
                                </tr>
                                <tr align="center" >
                                  <td bordercolor="1"><strong><span class="Estilo18"><strong>/time</strong></span></strong></td>
                                  <td align="center" bordercolor="1" class="Estilo18"></td>
                                  <td align="center" bordercolor="1" class="Estilo18">Display current 
                                    in-game time</td>
                                </tr>
                                <tr align="center" > 
                                  <td bordercolor="1"><strong><span class="Estilo18"><strong>/sit</strong></span></strong></td>
                                  <td align="center" bordercolor="1" class="Estilo18"></td>
                                  <td align="center" bordercolor="1" class="Estilo18">Sit down</td>
                                </tr>
                                <tr align="center" > 
                                  <td bordercolor="1"><strong><span class="Estilo18"><strong>/stand</strong></span></strong></td>
                                  <td align="center" bordercolor="1" class="Estilo18"></td>
                                  <td align="center" bordercolor="1" class="Estilo18">Stand up</td>
                                </tr>
                                <tr align="center" > 
                                  <td bordercolor="1"><strong><span class="Estilo18"><strong>/walk</strong></span></strong></td>
                                  <td align="center" bordercolor="1" class="Estilo18"></td>
                                  <td align="center" bordercolor="1" class="Estilo18">Toggle walk 
                                    on/off</td>
                                </tr>
                                <tr align="center" > 
                                  <td bordercolor="1"><strong><span class="Estilo18"><strong>/run</strong></span></strong></td>
                                  <td align="center" bordercolor="1" class="Estilo18"></td>
                                  <td align="center" bordercolor="1" class="Estilo18">Toggle run on/off</td>
                                </tr>
                                <tr align="center" > 
                                  <td bordercolor="1"><strong><span class="Estilo18"><strong>/mountdismount</strong></span></strong></td>
                                  <td align="center" bordercolor="1" class="Estilo18"></td>
                                  <td align="center" bordercolor="1" class="Estilo18">Mount or dismount 
                                    a pet </td>
                                </tr>
                                <tr align="center" >
                                  <td bordercolor="1"><strong><span class="Estilo18"><strong>/start_videorecording</strong></span></strong></td>
                                  <td align="center" bordercolor="1" class="Estilo18"></td>
                                  <td align="center" bordercolor="1" class="Estilo18">Begin recording gameplay</td>
                                </tr>
                                <tr align="center" >
                                  <td bordercolor="1"><strong><span class="Estilo18"><strong>/end_videorecording</strong></span></strong></td>
                                  <td align="center" bordercolor="1" class="Estilo18"></td>
                                  <td align="center" bordercolor="1" class="Estilo18">Stop recording gameplay </td>
                                </tr>
                                <tr > 
                                  <td colspan="3" bordercolor="1"><div align="center"><strong><span class="Estilo18"><strong>Combat</strong></span></strong></div></td>
                                </tr>
                                <tr align="center" > 
                                  <td bordercolor="1"><strong><span class="Estilo18"><strong>/attack</strong></span></strong></td>
                                  <td align="center" bordercolor="1" class="Estilo18">[target]</td>
                                  <td align="center" bordercolor="1" class="Estilo18">Attack the target</td>
                                </tr>
                                <tr align="center" > 
                                  <td bordercolor="1"><strong><span class="Estilo18"><strong>/attackforce</strong></span></strong></td>
                                  <td align="center" bordercolor="1" class="Estilo18">[target]</td>
                                  <td align="center" bordercolor="1" class="Estilo18">Force attack 
                                    a target</td>
                                </tr>
                                <tr align="center" > 
                                  <td bordercolor="1"><strong><span class="Estilo18"><strong>/attackstand</strong></span></strong></td>
                                  <td align="center" bordercolor="1" class="Estilo18">[target]</td>
                                  <td align="center" bordercolor="1" class="Estilo18">Attack while 
                                    in a fixed position</td>
                                </tr>
                                <!--
	<tr  align="center">
		<td class="np02">/useskill</td>
		<td align="center" class="np02">[skill name]</td>
		<td align="center" class="np02">Use an active skill/spell</td>
	</tr>
	<tr  align="center">
		<td class="np02">/useskillforce</td>
		<td align="center" class="np02">[skill name]</td>
		<td align="center" class="np02">Force use an active skill/spell</td>
	</tr>
	<tr  align="center">
		<td class="np02">/useskillstand</td>
		<td align="center" class="np02">[skill name]</td>
		<td align="center" class="np02">Use skill/spell while in a fixated position</td>
	</tr>
	-->
                                <tr align="center" > 
                                  <td bordercolor="1"><strong><span class="Estilo18"><strong>/target</strong></span></strong></td>
                                  <td align="center" bordercolor="1" class="Estilo18">[target]</td>
                                  <td align="center" bordercolor="1" class="Estilo18">Select target</td>
                                </tr>
                                <tr align="center" > 
                                  <td bordercolor="1"><strong><span class="Estilo18"><strong>/targetnext</strong></span></strong></td>
                                  <td align="center" bordercolor="1" class="Estilo18"></td>
                                  <td align="center" bordercolor="1" class="Estilo18">Select next 
                                    attackable target</td>
                                </tr>
                                <tr align="center" > 
                                  <td bordercolor="1"><strong><span class="Estilo18"><strong>/assist</strong></span></strong></td>
                                  <td align="center" bordercolor="1" class="Estilo18">[target]</td>
                                  <td align="center" bordercolor="1" class="Estilo18">Choose the selected 
                                    target</td>
                                </tr>
                                <tr align="center" >
                                  <td bordercolor="1"><strong><span class="Estilo18"><strong>/olympiadstat</strong></span></strong></td>
                                  <td align="center" bordercolor="1" class="Estilo18">&nbsp;</td>
                                  <td align="center" bordercolor="1" class="Estilo18">Displays the wins, defeats, and Olympiad points for the current period </td>
                                </tr>
                                <tr > 
                                  <td colspan="3" bordercolor="1"><div align="center"><strong><span class="Estilo18"><strong>Party</strong></span></strong></div></td>
                                </tr>
                                <tr align="center" > 
                                  <td bordercolor="1"><strong><span class="Estilo18"><strong>/invite</strong></span></strong></td>
                                  <td align="center" bordercolor="1" class="Estilo18">[name]</td>
                                  <td align="center" bordercolor="1" class="Estilo18">Invite to party</td>
                                </tr>
                                <tr align="center" > 
                                  <td bordercolor="1"><strong><span class="Estilo18"><strong>/leave</strong></span></strong></td>
                                  <td align="center" bordercolor="1" class="Estilo18"></td>
                                  <td align="center" bordercolor="1" class="Estilo18">Leave current 
                                    party</td>
                                </tr>
                                <tr align="center" > 
                                  <td bordercolor="1"><strong><span class="Estilo18"><strong>/dismiss</strong></span></strong></td>
                                  <td align="center" bordercolor="1" class="Estilo18">[name]</td>
                                  <td align="center" bordercolor="1" class="Estilo18">Dismiss from 
                                    party</td>
                                </tr>
                                <tr align="center" > 
                                  <td bordercolor="1"><strong><span class="Estilo18"><strong>/partymatching</strong></span></strong></td>
                                  <td align="center" bordercolor="1" class="Estilo18"></td>
                                  <td align="center" bordercolor="1" class="Estilo18">Look for a party</td>
                                </tr>
                                <tr align="center" > 
                                  <td bordercolor="1"><strong><span class="Estilo18"><strong>/partyinfo</strong></span></strong></td>
                                  <td align="center" bordercolor="1" class="Estilo18"></td>
                                  <td align="center" bordercolor="1" class="Estilo18">Display information 
                                    about the party</td>
                                </tr>
                                <tr align="center" > 
                                  <td height="24" bordercolor="1"><strong><span class="Estilo18"><strong>/changepartyleader</strong></span></strong></td>
                                  <td align="center" bordercolor="1" class="Estilo18"></td>
                                  <td align="center" bordercolor="1" class="Estilo18">Give authority 
                                    to a party member (party leader only)</td>
                                </tr>
                                <tr > 
                                  <td colspan="3" bordercolor="1"><div align="center"><strong><span class="Estilo18"><strong>Exchange</strong></span></strong></div></td>
                                </tr>
                                <tr align="center" > 
                                  <td bordercolor="1"><strong><span class="Estilo18"><strong>/pickup</strong></span></strong></td>
                                  <td align="center" bordercolor="1" class="Estilo18"></td>
                                  <td align="center" bordercolor="1" class="Estilo18">Pick up nearest 
                                    object</td>
                                </tr>
                                <tr align="center" > 
                                  <td bordercolor="1"><strong><span class="Estilo18"><strong>/trade</strong></span></strong></td>
                                  <td align="center" bordercolor="1" class="Estilo18">[target]</td>
                                  <td align="center" bordercolor="1" class="Estilo18">Trade with the 
                                    player targeted</td>
                                </tr>
                                <tr align="center" > 
                                  <td bordercolor="1"><strong><span class="Estilo18"><strong>/vendor</strong></span></strong></td>
                                  <td align="center" bordercolor="1" class="Estilo18">[item]</td>
                                  <td align="center" bordercolor="1" class="Estilo18">Set up a private 
                                    store for selling</td>
                                </tr>
                                <tr align="center" > 
                                  <td bordercolor="1"><strong><span class="Estilo18"><strong>/buy</strong></span></strong></td>
                                  <td align="center" bordercolor="1" class="Estilo18">[item]</td>
                                  <td align="center" bordercolor="1" class="Estilo18">Set up a private 
                                    store for buying</td>
                                </tr>
                                <tr align="center" > 
                                  <td bordercolor="1"><strong><span class="Estilo18"><strong>/dwarvenmanufacture</strong></span></strong></td>
                                  <td align="center" bordercolor="1" class="Estilo18">&nbsp;</td>
                                  <td align="center" bordercolor="1" class="Estilo18">Set up a private 
                                    Dwarven crafting store</td>
                                </tr>
                                <tr align="center" >
                                  <td bordercolor="1"><strong><span class="Estilo18"><strong>/generalmanufacture</strong></span></strong></td>
                                  <td align="center" bordercolor="1" class="Estilo18">&nbsp;</td>
                                  <td align="center" bordercolor="1" class="Estilo18">Set up a private common crafting store </td>
                                </tr>
                                <tr align="center" > 
                                  <td bordercolor="1"><strong><span class="Estilo18"><strong>/findprivatestore</strong></span></strong></td>
                                  <td align="center" bordercolor="1" class="Estilo18">[text]</td>
                                  <td align="center" bordercolor="1" class="Estilo18">Highlight private 
                                    stores that match search criteria text</td>
                                </tr>
                                <tr align="center" >
                                  <td colspan="3" bordercolor="1" class="Estilo1"><div align="left" class="Estilo18">
                                    <div align="center"><strong>Clans</strong></div>
                                  </div></td>
                                </tr>
                                <tr align="center" >
                                  <td bordercolor="1"><strong><span class="Estilo18"><strong>/nick</strong></span></strong></td>
                                  <td align="center" bordercolor="1" class="Estilo18">&nbsp;</td>
                                  <td align="center" bordercolor="1" class="Estilo18">Set your own title. </td>
                                </tr>
                                <tr align="center" >
                                  <td bordercolor="1"><strong><span class="Estilo18"><strong> /enemylist</strong></span></strong></td>
                                  <td align="center" bordercolor="1" class="Estilo18">&nbsp;</td>
                                  <td align="center" bordercolor="1" class="Estilo18"> Display  list of clans on which your clan has declared war but have not declared in return </td>
                                </tr>
                                <tr align="center" >
                                  <td bordercolor="1"><strong><span class="Estilo18"><strong> /attackerlist</strong></span></strong></td>
                                  <td align="center" bordercolor="1" class="Estilo18">&nbsp;</td>
                                  <td align="center" bordercolor="1" class="Estilo18"> Display list of opposing clans which have declared war on your clan that have not declared in return </td>
                                </tr>
                                <tr align="center" >
                                  <td bordercolor="1"><strong><span class="Estilo18"><strong> /clanwarlist</strong></span></strong></td>
                                  <td align="center" bordercolor="1" class="Estilo18">&nbsp;</td>
                                  <td align="center" bordercolor="1" class="Estilo18"> Display list of opposing clans and friendly forces which have mutually declared war on one another </td>
                                </tr>
                                <tr align="center" >
                                  <td bordercolor="1"><strong><span class="Estilo18"><strong>/clanwarstart</strong></span></strong></td>
                                  <td align="center" bordercolor="1" class="Estilo18">&nbsp;</td>
                                  <td align="center" bordercolor="1" class="Estilo18">Declare war against an enemy clan </td>
                                </tr>
                                <tr align="center" >
                                  <td bordercolor="1"><strong><span class="Estilo18"><strong> /clanwarstop</strong></span></strong></td>
                                  <td align="center" bordercolor="1" class="Estilo18">&nbsp;</td>
                                  <td align="center" bordercolor="1" class="Estilo18">End a clan war </td>
                                </tr>
                                <tr align="center" >
                                  <td bordercolor="1"><strong><span class="Estilo18"><strong>/siegestatus</strong></span></strong></td>
                                  <td align="center" bordercolor="1" class="Estilo18">&nbsp;</td>
                                  <td align="center" bordercolor="1" class="Estilo18"> Locate clan members in a siege (Noblesse clan leaders only) </td>
                                </tr>
                                <tr align="center" >
                                  <td bordercolor="1"><strong><span class="Estilo18"><strong>/clanpenalty</strong></span></strong></td>
                                  <td align="center" bordercolor="1" class="Estilo18">&nbsp;</td>
                                  <td align="center" bordercolor="1" class="Estilo18">View a list of any clan penalties. </td>
                                </tr>
                                <tr > 
                                  <td colspan="3" bordercolor="1"><div align="center"><strong><span class="Estilo18"><strong>Alliances</strong></span></strong></div></td>
                                </tr>
                                <tr align="center" > 
                                  <td bordercolor="1"><strong><span class="Estilo18"><strong>/allyinvite</strong></span></strong></td>
                                  <td align="center" bordercolor="1" class="Estilo18">[target clan 
                                    leader]</td>
                                  <td align="center" bordercolor="1" class="Estilo18">Invite a clan 
                                    into an alliance</td>
                                </tr>
                                <tr align="center" > 
                                  <td bordercolor="1"><strong><span class="Estilo18"><strong>/allydismiss</strong></span></strong></td>
                                  <td align="center" bordercolor="1" class="Estilo18">[clan]</td>
                                  <td align="center" bordercolor="1" class="Estilo18">Dismiss a clan 
                                    from the alliance</td>
                                </tr>
                                <tr align="center" > 
                                  <td bordercolor="1"><strong><span class="Estilo18"><strong>/allyleave</strong></span></strong></td>
                                  <td align="center" bordercolor="1" class="Estilo18"></td>
                                  <td align="center" bordercolor="1" class="Estilo18">Leave an alliance</td>
                                </tr>
                                <tr align="center" > 
                                  <td bordercolor="1"><strong><span class="Estilo18"><strong>/allydissolve</strong></span></strong></td>
                                  <td align="center" bordercolor="1" class="Estilo18"></td>
                                  <td align="center" bordercolor="1" class="Estilo18">Dissolve the 
                                    entire alliance</td>
                                </tr>
                                <tr align="center" > 
                                  <td bordercolor="1"><strong><span class="Estilo18"><strong>/allycrest</strong></span></strong></td>
                                  <td align="center" bordercolor="1" class="Estilo18"></td>
                                  <td align="center" bordercolor="1" class="Estilo18">Add an ally's 
                                    crest</td>
                                </tr>
                                <tr align="center" > 
                                  <td bordercolor="1"><strong><span class="Estilo18"><strong>/allyinfo</strong></span></strong></td>
                                  <td align="center" bordercolor="1" class="Estilo18"></td>
                                  <td align="center" bordercolor="1" class="Estilo18">Display alliance 
                                    status</td>
                                </tr>
                                <tr > 
                                  <td colspan="3" bordercolor="1"><div align="center"><strong><span class="Estilo18"><strong>Social 
                                    Emotions</strong></span></strong></div></td>
                                </tr>
                                <tr > 
                                  <td colspan="3" bordercolor="1"><strong><span class="Estilo18"><strong>/socialno</strong></span></strong></td>
                                </tr>
                                <tr > 
                                  <td colspan="3" bordercolor="1"><strong><span class="Estilo18"><strong>/socialyes</strong></span></strong></td>
                                </tr>
                                <tr > 
                                  <td colspan="3" bordercolor="1"><strong><span class="Estilo18"><strong>/socialbow</strong></span></strong></td>
                                </tr>
                                <tr > 
                                  <td colspan="3" bordercolor="1"><strong><span class="Estilo18"><strong>/socialunaware</strong></span></strong></td>
                                </tr>
								<tr > 
                                  <td colspan="3" bordercolor="1"><strong><span class="Estilo18"><strong>/sociallaugh</strong></span></strong></td>
                                </tr>
                                <tr > 
                                  <td colspan="3" bordercolor="1"><strong><span class="Estilo18"><strong>/socialhello</strong></span></strong></td>
                                </tr>
                                <tr > 
                                  <td colspan="3" bordercolor="1"><strong><span class="Estilo18"><strong>/socialvictory</strong></span></strong></td>
                                </tr>
                                <tr > 
                                  <td colspan="3" bordercolor="1"><strong><span class="Estilo18"><strong>/socialcharge</strong></span></strong></td>
                                </tr>
                                <tr > 
                                  <td colspan="3" bordercolor="1"><strong><span class="Estilo18"><strong>/socialdance</strong></span></strong></td>
                                </tr>
                                <tr > 
                                  <td colspan="3" bordercolor="1"><strong><span class="Estilo18"><strong>/socialsad</strong></span></strong></td>
                                </tr>
                                <tr > 
                                  <td colspan="3" bordercolor="1"><strong><span class="Estilo18"><strong>/socialapplause</strong></span></strong></td>
                                </tr>
        </tbody></table>
        <? }else{
		include "error.php";
		}
		?>