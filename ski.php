<table align="center" border="0" cellpadding="0" cellspacing="1" width="100%">
  <tbody>
    <tr>
      <td><strong>Skills</strong></td>
    </tr>
    <tr>
      <td colspan="3"><ul>
        <li>The  fishing skills Pumping and Reeling determine fishing success. These  skills can be learned by paying adena to the Fisherman&rsquo;s Guild Members.  Use the Reeling skill while the fish struggles and resists, and the  Pumping skill while the fish is exhausted.</li>
        <li>Certain  Area-of-Effect spells and skills may be used in town for display  purposes only. Targets suffer no damage and the user will not flag.</li>
        <li>Using  a regular bow attack will consume some MP. The amount will vary  depending on the bow. However, using a special bow skill, such as Power  Shot, will consume MP without using an arrow.</li>
        <li>Neither Spoil nor Sweep will help you get items from players you kill in PvP.</li>
        <li>Using  certain stackable skills may cost additional MP. For example, using two  60 MP blade dances at the same time will cost slightly more than 120 MP.</li>
        <li>Although Fake Death can sometimes deter monsters, players can still hit you.</li>
        <li>Creatures you have summoned with crystals will disappear when you log out.</li>
        <li>You must start from a standing position to use the Relax skill.</li>
      </ul></td>
    </tr>
  </tbody>
</table>
