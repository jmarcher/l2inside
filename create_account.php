<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Register on l2inside</title>
<style type="text/css">
<!--
body {
	background-color: #37301D;
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:11px;
	color: #FFFFEA;
}
a {
	color: #FFFFFF;
	font-size:12px;
}
div{
	font-size:12px;
}
strong{
	font-size:12px;
}
b{
	font-size:12px;
}

span{
	font-size:12px;
}
-->
</style>
<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<script src="SpryAssets/SpryValidationPassword.js" type="text/javascript"></script>
<script src="SpryAssets/SpryValidationConfirm.js" type="text/javascript"></script>
<link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
<link href="SpryAssets/SpryValidationPassword.css" rel="stylesheet" type="text/css" />
<link href="SpryAssets/SpryValidationConfirm.css" rel="stylesheet" type="text/css" />
</head>

<body>
<?php

//Función de seguridad, contra injeccion SQL              
function sql_quote($valor) {
	if(get_magic_quotes_gpc()) {
		return $valor;
	}
	if(function_exists("mysql_real_escape_string")) {
		$valor = mysql_real_escape_string($valor);
	}else{
		$valor = addslashes($valor);
	}
	return $valor;
}

//Función que realiza las queries a la base de datos
function do_sql($sql){  
	global $host,$usuario,$pass,$basededatos;        
	$conection= mysql_connect("localhost","root","uphijmw8c9");
	mysql_select_db ("server",$conection);    
	$res = mysql_query (sql_quote($sql)) or die ("<strong>Error: </strong> ".mysql_error()."");
	return $res;
	mysql_close($conection);
}
function l2j_encrypt ($pass) {return base64_encode(pack("H*", sha1(utf8_encode($pass))));}
if($_POST){
		if($_GET['do']=="true"){
			if(isset($_POST['username'])){
				if(isset($_POST['password'])){
					if(isset($_POST['repass'])){
						if(isset($_POST['email'])){
							if($_POST['password']==$_POST['repass']){
								if(isset($_POST['question'])){
									if(isset($_POST['answer'])){
										$res_exists=do_sql("select `accessLevel` from `accounts` where login='".$_POST['username']."';");
										if(mysql_num_rows($res_exists)==0){
											$res_exists=do_sql("select `accessLevel` from `accounts` where email='".$_POST['email']."';");
											if(mysql_num_rows($res_exists)==0){
												if(do_sql("INSERT INTO `accounts` (`login`, `password`, `lastactive`, `accessLevel`, `lastIP`, `lastServer`, `email`, `pregunta`, `respuesta`) VALUES ('".$_POST['username']."', '".l2j_encrypt($_POST['password'])."', 0, 0, '0', 0, '".$_POST['email']."', '".$_POST['question']."', '".$_POST['answer']."');
")){
													echo "<strong>Operation done</strong>";
													$correct=true;
												}
											}else{
												echo "Another username is registered with this email.";
											}
										}else{
											echo "Username is used by another person.";
										}
									}
								}
							}
						}
					}
				}
			}
			if(!$correct){
				echo "<br />
There was an error during the registration.";
			}
		}
}
?>
<form action="?do=true" method="post" enctype="multipart/form-data" name="register" id="register">
  <span id="sprytextfield1">
  <label>Username:
    <input type="text" name="username" id="username" />
  </label>
  <span class="textfieldRequiredMsg">You have to fill username field to continue.</span></span>
  <p><span id="sprypassword1">
  <label>Password:
    <input type="password" name="password" id="password" />
  </label>
  <span class="passwordRequiredMsg">You have to write a password to continue.</span><span class="passwordMinCharsMsg">You have to write 3 characters atleast.</span><span class="passwordMaxCharsMsg">You can write more than 16 characters.</span></span></p>
  <p><span id="spryconfirm1">
    <label>Retype password:
      <input type="password" name="repass" id="repass" />
    </label>
  <span class="confirmRequiredMsg">You have to retype the password.</span><span class="confirmInvalidMsg">The passwords did not<br />
  <br />
match.</span></span></p>
  <p><span id="sprytextfield2">
  <label>E-Mail (should be real):
    <input type="text" name="email" id="email" />
  </label>
  <span class="textfieldRequiredMsg">You have to fill the mail field.</span><span class="textfieldInvalidFormatMsg">Invalid format.</span></span></p>
  <p><span id="sprytextfield3">
    <label>Secret question:
      <input type="text" name="question" id="question" />
    </label>
  <span class="textfieldRequiredMsg">You must fill this field.</span></span></p>
  <p><span id="sprytextfield4">
    <label>Secret answer:
      <input type="text" name="answer" id="answer" />
    </label>
  <span class="textfieldRequiredMsg">You must fill this field.</span></span></p>
  <p>
    <label>
      <input type="submit" name="button" id="button" value="Register" />
    </label>
  </p>
</form>
<script type="text/javascript">
<!--
var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1", "none", {validateOn:["change"]});
var sprypassword1 = new Spry.Widget.ValidationPassword("sprypassword1", {validateOn:["change"], minChars:3, maxChars:16});
var spryconfirm1 = new Spry.Widget.ValidationConfirm("spryconfirm1", "password", {validateOn:["change"]});
var sprytextfield2 = new Spry.Widget.ValidationTextField("sprytextfield2", "email", {validateOn:["change"]});
var sprytextfield3 = new Spry.Widget.ValidationTextField("sprytextfield3");
var sprytextfield4 = new Spry.Widget.ValidationTextField("sprytextfield4");
//-->
</script>
</body>
</html>