<?php
/***************************************************************************
 *                                L2 Mobfinder
 *                            --------------------
 *   begin                : Wednesdey, Jun 20, 2006
 *   copyright            : (C) 2006 Anubis <anubis@gods.de>
 *
 ***************************************************************************/

### HACKKK: f�r den IE (template: l2)
$css_file = ( ereg( 'MSIE ([0-9].[0-9]{1,2})', $_SERVER['HTTP_USER_AGENT'] ) ) ? 'layout_IE.css' : 'layout.css';

$nav_array = array();
foreach( $pages as $k => $v)
{
	$nav_array[] = '<a href="'. $v['LINKURL'] .'" title="'. $v['LINKNAME'] .'">'. $v['LINKNAME'] .'</a>';
}
if( strlen(MAINPAGE_NAME) > 0 && strlen(MAINPAGE_URL) > 7 )
{ $HTML_NAVIGATION = '<a href="'. MAINPAGE_URL .'" title="'. MAINPAGE_NAME .'">'. MAINPAGE_NAME .'</a>&nbsp;&gt;&gt;&nbsp;'; }
else
{ $HTML_NAVIGATION = ''; }

$HTML_NAVIGATION .= implode( '&nbsp;|&nbsp;', $nav_array );

$HTML_NAVIGATION .= '&nbsp;|&nbsp;<a href="index.php?page='.$pid.'&amp;lang=de" title="Deutsch"><img src="images/flags/de.gif" alt="" /></a>';
$HTML_NAVIGATION .= '&nbsp;<a href="index.php?page='.$pid.'&amp;lang=en" title="English"><img src="images/flags/gb.gif" alt="" /></a>';

switch( $pid )
{
	case PAGE_NPCMOBS:		$background_img = ($template_name != 'l2') ? 'background2.jpg' : 'background4.jpg'; break;
	case PAGE_ITEMS:		$background_img = ($template_name != 'l2') ? 'background1.jpg' : 'background3.jpg'; break;
	case PAGE_ARMORS:		$background_img = ($template_name != 'l2') ? 'background1.jpg' : 'background2.jpg'; break;
	case PAGE_WEAPONS:		$background_img = ($template_name != 'l2') ? 'background1.jpg' : 'background4.jpg'; break;
	case PAGE_SHOPS:		$background_img = ($template_name != 'l2') ? 'background2.jpg' : 'background7.jpg'; break;
	case PAGE_RECIPES:		$background_img = ($template_name != 'l2') ? 'background2.jpg' : 'background5.jpg'; break;
	default:				$background_img = ($template_name != 'l2') ? 'background2.jpg' : 'background4.jpg'; break;
}

$FORM_HTML = '<tr><td>';
include_once( 'includes/content_form_search.php' );
$FORM_HTML .= $FORM .'</td></tr>';

// Error-Content START (if available) -------------------------------------------------------
$error_id = intval($_GET['error']);
if( $error_id > 0 )
{
	include_once( 'includes/content_error.php' );
	$ERROR_HTML = $ERROR;
}
// Error-Content END -------------------------------------------------------

$tpl->set_filenames(array(
	'header' => 'page_header.tpl')
);

$tpl->assign_vars(array(
	'LINKNAME'		 => $pages[$pid]['LINKNAME'],
	'ENCODING'		 => $lang['ENCODING'],
	'CSS_FILE'		 => $css_file,
	'SITE_TITLE'	 => SITE_TITLE,
	'BACKGROUND_IMG' => $background_img,
	'NAVIGATION'	 => $HTML_NAVIGATION,
	'ERROR_HTML'	 => $ERROR_HTML,
	'SEARCH_FORM'	 => $FORM_HTML
	)
);

$tpl->pparse('header');
$tpl->destroy();

?>
