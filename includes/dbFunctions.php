<?php
/***************************************************************************
 *                                L2 Mobfinder
 *                            --------------------
 *   begin                : Wednesdey, Jun 20, 2006
 *   copyright            : (C) 2006 Anubis <anubis@gods.de>
 *
 ***************************************************************************/

function prepage_SearchString( $str )
{
	$str = trim( strip_tags( $str ) );
	$str = str_replace( ' ', '%', $str );
	$str_len = strlen( $str );
	$str = addslashes( $str );
	if( $str_len >= SEARCH_MIN_CHAR )
	{ return $str; }
	else
	{ return false; }
}

function drop_check( $npc_id, $field )
{
	global $dbc;
	
	$result = false;
	if( mysql_num_rows( $dbc->query('SELECT * FROM droplist WHERE '.$field.' = '.$npc_id ) ) > 0 )
		$result = true;
	return $result;
}

function GetImage( $item_id )
{
	global $dbc;
	
	$lnk = '';
	$query_result = $dbc->query('SELECT pic FROM '. TABLE_ITEMS .' WHERE id='.$item_id);
	if (mysql_num_rows($query_result)==1)
	{
		$img = mysql_fetch_row($query_result);
		$lnk = '<img class="icon" src="'. IMAGE_PATH_ITEMS . $img[0] .'.png" alt="" />';
	}
	return $lnk;
}

function GetName( $item_id )
{
	global $dbc;
	
	$query_result = $dbc->query('SELECT name FROM '. TABLE_ITEMS .' WHERE id='.$item_id);
	if (mysql_num_rows($query_result)==1)
	{
		$name = mysql_fetch_row($query_result);
		$result = stripslashes($name[0]);
	}
	return $result;
}

function GetRecID( $item_id )
{
	global $dbc;
	
	$sql = 'SELECT id FROM '.TABLE_RECIPES.' WHERE item = '. $item_id .' OR recid = '. $item_id;
	$query_result = $dbc->query( $sql );
	if (mysql_num_rows($query_result)==1)
	{
		$rid = mysql_fetch_assoc($query_result);
		return $rid['id'];
	}
	else
		return false;
}

function GetDescription( $item_id )
{
	global $dbc;
	
	$query_result = $dbc->query('SELECT blabla FROM '. TABLE_ITEMS .' WHERE id='.$item_id);
	if (mysql_num_rows($query_result)==1)
	{
		$desc = mysql_fetch_row($query_result);
		$result = $desc[0];
	}
	return $result;
}

function get_PageStats()
{
	global $dbc;
	
	$sql = 'SELECT count(*) FROM armor';
	$r = $dbc->query( $sql );
	$page_stats['items'] = mysql_result( $r, 0, 0 );
	$sql = 'SELECT count(*) FROM etcitem';
	$r = $dbc->query( $sql );
	$page_stats['items'] += mysql_result( $r, 0, 0 );
	$sql = 'SELECT count(*) FROM weapon';
	$r = $dbc->query( $sql );
	$page_stats['items'] += mysql_result( $r, 0, 0 );
	
	$sql = 'SELECT count(*) FROM npc WHERE (type = \'L2Monster\' 
			OR type = \'L2RaidBoss\' 
			OR type = \'L2Minion\')';
	$r = $dbc->query( $sql );
	$page_stats['monsters'] = mysql_result( $r, 0, 0 );
	
	$sql = 'SELECT count(*) FROM raidboss_spawnlist';
	$r = $dbc->query( $sql );
	$page_stats['raids'] = mysql_result( $r, 0, 0 );
	
	$sql = 'SELECT count(*) FROM npc WHERE (type = \'L2Npc\' 
			OR type = \'L2NPC\' 
			OR type = \'L2Artefact\' 
			OR type = \'L2Guard\' 
			OR type = \'L2Merchant\' 
			OR type = \'L2Teleporter\' 
			OR type = \'L2Trainer\' 
			OR type = \'L2VillageMaster\' 
			OR type = \'L2ClanHallManager\' 
			OR type = \'L2Warehouse\' 
			OR type = \'L2ClassMaster\' 
			OR type = \'L2SiegeGuard\' 
			OR type = \'L2SiegeNpc\')';
	$r = $dbc->query( $sql );
	$page_stats['npcs'] = mysql_result( $r, 0, 0 );
	
	return $page_stats;
}

function update_Stats( $name )
{
	global $dbc;
	
	$sql = 'SELECT count FROM '. TABLE_STATS .' WHERE name = \''. $name .'\'';
	$r = $dbc->query( $sql );
	if( mysql_num_rows( $r ) > 0 )
	{
		$sql = 'UPDATE '. TABLE_STATS .' SET count = count+1 WHERE name = \''. $name .'\'';
		$r = $dbc->query( $sql );
	}
	else
	{
		$sql = 'INSERT INTO '. TABLE_STATS .' SET name = \''. $name .'\', count = 1';
		$r = $dbc->query( $sql );
	}
}

?>
