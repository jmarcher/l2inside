<?php
/***************************************************************************
 *                                L2 Mobfinder
 *                            --------------------
 *   begin                : Wednesdey, Jun 20, 2006
 *   copyright            : (C) 2006 Anubis <anubis@gods.de>
 *
 ***************************************************************************/

// Shop-ID
if( $pid == PAGE_NPCMOBS )
{
	$sql = 'SELECT MAX(level) FROM npc';
	$r = $dbc->query( $sql );
	$max_level = mysql_result( $r, 0, 0 );
	$select_options = '<option value="">lvl</option>';
	for( $i=1; $i<=$max_level; $i++)
		$select_options .= '<option value="'.$i.'">'.$i.'</option>';
	$input_search = '<input class="post" type="text" size="25" maxlength="20" name="search_string" value="'. $_POST['search_string'] .'" onfocus="this.select()" />';
	$input_search .= ' <select name="level" onchange="submit();">'. $select_options .'</select>';
	$input_search .= ' <input type="submit" value="'. $lang['FORM_SEARCH_SUBMIT'] .'" class="catbutton" />';
}
else if( $pid == PAGE_SHOPS )
{
	$sql = 'SELECT DISTINCT merchant_buylists.item_id,'. TABLE_ITEMS .'.name
			FROM merchant_buylists, '. TABLE_ITEMS .', merchant_shopids 
			WHERE merchant_buylists.item_id = '. TABLE_ITEMS .'.id 
			AND merchant_buylists.shop_id = merchant_shopids.shop_id 
			AND merchant_shopids.npc_id != \'gm\' 
			ORDER BY '. TABLE_ITEMS .'.name';
	$result = $dbc->query( $sql );
	
	$select_options = '<option value="">-------------------------- '. $lang['NAVI_SHOPS'] .' --------------------------</option>';
	while( $row = mysql_fetch_assoc( $result ) )
	{
		$selected = ( !empty($_POST['item_id']) && $_POST['item_id'] == $row['item_id'] ) ? ' selected="selected"' : '';
		$select_options .= '<option value="'.$row['item_id'].'" '.$selected.'>'.stripslashes($row['name']).'</option>';
	}
	$input_search = '<select name="item_id" onchange="submit();">'. $select_options .'</select>';
}
else if( $pid == PAGE_RECIPES )
{
	$sql = 'SELECT '.TABLE_RECIPES.'.id, '.TABLE_RECIPES.'.lvl, '.TABLE_RECIPES.'.success, '.TABLE_RECIPES.'.mp, 
			(SELECT name FROM '.TABLE_ITEMS.' WHERE '.TABLE_ITEMS.'.id =  '.TABLE_RECIPES.'.recid) as name, 
			(SELECT grade FROM '.TABLE_ITEMS.' WHERE '.TABLE_ITEMS.'.id =  '.TABLE_RECIPES.'.recid) as grade 
			FROM '.TABLE_RECIPES.' 
			ORDER BY name';
	$r = $dbc->query( $sql );
	$select_options = '<option value="">------------------------------ '. $lang['NAVI_RECIPES'] .' ------------------------------</option>';
	while( $row = mysql_fetch_assoc( $r ) )
	{
		if( strpos( $row['name'], 'Recipe' ) !== false )
		{
			$row['name'] = preg_replace( "/^Recipe(\:|\:\s|\s\:\s|\s-\s)/", '', stripslashes($row['name']) );
			$row['name'] = trim( preg_replace( "/\s?\([\d\%\s]+\)/", '', $row['name'] ) );
			$selected = ( intval($_POST['rec_id']) == $row['id'] || intval($_GET['rid']) == $row['id'] ) ? ' selected="selected"' : '';
			$select_options .= '<option value="'.$row['id'].'" '.$selected.'>'.$row['name'].' ('.$row['grade'].') LVL:'.$row['lvl'].' ['.$row['success'].'%]</option>';
		}
	}
	$input_search = ' <select name="rec_id" onchange="submit();">'. $select_options .'</select>';
}
else
{
	$input_search = '<input class="post" type="text" size="25" maxlength="20" name="search_string" value="'. $_POST['search_string'] .'" onfocus="this.select()" />';
	$input_search .= ' <input type="submit" value="'. $lang['FORM_SEARCH_SUBMIT'] .'" class="catbutton" />';
}

include_once( $current_template_path .'form_search.php' );

?>