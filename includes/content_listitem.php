<?php
/***************************************************************************
 *                                L2 Mobfinder
 *                            --------------------
 *   begin                : Wednesdey, Jun 20, 2006
 *   copyright            : (C) 2006 Anubis <anubis@gods.de>
 *
 ***************************************************************************/

$_GET['item_id'] = intval($_GET['item_id']);
$_GET['rec_id'] = intval($_GET['rec_id']);
$_POST['rec_id'] = intval($_POST['rec_id']);
if( $_POST['rec_id'] > 0 ) $rec_id = $_POST['rec_id'];
else if( $_GET['rid'] > 0 ) $rec_id = $_GET['rid'];

### START FUNCTIONS ##############################################################################################
#

/// Globale Variablen f�r crawl_Recipes()
$_crawl_depth['max'] = 0;
$_crawl_depth['current'] = 0;
$_crawl_result = array();
function crawl_Recipes( $rec_id, $order='', $quantity=0 )
{
	global $dbc, $_crawl_result, $_crawl_depth;
	
	if($order=='price') $order_sql = ' ORDER BY price';
	else $order_sql = ' ORDER BY name';
	
	if( $rec_id > 0 )
	{
		$quantity_sql = ( $quantity > 0 ) ? ' * '.$quantity : '';
		
		$sql = 'SELECT '.TABLE_RECIPES.'.id as rec_id, '.TABLE_ITEMS.'.id as item_id, '.TABLE_ITEMS.'.name, 
					('.TABLE_RECITEMS.'.q '.$quantity_sql.') as quantity, (('.TABLE_ITEMS.'.price * '.TABLE_RECITEMS.'.q) '.$quantity_sql.') as price, 
					(('.TABLE_ITEMS.'.weight * '.TABLE_RECITEMS.'.q) '.$quantity_sql.') as weight 
					FROM '.TABLE_RECIPES.', '.TABLE_RECITEMS.', '.TABLE_ITEMS.' 
					WHERE '.TABLE_RECIPES.'.id = '. $rec_id .' 
					AND '.TABLE_RECIPES.'.id = '.TABLE_RECITEMS.'.rid 
					AND '.TABLE_RECITEMS.'.item = '.TABLE_ITEMS.'.id '
					.$order_sql;
		$r = $dbc->query( $sql );
		
		if( mysql_num_rows( $r ) > 0 )
		{
			while( $item_data = mysql_fetch_assoc( $r ) )
			{
				$item_data['price'] = number_format_lang( $item_data['price'], 0 );
				
				$_crawl_result[] = array( 'depth' => $_crawl_depth['current'], 'data' => $item_data );
				
				$sql_sub = 'SELECT '.TABLE_RECIPES.'.id as rec_id, '.TABLE_ITEMS.'.name 
						FROM '.TABLE_RECIPES.', '.TABLE_ITEMS.' 
						WHERE '.TABLE_RECIPES.'.item = '. $item_data['item_id'] .' 
						AND '.TABLE_RECIPES.'.item = '.TABLE_ITEMS.'.id ';
				$r_sub = $dbc->query( $sql_sub );
				
				$rec_data = array();
				if( mysql_num_rows( $r_sub ) > 0 && $rec_id != $rec_data['rec_id'] )
				{
					$_crawl_depth['current']++;
					if( $_crawl_depth['current'] > $_crawl_depth['max'] )
						$_crawl_depth['max'] = $_crawl_depth['current'];
					$rec_data = mysql_fetch_assoc( $r_sub );
					crawl_Recipes( $rec_data['rec_id'], $order, $item_data['quantity'] );
				}
			}
			if( $_crawl_depth['current'] > 0 )
			{ $_crawl_depth['current']--; }
		}
	}
}

/// Globale Variablen f�r template()
$build_table_head = true;
$table_head = '';
$lnk = '';
$lnk_close = '';
$count = 0;
function template( $params, $depth=0, $max_depth=0 )
{
	global $build_table_head, $table_head, $lang, $count, $pid;
	
	if( $build_table_head ) $table_head = '<tr>';
	
	$result = '<tr '.row_Class( $count ).">\n";
	
	for( $i=1; $i<=$depth; $i++ )
	{
		$result .= '<td>&nbsp;</td>'."\n";
	}
	
	foreach( $params as $k => $value )
	{
		switch ($k)
		{
			case 'item_id':
					build_table_head( '', '' );
					$result .= build_item_prop('img',GetImage($params['item_id']), '');
					break;
			case 'name':
					#$max_depth = ($depth > 0) ? '' : '';
					build_table_head($lang['LIST_HEAD_NAME'], $k, 'left', $max_depth );
					if( drop_check( $params['item_id'], 'itemId' ) ) $lnk = '<a href="index.php?page='.PAGE_NPCMOBS.'&amp;item_id='.$params['item_id'].'"><img border="0" src="images/mob.gif" width="14" height="14" alt="" title="'.$lang['IMG_TITLE_DROPFROM'].'" /></a>&nbsp;';
					$rid = GetRecID( $params['item_id'] );
					if( $rid > 0 ) $lnk2 = '&nbsp;<a href="index.php?page='.PAGE_RECIPES.'&amp;rid='.$rid.'"><img src="images/recipe.gif" alt="" title="'.$lang['IMG_TITLE_RECIPE'].'" /></a>';
					$itm_name = wordwrap( GetName( $params['item_id'] ), 40, '<br />' );
					if( $itm_name=='' ) $itm_name = wordwrap( $params['name'], 40, '<br />' );
					$result .= build_item_prop($k, $lnk.$itm_name.$lnk2, 'left', $depth, $max_depth);
					
					if( $pid != PAGE_RECIPES )
					{
						build_table_head($lang['LIST_HEAD_DESCRIPTION'], $k);
						$result .= build_item_prop('desc', GetDescription($params['item_id']), 'left');
					}
					break;
			case 'weight':
					build_table_head($lang['LIST_HEAD_WEIGHT'], $k);
					$result .= build_item_prop($k, $params[$k]);
					break;
			case 'price':
					build_table_head($lang['LIST_HEAD_PRICE'], $k);
					$result .= build_item_prop($k, $params[$k]);
					break;
			case 'item_type':
					build_table_head($lang['LIST_HEAD_TYPE'], $k);
					$result .= build_item_prop($k, $params[$k]);
					break;
			case 'crystallizable':
					break;
			case 'armor_type':
					break;
			case 'crystal_type':
					build_table_head($lang['LIST_HEAD_CRYSTTYPE'], $k);
					$result .= build_item_prop($k, $params[$k]);
					break;
			case 'p_def':
					build_table_head($lang['LIST_HEAD_PDEF'], $k);
					$result .= build_item_prop($k, $params[$k]);
					break;
			case 'm_def':
					build_table_head($lang['LIST_HEAD_MDEF'], $k);
					$result .= build_item_prop($k, $params[$k]);
					break;
			case 'mp_bonus':
					build_table_head($lang['LIST_HEAD_MP'], $k);
					$result .= build_item_prop($k, $params[$k]);
					break;
			case 'crystal_count':
					build_table_head($lang['LIST_HEAD_CRISTCOUNT'], $k);
					if( $params[$k] == 0 ) $value='';
					else $value = $params[$k];
					$result .= build_item_prop($k, $value);
					break;
			case 'bodypart':
					break;
			case 'soulshots':
					build_table_head($lang['LIST_HEAD_SOULSHOTS'], $k);
					$result .= build_item_prop($k, $params[$k]);
					break;
			case 'spiritshots':
					build_table_head($lang['LIST_HEAD_SPIRITSHOTS'], $k);
					$result .= build_item_prop($k, $params[$k]);
					break;
			case 'p_dam':
					build_table_head($lang['LIST_HEAD_PATK'], $k);
					$result .= build_item_prop($k, $params[$k]);
					break;
			case 'weaponType':
					build_table_head($lang['LIST_HEAD_WEAPONTYPE'], $k);
					$result .= build_item_prop($k, $params[$k]);
					break;
			case 'hit_modify':
					break;
			case 'atk_speed':
					build_table_head($lang['LIST_HEAD_ATKSPEED'], $k);
					$result .= build_item_prop($k, $params[$k]);
					break;
			case 'mp_consume':
					break;
			case 'm_dam':
					build_table_head($lang['LIST_HEAD_MATK'], $k);
					$result .= build_item_prop($k, $params[$k]);
					break;
			case 'quantity':
					build_table_head($lang['LIST_HEAD_QUANTITY']);
					$result .= build_item_prop($k, $params[$k]);
					break;
		}
	}
	if( $build_table_head ) $table_head .= '</tr>';
	else $table_head = '';
	$build_table_head = false;
	
	$count++;
	
	$result .= "</tr>\n";
	return $table_head.$result;
}
#
### END FUNCTIONS ################################################################################################


$ROW_RESULTS = '';

if( $_POST['action'] == 'search' || $_GET['item_id'] > 0 || $rec_id > 0 )
{
	if( !empty($_POST['search_string']) )
		$search_string = prepage_SearchString( $_POST['search_string'] );
	
	if( $search_string !== false || $_GET['item_id'] > 0 || $rec_id > 0 )
	{
			$where_sql = 'name LIKE \'%'.$search_string.'%\'';
		if( $_GET['item_id'] > 0 )
			$where_sql = 'item_id='.$_GET['item_id'];
			
		$allowed_orders = array( 'name', 'price' );
		if( in_array( $_GET['order'], $allowed_orders ) )
			$order_sql = ' ORDER BY '.$_GET['order'];
		else if( in_array( DEFAULT_SORT_ORDER, $allowed_orders ) )
			$order_sql = ' ORDER BY '.DEFAULT_SORT_ORDER;
		else
			$order_sql = ' ORDER BY name';
		
		switch( $pid )
		{
			case PAGE_ITEMS:
					update_Stats( 'search_item' );
					$colspan = 6;
					$query_itm = 'SELECT item_id, name, item_type, weight, price 
								FROM etcitem 
								WHERE ('.$where_sql.') '.$order_sql;
					break;
			
			case PAGE_ARMORS:
					update_Stats( 'search_armor' );
					$colspan = 10;
					$query_itm = 'SELECT item_id, name, p_def, m_def, crystal_type, crystal_count, weight, price, 
								armor_type, mp_bonus,  crystallizable 
								FROM armor 
								WHERE ('.$where_sql.') '.$order_sql;
					break;
			
			case PAGE_WEAPONS:
					update_Stats( 'search_weapon' );
					$colspan = 13;
					$query_itm = 'SELECT item_id, name, crystal_type, crystal_count, p_dam, m_dam, bodypart, 
								soulshots, spiritshots, atk_speed, weight, price, weaponType, hit_modify, 
								mp_consume, bodypart, crystallizable 
								FROM weapon 
								WHERE ('.$where_sql.') '.$order_sql;
					break;
			
			case PAGE_RECIPES:
					update_Stats( 'search_recipe' );
					crawl_Recipes( $rec_id );
					foreach( $_crawl_result as $v )
					{
						$ROW_RESULTS .= template($v['data'], $v['depth'], $_crawl_depth['max']);
					}
					$col_plus = ( $_crawl_depth['max'] > 0 ) ? $_crawl_depth['max']+1 : 1;
					$colspan = 4 + $col_plus;
					break;
		}
		if( $pid != PAGE_RECIPES )
		{
			$query_result = $dbc->query( $query_itm );
			if( mysql_num_rows($query_result) > 0 )
			{
				while( $items = mysql_fetch_assoc($query_result) )
				{ $ROW_RESULTS .= template($items); }
			}
			else
				header( 'Location: '. DOMAIN_NAME .'index.php?page='. $pid .'&error=1' );
		}
	}
	else
		header( 'Location: '. DOMAIN_NAME .'index.php?page='. $pid .'&error=2' );
}


if( !empty($ROW_RESULTS) )
{
	$TABLE_RESULT = '<table class="forumline" width="100%" cellspacing="1" cellpadding="0" border="0">';
	$TABLE_RESULT .= $ROW_RESULTS;
	$TABLE_RESULT .= '<tr><td colspan="'. $colspan .'" class="listing_buttom">&nbsp;'.$count.' '.$lang['LIST_BUTTOM_RESULTS'].'&nbsp;</td></tr>';
	$TABLE_RESULT .= '</table>';
}

$tpl->set_filenames(array(
	'listitem' => 'page_listitem.tpl')
);

$tpl->assign_vars(array(
	'TABLE_RESULT'		 => $TABLE_RESULT
	)
);

$tpl->pparse('listitem');
$tpl->destroy();

?>
