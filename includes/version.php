<?php
/*
Con este archivo, puede verificar,
las nuevas versiones que pueden acontecer a ImgZplat
*/
defined( '_VALID_MOS' ) or die( 'Restricted access' );
class ImgZplatVersion {
	var $PRODUCT 	= 'ImgZplat';
	var $RELEASE 	= '3.0';
	var $DEV_STATUS = 'Estable';
	var $DEV_LEVEL 	= '2';
	var $BUILD	 	= '$Revision: 4811 $';
	var $CODENAME 	= 'Por vb2005';
	var $RELDATE 	= '9 Septiembre 2006';
	var $RELTIME 	= '20:00';
	var $RELTZ 		= 'UTC';
	var $COPYRIGHT 	= "Copyright (C) 2005 - 2006 Portal-Hack.Org. All rights reserved.";
	var $URL 		= '<a href="http://www.portal-hack.org">ImgZplat</a> es Software Libre distribuido bajo licencia GNU/GPL.';
	var $SITE 		= 1;
	var $RESTRICT	= 0;
	var $SVN		= 0;

	
	/**
	 * @el formato largo de la version
	 */
	function getLongVersion() {
		return $this->PRODUCT .' '. $this->RELEASE .'.'. $this->DEV_LEVEL .' '
			. $this->DEV_STATUS
			.' [ '.$this->CODENAME .' ] '. $this->RELDATE .' '
			. $this->RELTIME .' '. $this->RELTZ;
	}

	/**
	 * @regresa el formato de la version
	 */
	function getShortVersion() {
		return $this->RELEASE .'.'. $this->DEV_LEVEL;
	}

	/**
	 * @regresa variables de retorno seguros
	 */
	function getHelpVersion() {
		if ($this->RELEASE > '1.0') {
			return '.' . str_replace( '.', '', $this->RELEASE );
		} else {
			return '';
		}
	}
}
$_VERSION = new ImgZplatVersion();

$version = $_VERSION->PRODUCT .' '. $_VERSION->RELEASE .'.'. $_VERSION->DEV_LEVEL .' '
. $_VERSION->DEV_STATUS
.' [ '.$_VERSION->CODENAME .' ] '. $_VERSION->RELDATE .' '
. $_VERSION->RELTIME .' '. $_VERSION->RELTZ;
?>