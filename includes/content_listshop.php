<?php
/***************************************************************************
 *                                L2 Mobfinder
 *                            --------------------
 *   begin                : Wednesdey, Jun 20, 2006
 *   copyright            : (C) 2006 Anubis <anubis@gods.de>
 *
 ***************************************************************************/

$_POST['item_id'] = intval( $_POST['item_id'] );

### START FUNCTIONS ##############################################################################################
#
function getShopInfo( $item_id )
{
	global $dbc;
	
	$sql = 'SELECT shop_id,price FROM merchant_buylists WHERE item_id = '. $item_id;
	$result = $dbc->query( $sql );
	
	$count = 0;
	$shop_data = array();
	while( $row = mysql_fetch_assoc( $result ) )
	{
		$tmp = array();
		
		$tmp['shop_id'] = $row['shop_id'];
		$tmp['shop_price'] = $row['price'];
		$sql2 = 'SELECT npc_id FROM merchant_shopids WHERE shop_id = '. $tmp['shop_id'];
		$result2 = $dbc->query( $sql2 );
		if( mysql_num_rows( $result2 ) > 0 )
		{
			$tmp['npc_id'] = mysql_result( $result2, 0, 'npc_id' );
			if( $tmp['npc_id'] != 'gm' )
			{
				$sql2 = 'SELECT name,level,sex FROM npc WHERE id = '. $tmp['npc_id'];
				$result2 = $dbc->query( $sql2 );
				if( mysql_num_rows( $result2 ) > 0 )
				{
					$tmp['npc_name'] = mysql_result( $result2, 0, 'name' );
					$tmp['npc_sex'] = mysql_result( $result2, 0, 'sex' );
					$tmp['npc_level'] = mysql_result( $result2, 0, 'level' );
				}
				$sql2 = 'SELECT merchant_area_id FROM merchants WHERE npc_id = 100'. $tmp['npc_id'];
				$result2 = $dbc->query( $sql2 );
				if( mysql_num_rows( $result2 ) > 0 )
				{
					$tmp['area_id'] = mysql_result( $result2, 0, 'merchant_area_id' );
					$sql2 = 'SELECT merchant_area_name FROM merchant_areas_list WHERE merchant_area_id = '. $tmp['area_id'];
					$result2 = $dbc->query( $sql2 );
					if( mysql_num_rows( $result2 ) > 0 )
					{ $tmp['area_name'] = mysql_result( $result2, 0, 'merchant_area_name' ); }
				}
				$shop_data[$count]['shop_id']	 = $tmp['shop_id'];
				$shop_data[$count]['shop_price'] = number_format_lang( $tmp['shop_price'], 2 );
				$shop_data[$count]['npc_id']	 = $tmp['npc_id'];
				$shop_data[$count]['npc_name']	 = stripslashes($tmp['npc_name']);
				$shop_data[$count]['npc_level']	 = $tmp['npc_level'];
				$shop_data[$count]['npc_sex']	 = $tmp['npc_sex'];
				$shop_data[$count]['area_name']	 = $tmp['area_name'];
				$shop_data[$count]['row_class']	 = row_Class( $count );
				$count++;
			}
		}
	}
	return $shop_data;
}
#
### END FUNCTIONS ################################################################################################


if( $_POST['action'] == 'search' && $_POST['item_id'] > 0 )
{
	update_Stats( 'search_shop' );
	$shops = getShopInfo( $_POST['item_id'] );
	$ROW_RESULTS = '<tr>';
	$ROW_RESULTS .= '<th style="text-align:left;">&nbsp;'.$lang['LIST_HEAD_NAME'].'&nbsp;</th>';
	$ROW_RESULTS .= '<th>&nbsp;'.$lang['LIST_HEAD_LEVEL'].'&nbsp;</th>';
	$ROW_RESULTS .= '<th>&nbsp;'.$lang['LIST_HEAD_LOCATION'].'&nbsp;</th>';
	$ROW_RESULTS .= '<th>&nbsp;'.$lang['LIST_HEAD_PRICE'].'&nbsp;</th>';
	$ROW_RESULTS .= '</tr>';
	
	#array_multisort( $shops, SORT_ASC, SORT_STRING );
	foreach( $shops as $v )
	{
		$lnk = '<a href="index.php?page='.PAGE_SHOPS.'&amp;action=showmap&amp;npc_id='.$v['npc_id'].'"><img border="0" src="images/locate.gif" height="14" alt="" title="'.$lang['IMG_TITLE_SHOWONMAP'].'" /></a>&nbsp;';
		$ROW_RESULTS .= '<tr '.$v['row_class'].'>';
		$ROW_RESULTS .= '<td style="text-align:left;" nowrap="nowrap">&nbsp;'.$lnk.trim($v['npc_name']).'&nbsp;</td>';
		$ROW_RESULTS .= '<td>&nbsp;'.$v['npc_level'].'&nbsp;</td>';
		$ROW_RESULTS .= '<td>&nbsp;'.$v['area_name'].'&nbsp;</td>';
		$ROW_RESULTS .= '<td>&nbsp;'.$v['shop_price'].'&nbsp;</td>';
		$ROW_RESULTS .= '</tr>';
	}
}

$colspan = 4;

if( !empty($ROW_RESULTS) )
{
	$TABLE_RESULT = '<table class="forumline" width="100%" cellspacing="1" cellpadding="0" border="0">';
	$TABLE_RESULT .= $ROW_RESULTS;
	$TABLE_RESULT .= '<tr><td colspan="'. $colspan .'" class="listing_buttom">&nbsp;'.count($shops).' '.$lang['LIST_BUTTOM_RESULTS'].'&nbsp;</td></tr>';
	$TABLE_RESULT .= '</table>';
}

$tpl->set_filenames(array(
	'listshop' => 'page_listshop.tpl')
);

$tpl->assign_vars(array(
	'TABLE_RESULT'		 => $TABLE_RESULT
	)
);

$tpl->pparse('listshop');
$tpl->destroy();

?>
