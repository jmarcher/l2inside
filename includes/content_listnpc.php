<?php
/***************************************************************************
 *                                L2 Mobfinder
 *                            --------------------
 *   begin                : Wednesdey, Jun 20, 2006
 *   copyright            : (C) 2006 Anubis <anubis@gods.de>
 *
 ***************************************************************************/

$_GET['item_id'] = intval($_GET['item_id']);
$_GET['npc_id'] = intval($_GET['npc_id']);

### START FUNCTIONS ##############################################################################################
#
/// Globale Variablen
$build_table_head = true;
$table_head = '';
$lnk = '';
$lnk_close = '';
$count = 0;

function template( $params, $row_count )
{
	global $build_table_head, $table_head, $lang, $count;
	
	if( $build_table_head ) $table_head = '<tr>';
	
	$result = '<tr '.row_Class( $row_count ).">\n";
	
	foreach( $params as $k => $value )
	{
		switch( $k )
		{
			case 'id':
					break;
			case 'name':
					$drop_check = drop_check( $params['id'], 'mobId' );
					$lnk = '<a href="index.php?page='.PAGE_NPCMOBS.'&amp;action=showmap&amp;npc_id='.$params['id'].'"><img border="0" src="images/locate.gif" height="14" alt="" title="'.$lang['IMG_TITLE_SHOWONMAP'].'" /></a>&nbsp;';
					if ($drop_check) $lnk .= '<a href="index.php?page='.PAGE_NPCMOBS.'&amp;action=droplist&amp;npc_id='.$params['id'].'"><img border="0" src="images/drop.gif" height="14" alt="" title="'.$lang['IMG_TITLE_SHOWDROP'].'" /></a>&nbsp;';
					if ($_GET['item_id']>0) $lnk .= '<a href="index.php?page='.PAGE_NPCMOBS.'&amp;npc_id='.$params['id'].'"><img border="0" src="images/info.gif" height="14" alt="" title="'.$lang['IMG_TITLE_INFO'].'" /></a>&nbsp;';
					build_table_head($lang['LIST_HEAD_NAME'], $k, 'left');
					$result .= build_item_prop($k, $lnk.stripslashes($params[$k]), 'left');
					break;
			case 'level':
					build_table_head($lang['LIST_HEAD_LEVEL'], $k);
					$result .= build_item_prop($k, $params[$k]);
					break;
			case 'aggro':
					build_table_head($lang['LIST_HEAD_TYPE'], $k);
					if ($params[$k]==0) $params[$k]='passive';
					else $params[$k]='aggressive';
					$result .= build_item_prop($k, $params[$k]);
					break;
			case 'hp':
					build_table_head($lang['LIST_HEAD_HP'], $k);
					$result .= build_item_prop($k, $params[$k]);
					break;
			case 'exp':
					build_table_head($lang['LIST_HEAD_XP'], $k);
					$result .= build_item_prop($k, $params[$k]);
					break;
			case 'min';
					build_table_head($lang['LIST_HEAD_DROPCOUNT'], 'max');
					if ($params['min']<>$params['max']) $str = $params['min'].'-'.$params['max'];
					else $str = $params['min'];
					$result .= build_item_prop('', $str);
					break;
			case 'chance':
					build_table_head($lang['LIST_HEAD_CHANCE'], $k);
					$result .= build_item_prop($k, chance_calc($params[$k], $params['sweep']).'%');
					break;
		}
	}
	
	if( $build_table_head ) $table_head .= '</tr>'."\n";
	$build_table_head = false;
	
	$count++;
	
	$result .= "</tr>\n\n";
	return $result;
}

function search_NPC()
{
	global $dbc;
	
	$str = prepage_SearchString( $_POST['search_string'] );
	$level = intval( $_POST['level'] );
	
	$allowed_orders = array( 'name', 'level', 'hp' );
	if( in_array( $_GET['order'], $allowed_orders ) )
		$order_sql = $_GET['order'];
	else if( in_array( DEFAULT_SORT_ORDER, $allowed_orders ) )
		$order_sql = DEFAULT_SORT_ORDER;
	else
		$order_sql = 'name';
	
	if( $str !== false || $_GET['item_id'] > 0 || $_GET['npc_id'] > 0 || $level > 0 )
	{
		if( $_GET['item_id'] < 1 )
		{
			if( !empty($str) ) $where_sql = 'npc.name LIKE \'%'. $str .'%\' ';
			if( $_GET['npc_id'] > 0 ) $where_sql = 'npc.id = '. $_GET['npc_id'];
			if( $level > 0 ) $where_sql = 'npc.level = '. $level;
		
			$query_bosses = 'SELECT DISTINCT npc.id, npc.name, npc.level, npc.type,npc.aggro, 
					npc.hp, npc.exp, npc.sp, raidboss_spawnlist.boss_id
		            FROM npc, raidboss_spawnlist
		            WHERE (('.$where_sql.') AND (npc.id=raidboss_spawnlist.boss_id))
		            ORDER BY npc.'.$order_sql;
			$query_minions = 'SELECT DISTINCT npc.id, npc.name, npc.level, npc.type,npc.aggro, 
		            npc.hp, npc.exp, npc.sp, minions.minion_id
		            FROM npc, minions
		            WHERE (('.$where_sql.') AND (npc.id=minions.minion_id))
		            ORDER BY npc.'.$order_sql;
			$query_npc = 'SELECT DISTINCT npc.id, npc.name, npc.level, npc.type,npc.aggro, 
		            npc.hp, npc.exp, npc.sp, spawnlist.npc_templateid
		            FROM npc, spawnlist
		            WHERE (('.$where_sql.') AND (npc.id=spawnlist.npc_templateid))
		            ORDER BY npc.'.$order_sql;
		}
		else
		{
			$query_bosses = 'SELECT DISTINCT npc.id, npc.name, npc.level,
		            droplist.mobId, droplist.itemId, droplist.min, droplist.max, droplist.chance, droplist.sweep
		            FROM npc, droplist, raidboss_spawnlist
		            WHERE ((droplist.itemId='.$_GET['item_id'].') AND (npc.id = raidboss_spawnlist.boss_id) AND (npc.id=droplist.mobId) )
		            ORDER BY droplist.chance DESC, '.$order_sql;
			$query_minions = 'SELECT DISTINCT npc.id, npc.name, npc.level,
		            droplist.mobId, droplist.itemId, droplist.min, droplist.max, droplist.chance, droplist.sweep
		            FROM npc, droplist, minions
		            WHERE ((droplist.itemId='.$_GET['item_id'].') AND (npc.id = minions.minion_id) AND (npc.id=droplist.mobId) )
		            ORDER BY droplist.chance DESC, '.$order_sql;
			$query_npc = 'SELECT DISTINCT npc.id, npc.name, npc.level,
		            droplist.mobId, droplist.itemId, droplist.min, droplist.max, droplist.chance, droplist.sweep
		            FROM npc, droplist, spawnlist
		            WHERE ((droplist.itemId='.$_GET['item_id'].') AND (npc.id = spawnlist.npc_templateid) AND (npc.id=droplist.mobId) )
		            ORDER BY droplist.chance DESC, '.$order_sql;
		}
		$result_bosses = $dbc->query( $query_bosses );
		$row_count1 = $row_count2 = 0;
		while( $row = mysql_fetch_assoc( $result_bosses ) )
		{
			if( $_GET['item_id'] > 0 )
			{
				if( $row['sweep'] == 0 ) { $search_result['l2bosses'] .= template($row, $row_count1); $row_count1++; }
				else { $search_result['l2bosses_spoil'] .= template($row, $row_count2); $row_count2++; }
			}
			else { $search_result['l2bosses'] .= template($row, $row_count1); $row_count1++; }
		}
		$result_minions = $dbc->query( $query_minions );
		$row_count1 = $row_count2 = 0;
		while( $row = mysql_fetch_assoc( $result_minions ) )
		{
			if( $_GET['item_id'] > 0 )
			{
				if( $row['sweep'] == 0 ) { $search_result['l2minions'] .= template($row, $row_count1); $row_count1++; }
				else { $search_result['l2minions_spoil'] .= template($row, $row_count2); $row_count2++; }
			}
			else
			{ $search_result['l2minions'] .= template($row, $row_count1); $row_count1++; }
		}
		
		$result = $dbc->query( $query_npc );
		$row_count1 = $row_count2 = $row_count3 = $row_count4 = 0;
		if( mysql_num_rows( $result ) > 0 || mysql_num_rows( $result_bosses ) > 0 )
		{
			while ( $row = mysql_fetch_assoc( $result ) )
			{
				if ( ($row['type']=='L2NPC')
			    	 || ($row['type']=='L2Npc')
					 || ($row['type']=='L2Artefact')
			     	 || ($row['type']=='L2Guard')
			     	 || ($row['type']=='L2Merchant')
			     	 || ($row['type']=='L2Teleporter')
			     	 || ($row['type']=='L2Trainer')
			     	 || ($row['type']=='L2VillageMaster')
			     	 || ($row['type']=='L2Warehouse')
			     	 || ($row['type']=='L2ClassMaster')
			     	 || ($row['type']=='L2SiegeNpc')
					)
				{
					if( $_GET['item_id'] > 0 )
					{
						if( $row['sweep'] == 0 ) { $search_result['l2npc'] .= template($row, $row_count1); $row_count1++; }
						else { $search_result['l2npc_spoil'] .= template($row, $row_count2); $row_count2++; }
					}
					else { $search_result['l2npc'] .= template($row, $row_count1); $row_count1++; }
				}
				else
				{
					if( $_GET['item_id'] > 0 )
					{
						if( $row['sweep'] == 0 ) { $search_result['l2monsters'] .= template($row, $row_count3); $row_count3++; }
						else { $search_result['l2monsters_spoil'] .= template($row, $row_count4); $row_count4++; }
					}
					else { $search_result['l2monsters'] .= template($row, $row_count3); $row_count3++; }
				}
			}
		}
		if( count($search_result) > 0 )
			return $search_result;
		else
			return 1;
	}
	else
		return 2;
}
#
### END FUNCTIONS ################################################################################################


$colspan = 5;
$ROW_RESULTS = '';

if( $_POST['action'] == 'search'
	 || $_GET['item_id'] > 0 || $_GET['npc_id'] > 0 || $_POST['level'] > 0 )
{
	update_Stats( 'search_npc' );
	$search_results = search_NPC();
	$error_id = ( is_int($search_results) ) ? $search_results : 0;
	if( $error_id > 0 )
		header( 'Location: '. DOMAIN_NAME .'index.php?page='. $pid .'&error='. $error_id );
	
	$ROW_RESULTS .= $table_head;
	if( count($search_results['l2monsters']) > 0 || ($_GET['item_id'] > 0 && count($search_results['l2monsters_spoil']) > 0 ) )
	{
		$ROW_RESULTS .= '<tr><td colspan="'. $colspan .'" class="listing_top">'.$lang['LIST_GROUP_MONSTERS'].'</td></tr>';
		if ($_GET['item_id']>0)
		{
			if (count($search_results['l2monsters'])>0)       $ROW_RESULTS .= '<tr><td colspan="'. $colspan .'" class="listing_sub" style="text-align:left;">&nbsp;<b>'.$lang['LIST_GROUP_DROP'].'</b></td></tr>'.$search_results['l2monsters'];
			if (count($search_results['l2monsters_spoil'])>0) $ROW_RESULTS .= '<tr><td colspan="'. $colspan .'" class="listing_sub" style="text-align:left;">&nbsp;<b>'.$lang['LIST_GROUP_SPOIL'].'</b></td></tr>'.$search_results['l2monsters_spoil'];
		}
		else $ROW_RESULTS .= $search_results['l2monsters'];
	}
	if( count($search_results['l2bosses']) > 0 || ($_GET['item_id'] > 0 && count($search_results['l2bosses_spoil']) > 0 ) )
	{
		$ROW_RESULTS .= '<tr><td colspan="'. $colspan .'" class="listing_top">'.$lang['LIST_GROUP_BOSSES'].'</td></tr>';
		if ($_GET['item_id']>0)
		{
			if (count($search_results['l2bosses'])>0)       $ROW_RESULTS .= '<tr><td colspan="'. $colspan .'" class="listing_sub" style="text-align:left;">&nbsp;<b>'.$lang['LIST_GROUP_DROP'].'</b></td></tr>'.$search_results['l2bosses'];
			if (count($search_results['l2bosses_spoil'])>0) $ROW_RESULTS .= '<tr><td colspan="'. $colspan .'" class="listing_sub" style="text-align:left;">&nbsp;<b>'.$lang['LIST_GROUP_SPOIL'].'</b></td></tr>'.$search_results['l2bosses_spoil'];
		}
		else $ROW_RESULTS .= $search_results['l2bosses'];
	}
	if( count($search_results['l2minions']) > 0 || ($_GET['item_id'] > 0 && count($search_results['l2minions_spoil']) > 0 ) )
	{
		$ROW_RESULTS .= '<tr><td colspan="'. $colspan .'" class="listing_top">'.$lang['LIST_GROUP_MINIONS'].'</td></tr>';
		if ($_GET['item_id']>0)
		{
			if (count($search_results['l2minions'])>0)       $ROW_RESULTS .= '<tr><td colspan="'. $colspan .'" class="listing_sub" style="text-align:left;">&nbsp;<b>'.$lang['LIST_GROUP_DROP'].'</b></td></tr>'.$search_results['l2minions'];
			if (count($search_results['l2minions_spoil'])>0) $ROW_RESULTS .= '<tr><td colspan="'. $colspan .'" class="listing_sub" style="text-align:left;">&nbsp;<b>'.$lang['LIST_GROUP_SPOIL'].'</b></td></tr>'.$search_results['l2minions_spoil'];
		}
		else $ROW_RESULTS .= $search_results['l2minions'];
	}
	if( count($search_results['l2npc']) > 0 || ($_GET['item_id'] > 0 && count($search_results['l2npc_spoil']) > 0 ) )
	{
		$ROW_RESULTS .= '<tr><td colspan="'. $colspan .'" class="listing_top">'.$lang['LIST_GROUP_NPCS'].'</td></tr>';
		if ($_GET['item_id']>0)
		{
			if (count($search_results['l2npc'])>0)       $ROW_RESULTS .= '<tr><td colspan="'. $colspan .'" class="listing_sub" style="text-align:left;">&nbsp;<b>'.$lang['LIST_GROUP_DROP'].'</b></td></tr>'.$search_results['l2npc'];
			if (count($search_results['l2npc_spoil'])>0) $ROW_RESULTS .= '<tr><td colspan="'. $colspan .'" class="listing_sub" style="text-align:left;">&nbsp;<b>'.$lang['LIST_GROUP_SPOIL'].'</b></td></tr>'.$search_results['l2npc_spoil'];
		}
		else $ROW_RESULTS .= $search_results['l2npc'];
	}
}


if( !empty($ROW_RESULTS) )
{
	$rate_stats = ($_GET['item_id'] >0) ? '&nbsp;&nbsp;[ DropRateAdena: '.number_format_lang($server_properties['RateDropAdena'], 1).' - DropRateItems: '.number_format_lang($server_properties['RateDropItems'], 1).' - DropRateSpoil: '.number_format_lang($server_properties['RateDropSpoil'], 1).' ]' : '';
	$TABLE_RESULT = '<table class="forumline" width="100%" cellspacing="1" cellpadding="0" border="0">';
	$TABLE_RESULT .= $ROW_RESULTS;
	$TABLE_RESULT .= '<tr><td colspan="'. $colspan .'" class="listing_buttom">&nbsp;'.$count.' '.$lang['LIST_BUTTOM_RESULTS'].$rate_stats.'&nbsp;</td></tr>';
	$TABLE_RESULT .= '</table>';
}

$tpl->set_filenames(array(
	'listnpc' => 'page_listnpc.tpl')
);

$tpl->assign_vars(array(
	'TABLE_RESULT'		 => $TABLE_RESULT
	)
);

$tpl->pparse('listnpc');
$tpl->destroy();

?>
