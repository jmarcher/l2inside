<?php
/***************************************************************************
 *                                L2 Mobfinder
 *                            --------------------
 *   begin                : Wednesdey, Jun 20, 2006
 *   copyright            : (C) 2006 Anubis <anubis@gods.de>
 *
 ***************************************************************************/

$page_stats['items'] = 0;
$page_stats['monsters'] = 0;
$page_stats['raids'] = 0;
$page_stats['npcs'] = 0;
$page_stats = get_PageStats();


$tpl->set_filenames(array(
	'footer' => 'page_footer.tpl')
);

$tpl->assign_vars(array(
	'STATS_PAGE'		 => 'Page rendered in '. end_time($start_time) .' seconds',
	'STATS_DATABASE'	 => 'Database: Items ('. $page_stats['items'] .') - Monsters ('. $page_stats['monsters'] .') - Raids ('. $page_stats['raids'] .') - NPCs ('. $page_stats['npcs'] .')'
	)
);

$tpl->pparse('footer');
$tpl->destroy();

?>