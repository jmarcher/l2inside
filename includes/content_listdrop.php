<?php
/***************************************************************************
 *                                L2 Mobfinder
 *                            --------------------
 *   begin                : Wednesdey, Jun 20, 2006
 *   copyright            : (C) 2006 Anubis <anubis@gods.de>
 *
 ***************************************************************************/

$_GET['npc_id'] = intval($_GET['npc_id']);

### START FUNCTIONS ##############################################################################################
#
/// Globale Variablen
$build_table_head = true;
$table_head = '';
$count = 0;

function template( $params, $page_id, $drop_list, $row_count )
{
	global $build_table_head, $table_head, $server_properties, $lang, $count;
	
	if( $build_table_head ) $table_head = '<tr>';
	
	$result = '<tr '.row_Class( $row_count ).">\n";
	
	build_table_head('', '');
	$result .= build_item_prop('img', GetImage($params['item_id']));
	$lnk .= '<a href="index.php?page='.PAGE_NPCMOBS.'&amp;item_id='.$params['item_id'].'"><img border="0" src="images/mob.gif" height="14" alt="" title="'.$lang['IMG_TITLE_DROPFROM'].'" /></a>&nbsp;';
	$lnk .= '<a href="index.php?page='.$page_id.'&amp;item_id='.$params['item_id'].'"><img border="0" src="images/info.gif" height="14" alt="" title="'.$lang['IMG_TITLE_ITEMINFO'].'" /></a>&nbsp;';
	
	build_table_head($lang['LIST_HEAD_NAME'], $k, 'left');
	
	$itm_name= GetName($params['item_id']);
	if( trim($itm_name) == '' ) $itm_name = $params['name'];
	
	$result .= build_item_prop('name', $lnk.$itm_name, 'left');
	
	build_table_head($lang['LIST_HEAD_DROPCOUNT']);
	$x = 1;
	if( $drop_list['itemId'] == 57 ) $x = $server_properties['RateDropAdena'];
	if( trim($drop_list['min']) == trim($drop_list['max']) )
	{
		$result .= build_item_prop('', $drop_list['min']*$x);
	}
	else
	{
		$result .= build_item_prop('', $drop_list['min']*$x.'-'.$drop_list['max']*$x);
	}
	
	build_table_head($lang['LIST_HEAD_CHANCE']);
	$result .= build_item_prop('', chance_calc($drop_list['chance'], $drop_list['sweep']).'%');
	
	if( $build_table_head ) $table_head .= '</tr>'."\n";
	$build_table_head = false;
	
	$count++;
	
	$result .= "</tr>\n\n";
	return $result;
}

function finditem( $drop_arr, $row_count )
{
	global $dbc;
	
	/*
	$pid
	1-npcs
	2-etcitem
	3-armor
	4-weapon
	5-shop
	*/
	
	if( trim($drop_arr['min']) == trim($drop_arr['max']) )
	{
		$drop_count = $drop_arr['min'];
	}
	else
	{
		$drop_count = $drop_arr['min'].'-'.$drop_arr['max'];
	}
	
	$result = $dbc->query('SELECT * FROM etcitem WHERE item_id='.$drop_arr['itemId']);
	if( mysql_num_rows( $result ) == 1 )
	{
		$items = mysql_fetch_assoc( $result );
		$tt = template( $items, PAGE_ITEMS, $drop_arr, $row_count );
		return $tt;
		exit;
	}
	$result = $dbc->query('SELECT * FROM armor WHERE item_id='.$drop_arr['itemId']);
	if( mysql_num_rows( $result ) == 1 )
	{
		$items = mysql_fetch_assoc( $result );
		$tt = template( $items, PAGE_ARMORS, $drop_arr, $row_count );
		return $tt;
		exit;
	}
	$result = $dbc->query('SELECT * FROM weapon WHERE item_id='.$drop_arr['itemId']);
	if( mysql_num_rows( $result ) == 1 )
	{
		$items = mysql_fetch_assoc( $result );
		$tt = template( $items, PAGE_WEAPONS, $drop_arr, $row_count );
		return $tt;
		exit;
	}
}
#
### END FUNCTIONS ################################################################################################


$colspan = 4;
$ROW_RESULTS = '';

$row_count1 = 0;
$row_count2 = 0;
if( $_GET['npc_id'] > 0 )
{
	$query_drop = 'SELECT * FROM droplist WHERE mobid='.$_GET['npc_id'].' ORDER BY chance DESC';

	$query_result = $dbc->query( $query_drop );

	if( mysql_num_rows($query_result) > 0 )
	{
		while( $drop = mysql_fetch_array($query_result) )
		{
			if( $drop['sweep'] == 0 ) {	$NoSpoil .= finditem( $drop, $row_count1 );	$row_count1++; }
			else { $Spoil .= finditem( $drop, $row_count2 ); $row_count2++; }
		}
		$ROW_RESULTS .= $table_head;
		if( count($NoSpoil) > 0 )
		{
			$ROW_RESULTS .= '<tr><td colspan="'. $colspan .'" class="listing_top"><b>'.$lang['LIST_GROUP_DROP'].'</b></td></tr>'."\n";
			$ROW_RESULTS .= $NoSpoil;
		}
		if( count($Spoil) > 0 )
		{
			$ROW_RESULTS .= '<tr><td colspan="'. $colspan .'" class="listing_top"><b>'.$lang['LIST_GROUP_SPOIL'].'</b></td></tr>'."\n";
			$ROW_RESULTS .= $Spoil;
		}
	}
	else
		header( 'Location: '. DOMAIN_NAME .'index.php?page='. $pid .'&error=1' );
}


if( !empty($ROW_RESULTS) )
{
	$rate_stats = '&nbsp;&nbsp;[ DropRateAdena: '.number_format_lang($server_properties['RateDropAdena'], 1).' - DropRateItems: '.number_format_lang($server_properties['RateDropItems'], 1).' - DropRateSpoil: '.number_format_lang($server_properties['RateDropSpoil'], 1).' ]';
	$TABLE_RESULT = '<table class="forumline" width="100%" cellspacing="1" cellpadding="0" border="0">';
	$TABLE_RESULT .= $ROW_RESULTS;
	$TABLE_RESULT .= '<tr><td colspan="'. $colspan .'" class="listing_buttom">&nbsp;'.$count.' '.$lang['LIST_BUTTOM_RESULTS'].$rate_stats.'&nbsp;</td></tr>';
	$TABLE_RESULT .= '</table>';
}

$tpl->set_filenames(array(
	'listdrop' => 'page_listdrop.tpl')
);

$tpl->assign_vars(array(
	'TABLE_RESULT'		 => $TABLE_RESULT
	)
);

$tpl->pparse('listdrop');
$tpl->destroy();

?>
