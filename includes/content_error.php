<?php
/***************************************************************************
 *                                L2 Mobfinder
 *                            --------------------
 *   begin                : Wednesdey, Jun 20, 2006
 *   copyright            : (C) 2006 Anubis <anubis@gods.de>
 *
 ***************************************************************************/

switch( $error_id )
{
	case 1: $error_msg = $lang['ERROR_001'] .' ['. $error_id .']'; break;
	case 2: $error_msg = $lang['ERROR_002'] .' ['. $error_id .']'; break;
	case 3: $error_msg = $lang['ERROR_003'] .' ['. $error_id .']'; break;
	case 4: $error_msg = $lang['ERROR_004'] .' ['. $error_id .']'; break;
	case 5: $error_msg = $lang['ERROR_005'] .' ['. $error_id .']'; break;
	default: $error_msg = $lang['ERROR_DEFAULT'] .' ['. $error_id .']';
}

include_once( $current_template_path .'form_error.php' );

?>