<?php
/***************************************************************************
 *                                L2 Mobfinder
 *                            --------------------
 *   begin                : Wednesdey, Jun 20, 2006
 *   copyright            : (C) 2006 Anubis <anubis@gods.de>
 *
 ***************************************************************************/

$tpl->set_filenames(array(
	'drawmap' => 'page_drawmap.tpl')
);

$tpl->assign_vars(array(
	'IFRAME_SRC'	 => 'drawmap.php?npc_id='. $_GET['npc_id'],
	'BACK_BUTTON'	 => '<a href="javascript:history.back();">'.$lang['BACK'].'</a>'
	)
);

$tpl->pparse('drawmap');
$tpl->destroy();

?>