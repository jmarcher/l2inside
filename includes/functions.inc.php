<?php
/***************************************************************************
 *                                L2 Mobfinder
 *                            --------------------
 *   begin                : Wednesdey, Jun 20, 2006
 *   copyright            : (C) 2006 Anubis <anubis@gods.de>
 *
 ***************************************************************************/

function start_time() {
	$start_time = explode(' ', microtime());
	$start_time = $start_time[0] + $start_time[1];
	return $start_time;
}

function end_time( $start_time) {
	$end_time = explode(' ', microtime());
	$end_time = $end_time[0] + $end_time[1];
	$total_time = sprintf("%.5f", $end_time - $start_time);
	return $total_time;
}

//----------- IPget-function ip(); -----------
function iptype1()
{
	if (getenv('HTTP_CLIENT_IP'))
		return getenv('HTTP_CLIENT_IP');
	else
		return false;
}
function iptype2()
{
	if (getenv('HTTP_X_FORWARDED_FOR'))
		return getenv('HTTP_X_FORWARDED_FOR');
	else
		return false;
}
function iptype3()
{
	if (getenv('REMOTE_ADDR'))
		return getenv('REMOTE_ADDR');
	else
		return false;
}
function ip()
{
	$ip1 = iptype1();
	$ip2 = iptype2();
	$ip3 = iptype3();
	if ( $ip1 && $ip1 != 'unknown')
		return $ip1;
	else if ( $ip2 && $ip2 != 'unknown')
		return $ip2;
	else if ( $ip3 && $ip3 != 'unknown')
		return $ip3;
	else
		return false;
}
//----------- End: IPget-function

function number_format_lang( $number, $dec=0)
{
  global $lang;
	$out = number_format( $number, $dec, $lang['DEC_POINT'], $lang['THOUSANDS_SEP']);
	return $out;
}

function display_size($file_size)
{
	if($file_size >= 1073741824) {
		$file_size = round($file_size / 1073741824 * 100) / 100;
		$size_unit = 'gb';
	} else if($file_size >= 1048576) {
		$file_size = round($file_size / 1048576 * 100) / 100;
		$size_unit = 'mb';
	} else if($file_size >= 1024) {
		$file_size = round($file_size / 1024 * 100) / 100;
		$size_unit = 'kb';
	} else {
		$file_size = $file_size;
		$size_unit = 'b';
	}
	$file_size = number_format_lang( $file_size, 2) . $size_unit;
	return $file_size;
}

function dskspace($dir)
{
   $s = stat($dir);
   $space = $s["blocks"]*512;
   if (is_dir($dir))
   {
     $dh = opendir($dir);
     while (($file = readdir($dh)) !== false)
       if ($file != "." and $file != "..")
         $space += dskspace($dir."/".$file);
     closedir($dh);
   }
   return $space;
}

function redirect( $url)
{
	// Ein Beispiel f�r den Einsatz der seit PHP 4.3.0 verf�gbaren optionalen
	// Parameter file und line. Beachten Sie, dass $filename und $linenum f�r
	// einen sp�teren Einsatz �bergeben werden.
	// Weisen Sie ihnen davor keine Werte zu.
	if (!headers_sent($filename, $linenum)) {
		header('Location: ' . DOMAIN_NAME . $url);
		//header('Location: http://'.$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF']).'/'.$url);
		exit;
	
	// Wahrscheinlich wollen Sie hier eine Fehlermeldung ausl�sen
	} else {
	   // Headers already sent in $filename on line $linenum<br>\n
	   print "<div align=\"center\">" .
	         "Cannot redirect, for now please click this <a " .
	         "href=\"". DOMAIN_NAME ."\">link</a> instead</div>\n";
	}
}

function chance_calc( $db_chance, $db_sweep=0 )
{
	global $server_properties;
	
	if( $db_sweep == 0 )
		$result = $db_chance/10000*$server_properties['RateDropItems'];
	else
		$result = $db_chance/10000*$server_properties['RateDropSpoil'];
	if ($result > 100) $result = 100;
	return $result;
}

function consumable_calc( $adena )
{
	global $server_properties;
	return $adena * $server_properties['RateConsumableCost'];
}

function row_Class( $i )
{
	return ( $i % 2 == 0 ) ? ' class="row1"' : ' class="row2"';
}

/*
function build_order_link( $order )
{
	global $lnk, $lnk_close;
	
	#if( $order=='' ) exit;
	$lnk='';
	$lnk_close='';
	
	if( !empty($_GET['npc_name']) )
	{
		$str = '&npc_name='.$_GET['npc_name'];
		$lnk = '<a href=?action='.$_GET['action'].$str.'&order='.$order.'>';
		$lnk_close = '</a>';
	}
}
*/

function build_table_head( $col_name, $order='', $alignment='', $max_depth=0 )
{
	global $lnk, $lnk_close, $table_head, $build_table_head, $lang;
	
	if( $build_table_head )
	{
		$max_depth = ( is_int($max_depth) && $max_depth > 0 ) ? ' colspan="'. ($max_depth+1) .'"' : '';
		$alignment = ( !empty($alignment) ) ? ' style="text-align:'.$alignment.';"' : '';
		$table_head .= '<th '.$alignment.$max_depth.' nowrap="nowrap">&nbsp;'.$lnk.$col_name.$lnk_close.'&nbsp;</th>'."\n";
	}
}

function build_item_prop( $param_name, $param_value, $alignment='', $depth=0, $max_depth=0 )
{
	if( $param_name == 'name' )
	{
		// Workaround f�r i-Explorer bei Rezept-Anzeige
		#$width = ( $depth == $max_depth ) ? ' width="80%"' : '';
		$nowrap = ' nowrap="nowrap"';
	}
	
	if( $param_name == 'img' )
	{
		$width = ' width="2%"';
		$nowrap = ' nowrap="nowrap"';
	}
	
	$space = '&nbsp;';
	if( $param_name == 'desc' )
	{ $space = ''; $color = ' class="grey"'; }
	
	if( $param_name == 'quantity' )
	{ $param_value = '<b>'.$param_value.'</b>'; $color = ' class="highlight1"'; }
	$alignment = ( !empty($alignment) ) ? ' style="text-align:'.$alignment.';"' : '';
	
	$depth = $max_depth - $depth;
	$depth = ( is_int($depth) && $depth > 0 ) ? ' colspan="'. ($depth+1) .'"' : '';
		
	$result = '<td'.$alignment.$depth.$width.$color.$nowrap.'>'.$space.$param_value."&nbsp;</td>\n";
	return $result;
}

function check_Email( $email )
{
	/// Email-Check
	$pattern = "/^[\w.-]+@[\w.-]+\.(com|net|de|nl|uk|org|edu|mil|cc|gov|gob|info|tv|biz)$/";
	if( preg_match( $pattern, $email ) )
	{
		if( strpos( $email, 'mailinator' ) !== false ) return false;
		else if( strpos( $email, 'mailtic' ) !== false ) return false;
		else if( strpos( $email, 'dodgeit' ) !== false ) return false;
		else return true;
	}
	else return false;
}

/* zur Zeit nicht benutzt */
function check_URL( $url )
{
	/// URL-Check (muss verbessert werden)
	$pattern = "/^(http|https)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?$/";
	#$pattern = "/^(http|https)\:\/\/([\w-_]+\.)+([\w-_]+)(\/)?[\w\.-_]*$/";
	if( preg_match( $pattern, $url ) )
		return true;
	else
		return false;
}

//
// This function is for compatibility with PHP 4.x's realpath()
// function.  In later versions of PHP, it needs to be called
// to do checks with some functions.  Older versions of PHP don't
// seem to need this, so we'll just return the original value.
function phpbb_realpath($path)
{
	return (!@function_exists('realpath') || !@realpath('./includes/functions.inc.php')) ? $path : @realpath($path);
}

?>