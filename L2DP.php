<?php

function l2dp_header()
{
  global $l2dp_lang;

  if ( isset( $_GET['language'] ) )
  {
    setcookie( 'language', '', time()-3600, '/' );
    setcookie( 'language', $_GET['language'], time()+3600*24*30, '/' );
    $l2dp_lang = $_GET['language'];
  }
  else
  {
    if ( ( isset( $_COOKIE['language'] ) )
      && ( strlen( $_COOKIE['language'] ) == 2 ) )
    {
      $l2dp_lang = $_COOKIE['language'];
    }
    else
    {
      setcookie( 'language', '', time()-3600, '/' );
      setcookie( 'language', 'en', time()+3600*24*30, '/' );
      $l2dp_lang = 'en';
    }
  }

echo <<<EOD
<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en-US" xml:lang="en-US">
<head>
  <META http-equiv=Content-Type content="text/html; charset=UTF-8">
  <LINK href="l2dp.css" rel="stylesheet" type="text/css"/>

EOD;
}

function l2dp_body()
{
  echo "</head>\n";
  echo "<body bgcolor=\"#FFFFFF\" text=\"#000000\" link=\"#0000FF\"";
  echo " vlink=\"#FF00FF\" alink=\"#FF0000\">\n";

  if ( strstr( $_SERVER['REQUEST_URI'], '?' ) )
  {
    $base = $_SERVER['REQUEST_URI'] . '&';
  }
  else
  {
    $base = $_SERVER['REQUEST_URI'] . '?';
  }
  echo "<table border=0 width=100%><tr><td>";
  echo "<a href=\"index.php\">[home]</a>";
  echo "<td align=right><a href=\"" . $base . "language=en\">English</a> - ";
  echo "<a href=\"" . $base . "language=kr\">Korean</a> - ";
  echo "<a href=\"" . $base . "language=jp\">Japanese</a> - ";
  echo "<a href=\"" . $base . "language=tw\">Taiwan</a> - ";
  echo "<a href=\"" . $base . "language=cn\">China</a></td></tr><table><br>\n";
  echo "<table width=740 border=0 cellpadding=0 cellspacing=0>";
  echo "<tr><td width=\"100%\">\n\n";
}

function l2dp_footer()
{
  echo "</td></tr></table><p>"
    . "<a href=\"http://www.mithral.com/~beberg/L2DP/\">L2DP</a> data from "
    . gmdate('Y-M-d H:i:s \U\T\C', time() ) . "</p>\n";
  echo "</body></html>";
}

function l2dp_connect( $database )
{
  // PostgreSQL version
  //return pg_connect( "host=localhost user=root dbname=$database" ); 

  // MySQL version
  $c = mysql_connect( "localhost", "root", "vbyasp" );
  mysql_select_db( "l2jdb" );
  return $c;
}

function l2dp_disconnect( $db_conn )
{
  mysql_close( $db_conn ); 
}

?>
