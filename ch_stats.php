<? if(isset($_GET['file'])){?>
<table align="center" border="1" style="border-collapse:collapse">
<?
	echo "<tr><td><strong>Class name</strong></td><td>STR</td><td>CON</td><td>DEX</td><td>&nbsp;</td><td>INT</td><td>WIT</td><td>MEN</td><td>&nbsp;</td><td>P. Atk.</td><td>P. Def.</td><td>&nbsp;</td><td>M. Atk.</td><td>M. Def.</td><td>Atk. Speed</td><td>Cast. Speed</td><td>Accuracy</td><td>Critical</td><td>Evasion</td><td>Speed</td></tr>";
$templ=do_sql("SELECT * FROM `char_templates`");
while ($rt=mysql_fetch_assoc($templ))
{
	echo "<tr><td><strong>".$rt['ClassName']."</strong></td><td>".$rt['STR']."</td><td>".$rt['CON']."</td><td>".$rt['DEX']."</td><td>&nbsp;</td><td>".$rt['_INT']."</td><td>".$rt['WIT']."</td><td>".$rt['MEN']."</td><td>&nbsp;</td><td>".$rt['P_ATK']."</td><td>".$rt['P_DEF']."</td><td>&nbsp;</td><td>".$rt['M_ATK']."</td><td>".$rt['M_DEF']."</td><td>".$rt['P_SPD']."</td><td>".$rt['M_SPD']."</td><td>".$rt['ACC']."</td><td>".$rt['CRITICAL']."</td><td>".$rt['EVASION']."</td><td>".$rt['MOVE_SPD']."</td></tr>";
}
?>

</table>
<ul>
  <li><strong>STR</strong> (STRENGTH)<br />
  </li>
  <ul>
    <li>Affects P.Atk. stat through the strength modifier</li>
  </ul>
  <li><strong>CON</strong> (CONSTITUTION)<br />
  </li>
  <ul>
    <li>affects maximum HP and CP through the CON modifier</li>
    <li>affects HP recovery speed</li>
    <li>affects weight limit</li>
    <li>affects underwater breath gauge</li>
    <li>affects shock (stun) resistance</li>
    <li>affects bleeding resistance.</li>
  </ul>
  <li><strong>DEX</strong> (DEXTERITY)<br />
  </li>
  <ul>
    <li>affects Atk. Spd., Critical and Speed stats through the DEX modifier</li>
    <ul>
      <li>each point of dex gained makes around a 1% difference to each of those           three stats except at 43 DEX, where there is a jump of almost 2%</li>
    </ul>
    <li>affects both Accuracy and Evasion stats directly</li>
    <ul>
      <li>amount gained through each point of dex varies, but as a general rule           you can expect between .75 and .45 of accuracy and evasion per point of           dex gained</li>
      <li>the more dex you have, the less of these two stats you will gain by           adding to dex</li>
    </ul>
    <li>affects shield block rate</li>
    <ul>
      <li>Rumored to affect excellent shield defense rate</li>
    </ul>
  </ul>
  <li><strong>INT</strong> (INTELLIGENCE)<br />
  </li>
  <ul>
    <li>affects M.Atk. stat through the INT modifier</li>
    <ul>
      <li>INT modifier forthcoming</li>
    </ul>
    <li>affects curse land rates through M.Atk.</li>
  </ul>
  <li><strong>WIT</strong> (MENTAL)</li>
  <ul>
    <li>affects Casting Spd. stat through the WIT modifier</li>
    <ul>
      <li>each point of wit gained makes a 5% difference to Casting spd.</li>
    </ul>
    <li>affects chance of magic critical hits</li>
    <li>affects resistance to Hold (aka Roots) and Sleep</li>
    <li>affects resistance to essentially every single type of debuff not listed         by the other stats</li>
  </ul>
  <li><strong>MEN</strong> (WISDOM)<br />
  </li>
  <ul>
    <li>affects M.Def. and maximum MP stats through the MEN modifier</li>
    <ul>
      <li>each point of men gained or lost makes a difference of 1%</li>
    </ul>
    <li>affects MP recovery speed</li>
    <li>affects poison resistance</li>
    <li>affects curse resistance through M.Def.</li>
    <li>affects probability of magic interruption</li>
  </ul>
</ul>
<table border="1" cellpadding="0" cellspacing="0" width="100%">
  <tbody>
    <tr height="17">
      <td colspan="3" height="17">Changed stats and bonus:</td>
      <td colspan="7"></td>
    </tr>
    <tr height="17">
      <td colspan="10" height="17"></td>
    </tr>
    <tr height="17">
      <td height="17">STR</td>
      <td>DE Fighter</td>
      <td>Human Fighter</td>
      <td>Elf Fighter</td>
      <td>Orc Fighter</td>
      <td>Dwarf Fighter</td>
      <td>DE Mage</td>
      <td>Human Mage</td>
      <td>Elf Mage</td>
      <td>Orc Mage</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">10</td>
      <td>21% PAtk</td>
      <td>21% PAtk</td>
      <td>19% PAtk</td>
      <td>21% PAtk</td>
      <td>20% PAtk</td>
      <td>15% PAtk</td>
      <td>16% PAtk</td>
      <td>15% PAtk</td>
      <td>16% PAtk</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">9</td>
      <td>19% PAtk</td>
      <td>18% PAtk</td>
      <td>17% PAtk</td>
      <td>18% PAtk</td>
      <td>18% PAtk</td>
      <td>14% PAtk</td>
      <td>14% PAtk</td>
      <td>13% PAtk</td>
      <td>14% PAtk</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">8</td>
      <td>16% PAtk</td>
      <td>16% PAtk</td>
      <td>15% PAtk</td>
      <td>16% PAtk</td>
      <td>16% PAtk</td>
      <td>12% PAtk</td>
      <td>12% PAtk</td>
      <td>12% PAtk</td>
      <td>12% PAtk</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">7</td>
      <td>14% PAtk</td>
      <td>14% PAtk</td>
      <td>13% PAtk</td>
      <td>14% PAtk</td>
      <td>14% PAtk</td>
      <td>10% PAtk</td>
      <td>11% PAtk</td>
      <td>10% PAtk</td>
      <td>11% PAtk</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">6</td>
      <td>12% PAtk</td>
      <td>12% PAtk</td>
      <td>11% PAtk</td>
      <td>12% PAtk</td>
      <td>12% PAtk</td>
      <td>9% PAtk</td>
      <td>9% PAtk</td>
      <td>9% PAtk</td>
      <td>9% PAtk</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">5</td>
      <td>10% PAtk</td>
      <td>10% PAtk</td>
      <td>9% PAtk</td>
      <td>10% PAtk</td>
      <td>10% PAtk</td>
      <td>7% PAtk</td>
      <td>8% PAtk</td>
      <td>8% PAtk</td>
      <td>7% PAtk</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">4</td>
      <td>8% PAtk</td>
      <td>8% PAtk</td>
      <td>7% PAtk</td>
      <td>8% PAtk</td>
      <td>8% PAtk</td>
      <td>6% PAtk</td>
      <td>6% PAtk</td>
      <td>6% PAtk</td>
      <td>6% PAtk</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">3</td>
      <td>6% PAtk</td>
      <td>6% PAtk</td>
      <td>5% PAtk</td>
      <td>6% PAtk</td>
      <td>6% PAtk</td>
      <td>4% PAtk</td>
      <td>5% PAtk</td>
      <td>4% PAtk</td>
      <td>4% PAtk</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">2</td>
      <td>4% PAtk</td>
      <td>4% PAtk</td>
      <td>3% PAtk</td>
      <td>4% PAtk</td>
      <td>4% PAtk</td>
      <td>3% PAtk</td>
      <td>3% PAtk</td>
      <td>3% PAtk</td>
      <td>3% PAtk</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">1</td>
      <td>2% PAtk</td>
      <td>2% PAtk</td>
      <td>1% PAtk</td>
      <td>2% PAtk</td>
      <td>2% PAtk</td>
      <td>1% PAtk</td>
      <td>2% PAtk</td>
      <td>1% PAtk</td>
      <td>1% PAtk</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">0</td>
      <td>0% PAtk</td>
      <td>0% PAtk</td>
      <td>0% PAtk</td>
      <td>0% PAtk</td>
      <td>0% PAtk</td>
      <td>0% PAtk</td>
      <td>0% PAtk</td>
      <td>0% PAtk</td>
      <td>0% PAtk</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">-1</td>
      <td>-2% PAtk</td>
      <td>-2% PAtk</td>
      <td>-2% PAtk</td>
      <td>-2% PAtk</td>
      <td>-2% PAtk</td>
      <td>-2% PAtk</td>
      <td>-1% PAtk</td>
      <td>-2% PAtk</td>
      <td>-2% PAtk</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">-2</td>
      <td>-4% PAtk</td>
      <td>-4% PAtk</td>
      <td>-4% PAtk</td>
      <td>-4% PAtk</td>
      <td>-4% PAtk</td>
      <td>-3% PAtk</td>
      <td>-2% PAtk</td>
      <td>-3% PAtk</td>
      <td>-3% PAtk</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">-3</td>
      <td>-6% PAtk</td>
      <td>-6% PAtk</td>
      <td>-6% PAtk</td>
      <td>-6% PAtk</td>
      <td>-5% PAtk</td>
      <td>-5% PAtk</td>
      <td>-3% PAtk</td>
      <td>-4% PAtk</td>
      <td>-5% PAtk</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">-4</td>
      <td>-8% PAtk</td>
      <td>-7% PAtk</td>
      <td>-7% PAtk</td>
      <td>-7% PAtk</td>
      <td>-7% PAtk</td>
      <td>-6% PAtk</td>
      <td>-4% PAtk</td>
      <td>-6% PAtk</td>
      <td>-6% PAtk</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">-5</td>
      <td>-9% PAtk</td>
      <td>-9% PAtk</td>
      <td>-9% PAtk</td>
      <td>-9% PAtk</td>
      <td>-9% PAtk</td>
      <td>-7% PAtk</td>
      <td>-6% PAtk</td>
      <td>-7% PAtk</td>
      <td>-8% PAtk</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">-6</td>
      <td>-11% PAtk</td>
      <td>-11% PAtk</td>
      <td>-11% PAtk</td>
      <td>-11% PAtk</td>
      <td>-11% PAtk</td>
      <td>-9% PAtk</td>
      <td>-7% PAtk</td>
      <td>-8% PAtk</td>
      <td>-9% PAtk</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">-7</td>
      <td>-13% PAtk</td>
      <td>-13% PAtk</td>
      <td>-12% PAtk</td>
      <td>-13% PAtk</td>
      <td>-12% PAtk</td>
      <td>-10% PAtk</td>
      <td>-8% PAtk</td>
      <td>-10% PAtk</td>
      <td>-10% PAtk</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">-8</td>
      <td>-15% PAtk</td>
      <td>-14% PAtk</td>
      <td>-14% PAtk</td>
      <td>-14% PAtk</td>
      <td>-14% PAtk</td>
      <td>-11% PAtk</td>
      <td>-10% PAtk</td>
      <td>-11% PAtk</td>
      <td>-12% PAtk</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">-9</td>
      <td>-16% PAtk</td>
      <td>-16% PAtk</td>
      <td>-15% PAtk</td>
      <td>-16% PAtk</td>
      <td>-16% PAtk</td>
      <td>-13% PAtk</td>
      <td>-11% PAtk</td>
      <td>-12% PAtk</td>
      <td>-13% PAtk</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">-10</td>
      <td>-18% PAtk</td>
      <td>-18% PAtk</td>
      <td>-17% PAtk</td>
      <td>-18% PAtk</td>
      <td>-17% PAtk</td>
      <td>-14% PAtk</td>
      <td>-12% PAtk</td>
      <td>-13% PAtk</td>
      <td>-14% PAtk</td>
    </tr>
    <tr height="17">
      <td colspan="10" height="17"></td>
    </tr>
    <tr height="17">
      <td height="17">DEX</td>
      <td>DE Fighter</td>
      <td>Human Fighter</td>
      <td>Elf Fighter</td>
      <td>Orc Fighter</td>
      <td>Dwarf Fighter</td>
      <td>DE Mage</td>
      <td>Human Mage</td>
      <td>Elf Mage</td>
      <td>Orc Mage</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">10</td>
      <td>11%Speed</td>
      <td>10%Speed</td>
      <td>11%Speed</td>
      <td>10%Speed</td>
      <td>10%Speed</td>
      <td>10%Speed</td>
      <td>10%Speed</td>
      <td>10%Speed</td>
      <td>9%Speed</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>11%Crit</td>
      <td>10%Crit</td>
      <td>11%Crit</td>
      <td>10%Crit</td>
      <td>10%Crit</td>
      <td>10%Crit</td>
      <td>10%Crit</td>
      <td>10%Crit</td>
      <td>9%Crit</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>11%At Spd</td>
      <td>10%At Spd</td>
      <td>11%At Spd</td>
      <td>10%At Spd</td>
      <td>10%At Spd</td>
      <td>10%At Spd</td>
      <td>10%At Spd</td>
      <td>10%At Spd</td>
      <td>9%At Spd</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>4,81 Acc</td>
      <td>5,08 Acc</td>
      <td>4,75 Acc</td>
      <td>5,41 Acc</td>
      <td>5,15 Acc</td>
      <td>5,69 Acc</td>
      <td>5,91 Acc</td>
      <td>5,59 Acc</td>
      <td>6,03 Acc</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>4,81 Eva</td>
      <td>5,08 Eva</td>
      <td>4,75 Eva</td>
      <td>5,41 Eva</td>
      <td>5,15 Eva</td>
      <td>5,69 Eva</td>
      <td>5,91 Eva</td>
      <td>5,59 Eva</td>
      <td>6,03 Eva</td>
    </tr>
    <tr height="17">
      <td colspan="10" height="17"></td>
    </tr>
    <tr height="17">
      <td align="center" height="17">9</td>
      <td>10%Speed</td>
      <td>9%Speed</td>
      <td>10% Speed</td>
      <td>9%Speed</td>
      <td>9%Speed</td>
      <td>9%Speed</td>
      <td>9%Speed</td>
      <td>9%Speed</td>
      <td>8%Speed</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>10%Crit</td>
      <td>9%Crit</td>
      <td>10% Crit</td>
      <td>9%Crit</td>
      <td>9%Crit</td>
      <td>9%Crit</td>
      <td>9%Crit</td>
      <td>9%Crit</td>
      <td>8%Crit</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>10%At Spd</td>
      <td>9%At Spd</td>
      <td>10% At Spd</td>
      <td>9%At Spd</td>
      <td>9%At Spd</td>
      <td>9%At Spd</td>
      <td>9%At Spd</td>
      <td>9%At Spd</td>
      <td>8%At Spd</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>4,36 Acc</td>
      <td>4,6 Acc</td>
      <td>4,3 Acc</td>
      <td>4,9 Acc</td>
      <td>4,67 Acc</td>
      <td>5,17 Acc</td>
      <td>5,37 Acc</td>
      <td>5,07 Acc</td>
      <td>5,48 Acc</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>4,36 Eva</td>
      <td>4,6 Eva</td>
      <td>4,3 Eva</td>
      <td>4,9 Eva</td>
      <td>4,67 Eva</td>
      <td>5,17 Eva</td>
      <td>5,37 Eva</td>
      <td>5,07 Eva</td>
      <td>5,48 Eva</td>
    </tr>
    <tr height="17">
      <td colspan="10" height="17"></td>
    </tr>
    <tr height="17">
      <td align="center" height="17">8</td>
      <td>8%Speed</td>
      <td>8%Speed</td>
      <td>9%Speed</td>
      <td>8%Speed</td>
      <td>8%Speed</td>
      <td>8%Speed</td>
      <td>8%Speed</td>
      <td>8%Speed</td>
      <td>7%Speed</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>8%Crit</td>
      <td>8%Crit</td>
      <td>9%Crit</td>
      <td>8%Crit</td>
      <td>8%Crit</td>
      <td>8%Crit</td>
      <td>8%Crit</td>
      <td>8%Crit</td>
      <td>7%Crit</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>8%At Spd</td>
      <td>8%At Spd</td>
      <td>9%At Spd</td>
      <td>8%At Spd</td>
      <td>8%At Spd</td>
      <td>8%At Spd</td>
      <td>8%At Spd</td>
      <td>8%At Spd</td>
      <td>7%At Spd</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>3,9 Acc</td>
      <td>4,12 Acc</td>
      <td>3,85 Acc</td>
      <td>4,39 Acc</td>
      <td>4,18 Acc</td>
      <td>4,63 Acc</td>
      <td>4,82 Acc</td>
      <td>4,55 Acc</td>
      <td>4,91 Acc</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>3,9 Eva</td>
      <td>4,12 Eva</td>
      <td>3,85 Eva</td>
      <td>4,39 Eva</td>
      <td>4,18 Eva</td>
      <td>4,63 Eva</td>
      <td>4,82 Eva</td>
      <td>4,55 Eva</td>
      <td>4,91 Eva</td>
    </tr>
    <tr height="17">
      <td colspan="10" height="17"></td>
    </tr>
    <tr height="17">
      <td align="center" height="17">7</td>
      <td>7%Speed</td>
      <td>7%Speed</td>
      <td>7%Speed</td>
      <td>7%Speed</td>
      <td>7%Speed</td>
      <td>7%Speed</td>
      <td>7%Speed</td>
      <td>7%Speed</td>
      <td>6%Speed</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>7%Crit</td>
      <td>7%Crit</td>
      <td>7%Crit</td>
      <td>7%Crit</td>
      <td>7%Crit</td>
      <td>7%Crit</td>
      <td>7%Crit</td>
      <td>7%Crit</td>
      <td>6%Crit</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>7%At Spd</td>
      <td>7%At Spd</td>
      <td>7%At Spd</td>
      <td>7%At Spd</td>
      <td>7%At Spd</td>
      <td>7%At Spd</td>
      <td>7%At Spd</td>
      <td>7%At Spd</td>
      <td>6%At Spd</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>3,43 Acc</td>
      <td>3,63 Acc</td>
      <td>3,39 Acc</td>
      <td>3,87 Acc</td>
      <td>3,69 Acc</td>
      <td>4,09 Acc</td>
      <td>4,25 Acc</td>
      <td>4,01 Acc</td>
      <td>4,34 Acc</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>3,43 Eva</td>
      <td>3,63 Eva</td>
      <td>3,39 Eva</td>
      <td>3,87 Eva</td>
      <td>3,69 Eva</td>
      <td>4,09 Eva</td>
      <td>4,25 Eva</td>
      <td>4,01 Eva</td>
      <td>4,34 Eva</td>
    </tr>
    <tr height="17">
      <td colspan="10" height="17"></td>
    </tr>
    <tr height="17">
      <td align="center" height="17">6</td>
      <td>6%Speed</td>
      <td>6%Speed</td>
      <td>6%Speed</td>
      <td>6%Speed</td>
      <td>6%Speed</td>
      <td>6%Speed</td>
      <td>6%Speed</td>
      <td>6%Speed</td>
      <td>5%Speed</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>6%Crit</td>
      <td>6%Crit</td>
      <td>6%Crit</td>
      <td>6%Crit</td>
      <td>6%Crit</td>
      <td>6%Crit</td>
      <td>6%Crit</td>
      <td>6%Crit</td>
      <td>5%Crit</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>6%At Spd</td>
      <td>6%At Spd</td>
      <td>6%At Spd</td>
      <td>6%At Spd</td>
      <td>6%At Spd</td>
      <td>6%At Spd</td>
      <td>6%At Spd</td>
      <td>6%At Spd</td>
      <td>5%At Spd</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>2,96 Acc</td>
      <td>3,14 Acc</td>
      <td>2,92 Acc</td>
      <td>3,35 Acc</td>
      <td>3,18 Acc</td>
      <td>3,54 Acc</td>
      <td>3,68 Acc</td>
      <td>3,47 Acc</td>
      <td>3,76 Acc</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>2,96 Eva</td>
      <td>3,14 Eva</td>
      <td>2,92 Eva</td>
      <td>3,35 Eva</td>
      <td>3,18 Eva</td>
      <td>3,54 Eva</td>
      <td>3,68 Eva</td>
      <td>3,47 Eva</td>
      <td>3,76 Eva</td>
    </tr>
    <tr height="17">
      <td colspan="10" height="17"></td>
    </tr>
    <tr height="17">
      <td align="center" height="17">5</td>
      <td>5%Speed</td>
      <td>5%Speed</td>
      <td>5%Speed</td>
      <td>5%Speed</td>
      <td>5%Speed</td>
      <td>5%Speed</td>
      <td>5%Speed</td>
      <td>5%Speed</td>
      <td>4%Speed</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>5%Crit</td>
      <td>5%Crit</td>
      <td>5%Crit</td>
      <td>5%Crit</td>
      <td>5%Crit</td>
      <td>5%Crit</td>
      <td>5%Crit</td>
      <td>5%Crit</td>
      <td>4%Crit</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>5%At Spd</td>
      <td>5%At Spd</td>
      <td>5%At Spd</td>
      <td>5%At Spd</td>
      <td>5%At Spd</td>
      <td>5%At Spd</td>
      <td>5%At Spd</td>
      <td>5%At Spd</td>
      <td>4%At Spd</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>2,48 Acc</td>
      <td>2,63 Acc</td>
      <td>2,45 Acc</td>
      <td>2,81 Acc</td>
      <td>2,67 Acc</td>
      <td>2,97 Acc</td>
      <td>3,1 Acc</td>
      <td>2,92 Acc</td>
      <td>3,17 Acc</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>2,48 Eva</td>
      <td>2,63 Eva</td>
      <td>2,45 Eva</td>
      <td>2,81 Eva</td>
      <td>2,67 Eva</td>
      <td>2,97 Eva</td>
      <td>3,1 Eva</td>
      <td>2,92 Eva</td>
      <td>3,17 Eva</td>
    </tr>
    <tr height="17">
      <td colspan="10" height="17"></td>
    </tr>
    <tr height="17">
      <td align="center" height="17">4</td>
      <td>4% Speed</td>
      <td>4%Speed</td>
      <td>4%Speed</td>
      <td>4%Speed</td>
      <td>4%Speed</td>
      <td>4%Speed</td>
      <td>4%Speed</td>
      <td>4%Speed</td>
      <td>3%Speed</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>4% Crit</td>
      <td>4%Crit</td>
      <td>4%Crit</td>
      <td>4%Crit</td>
      <td>4%Crit</td>
      <td>4%Crit</td>
      <td>4%Crit</td>
      <td>4%Crit</td>
      <td>3%Crit</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>4% At Spd</td>
      <td>4%At Spd</td>
      <td>4%At Spd</td>
      <td>4%At Spd</td>
      <td>4%At Spd</td>
      <td>4%At Spd</td>
      <td>4%At Spd</td>
      <td>4%At Spd</td>
      <td>3%At Spd</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>2Acc</td>
      <td>2,12 Acc</td>
      <td>1,97 Acc</td>
      <td>2,27 Acc</td>
      <td>2,15 Acc</td>
      <td>2,4 Acc</td>
      <td>2,51 Acc</td>
      <td>2,35 Acc</td>
      <td>2,56 Acc</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>2Eva</td>
      <td>2,12 Eva</td>
      <td>1,97 Eva</td>
      <td>2,27 Eva</td>
      <td>2,15 Eva</td>
      <td>2,4 Eva</td>
      <td>2,51 Eva</td>
      <td>2,35 Eva</td>
      <td>2,56 Eva</td>
    </tr>
    <tr height="17">
      <td colspan="10" height="17"></td>
    </tr>
    <tr height="17">
      <td align="center" height="17">3</td>
      <td>3%Speed</td>
      <td>3%Speed</td>
      <td>3%Speed</td>
      <td>3%Speed</td>
      <td>3%Speed</td>
      <td>3%Speed</td>
      <td>3%Speed</td>
      <td>3%Speed</td>
      <td>2%Speed</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>3%Crit</td>
      <td>3%Crit</td>
      <td>3%Crit</td>
      <td>3%Crit</td>
      <td>3%Crit</td>
      <td>3%Crit</td>
      <td>3%Crit</td>
      <td>3%Crit</td>
      <td>2%Crit</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>3%At Spd</td>
      <td>3%At Spd</td>
      <td>3%At Spd</td>
      <td>3%At Spd</td>
      <td>3%At Spd</td>
      <td>3%At Spd</td>
      <td>3%At Spd</td>
      <td>3%At Spd</td>
      <td>2%At Spd</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>1,51 Acc</td>
      <td>1,6 Acc</td>
      <td>1,49 Acc</td>
      <td>1,72 Acc</td>
      <td>1,63 Acc</td>
      <td>1,82 Acc</td>
      <td>1,9 Acc</td>
      <td>1,78 Acc</td>
      <td>1,94 Acc</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>1,51 Eva</td>
      <td>1,6 Eva</td>
      <td>1,49 Eva</td>
      <td>1,72 Eva</td>
      <td>1,63 Eva</td>
      <td>1,82 Eva</td>
      <td>1,9 Eva</td>
      <td>1,78 Eva</td>
      <td>1,94 Eva</td>
    </tr>
    <tr height="17">
      <td colspan="10" height="17"></td>
    </tr>
    <tr height="17">
      <td align="center" height="17">2</td>
      <td>2%Speed</td>
      <td>2%Speed</td>
      <td>2%Speed</td>
      <td>2%Speed</td>
      <td>2%Speed</td>
      <td>2%Speed</td>
      <td>2%Speed</td>
      <td>2%Speed</td>
      <td>1%Speed</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>2%Crit</td>
      <td>2%Crit</td>
      <td>2%Crit</td>
      <td>2%Crit</td>
      <td>2%Crit</td>
      <td>2%Crit</td>
      <td>2%Crit</td>
      <td>2%Crit</td>
      <td>1%Crit</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>2%At Spd</td>
      <td>2%At Spd</td>
      <td>2%At Spd</td>
      <td>2%At Spd</td>
      <td>2%At Spd</td>
      <td>2%At Spd</td>
      <td>2%At Spd</td>
      <td>2%At Spd</td>
      <td>1%At Spd</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>1,02 Acc</td>
      <td>1,08 Acc</td>
      <td>1 Acc</td>
      <td>1,15 Acc</td>
      <td>1,09 Acc</td>
      <td>1,23 Acc</td>
      <td>1,28 Acc</td>
      <td>1,2 Acc</td>
      <td>1,31 Acc</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>1,02 Eva</td>
      <td>1,08 Eva</td>
      <td>1 Eva</td>
      <td>1,15 Eva</td>
      <td>1,09 Eva</td>
      <td>1,23 Eva</td>
      <td>1,28 Eva</td>
      <td>1,2 Eva</td>
      <td>1,31 Eva</td>
    </tr>
    <tr height="17">
      <td colspan="10" height="17"></td>
    </tr>
    <tr height="17">
      <td align="center" height="17">1</td>
      <td>0,99% Speed</td>
      <td>1%Speed</td>
      <td>1%Speed</td>
      <td>1%Speed</td>
      <td>1%Speed</td>
      <td>1%Speed</td>
      <td>1%Speed</td>
      <td>1%Speed</td>
      <td>0%Speed</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>0,99% Crit</td>
      <td>1%Crit</td>
      <td>1%Crit</td>
      <td>1%Crit</td>
      <td>1%Crit</td>
      <td>1%Crit</td>
      <td>1%Crit</td>
      <td>1%Crit</td>
      <td>0%Crit</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>0,99% At Spd</td>
      <td>1%At Spd</td>
      <td>1%At Spd</td>
      <td>1%At Spd</td>
      <td>1%At Spd</td>
      <td>1%At Spd</td>
      <td>1%At Spd</td>
      <td>1%At Spd</td>
      <td>0%At Spd</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>0,51Acc</td>
      <td>0,54 Acc</td>
      <td>0,5 Acc</td>
      <td>0,58 Acc</td>
      <td>0,54 Acc</td>
      <td>0,62 Acc</td>
      <td>0,65 Acc</td>
      <td>0,61 Acc</td>
      <td>0,66 Acc</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>0,51Eva</td>
      <td>0,54 Eva</td>
      <td>0,5 Eva</td>
      <td>0,58 Eva</td>
      <td>0,54 Eva</td>
      <td>0,62 Eva</td>
      <td>0,65 Eva</td>
      <td>0,61 Eva</td>
      <td>0,66 Eva</td>
    </tr>
    <tr height="17">
      <td colspan="10" height="17"></td>
    </tr>
    <tr height="17">
      <td align="center" height="17">0</td>
      <td>0% Speed</td>
      <td>0% Speed</td>
      <td>0% Speed</td>
      <td>0% Speed</td>
      <td>0% Speed</td>
      <td>0% Speed</td>
      <td>0% Speed</td>
      <td>0% Speed</td>
      <td>0% Speed</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>0% Crit</td>
      <td>0% Crit</td>
      <td>0% Crit</td>
      <td>0% Crit</td>
      <td>0% Crit</td>
      <td>0% Crit</td>
      <td>0% Crit</td>
      <td>0% Crit</td>
      <td>0% Crit</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>0% At Spd</td>
      <td>0% At Spd</td>
      <td>0% At Spd</td>
      <td>0% At Spd</td>
      <td>0% At Spd</td>
      <td>0% At Spd</td>
      <td>0% At Spd</td>
      <td>0% At Spd</td>
      <td>0% At Spd</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>0Acc</td>
      <td>0Acc</td>
      <td>0Acc</td>
      <td>0Acc</td>
      <td>0Acc</td>
      <td>0Acc</td>
      <td>0Acc</td>
      <td>0Acc</td>
      <td>0Acc</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>0Eva</td>
      <td>0Eva</td>
      <td>0Eva</td>
      <td>0Eva</td>
      <td>0Eva</td>
      <td>0Eva</td>
      <td>0Eva</td>
      <td>0Eva</td>
      <td>0Eva</td>
    </tr>
    <tr height="17">
      <td colspan="10" height="17"></td>
    </tr>
    <tr height="17">
      <td align="center" height="17">-1</td>
      <td>-1%Speed</td>
      <td>-1%Speed</td>
      <td>-0,99%Speed</td>
      <td>-1%Speed</td>
      <td>-1%Speed</td>
      <td>-1%Speed</td>
      <td>0%Speed</td>
      <td>-1%Speed</td>
      <td>-1%Speed</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>-1%Crit</td>
      <td>-1%Crit</td>
      <td>-0,99%Crit</td>
      <td>-1%Crit</td>
      <td>-1%Crit</td>
      <td>-1%Crit</td>
      <td>0%Crit</td>
      <td>-1%Crit</td>
      <td>-1%Crit</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>-1%At Spd</td>
      <td>-1%At Spd</td>
      <td>-0,99%At Spd</td>
      <td>-1%At Spd</td>
      <td>-1%At Spd</td>
      <td>-1%At Spd</td>
      <td>0%At Spd</td>
      <td>-1%At Spd</td>
      <td>-1%At Spd</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>-0,51 Acc</td>
      <td>-0,54 Acc</td>
      <td>-0,51 Acc</td>
      <td>-0,59 Acc</td>
      <td>-0,57 Acc</td>
      <td>-0,63 Acc</td>
      <td>-0,66 Acc</td>
      <td>-0,62 Acc</td>
      <td>-0,68 Acc</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>-0,51 Eva</td>
      <td>-0,54 Eva</td>
      <td>-0,51 Eva</td>
      <td>-0,59 Eva</td>
      <td>-0,57 Eva</td>
      <td>-0,63 Eva</td>
      <td>-0,66 Eva</td>
      <td>-0,62 Eva</td>
      <td>-0,68 Eva</td>
    </tr>
    <tr height="17">
      <td colspan="10" height="17"></td>
    </tr>
    <tr height="17">
      <td align="center" height="17">-2</td>
      <td>-2%Speed</td>
      <td>-2%Speed</td>
      <td>-2%Speed</td>
      <td>-2%Speed</td>
      <td>-2%Speed</td>
      <td>-2%Speed</td>
      <td>-1%Speed</td>
      <td>-2%Speed</td>
      <td>-2%Speed</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>-2%Crit</td>
      <td>-2%Crit</td>
      <td>-2%Crit</td>
      <td>-2%Crit</td>
      <td>-2%Crit</td>
      <td>-2%Crit</td>
      <td>-1%Crit</td>
      <td>-2%Crit</td>
      <td>-2%Crit</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>-2%At Spd</td>
      <td>-2%At Spd</td>
      <td>-2%At Spd</td>
      <td>-2%At Spd</td>
      <td>-2%At Spd</td>
      <td>-2%At Spd</td>
      <td>-1%At Spd</td>
      <td>-2%At Spd</td>
      <td>-2%At Spd</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>-1,04 Acc</td>
      <td>-1,12 Acc</td>
      <td>-1,03 Acc</td>
      <td>-1,2 Acc</td>
      <td>-1,14 Acc</td>
      <td>-1,28 Acc</td>
      <td>-1,34 Acc</td>
      <td>-1,25 Acc</td>
      <td>-1,38 Acc</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>-1,04 Eva</td>
      <td>-1,12 Eva</td>
      <td>-1,03 Eva</td>
      <td>-1,2 Eva</td>
      <td>-1,14 Eva</td>
      <td>-1,28 Eva</td>
      <td>-1,34 Eva</td>
      <td>-1,25 Eva</td>
      <td>-1,38 Eva</td>
    </tr>
    <tr height="17">
      <td colspan="10" height="17"></td>
    </tr>
    <tr height="17">
      <td align="center" height="17">-3</td>
      <td>-3%Speed</td>
      <td>-3%Speed</td>
      <td>-3%Speed</td>
      <td>-3%Speed</td>
      <td>-3%Speed</td>
      <td>-2%Speed</td>
      <td>-2%Speed</td>
      <td>-3%Speed</td>
      <td>-3%Speed</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>-3%Crit</td>
      <td>-3%Crit</td>
      <td>-3%Crit</td>
      <td>-3%Crit</td>
      <td>-3%Crit</td>
      <td>-2%Crit</td>
      <td>-2%Crit</td>
      <td>-3%Crit</td>
      <td>-3%Crit</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>-3%At Spd</td>
      <td>-3%At Spd</td>
      <td>-3%At Spd</td>
      <td>-3%At Spd</td>
      <td>-3%At Spd</td>
      <td>-2%At Spd</td>
      <td>-2%At Spd</td>
      <td>-3%At Spd</td>
      <td>-3%At Spd</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>-1,58 Acc</td>
      <td>-1,69 Acc</td>
      <td>-1,55 Acc</td>
      <td>-1,82 Acc</td>
      <td>-1,72 Acc</td>
      <td>-1,94 Acc</td>
      <td>-2,04 Acc</td>
      <td>-1,9 Acc</td>
      <td>-2,1 Acc</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>-1,58 Eva</td>
      <td>-1,69 Eva</td>
      <td>-1,55 Eva</td>
      <td>-1,82 Eva</td>
      <td>-1,72 Eva</td>
      <td>-1,94 Eva</td>
      <td>-2,04 Eva</td>
      <td>-1,9 Eva</td>
      <td>-2,1 Eva</td>
    </tr>
    <tr height="17">
      <td colspan="10" height="17"></td>
    </tr>
    <tr height="17">
      <td align="center" height="17">-4</td>
      <td>-4%Speed</td>
      <td>-4%Speed</td>
      <td>-4%Speed</td>
      <td>-4%Speed</td>
      <td>-4%Speed</td>
      <td>-3%Speed</td>
      <td>-3%Speed</td>
      <td>-3%Speed</td>
      <td>-4%Speed</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>-4%Crit</td>
      <td>-4%Crit</td>
      <td>-4%Crit</td>
      <td>-4%Crit</td>
      <td>-4%Crit</td>
      <td>-3%Crit</td>
      <td>-3%Crit</td>
      <td>-3%Crit</td>
      <td>-4%Crit</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>-4%At Spd</td>
      <td>-4%At Spd</td>
      <td>-4%At Spd</td>
      <td>-4%At Spd</td>
      <td>-4%At Spd</td>
      <td>-3%At Spd</td>
      <td>-3%At Spd</td>
      <td>-3%At Spd</td>
      <td>-4%At Spd</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>-2,12 Acc</td>
      <td>-2,27 Acc</td>
      <td>-2,09 Acc</td>
      <td>-2,45 Acc</td>
      <td>-2,31 Acc</td>
      <td>-2,62 Acc</td>
      <td>-2,76 Acc</td>
      <td>-2,56 Acc</td>
      <td>-2,83 Acc</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>-2,12 Eva</td>
      <td>-2,27 Eva</td>
      <td>-2,09 Eva</td>
      <td>-2,45 Eva</td>
      <td>-2,31 Eva</td>
      <td>-2,62 Eva</td>
      <td>-2,76 Eva</td>
      <td>-2,56 Eva</td>
      <td>-2,83 Eva</td>
    </tr>
    <tr height="17">
      <td colspan="10" height="17"></td>
    </tr>
    <tr height="17">
      <td align="center" height="17">-5</td>
      <td>-5%Speed</td>
      <td>-5%Speed</td>
      <td>-5%Speed</td>
      <td>-5%Speed</td>
      <td>-5%Speed</td>
      <td>-4%Speed</td>
      <td>-4%Speed</td>
      <td>-4%Speed</td>
      <td>-5%Speed</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>-5%Crit</td>
      <td>-5%Crit</td>
      <td>-5%Crit</td>
      <td>-5%Crit</td>
      <td>-5%Crit</td>
      <td>-4%Crit</td>
      <td>-4%Crit</td>
      <td>-4%Crit</td>
      <td>-5%Crit</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>-5%At Spd</td>
      <td>-5%At Spd</td>
      <td>-5%At Spd</td>
      <td>-5%At Spd</td>
      <td>-5%At Spd</td>
      <td>-4%At Spd</td>
      <td>-4%At Spd</td>
      <td>-4%At Spd</td>
      <td>-5%At Spd</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>-2,67 Acc</td>
      <td>-2,86 Acc</td>
      <td>-2,63 Acc</td>
      <td>-3,1 Acc</td>
      <td>-2,92 Acc</td>
      <td>-3,32 Acc</td>
      <td>-3,49 Acc</td>
      <td>-3,24 Acc</td>
      <td>-3,6 Acc</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>-2,67 Eva</td>
      <td>-2,86 Eva</td>
      <td>-2,63 Eva</td>
      <td>-3,1 Eva</td>
      <td>-2,92 Eva</td>
      <td>-3,32 Eva</td>
      <td>-3,49 Eva</td>
      <td>-3,24 Eva</td>
      <td>-3,6 Eva</td>
    </tr>
    <tr height="17">
      <td colspan="10" height="17"></td>
    </tr>
    <tr height="17">
      <td align="center" height="17">-6</td>
      <td>-6%Speed</td>
      <td>-6%Speed</td>
      <td>-6%Speed</td>
      <td>-5%Speed</td>
      <td>-6%Speed</td>
      <td>-5%Speed</td>
      <td>-5%Speed</td>
      <td>-5%Speed</td>
      <td>-6%Speed</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>-6%Crit</td>
      <td>-6%Crit</td>
      <td>-6%Crit</td>
      <td>-5%Crit</td>
      <td>-6%Crit</td>
      <td>-5%Crit</td>
      <td>-5%Crit</td>
      <td>-5%Crit</td>
      <td>-6%Crit</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>-6%At Spd</td>
      <td>-6%At Spd</td>
      <td>-6%At Spd</td>
      <td>-5%At Spd</td>
      <td>-6%At Spd</td>
      <td>-5%At Spd</td>
      <td>-5%At Spd</td>
      <td>-5%At Spd</td>
      <td>-6%At Spd</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>-3,24 Acc</td>
      <td>-3,47 Acc</td>
      <td>-3,18 Acc</td>
      <td>-3,76 Acc</td>
      <td>-3,54 Acc</td>
      <td>-4,04 Acc</td>
      <td>-4,26 Acc</td>
      <td>-3,94 Acc</td>
      <td>-4,39 Acc</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>-3,24 Eva</td>
      <td>-3,47 Eva</td>
      <td>-3,18 Eva</td>
      <td>-3,76 Eva</td>
      <td>-3,54 Eva</td>
      <td>-4,04 Eva</td>
      <td>-4,26 Eva</td>
      <td>-3,94 Eva</td>
      <td>-4,39 Eva</td>
    </tr>
    <tr height="17">
      <td colspan="10" height="17"></td>
    </tr>
    <tr height="17">
      <td align="center" height="17">-7</td>
      <td>-7%Speed</td>
      <td>-7%Speed</td>
      <td>-7%Speed</td>
      <td>-6%Speed</td>
      <td>-7%Speed</td>
      <td>-6%Speed</td>
      <td>-6%Speed</td>
      <td>-6%Speed</td>
      <td>-7%Speed</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>-7%Crit</td>
      <td>-7%Crit</td>
      <td>-7%Crit</td>
      <td>-6%Crit</td>
      <td>-7%Crit</td>
      <td>-6%Crit</td>
      <td>-6%Crit</td>
      <td>-6%Crit</td>
      <td>-7%Crit</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>-7%At Spd</td>
      <td>-7%At Spd</td>
      <td>-7%At Spd</td>
      <td>-6%At Spd</td>
      <td>-7%At Spd</td>
      <td>-6%At Spd</td>
      <td>-6%At Spd</td>
      <td>-6%At Spd</td>
      <td>-7%At Spd</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>-3,81 Acc</td>
      <td>-4,09 Acc</td>
      <td>-3,75 Acc</td>
      <td>-4,44 Acc</td>
      <td>-4,17 Acc</td>
      <td>-4,77 Acc</td>
      <td>-5,05 Acc</td>
      <td>-4,66 Acc</td>
      <td>-5,2 Acc</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>-3,81 Eva</td>
      <td>-4,09 Eva</td>
      <td>-3,75 Eva</td>
      <td>-4,44 Eva</td>
      <td>-4,17 Eva</td>
      <td>-4,77 Eva</td>
      <td>-5,05 Eva</td>
      <td>-4,66 Eva</td>
      <td>-5,2 Eva</td>
    </tr>
    <tr height="17">
      <td colspan="10" height="17"></td>
    </tr>
    <tr height="17">
      <td align="center" height="17">-8</td>
      <td>-8%Speed</td>
      <td>-8%Speed</td>
      <td>-8%Speed</td>
      <td>-7%Speed</td>
      <td>-8%Speed</td>
      <td>-7%Speed</td>
      <td>-7%Speed</td>
      <td>-7%Speed</td>
      <td>-7%Speed</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>-8%Crit</td>
      <td>-8%Crit</td>
      <td>-8%Crit</td>
      <td>-7%Crit</td>
      <td>-8%Crit</td>
      <td>-7%Crit</td>
      <td>-7%Crit</td>
      <td>-7%Crit</td>
      <td>-7%Crit</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>-8%At Spd</td>
      <td>-8%At Spd</td>
      <td>-8%At Spd</td>
      <td>-7%At Spd</td>
      <td>-8%At Spd</td>
      <td>-7%At Spd</td>
      <td>-7%At Spd</td>
      <td>-7%At Spd</td>
      <td>-7%At Spd</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>-4,39 Acc</td>
      <td>-4,72 Acc</td>
      <td>-4,32 Acc</td>
      <td>-5,14 Acc</td>
      <td>-4,82 Acc</td>
      <td>-5,54 Acc</td>
      <td>-5,86 Acc</td>
      <td>-5,39 Acc</td>
      <td>-6,05 Acc</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>-4,39 Eva</td>
      <td>-4,72 Eva</td>
      <td>-4,32 Eva</td>
      <td>-5,14 Eva</td>
      <td>-4,82 Eva</td>
      <td>-5,54 Eva</td>
      <td>-5,86 Eva</td>
      <td>-5,39 Eva</td>
      <td>-6,05 Eva</td>
    </tr>
    <tr height="17">
      <td colspan="10" height="17"></td>
    </tr>
    <tr height="17">
      <td align="center" height="17">-9</td>
      <td>-9%Speed</td>
      <td>-9%Speed</td>
      <td>-9%Speed</td>
      <td>-8%Speed</td>
      <td>-8%Speed</td>
      <td>-8%Speed</td>
      <td>-7%Speed</td>
      <td>-8%Speed</td>
      <td>-8%Speed</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>-9%Crit</td>
      <td>-9%Crit</td>
      <td>-9%Crit</td>
      <td>-8%Crit</td>
      <td>-8%Crit</td>
      <td>-8%Crit</td>
      <td>-7%Crit</td>
      <td>-8%Crit</td>
      <td>-8%Crit</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>-9%At Spd</td>
      <td>-9%At Spd</td>
      <td>-9%At Spd</td>
      <td>-8%At Spd</td>
      <td>-8%At Spd</td>
      <td>-8%At Spd</td>
      <td>-7%At Spd</td>
      <td>-8%At Spd</td>
      <td>-8%At Spd</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>-4,98 Acc</td>
      <td>-5,37 Acc</td>
      <td>-4,9 Acc</td>
      <td>-5,86 Acc</td>
      <td>-5,48 Acc</td>
      <td>-6,33 Acc</td>
      <td>-6,71 Acc</td>
      <td>-6,16 Acc</td>
      <td>-6,94 Acc</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>-4,98 Eva</td>
      <td>-5,37 Eva</td>
      <td>-4,9 Eva</td>
      <td>-5,86 Eva</td>
      <td>-5,48 Eva</td>
      <td>-6,33 Eva</td>
      <td>-6,71 Eva</td>
      <td>-6,16 Eva</td>
      <td>-6,94 Eva</td>
    </tr>
    <tr height="17">
      <td colspan="10" height="17"></td>
    </tr>
    <tr height="17">
      <td align="center" height="17">-10</td>
      <td>-10%Speed</td>
      <td>-9%Speed</td>
      <td>-10%Speed</td>
      <td>-9%Speed</td>
      <td>-9%Speed</td>
      <td>-9%Speed</td>
      <td>-8%Speed</td>
      <td>-9%Speed</td>
      <td>-9%Speed</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>-10%Crit</td>
      <td>-9%Crit</td>
      <td>-10%Crit</td>
      <td>-9%Crit</td>
      <td>-9%Crit</td>
      <td>-9%Crit</td>
      <td>-8%Crit</td>
      <td>-9%Crit</td>
      <td>-9%Crit</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>-10%At Spd</td>
      <td>-9%At Spd</td>
      <td>-10%At Spd</td>
      <td>-9%At Spd</td>
      <td>-9%At Spd</td>
      <td>-9%At Spd</td>
      <td>-8%At Spd</td>
      <td>-9%At Spd</td>
      <td>-9%At Spd</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>-5,59 Acc</td>
      <td>-6,03 Acc</td>
      <td>-5,49 Acc</td>
      <td>-6,59 Acc</td>
      <td>-6,16 Acc</td>
      <td>-7,14 Acc</td>
      <td>-7,6 Acc</td>
      <td>-6,95 Acc</td>
      <td>-7,86 Acc</td>
    </tr>
    <tr height="17">
      <td height="17"></td>
      <td>-5,59 Eva</td>
      <td>-6,03 Eva</td>
      <td>-5,49 Eva</td>
      <td>-6,59 Eva</td>
      <td>-6,16 Eva</td>
      <td>-7,14 Eva</td>
      <td>-7,6 Eva</td>
      <td>-6,95 Eva</td>
      <td>-7,86 Eva</td>
    </tr>
    <tr height="17">
      <td colspan="10" height="17"></td>
    </tr>
    <tr height="17">
      <td height="17">CON</td>
      <td>DE Fighter</td>
      <td>Human Fighter</td>
      <td>Elf Fighter</td>
      <td>Orc Fighter</td>
      <td>Dwarf Fighter</td>
      <td>DE Mage</td>
      <td>Human Mage</td>
      <td>Elf Mage</td>
      <td>Orc Mage</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">10</td>
      <td>10920 Weight</td>
      <td>13000 Weight</td>
      <td>11960 Weight</td>
      <td>14040 Weight</td>
      <td>13520 Weight</td>
      <td>9880 Weight</td>
      <td>10400 Weight</td>
      <td>10400 Weight</td>
      <td>10920 Weight</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">9</td>
      <td>9880 Weight</td>
      <td>11440 Weight</td>
      <td>10400 Weight</td>
      <td>12480 Weight</td>
      <td>12480 Weight</td>
      <td>8840 Weight</td>
      <td>9360 Weight</td>
      <td>9360 Weight</td>
      <td>9880 Weight</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">8</td>
      <td>8840 Weight</td>
      <td>9880 Weight</td>
      <td>9360 Weight</td>
      <td>10920 Weight</td>
      <td>10920 Weight</td>
      <td>7800 Weight</td>
      <td>8320 Weight</td>
      <td>8320 Weight</td>
      <td>8320 Weight</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">7</td>
      <td>7280 Weight</td>
      <td>8840 Weight</td>
      <td>8320 Weight</td>
      <td>9880 Weight</td>
      <td>9360 Weight</td>
      <td>6760 Weight</td>
      <td>7280 Weight</td>
      <td>7280 Weight</td>
      <td>7280 Weight</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">6</td>
      <td>6240 Weight</td>
      <td>7280 Weight</td>
      <td>6760 Weight</td>
      <td>8320 Weight</td>
      <td>7800 Weight</td>
      <td>5720 Weight</td>
      <td>6240 Weight</td>
      <td>6240 Weight</td>
      <td>6240 Weight</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">5</td>
      <td>5200 Weight</td>
      <td>6240 Weight</td>
      <td>5720 Weight</td>
      <td>6760 Weight</td>
      <td>6760 Weight</td>
      <td>4680 Weight</td>
      <td>5200 Weight</td>
      <td>5200 Weight</td>
      <td>5200 Weight</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">4</td>
      <td>4160 Weight</td>
      <td>4680 Weight</td>
      <td>4680 Weight</td>
      <td>5200 Weight</td>
      <td>5200 Weight</td>
      <td>3640 Weight</td>
      <td>4160 Weight</td>
      <td>4160 Weight</td>
      <td>4160 Weight</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">3</td>
      <td>3120 Weight</td>
      <td>3640 Weight</td>
      <td>3120 Weight</td>
      <td>4160 Weight</td>
      <td>4160 Weight</td>
      <td>2600 Weight</td>
      <td>3120 Weight</td>
      <td>3120 Weight</td>
      <td>3120 Weight</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">2</td>
      <td>2080 Weight</td>
      <td>2080 Weight</td>
      <td>2080 Weight</td>
      <td>2600 Weight</td>
      <td>2600 Weight</td>
      <td>1560 Weight</td>
      <td>2080 Weight</td>
      <td>2080 Weight</td>
      <td>2080 Weight</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">1</td>
      <td>1040 Weight</td>
      <td>1040 Weight</td>
      <td>1040 Weight</td>
      <td>1560 Weight</td>
      <td>1560 Weight</td>
      <td>520 Weight</td>
      <td>1040 Weight</td>
      <td>1040 Weight</td>
      <td>1040 Weight</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">0</td>
      <td>0 Weight</td>
      <td>0 Weight</td>
      <td>0 Weight</td>
      <td>0 Weight</td>
      <td>0 Weight</td>
      <td>0 Weight</td>
      <td>0 Weight</td>
      <td>0 Weight</td>
      <td>0 Weight</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">-1</td>
      <td>-1040 Weight</td>
      <td>-1560 Weight</td>
      <td>-1040 Weight</td>
      <td>-1040 Weight</td>
      <td>-1040 Weight</td>
      <td>-1040 Weight</td>
      <td>-1040 Weight</td>
      <td>-520 Weight</td>
      <td>-1040 Weight</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">-2</td>
      <td>-2080 Weight</td>
      <td>-2600 Weight</td>
      <td>-2080 Weight</td>
      <td>-2600 Weight</td>
      <td>-2080 Weight</td>
      <td>-2080 Weight</td>
      <td>-2080 Weight</td>
      <td>-1560 Weight</td>
      <td>-2080 Weight</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">-3</td>
      <td>-3120 Weight</td>
      <td>-3640 Weight</td>
      <td>-3120 Weight</td>
      <td>-3640 Weight</td>
      <td>-3640 Weight</td>
      <td>-2600 Weight</td>
      <td>-2600 Weight</td>
      <td>-2600 Weight</td>
      <td>-3120 Weight</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">-4</td>
      <td>-4160 Weight</td>
      <td>-5200 Weight</td>
      <td>-4160 Weight</td>
      <td>-4680 Weight</td>
      <td>-4680 Weight</td>
      <td>-3640 Weight</td>
      <td>-3640 Weight</td>
      <td>-3120 Weight</td>
      <td>-4160 Weight</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">-5</td>
      <td>-5200 Weight</td>
      <td>-6240 Weight</td>
      <td>-5200 Weight</td>
      <td>-6240 Weight</td>
      <td>-5720 Weight</td>
      <td>-4680 Weight</td>
      <td>-4680 Weight</td>
      <td>-4160 Weight</td>
      <td>-5200 Weight</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">-6</td>
      <td>-6240 Weight</td>
      <td>-7280 Weight</td>
      <td>-6240 Weight</td>
      <td>-7280 Weight</td>
      <td>-7280 Weight</td>
      <td>-5200 Weight</td>
      <td>-5200 Weight</td>
      <td>-5200 Weight</td>
      <td>-6240 Weight</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">-7</td>
      <td>-7280 Weight</td>
      <td>-8320 Weight</td>
      <td>-7280 Weight</td>
      <td>-8320 Weight</td>
      <td>-8320 Weight</td>
      <td>-6240 Weight</td>
      <td>-6240 Weight</td>
      <td>-5720 Weight</td>
      <td>-6760 Weight</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">-8</td>
      <td>-7800 Weight</td>
      <td>-9360 Weight</td>
      <td>-8320 Weight</td>
      <td>-9880 Weight</td>
      <td>-9360 Weight</td>
      <td>-7280 Weight</td>
      <td>-7280 Weight</td>
      <td>-6760 Weight</td>
      <td>-7800 Weight</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">-9</td>
      <td>-8840 Weight</td>
      <td>-10400 Weight</td>
      <td>-9360 Weight</td>
      <td>-10920 Weight</td>
      <td>-10400 Weight</td>
      <td>-7800 Weight</td>
      <td>-7800 Weight</td>
      <td>-7800 Weight</td>
      <td>-8840 Weight</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">-10</td>
      <td>-9880 Weight</td>
      <td>-11440 Weight</td>
      <td>-10400 Weight</td>
      <td>-11960 Weight</td>
      <td>-11440 Weight</td>
      <td>-8840 Weight</td>
      <td>-8840 Weight</td>
      <td>-8320 Weight</td>
      <td>-9360 Weight</td>
    </tr>
    <tr height="17">
      <td colspan="10" height="17"></td>
    </tr>
    <tr height="17">
      <td height="17">INT</td>
      <td>DE Fighter</td>
      <td>Human Fighter</td>
      <td>Elf Fighter</td>
      <td>Orc Fighter</td>
      <td>Dwarf Fighter</td>
      <td>DE Mage</td>
      <td>Human Mage</td>
      <td>Elf Mage</td>
      <td>Orc Mage</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">10</td>
      <td>37,05% MAtk</td>
      <td>32,40% MAtk</td>
      <td>33,84% MAtk</td>
      <td>29,07% MAtk</td>
      <td>30,09% MAtk</td>
      <td>82,65% MAtk</td>
      <td>69,68% MAtk</td>
      <td>59,52% MAtk</td>
      <td>48,40% MAtk</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">9</td>
      <td>32,81% MAtk</td>
      <td>28,48% MAtk</td>
      <td>29,76% MAtk</td>
      <td>25,35% MAtk</td>
      <td>26,25% MAtk</td>
      <td>70,25% MAtk</td>
      <td>63,84% MAtk</td>
      <td>54,12% MAtk</td>
      <td>43,60% MAtk</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">8</td>
      <td>28,65% MAtk</td>
      <td>24,64% MAtk</td>
      <td>25,76% MAtk</td>
      <td>21,71% MAtk</td>
      <td>24,36% MAtk</td>
      <td>61,16% MAtk</td>
      <td>55,23% MAtk</td>
      <td>46,17% MAtk</td>
      <td>36,55% MAtk</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">7</td>
      <td>24,57% MAtk</td>
      <td>22,75% MAtk</td>
      <td>21,84% MAtk</td>
      <td>18,15% MAtk</td>
      <td>20,64% MAtk</td>
      <td>52,25% MAtk</td>
      <td>46,8% MAtk</td>
      <td>38,40% MAtk</td>
      <td>31,95% MAtk</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">6</td>
      <td>20,57% MAtk</td>
      <td>19,03% MAtk</td>
      <td>18,00% MAtk</td>
      <td>14,67% MAtk</td>
      <td>17,00% MAtk</td>
      <td>46,41% MAtk</td>
      <td>38,55% MAtk</td>
      <td>33,32% MAtk</td>
      <td>27,43% MAtk</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">5</td>
      <td>16,65% MAtk</td>
      <td>15,39% MAtk</td>
      <td>16,11% MAtk</td>
      <td>12,96% MAtk</td>
      <td>13,44% MAtk</td>
      <td>37,80% MAtk</td>
      <td>33,15% MAtk</td>
      <td>25,85% MAtk</td>
      <td>22,99% MAtk</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">4</td>
      <td>12,81% MAtk</td>
      <td>11,83% MAtk</td>
      <td>12,39% MAtk</td>
      <td>9,60% MAtk</td>
      <td>9,96% MAtk</td>
      <td>29,37% MAtk</td>
      <td>25,2% MAtk</td>
      <td>20,97% MAtk</td>
      <td>16,48% MAtk</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">3</td>
      <td>10,92% MAtk</td>
      <td>8,35% MAtk</td>
      <td>8,75% MAtk</td>
      <td>6,32% MAtk</td>
      <td>8,25% MAtk</td>
      <td>21,12% MAtk</td>
      <td>17,43% MAtk</td>
      <td>16,17% MAtk</td>
      <td>12,24% MAtk</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">2</td>
      <td>7,20% MAtk</td>
      <td>6,64% MAtk</td>
      <td>5,19% MAtk</td>
      <td>4,71% MAtk</td>
      <td>4,89% MAtk</td>
      <td>15,72% MAtk</td>
      <td>12,35% MAtk</td>
      <td>9,12% MAtk</td>
      <td>8,08% MAtk</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">1</td>
      <td>3,56% MAtk</td>
      <td>3,27% MAtk</td>
      <td>1,71% MAtk</td>
      <td>1,55% MAtk</td>
      <td>1,61% MAtk</td>
      <td>7,77% MAtk</td>
      <td>4,88% MAtk</td>
      <td>4,52% MAtk</td>
      <td>4% MAtk</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">0</td>
      <td>0% MAtk</td>
      <td>0% MAtk</td>
      <td>0% MAtk</td>
      <td>0% MAtk</td>
      <td>0% MAtk</td>
      <td>0% MAtk</td>
      <td>0% MAtk</td>
      <td>0% MAtk</td>
      <td>0% MAtk</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">-1</td>
      <td>-3,48% MAtk</td>
      <td>-1,61% MAtk</td>
      <td>-3,36% MAtk</td>
      <td>-3,04% MAtk</td>
      <td>-3,16% MAtk</td>
      <td>-5,08% MAtk</td>
      <td>-4,8% MAtk</td>
      <td>-4,44% MAtk</td>
      <td>-3,92% MAtk</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">-2</td>
      <td>-5,19% MAtk</td>
      <td>-4,77% MAtk</td>
      <td>-6,64% MAtk</td>
      <td>-4,53% MAtk</td>
      <td>-4,71% MAtk</td>
      <td>-12,55% MAtk</td>
      <td>-11,85% MAtk</td>
      <td>-10,95% MAtk</td>
      <td>-7,76% MAtk</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">-3</td>
      <td>-8,55% MAtk</td>
      <td>-6,32% MAtk</td>
      <td>-8,25% MAtk</td>
      <td>-7,45% MAtk</td>
      <td>-7,75% MAtk</td>
      <td>-17,43% MAtk</td>
      <td>-16,45% MAtk</td>
      <td>-15,19% MAtk</td>
      <td>-9,65% MAtk</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">-4</td>
      <td>-11,83% MAtk</td>
      <td>-9,36% MAtk</td>
      <td>-11,41% MAtk</td>
      <td>-8,88% MAtk</td>
      <td>-9,24% MAtk</td>
      <td>-22,23% MAtk</td>
      <td>-20,97% MAtk</td>
      <td>-19,35% MAtk</td>
      <td>-13,37% MAtk</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">-5</td>
      <td>-13,44% MAtk</td>
      <td>-10,85% MAtk</td>
      <td>-12,96% MAtk</td>
      <td>-11,68% MAtk</td>
      <td>-12,16% MAtk</td>
      <td>-29,28% MAtk</td>
      <td>-25,41% MAtk</td>
      <td>-23,43% MAtk</td>
      <td>-17,01% MAtk</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">-6</td>
      <td>-16,60% MAtk</td>
      <td>-13,77% MAtk</td>
      <td>-16,00% MAtk</td>
      <td>-13,05% MAtk</td>
      <td>-13,59% MAtk</td>
      <td>-33,88% MAtk</td>
      <td>-31,92% MAtk</td>
      <td>-27,43% MAtk</td>
      <td>-20,57% MAtk</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">-7</td>
      <td>-18,15% MAtk</td>
      <td>-15,2% MAtk</td>
      <td>-17,49% MAtk</td>
      <td>-14,40% MAtk</td>
      <td>-16,39% MAtk</td>
      <td>-38,40% MAtk</td>
      <td>-36,16% MAtk</td>
      <td>-31,35% MAtk</td>
      <td>-24,05% MAtk</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">-8</td>
      <td>-21,19% MAtk</td>
      <td>-18% MAtk</td>
      <td>-20,41% MAtk</td>
      <td>-17,04% MAtk</td>
      <td>-17,76% MAtk</td>
      <td>-42,84% MAtk</td>
      <td>-40,32% MAtk</td>
      <td>-35,19% MAtk</td>
      <td>-25,76% MAtk</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">-9</td>
      <td>-22,68% MAtk</td>
      <td>-19,37% MAtk</td>
      <td>-21,84% MAtk</td>
      <td>-18,33% MAtk</td>
      <td>-19,11% MAtk</td>
      <td>-49,35% MAtk</td>
      <td>-44,40% MAtk</td>
      <td>-37,08% MAtk</td>
      <td>-29,12% MAtk</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">-10</td>
      <td>-25,60% MAtk</td>
      <td>-20,72% MAtk</td>
      <td>-24,64% MAtk</td>
      <td>-19,60% MAtk</td>
      <td>-21,75% MAtk</td>
      <td>-53,59% MAtk</td>
      <td>-48,40% MAtk</td>
      <td>-40,80% MAtk</td>
      <td>-32,40% MAtk</td>
    </tr>
    <tr height="17">
      <td colspan="10" height="17"></td>
    </tr>
    <tr height="17">
      <td height="17">WIT</td>
      <td>DE Fighter</td>
      <td>Human Fighter</td>
      <td>Elf Fighter</td>
      <td>Orc Fighter</td>
      <td>Dwarf Fighter</td>
      <td>DE Mage</td>
      <td>Human Mage</td>
      <td>Elf Mage</td>
      <td>Orc Mage</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">10</td>
      <td>42% C.Speed</td>
      <td>41% C.Speed</td>
      <td>47% C.Speed</td>
      <td>42% C.Speed</td>
      <td>39% C.Speed</td>
      <td>60% C.Speed</td>
      <td>63% C.Speed</td>
      <td>73% C.Speed</td>
      <td>66% C.Speed</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">9</td>
      <td>37% C.Speed</td>
      <td>36% C.Speed</td>
      <td>41% C.Speed</td>
      <td>37% C.Speed</td>
      <td>34% C.Speed</td>
      <td>53% C.Speed</td>
      <td>55% C.Speed</td>
      <td>64% C.Speed</td>
      <td>58% C.Speed</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">8</td>
      <td>32% C.Speed</td>
      <td>31% C.Speed</td>
      <td>35% C.Speed</td>
      <td>32% C.Speed</td>
      <td>30% C.Speed</td>
      <td>46% C.Speed</td>
      <td>48% C.Speed</td>
      <td>55% C.Speed</td>
      <td>50% C.Speed</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">7</td>
      <td>27% C.Speed</td>
      <td>27% C.Speed</td>
      <td>30% C.Speed</td>
      <td>27% C.Speed</td>
      <td>25% C.Speed</td>
      <td>39% C.Speed</td>
      <td>41% C.Speed</td>
      <td>47% C.Speed</td>
      <td>43% C.Speed</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">6</td>
      <td>23% C.Speed</td>
      <td>22% C.Speed</td>
      <td>25% C.Speed</td>
      <td>23% C.Speed</td>
      <td>21% C.Speed</td>
      <td>33% C.Speed</td>
      <td>34% C.Speed</td>
      <td>39% C.Speed</td>
      <td>36% C.Speed</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">5</td>
      <td>18% C.Speed</td>
      <td>18% C.Speed</td>
      <td>20% C.Speed</td>
      <td>18% C.Speed</td>
      <td>17% C.Speed</td>
      <td>27% C.Speed</td>
      <td>28% C.Speed</td>
      <td>32% C.Speed</td>
      <td>29% C.Speed</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">4</td>
      <td>14% C.Speed</td>
      <td>14% C.Speed</td>
      <td>16% C.Speed</td>
      <td>14% C.Speed</td>
      <td>14% C.Speed</td>
      <td>21% C.Speed</td>
      <td>22% C.Speed</td>
      <td>25% C.Speed</td>
      <td>23% C.Speed</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">3</td>
      <td>10% C.Speed</td>
      <td>11% C.Speed</td>
      <td>11% C.Speed</td>
      <td>10% C.Speed</td>
      <td>10% C.Speed</td>
      <td>15% C.Speed</td>
      <td>16% C.Speed</td>
      <td>18% C.Speed</td>
      <td>17% C.Speed</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">2</td>
      <td>7% C.Speed</td>
      <td>7% C.Speed</td>
      <td>7% C.Speed</td>
      <td>7% C.Speed</td>
      <td>7% C.Speed</td>
      <td>10% C.Speed</td>
      <td>10% C.Speed</td>
      <td>12% C.Speed</td>
      <td>11% C.Speed</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">1</td>
      <td>3% C.Speed</td>
      <td>4% C.Speed</td>
      <td>3% C.Speed</td>
      <td>3% C.Speed</td>
      <td>3% C.Speed</td>
      <td>5% C.Speed</td>
      <td>5% C.Speed</td>
      <td>6% C.Speed</td>
      <td>5% C.Speed</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">0</td>
      <td>0% C.Speed</td>
      <td>0% C.Speed</td>
      <td>0% C.Speed</td>
      <td>0% C.Speed</td>
      <td>0% C.Speed</td>
      <td>0% C.Speed</td>
      <td>0% C.Speed</td>
      <td>0% C.Speed</td>
      <td>0% C.Speed</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">-1</td>
      <td>-4% C.Speed</td>
      <td>-3% C.Speed</td>
      <td>-4% C.Speed</td>
      <td>-4% C.Speed</td>
      <td>-3% C.Speed</td>
      <td>-4% C.Speed</td>
      <td>-5% C.Speed</td>
      <td>-6% C.Speed</td>
      <td>-5% C.Speed</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">-2</td>
      <td>-7% C.Speed</td>
      <td>-6% C.Speed</td>
      <td>-7% C.Speed</td>
      <td>-7% C.Speed</td>
      <td>-5% C.Speed</td>
      <td>-9% C.Speed</td>
      <td>-9% C.Speed</td>
      <td>-11% C.Speed</td>
      <td>-10% C.Speed</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">-3</td>
      <td>-10% C.Speed</td>
      <td>-8% C.Speed</td>
      <td>-11% C.Speed</td>
      <td>-10% C.Speed</td>
      <td>-8% C.Speed</td>
      <td>-13% C.Speed</td>
      <td>-14% C.Speed</td>
      <td>-16% C.Speed</td>
      <td>-14% C.Speed</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">-4</td>
      <td>-12% C.Speed</td>
      <td>-11% C.Speed</td>
      <td>-14% C.Speed</td>
      <td>-12% C.Speed</td>
      <td>-10% C.Speed</td>
      <td>-17% C.Speed</td>
      <td>-18% C.Speed</td>
      <td>-21% C.Speed</td>
      <td>-19% C.Speed</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">-5</td>
      <td>-15% C.Speed</td>
      <td>-13% C.Speed</td>
      <td>-17% C.Speed</td>
      <td>-15% C.Speed</td>
      <td>-13% C.Speed</td>
      <td>-20% C.Speed</td>
      <td>-22% C.Speed</td>
      <td>-25% C.Speed</td>
      <td>-23% C.Speed</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">-6</td>
      <td>-17% C.Speed</td>
      <td>-16% C.Speed</td>
      <td>-19% C.Speed</td>
      <td>-17% C.Speed</td>
      <td>-15% C.Speed</td>
      <td>-24% C.Speed</td>
      <td>-25% C.Speed</td>
      <td>-30% C.Speed</td>
      <td>-27% C.Speed</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">-7</td>
      <td>-20% C.Speed</td>
      <td>-18% C.Speed</td>
      <td>-22% C.Speed</td>
      <td>-20% C.Speed</td>
      <td>-17% C.Speed</td>
      <td>-27% C.Speed</td>
      <td>-29% C.Speed</td>
      <td>-34% C.Speed</td>
      <td>-30% C.Speed</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">-8</td>
      <td>-22% C.Speed</td>
      <td>-20% C.Speed</td>
      <td>-24% C.Speed</td>
      <td>-22% C.Speed</td>
      <td>-19% C.Speed</td>
      <td>-31% C.Speed</td>
      <td>-32% C.Speed</td>
      <td>-38% C.Speed</td>
      <td>-34% C.Speed</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">-9</td>
      <td>-24% C.Speed</td>
      <td>-22% C.Speed</td>
      <td>-27% C.Speed</td>
      <td>-24% C.Speed</td>
      <td>-21% C.Speed</td>
      <td>-34% C.Speed</td>
      <td>-36% C.Speed</td>
      <td>-41% C.Speed</td>
      <td>-37% C.Speed</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">-10</td>
      <td>-26% C.Speed</td>
      <td>-24% C.Speed</td>
      <td>-29% C.Speed</td>
      <td>-26% C.Speed</td>
      <td>-</td>
      <td>-37% C.Speed</td>
      <td>-39% C.Speed</td>
      <td>-45% C.Speed</td>
      <td>-41% C.Speed</td>
    </tr>
    <tr height="17">
      <td colspan="10" height="17"></td>
    </tr>
    <tr height="17">
      <td height="17">MEN</td>
      <td>DE Fighter</td>
      <td>Human Fighter</td>
      <td>Elf Fighter</td>
      <td>Orc Fighter</td>
      <td>Dwarf Fighter</td>
      <td>DE Mage</td>
      <td>Human Mage</td>
      <td>Elf Mage</td>
      <td>Orc Mage</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">10</td>
      <td>13% MDef</td>
      <td>14% MDef</td>
      <td>13% MDef</td>
      <td>14% MDef</td>
      <td>14% MDef</td>
      <td>15% MDef</td>
      <td>15% MDef</td>
      <td>16% MDef</td>
      <td>16% MDef</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">9</td>
      <td>12% MDef</td>
      <td>12% MDef</td>
      <td>12% MDef</td>
      <td>12% MDef</td>
      <td>12% MDef</td>
      <td>13% MDef</td>
      <td>13% MDef</td>
      <td>14% MDef</td>
      <td>14% MDef</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">8</td>
      <td>10% MDef</td>
      <td>11% MDef</td>
      <td>10% MDef</td>
      <td>11% MDef</td>
      <td>11% MDef</td>
      <td>12% MDef</td>
      <td>12% MDef</td>
      <td>12% MDef</td>
      <td>13% MDef</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">7</td>
      <td>9% MDef</td>
      <td>10% MDef</td>
      <td>9% MDef</td>
      <td>9% MDef</td>
      <td>9% MDef</td>
      <td>10% MDef</td>
      <td>10% MDef</td>
      <td>11% MDef</td>
      <td>11% MDef</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">6</td>
      <td>8% MDef</td>
      <td>8% MDef</td>
      <td>8% MDef</td>
      <td>8% MDef</td>
      <td>8% MDef</td>
      <td>8% MDef</td>
      <td>9% MDef</td>
      <td>9% MDef</td>
      <td>9% MDef</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">5</td>
      <td>6% MDef</td>
      <td>7% MDef</td>
      <td>6% MDef</td>
      <td>7% MDef</td>
      <td>7% MDef</td>
      <td>7% MDef</td>
      <td>7% MDef</td>
      <td>8% MDef</td>
      <td>8% MDef</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">4</td>
      <td>5% MDef</td>
      <td>6% MDef</td>
      <td>5% MDef</td>
      <td>5% MDef</td>
      <td>5% MDef</td>
      <td>5% MDef</td>
      <td>5% MDef</td>
      <td>6% MDef</td>
      <td>6% MDef</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">3</td>
      <td>4% MDef</td>
      <td>4% MDef</td>
      <td>4% MDef</td>
      <td>4% MDef</td>
      <td>4% MDef</td>
      <td>4% MDef</td>
      <td>4% MDef</td>
      <td>4% MDef</td>
      <td>5% MDef</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">2</td>
      <td>2% MDef</td>
      <td>3% MDef</td>
      <td>2% MDef</td>
      <td>3% MDef</td>
      <td>3% MDef</td>
      <td>3% MDef</td>
      <td>2% MDef</td>
      <td>3% MDef</td>
      <td>3% MDef</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">1</td>
      <td>1% MDef</td>
      <td>2% MDef</td>
      <td>1% MDef</td>
      <td>1% MDef</td>
      <td>1% MDef</td>
      <td>1% MDef</td>
      <td>1% MDef</td>
      <td>1% MDef</td>
      <td>1% MDef</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">0</td>
      <td>0% MDef</td>
      <td>0% MDef</td>
      <td>0% MDef</td>
      <td>0% MDef</td>
      <td>0% MDef</td>
      <td>0% MDef</td>
      <td>0% MDef</td>
      <td>0% MDef</td>
      <td>0% MDef</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">-1</td>
      <td>-2% MDef</td>
      <td>-1% MDef</td>
      <td>-2% MDef</td>
      <td>-1% MDef</td>
      <td>-1% MDef</td>
      <td>-2% MDef</td>
      <td>-2% MDef</td>
      <td>-1% MDef</td>
      <td>-2% MDef</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">-2</td>
      <td>-3% MDef</td>
      <td>-2% MDef</td>
      <td>-3% MDef</td>
      <td>-3% MDef</td>
      <td>-3% MDef</td>
      <td>-3% MDef</td>
      <td>-3% MDef</td>
      <td>-3% MDef</td>
      <td>-3% MDef</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">-3</td>
      <td>-4% MDef</td>
      <td>-3% MDef</td>
      <td>-4% MDef</td>
      <td>-4% MDef</td>
      <td>-4% MDef</td>
      <td>-5% MDef</td>
      <td>-5% MDef</td>
      <td>-4% MDef</td>
      <td>-4% MDef</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">-4</td>
      <td>-5% MDef</td>
      <td>-5% MDef</td>
      <td>-5% MDef</td>
      <td>-5% MDef</td>
      <td>-5% MDef</td>
      <td>-6% MDef</td>
      <td>-6% MDef</td>
      <td>-6% MDef</td>
      <td>-6% MDef</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">-5</td>
      <td>-7% MDef</td>
      <td>-6% MDef</td>
      <td>-7% MDef</td>
      <td>-6% MDef</td>
      <td>-6% MDef</td>
      <td>-7% MDef</td>
      <td>-8% MDef</td>
      <td>-7% MDef</td>
      <td>-7% MDef</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">-6</td>
      <td>-8% MDef</td>
      <td>-7% MDef</td>
      <td>-8% MDef</td>
      <td>-8% MDef</td>
      <td>-8% MDef</td>
      <td>-9% MDef</td>
      <td>-9% MDef</td>
      <td>-9% MDef</td>
      <td>-9% MDef</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">-7</td>
      <td>-9% MDef</td>
      <td>-8% MDef</td>
      <td>-9% MDef</td>
      <td>-9% MDef</td>
      <td>-9% MDef</td>
      <td>-10% MDef</td>
      <td>-10% MDef</td>
      <td>-10% MDef</td>
      <td>-10% MDef</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">-8</td>
      <td>-10% MDef</td>
      <td>-9% MDef</td>
      <td>-10% MDef</td>
      <td>-10% MDef</td>
      <td>-10% MDef</td>
      <td>-11% MDef</td>
      <td>-12% MDef</td>
      <td>-11% MDef</td>
      <td>-12% MDef</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">-9</td>
      <td>-11% MDef</td>
      <td>-11% MDef</td>
      <td>-11% MDef</td>
      <td>-11% MDef</td>
      <td>-11% MDef</td>
      <td>-13% MDef</td>
      <td>-13% MDef</td>
      <td>-13% MDef</td>
      <td>-13% MDef</td>
    </tr>
    <tr height="17">
      <td align="center" height="17">-10</td>
      <td>-13% MDef</td>
      <td>-12% MDef</td>
      <td>-13% MDef</td>
      <td>-12% MDef</td>
      <td>-12% MDef</td>
      <td>-14% MDef</td>
      <td>-14% MDef</td>
      <td>-14% MDef</td>
      <td>-14% MDef</td>
    </tr>
  </tbody>
</table>
<br />
<br />
<br />
<?
}else{
	include "error.php";
}
?>