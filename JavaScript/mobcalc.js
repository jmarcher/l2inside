onerror=handleErr
var txt=""

var d=new _pdocument();

function _pdocument()
{
	var _alldata=new Array();
	function _getElementById(id) {
		if (_alldata[id]) {			
			return _alldata[id];
		}
		else {
			var s=document.getElementById(id);
			_alldata[id]=s;
			return _alldata[id];
		}
	}
	this.gI=_getElementById;

}

function handleErr(msg,url,l)
{
txt="There was an error on this page.\n\n"
txt+="Error: " + msg + "\n"
txt+="URL: " + url + "\n"
txt+="Line: " + l + "\n\n"
txt+="Click OK to continue.\n\n"
alert(txt)
return true
}

//Summon Stats
var SIEGEGOLEMSLVL=new Array("49+","p")
var SIEGEGOLEMBLVL=new Array(55,"p")
var SIEGEGOLEMHP=new Array(500000,"p")
var SIEGEGOLEMMP=new Array(889.8,"p")
var SIEGEGOLEMPATK=new Array(1500,"p")
var SIEGEGOLEMMATK=new Array(265,"p")
var SIEGEGOLEMPDEF=new Array(837,"p")
var SIEGEGOLEMMDEF=new Array(674,"p")




//Buffs
var ACUMEN=new Array(0,1.15,1.23,1.3)
var AGILITY=new Array(0,2,3,4)
var BTB=new Array(0,1.1,1.15,1.2,1.25,1.3,1.35)
var BTS=new Array(0,1.1,1.15,1.2,1.25,1.3,1.35)
var CATBUFFS=new Array(0,1.06,1.08,1.1,0.25,0.27,0.3)
var CURSEOFSHADE=new Array(0,0.94,0.92,0.9)
var DEACC=new Array(0,12,13)
var DEASPD=new Array(0,0.83,0.8,0.77)
var DEMONICBD=new Array(0,0,0.85,0,0,0.7)
var EMPOWER=new Array(0,1.55,1.65,1.75,1.44)
var FOCUS=new Array(0,0.2,0.25,0.3)
var GREATERMIGHT=new Array(0,1.04,1.07,1.10)
var GREATERSHIELD=new Array(0,1.05,1.10,1.15)
var GUIDANCE=new Array(0,2,3,4)
var HASTE=new Array(0,1.15,1.33)
var HEX=new Array(0,0.77)
var HSCHOLACC=new Array(0,3,6,8,10,6,0,0,0,0,0)
var HSCHOLEVA=new Array(0,0,-3,-3,-3,-5,-5,-5,-8,-9,-10)
var HSMALARIA=new Array(0,1.04,1.08,1.12,1.16,1.08,1,1,1,1,1)
var MAGICBARRIER=new Array(0,1.15,1.23,1.3)
var MIGHT=new Array(0,1.08,1.12,1.15,1.25)
var PROPHECYHP=new Array(0,1.2,1.2,1,1)
var PROPHECYPATK=new Array(0,1.1,1.1,1,1)
var PROPHECYMATK=new Array(0,1.2,1,1.2,1)
var PROPHECYPDEF=new Array(0,1.2,1.2,1,1)
var PROPHECYMDEF=new Array(0,1.2,1,1.2,1)
var PROPHECYACC=new Array(0,4,4,0,4)
var PROPHECYEVA=new Array(0,0,0,0,4)
var PROPHECYSPEED=new Array(0,0.8,0.9,0.8,1)
var PROPHECYCRIT=new Array(1,1.1,1,1,1)
var PROPHECYASPD=new Array(0,1.2,1.2,1,1.2)
var PROPHECYCAST=new Array(0,1.2,1,1.2,1)
var PSYCHO=new Array(0,0.9)
var SANDBOMB=new Array(0,-6,0,-12)
var SHIELD=new Array(0,1.08,1.12,1.15)
var SLOW=new Array(0,0.7,0.55)
var TRIBUNAL=new Array (0,-0.3,-0.4,-0.5)
var UDPDEF=new Array(0,1800,3600,5400,3636,3672,3708,3744,3780,3816,3852,3888,3924,3960,3996,4032,4068,4104,4140,4176,4212,4248,4284,4320,4356,4392,4428,4464,4500,4536,4572,4608,4644,4680)
var UDMDEF=new Array(0,1350,2700,4050,2727,2754,2781,2808,2835,2862,2889,2916,2943,2970,2997,3024,3051,3078,3105,3132,3159,3186,3213,3240,3267,3294,3321,3348,3375,3402,3429,3456,3483,3510)
var WEAKNESS=new Array(0,0.83,0.8,0.77)
var WINDWALK=new Array(0,20,33)
var ZERK=new Array(0,1.05,1.08)
var ZERKMATK=new Array(0,1.1,1.16)
var ZERKMDEF=new Array(0,0.9,0.84)
var ZERKPDEF=new Array(0,0.95,0.92)
var ZERKSPEED=new Array(0,5,8)
var ZERKEVA=new Array(0,-2,-4)

//Weapon
var WEAPON=new Array()
WEAPON[0]=new Array(4,6,"unequipped","no SA",0,"",0,"",0,"",0,"",0,"",0,"",0,"",0,"",0,"",0)

//Armor
var HELMETPDEF=new Array(12,83,83,83,69,69,69,69,73,73,69,62,66,66,62,47,54,58,51,54,29,41,33,37,37,37,37,44,13,26,16,23,19)

function boxgoaway() {
//toggle display for fieldsets
	if (d.gI("BUFFSCHECK").checked==false)
		{d.gI("BUFFS").style.display='none'}
	else if (d.gI("BUFFSCHECK").checked==true)
		{d.gI("BUFFS").style.display='block'}
	if (d.gI("DEBUFFSCHECK").checked==false)
		{d.gI("DEBUFFS").style.display='none'}
	else if (d.gI("DEBUFFSCHECK").checked==true)
		{d.gI("DEBUFFS").style.display='block'}
}

function buffs(setbuff) {
//Buffs Autoselect
var setbuffs=d.gI("SETBUFFS").value
var APROPH=new Array("n",1,"n","n","n","n","n","n","n",3,"n","n","n","n","n",0,"n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n",4,6,"n","n","n","n","n",4,7,"n","n","n","n","n",4,5,"n","n","n","n","n")
var ABTB=new Array("n",5,5,4,3,2,1,0,"n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n")
var ABTS=new Array("n",5,5,4,3,2,1,0,"n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n")
var AFCS=new Array("n",2,2,2,2,2,1,1,0,2,2,2,1,1,0,5,5,4,4,3,3,3,3,"n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n")
var AHASTE=new Array("n",1,1,1,1,1,0,0,"n","n","n","n","n","n","n",3,3,3,3,3,3,2,2,"n","n","n","n","n","n","n","n","n","n",8,8,8,8,7,7,"n",8,8,8,8,7,7,"n",8,8,8,8,7,7,"n")
var AEMP=new Array("n","n","n","n","n","n","n","n","n",2,2,2,1,1,0,"n","n","n","n","n","n","n","n","n","n",4,"n","n","n","n","n","n","n",7,7,7,6,6,5,5,"n","n","n","n","n","n","n","n","n","n","n","n","n","n")
var AWW=new Array("n",1,1,1,1,1,1,1,1,1,1,1,1,1,1,"n","n","n","n","n","n","n","n","n","n",3,3,2,"n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n",10,10,10,10,10,9,9)
var AMB=new Array("n",1,1,1,1,1,0,0,"n","n","n","n","n","n","n",4,4,4,4,4,4,4,3,3,3,7,7,7,7,6,6,5,5,9,9,9,9,8,8,"n",9,9,9,9,8,8,"n",9,9,9,9,8,8,"n")
var AAGI=new Array("n","n","n","n","n","n","n","n","n","n","n","n","n","n","n",5,5,5,5,5,5,4,4,3,3,8,8,8,8,7,7,6,6,"n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n")
var AGUID=new Array("n",2,2,2,2,1,1,0,0,2,2,1,1,0,0,5,5,5,5,4,3,3,3,"n","n",8,8,8,8,7,6,6,6,"n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n")
var AACU=new Array("n",2,2,2,2,2,2,1,1,"n","n","n","n","n","n",5,5,5,5,5,5,4,4,4,3,8,8,8,8,7,7,7,6,"n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n","n")
var ASHIELD=new Array("n",2,2,2,2,2,2,2,2,2,2,2,2,2,2,8,8,8,8,8,8,8,8,8,8,5,5,5,5,5,4,4,3,14,14,14,13,13,12,12,14,14,14,13,13,12,12,14,14,14,13,13,12,12)
var AMIGHT=new Array("n",2,2,2,2,2,2,2,2,2,2,2,2,2,2,6,6,6,6,6,6,6,6,6,5,9,9,9,9,9,9,8,8,0,0,0,0,0,0,0,12,12,12,11,11,10,10,0,0,0,0,0,0,0)
if (setbuff && setbuffs=="0"){
	d.gI("BTBCHECK").checked=false
	d.gI("VITCHECK").checked=false
	d.gI("BTSCHECK").checked=false
	d.gI("MIGHTCHECK").checked=false
	d.gI("DOWACHECK").checked=false
	d.gI("GREATERMIGHTCHECK").checked=false
	d.gI("EMPCHECK").checked=false
	d.gI("DOMYCHECK").checked=false
	d.gI("SHIELDCHECK").checked=false
	d.gI("SOECHECK").checked=false
	d.gI("GREATERSHIELDCHECK").checked=false
	d.gI("MBCHECK").checked=false
	d.gI("WARDCHECK").checked=false
	d.gI("GUIDCHECK").checked=false
	d.gI("DOICHECK").checked=false
	d.gI("AGICHECK").checked=false
	d.gI("SWATCHECK").checked=false
	d.gI("FCSCHECK").checked=false
	d.gI("SOHCHECK").checked=false
	d.gI("WWCHECK").checked=false
	d.gI("SWINDCHECK").checked=false
	d.gI("DSHCHECK").checked=false
	d.gI("HASTECHECK").checked=false
	d.gI("DFURYCHECK").checked=false
	d.gI("ACUCHECK").checked=false
	d.gI("DCONCHECK").checked=false
	d.gI("ZERKCHECK").checked=false
	d.gI("QUEENCHECK").checked=false
	d.gI("PROPHCHECK").checked=false
	d.gI("UDCHECK").checked=false
	}
else if (setbuff) {
	var PROPH=APROPH[setbuffs]
	if (PROPH>=0)
		{d.gI("PROPHCHECK").checked=true
		d.gI("PROPH").selectedIndex=PROPH}
	var BTB=ABTB[setbuffs]
	if (BTB>=0)
		{d.gI("BTBCHECK").checked=true
		d.gI("BTB").selectedIndex=BTB}
	var BTS=ABTS[setbuffs]
	if (BTS>=0)
		{d.gI("BTSCHECK").checked=true
		d.gI("BTS").selectedIndex=BTS}
	var FCS=AFCS[setbuffs]
	if (FCS>=0)
		{d.gI("FCSCHECK").checked=true
		d.gI("FCS").selectedIndex=FCS}
	var HASTE=AHASTE[setbuffs]
	if (HASTE>=0)
		{d.gI("HASTECHECK").checked=true
		d.gI("HASTE").selectedIndex=HASTE}
	var EMP=AEMP[setbuffs]
	if (EMP>=0)
		{d.gI("EMPCHECK").checked=true
		d.gI("EMP").selectedIndex=EMP}
	var WW=AWW[setbuffs]
	if (WW>=0)
		{d.gI("WWCHECK").checked=true
		d.gI("WW").selectedIndex=WW}
	var MB=AMB[setbuffs]
	if (MB>=0)
		{d.gI("MBCHECK").checked=true
		d.gI("MB").selectedIndex=MB}
	var AGI=AAGI[setbuffs]
	if (AGI>=0)
		{d.gI("AGICHECK").checked=true
		d.gI("AGI").selectedIndex=AGI}
	var GUID=AGUID[setbuffs]
	if (GUID>=0)
		{d.gI("GUIDCHECK").checked=true
		d.gI("GUID").selectedIndex=GUID}
	var ACU=AACU[setbuffs]
	if (ACU>=0)
		{d.gI("ACUCHECK").checked=true
		d.gI("ACU").selectedIndex=ACU}
	var SHIELD=ASHIELD[setbuffs]
	if (SHIELD>=0)
		{d.gI("SHIELDCHECK").checked=true
		d.gI("SHIELD").selectedIndex=SHIELD}
	var MIGHT=AMIGHT[setbuffs]
	if (MIGHT>=0)
		{d.gI("MIGHTCHECK").checked=true
		d.gI("MIGHT").selectedIndex=MIGHT}
}
	if (d.gI("ACUCHECK").checked==false)
		{d.gI("ACU").disabled=true;}
	else
		{d.gI("ACU").disabled=false;}
	if (d.gI("AGICHECK").checked==false)
		{d.gI("AGI").disabled=true;}
	else
		{d.gI("AGI").disabled=false;}
	if (d.gI("ZERKCHECK").checked==false)
		{d.gI("ZERK").disabled=true;}
	else
		{d.gI("ZERK").disabled=false;}
	if (d.gI("BTBCHECK").checked==false)
		{d.gI("BTB").disabled=true;}
	else
		{d.gI("BTB").disabled=false;}
	if (d.gI("BTSCHECK").checked==false)
		{d.gI("BTS").disabled=true;}
	else
		{d.gI("BTS").disabled=false;}
	if (d.gI("DCONCHECK").checked==false)
		{d.gI("DCON").disabled=true;}
	else
		{d.gI("DCON").disabled=false;}
	if (d.gI("DFURYCHECK").checked==false)
		{d.gI("DFURY").disabled=true;}
	else
		{d.gI("DFURY").disabled=false;}
	if (d.gI("DOICHECK").checked==false)
		{d.gI("DOI").disabled=true;}
	else
		{d.gI("DOI").disabled=false;}
	if (d.gI("DOMYCHECK").checked==false)
		{d.gI("DOMY").disabled=true;}
	else
		{d.gI("DOMY").disabled=false;}
	if (d.gI("DSHCHECK").checked==false)
		{d.gI("DSH").disabled=true;}
	else
		{d.gI("DSH").disabled=false;}
	if (d.gI("DOWACHECK").checked==false)
		{d.gI("DOWA").disabled=true;}
	else
		{d.gI("DOWA").disabled=false;}
	if (d.gI("EMPCHECK").checked==false)
		{d.gI("EMP").disabled=true;}
	else
		{d.gI("EMP").disabled=false;}
	if (d.gI("FCSCHECK").checked==false)
		{d.gI("FCS").disabled=true;}
	else
		{d.gI("FCS").disabled=false;}
	if (d.gI("GREATERMIGHTCHECK").checked==true && d.gI("GREATERSHIELDCHECK").checked==false)
		{d.gI("GREATERMIGHT").disabled=false;
		d.gI("GREATERSHIELDCHECK").disabled=true;
		d.gI("GREATERSHIELDCHECK").checked=false;
		d.gI("GREATERSHIELD").disabled=true;}
	else if (d.gI("GREATERSHIELDCHECK").checked==true && d.gI("GREATERMIGHTCHECK").checked==false)
		{d.gI("GREATERSHIELD").disabled=false;
		d.gI("GREATERMIGHTCHECK").disabled=true;
		d.gI("GREATERMIGHTCHECK").checked=false;
		d.gI("GREATERMIGHT").disabled=true;}
	else if (d.gI("GREATERSHIELDCHECK").checked==true && d.gI("GREATERMIGHTCHECK").checked==true)
		{d.gI("GREATERMIGHT").disabled=false;
		d.gI("GREATERSHIELDCHECK").disabled=true;
		d.gI("GREATERSHIELDCHECK").checked=false;
		d.gI("GREATERSHIELD").disabled=true;}
	else
		{d.gI("GREATERMIGHT").disabled=true;
		d.gI("GREATERSHIELD").disabled=true;
		d.gI("GREATERSHIELDCHECK").disabled=false;
		d.gI("GREATERMIGHTCHECK").disabled=false;}
	if (d.gI("GUIDCHECK").checked==false)
		{d.gI("GUID").disabled=true;}
	else
		{d.gI("GUID").disabled=false;}
	if (d.gI("HASTECHECK").checked==false)
		{d.gI("HASTE").disabled=true;}
	else
		{d.gI("HASTE").disabled=false;}
	if (d.gI("MBCHECK").checked==false)
		{d.gI("MB").disabled=true;}
	else
		{d.gI("MB").disabled=false;}
	if (d.gI("MIGHTCHECK").checked==false)
		{d.gI("MIGHT").disabled=true;}
	else
		{d.gI("MIGHT").disabled=false;}
	if (d.gI("PROPHCHECK").checked==false)
		{d.gI("PROPH").disabled=true;}
	else
		{d.gI("PROPH").disabled=false;}
	if (d.gI("QUEENCHECK").checked==false)
		{d.gI("QUEEN").disabled=true;}
	else
		{d.gI("QUEEN").disabled=false;}
	if (d.gI("BLKSCHECK").checked==false)
		{d.gI("BLKS").disabled=true;
		d.gI("SHIELDCHECK").disabled=false;}
	else if (d.gI("BLKSCHECK").checked==true)
		{d.gI("BLKS").disabled=false;
		d.gI("SHIELDCHECK").disabled=true;
		d.gI("SHIELDCHECK").checked=false;}
	if (d.gI("SHIELDCHECK").checked==false)
		{d.gI("SHIELD").disabled=true;}
	else
		{d.gI("SHIELD").disabled=false;}
	if (d.gI("SOECHECK").checked==false)
		{d.gI("SOEA").disabled=true;}
	else
		{d.gI("SOEA").disabled=false;}
	if (d.gI("SOHCHECK").checked==false)
		{d.gI("SOH").disabled=true;}
	else
		{d.gI("SOH").disabled=false;}
	if (d.gI("VITCHECK").checked==false)
		{d.gI("VIT").disabled=true;}
	else
		{d.gI("VIT").disabled=false;}
	if (d.gI("WARDCHECK").checked==false)
		{d.gI("WARD").disabled=true;}
	else
		{d.gI("WARD").disabled=false;}
	if (d.gI("SWATCHECK").checked==false)
		{d.gI("SWAT").disabled=true;}
	else
		{d.gI("SWAT").disabled=false;}
	if (d.gI("SWINDCHECK").checked==false)
		{d.gI("SWIND").disabled=true;}
	else
		{d.gI("SWIND").disabled=false;}
	if (d.gI("UDCHECK").checked==false)
		{d.gI("UD").disabled=true;}
	else
		{d.gI("UD").disabled=false;}
	if (d.gI("BWWCHECK").checked==false)
		{d.gI("BWW").disabled=true;
		d.gI("WWCHECK").disabled=false;}
	else
		{d.gI("BWW").disabled=false;
		d.gI("WWCHECK").disabled=true;
		d.gI("WWCHECK").checked=false;}
	if (d.gI("WWCHECK").checked==false)
		{d.gI("WW").disabled=true;}
	else
		{d.gI("WW").disabled=false;}
}
function debuffs() {
	if (d.gI("DEACCCHECK").checked==false)
		{d.gI("DEACC").disabled=true;}
	else
		{d.gI("DEACC").disabled=false;}
	if (d.gI("ACCHECK").checked==false)
		{d.gI("ARCR").disabled=true;}
	else
		{d.gI("ARCR").disabled=false;}
	if (d.gI("DEASPDCHECK").checked==false)
		{d.gI("DEASPD").disabled=true;}
	else
		{d.gI("DEASPD").disabled=false;}
	if (d.gI("GLOOMCHECK").checked==false)
		{d.gI("GLOOM").disabled=true;}
	else
		{d.gI("GLOOM").disabled=false;}
	if (d.gI("COABYSSCHECK").checked==false)
		{d.gI("COABYSS").disabled=true;}
	else
		{d.gI("COABYSS").disabled=false;}
	if (d.gI("CODOOMCHECK").checked==false)
		{d.gI("CODOOM").disabled=true;}
	else
		{d.gI("CODOOM").disabled=false;}
	if (d.gI("COSHADECHECK").checked==false)
		{d.gI("COSHADE").disabled=true;}
	else
		{d.gI("COSHADE").disabled=false;}
	if (d.gI("DVCHECK").checked==false)
		{d.gI("DV").disabled=true;}
	else
		{d.gI("DV").disabled=false;}
	if (d.gI("DEMONICBDCHECK").checked==false)
		{d.gI("DEMONICBD").disabled=true;}
	else
		{d.gI("DEMONICBD").disabled=false;}
	if (d.gI("PSYCHOCHECK").checked==false)
		{d.gI("PSYCHO").disabled=true;}
	else
		{d.gI("PSYCHO").disabled=false;}
	if (d.gI("TRIBUNALCHECK").checked==false)
		{d.gI("TRIBUNAL").disabled=true;}
	else
		{d.gI("TRIBUNAL").disabled=false;}
	if (d.gI("FVCHECK").checked==false)
		{d.gI("FV").disabled=true;}
	else
		{d.gI("FV").disabled=false;}
	if (d.gI("CHOLCHECK").checked==false)
		{d.gI("CHOL").disabled=true;}
	else
		{d.gI("CHOL").disabled=false;}
	if (d.gI("MALCHECK").checked==false)
		{d.gI("MAL").disabled=true;}
	else
		{d.gI("MAL").disabled=false;}
	if (d.gI("IVCHECK").checked==false)
		{d.gI("IV").disabled=true;}
	else
		{d.gI("IV").disabled=false;}
	if (d.gI("LVORCHECK").checked==false)
		{d.gI("LVOR").disabled=true;}
	else
		{d.gI("LVOR").disabled=false;}
	if (d.gI("DEPATKCHECK").checked==false)
		{d.gI("DEPATK").disabled=true;}
	else
		{d.gI("DEPATK").disabled=false;}
	if (d.gI("DEPDEFCHECK").checked==false)
		{d.gI("DEPDEF").disabled=true;}
	else
		{d.gI("DEPDEF").disabled=false;}
	if (d.gI("SANDBOMBCHECK").checked==false)
		{d.gI("SANDBOMB").disabled=true;}
	else
		{d.gI("SANDBOMB").disabled=false;}
	if (d.gI("DESPAIRCHECK").checked==false)
		{d.gI("DESPAIR").disabled=true;}
	else
		{d.gI("DESPAIR").disabled=false;}
	if (d.gI("SBCHECK").checked==false)
		{d.gI("SB").disabled=true;}
	else
		{d.gI("SB").disabled=false;}
	if (d.gI("DESPEEDCHECK").checked==false)
		{d.gI("DESPEED").disabled=true;}
	else
		{d.gI("DESPEED").disabled=false;}
	if (d.gI("WVCHECK").checked==false)
		{d.gI("WV").disabled=true;}
	else
		{d.gI("WV").disabled=false;}
}

function calc()
{
var timeset1=new Date();
//placeholders
var AddASPD=0
var AddCAST=0
var AddCRIT=0
var AddCRIT60=0
var AddCRIT30=0
var AddSPEED=0
var AddSPEED60=0
var AddSPEED30=0
var AddHP=0
var AddMP=0
var AddMATK=0
var AddPATK=0
var AddPATK60=0
var AddPATK30=0
var AddMDEF=0
var AddMDEF60=0
var AddMDEF30=0
var AddPDEF=0
var AddPDEF60=0
var AddPDEF30=0
var AddSPEED=0
var apdef=0
var BaseHP=0
var BaseMP=0
var BaseRun=0
var Boot=0
var BuffACC=0
var BuffACC60=0
var BuffACC30=0
var BuffASPD=1
var BuffASPD60=1
var BuffASPD30=1
var BuffCAST=1
var BuffEVA=0
var BuffEVA60=0
var BuffEVA30=0
var BuffHP=1
var BuffMATK=1
var BuffMDEF=1
var BuffMDEF60=1
var BuffMDEF30=1
var BuffMP=1
var BuffPATK=1
var BuffPATK60=1
var BuffPATK30=1
var BuffPDEF=1
var BuffPDEF60=1
var BuffPDEF30=1
var BuffSPEED=1
var BuffSPEED60=1
var BuffSPEED30=1
var jmdef=0
var critical=0
var WpnAcc=0
var WpnPATK=0
var WpnMATK=0
var WpnType="unequipped"

var JOB=d.gI("class").value
var STR=40
var CON=43
var DEX=30
var INT=21
var WIT=11
var MEN=25
var STRMOD=1.2
var DEXMOD=1.1
var CONMOD=1.58
var INTMOD=0.81
var WITMOD=0.64
var MENMOD=1.28
var BaseRun=0

//Weapon SAs
var summonerlvl=document.forms['statcalculator'].LV;
summonerlvl.length=20
for (i=0;i<20;i++){
	if (JOB=="BB") {
		if (JOB=="SG") {
			if (SIEGEGOLEMBLVL[i]>=0) {
				summonerlvl.options[i] = new Option(SIEGEGOLEMSLVL[i],SIEGEGOLEMBLVL[i],false,summonerlvl.options[i].selected);
			} else {
				summonerlvl.length=i
				break
			}
		}
	}
	

//Level Modifier
var LVL = + d.gI("LV").value
var LVLMOD=(LVL+89)/100
var LVLVAR=d.gI("LV").selectedIndex

//Base HP/MP Calculation
if (JOB=="SG") {
	BaseHP=SIEGEGOLEMHP[LVLVAR]
	BaseMP=SIEGEGOLEMMP[LVLVAR]
	WpnPATK=SIEGEGOLEMPATK[LVLVAR]
	WpnMATK=SIEGEGOLEMMATK[LVLVAR]
	apdef=SIEGEGOLEMPDEF[LVLVAR]
	jmdef=SIEGEGOLEMMDEF[LVLVAR]
	WpnType="SG"
}


if (JOB=="BB"||JOB=="CO"||JOB=="MEW"||JOB=="BOX"||JOB=="MIR"||JOB=="SER"||JOB=="SH"||JOB=="SI"||JOB=="RM"||JOB=="CU") {
	BaseRun=140}
else if (JOB=="WHC") {
	BaseRun=10}
else if (JOB=="SG") {
	BaseRun=40}
else if (JOB=="KAT") {
	BaseRun=120}
else if (JOB=="MG"||JOB=="DP") {
	BaseRun=150}
else if (JOB=="NS") {
	BaseRun=170}
else if (JOB=="QC") {
	BaseRun=180}
else if (JOB=="KAI"||JOB=="SL") {
	BaseRun=190}
else if (JOB=="MER") {
	BuffEVA=BuffEVA+10
	BaseRun=190}

//Weapon Types
if (WpnType=="normal")
	{WpnSpd=253;WpnAcc=4.75;critical=40}
else if (WpnType=="CM")
	{WpnSpd=253;WpnAcc=4.75;critical=80}
else if (WpnType=="SG")
	{WpnSpd=150;WpnAcc=4.75;critical=40}

//Buffs
//Accuracy Debuffs
	if (d.gI("DEACCCHECK").checked==true)
		{var deacc=d.gI("DEACC").value;BuffACC=BuffACC-DEACC[deacc]}
//Acumen
	if (d.gI("ACUCHECK").checked==true)
		{var acu=d.gI("ACU").value;BuffCAST=BuffCAST*ACUMEN[acu]}
//Agility
	if (d.gI("AGICHECK").checked==true)
		{var agi=d.gI("AGI").value;BuffEVA=BuffEVA+AGILITY[agi]}
//Armor Crush
	if (d.gI("ACCHECK").checked==true)
		{var armorcrush=d.gI("ARCR").value;BuffPDEF=0.70*BuffPDEF;BuffMDEF=0.70*BuffMDEF}
//Attack Speed Debuffs
	if (d.gI("DEASPDCHECK").checked==true)
		{var deaspd=d.gI("DEASPD").value;BuffASPD=BuffASPD*DEASPD[deaspd]}
//Berserker Spirit
	if (d.gI("ZERKCHECK").checked==true)
		{var zerk=d.gI("ZERK").value;BuffPATK=BuffPATK*ZERK[zerk];BuffPDEF=BuffPDEF*ZERKPDEF[zerk];BuffMATK=BuffMATK*ZERKMATK[zerk];BuffMDEF=BuffMDEF*ZERKMDEF[zerk];BuffEVA=BuffEVA+ZERKEVA[zerk];AddSPEED=AddSPEED+ZERKSPEED[zerk];BuffASPD=BuffASPD*ZERK[zerk];BuffCAST=BuffCAST*ZERK[zerk]}
//Bless the Body
	if (d.gI("BTBCHECK").checked==true)
		{var btb=d.gI("BTB").value
		BuffHP=BuffHP*BTB[btb]}
//Bless the Soul
	if (d.gI("BTSCHECK").checked==true)
		{var bts=d.gI("BTS").value;BuffMP=BuffMP*BTS[bts]}
//Block Shield
	if (d.gI("BLKSCHECK").checked==true)
		{var blockshield=d.gI("BLKS").value;BuffPDEF=0.90*BuffPDEF}
//Block Wind Walk
	if (d.gI("BWWCHECK").checked==true)
		{var bww=d.gI("BWW").value;BuffSPEED=BuffSPEED*0.9}
//Curse Gloom
	if (d.gI("GLOOMCHECK").checked==true)
		{var gloom=d.gI("GLOOM").value;BuffMDEF=0.77*BuffMDEF}
//Curse of Abyss
	if (d.gI("COABYSSCHECK").checked==true)
		{var coabyss=d.gI("COABYSS").value;BuffMATK=0.70*BuffMATK;BuffEVA=BuffEVA-6;BuffSPEED=BuffSPEED*0.9;BuffCAST=BuffCAST*0.8}
//Curse of Doom
	if (d.gI("CODOOMCHECK").checked==true)
		{var codoom=d.gI("CODOOM").value;BuffPATK=0.83*BuffPATK}
//Curse of Shade
	if (d.gI("COSHADECHECK").checked==true)
		{var coshade=d.gI("COSHADE").value;BuffPDEF=CURSEOFSHADE[coshade]*BuffPDEF;BuffMDEF=CURSEOFSHADE[coshade]*BuffMDEF}
//Dance of Concentration
	if (d.gI("DCONCHECK").checked==true)
		{var dcon=d.gI("DCON").value;BuffCAST=BuffCAST*1.3}
//Dance of Fury
	if (d.gI("DFURYCHECK").checked==true)
		{var dfury=d.gI("DFURY").value;BuffASPD=BuffASPD*1.15}
//Dance of Inspiration
	if (d.gI("DOICHECK").checked==true)
		{var doi=d.gI("DOI").value;BuffACC=BuffACC+4}
//Dance of Mystic
	if (d.gI("DOMYCHECK").checked==true)
		{var domy=d.gI("DOMY").value;BuffMATK=1.2*BuffMATK}
//Dance of Shadow
	if (d.gI("DSHCHECK").checked==true)
		{var dsh=d.gI("DSH").value;BuffSPEED=BuffSPEED*0.5}
//Dance of Warrior
	if (d.gI("DOWACHECK").checked==true)
		{var dowa=d.gI("DOWA").value;BuffPATK=1.12*BuffPATK}
//Demonic Blade Dance
	if (d.gI("DEMONICBDCHECK").checked==true)
		{var demonicbd=d.gI("DEMONICBD").value;BuffPATK=BuffPATK*DEMONICBD[demonicbd];BuffMATK=BuffMATK*DEMONICBD[demonicbd]}
//Empower
	if (d.gI("EMPCHECK").checked==true)
		{var emp=d.gI("EMP").value;BuffMATK=BuffMATK*EMPOWER[emp]}
//Fire Vortex
	if (d.gI("FVCHECK").checked==true)
		{var fv=d.gI("FV").value;BuffSPEED=BuffSPEED*0.9;BuffASPD=BuffASPD*0.7;BuffCAST=BuffCAST*0.9}
//Focus
	if (d.gI("FCSCHECK").checked==true)
		{var fcs=d.gI("FCS").value;critical=critical*1.25}
//Greater Might
	if (d.gI("GREATERMIGHTCHECK").checked==true)
		{var greatermight=d.gI("GREATERMIGHT").value;BuffPATK=BuffPATK*GREATERMIGHT[greatermight]}
//Greater Shield
	if (d.gI("GREATERSHIELDCHECK").checked==true)
		{var greatershield=d.gI("GREATERSHIELD").value;BuffPDEF=BuffPDEF*GREATERSHIELD[greatershield]}
//Guidance
	if (d.gI("GUIDCHECK").checked==true)
		{var guid=d.gI("GUID").value;BuffACC=BuffACC+GUIDANCE[guid]}
//Haste
	if (d.gI("HASTECHECK").checked==true)
		{var haste=d.gI("HASTE").value;BuffASPD=BuffASPD*HASTE[haste]}
//Hot Springs Cholera
	if (d.gI("CHOLCHECK").checked==true)
		{var chol=d.gI("CHOL").value;BuffACC=BuffACC+HSCHOLACC[chol];BuffEVA=BuffEVA+HSCHOLEVA[chol]}
//Hot Springs Malaria
	if (d.gI("MALCHECK").checked==true)
		{var mal=d.gI("MAL").value;BuffCAST=BuffCAST*HSMALARIA[mal]}
//Ice Vortex
	if (d.gI("IVCHECK").checked==true)
		{var iv=d.gI("IV").value;BuffSPEED=BuffSPEED*0.7;BuffASPD=BuffASPD*0.9;BuffCAST=BuffCAST*0.9}
//Light Vortex
	if (d.gI("LVORCHECK").checked==true)
		{var lvor=d.gI("LVOR").value;BuffACC=BuffACC-6}
//Magic Barrier
	if (d.gI("MBCHECK").checked==true)
		{var mb=d.gI("MB").value;BuffMDEF=BuffMDEF*MAGICBARRIER[mb]}
//Might
	if (d.gI("MIGHTCHECK").checked==true)
		{var might=d.gI("MIGHT").value;BuffPATK=BuffPATK*MIGHT[might]}
//P.Atk. Debuffs
	if (d.gI("DEPATKCHECK").checked==true)
		{var depatk=d.gI("DEPATK").value;BuffPATK=BuffPATK*WEAKNESS[depatk]}
//P.Def. Debuffs
	if (d.gI("DEPDEFCHECK").checked==true)
		{var depdef=d.gI("DEPDEF").value;BuffPDEF=BuffPDEF*HEX[depdef]}
//Prophecies
	if (d.gI("PROPHCHECK").checked==true)
		{var proph=d.gI("PROPH").value
		BuffHP=BuffHP*PROPHECYHP[proph];
		BuffPATK=BuffPATK*PROPHECYPATK[proph];
		BuffMATK=BuffMATK*PROPHECYMATK[proph];
		BuffPDEF=BuffPDEF*PROPHECYPDEF[proph];
		BuffMDEF=BuffMDEF*PROPHECYMDEF[proph];
		BuffACC=BuffACC+PROPHECYACC[proph];
		BuffEVA=BuffEVA+PROPHECYEVA[proph];
		critical=critical*PROPHECYCRIT[proph];
		BuffSPEED=BuffSPEED*PROPHECYSPEED[proph];
		BuffASPD=BuffASPD*PROPHECYASPD[proph];
		BuffCAST=BuffCAST*PROPHECYCAST[proph]}
//Psycho Symphony
	if (d.gI("PSYCHOCHECK").checked==true)
		{var psycho=d.gI("PSYCHO").value;
		BuffSPEED=BuffSPEED*PSYCHO[psycho];
		BuffASPD=BuffASPD*PSYCHO[psycho];
		BuffCAST=BuffCAST*PSYCHO[psycho];}
//Queen Buffs
	var queen=d.gI("QUEEN").value
	if ((queen=="1"||queen=="2"||queen=="3") && d.gI("QUEENCHECK").checked==true)
		{BuffPATK=CATBUFFS[queen]*BuffPATK;BuffACC=BuffACC+2}
	else if ((queen=="4"||queen=="5"||queen=="6") && d.gI("QUEENCHECK").checked==true)
		{critical=critical*1.1}
//Sand Bomb
	if (d.gI("SANDBOMBCHECK").checked==true)
		{var sandbomb=d.gI("SANDBOMB").value;BuffACC=BuffACC+SANDBOMB[sandbomb]}
//Seal of Despair
	if (d.gI("DESPAIRCHECK").checked==true)
		{var despair=d.gI("DESPAIR").value;BuffMDEF=0.70*BuffMDEF;BuffACC=BuffACC-6;critical=critical+(critical*-0.3);BuffSPEED=BuffSPEED*0.8;BuffASPD=BuffASPD*0.8}
//Shield
	if (d.gI("SHIELDCHECK").checked==true)
		{var shield=d.gI("SHIELD").value;BuffPDEF=BuffPDEF*SHIELD[shield]}
//Shock Blast
	if (d.gI("SBCHECK").checked==true)
		{var shockblast=d.gI("SB").value;BuffPDEF=0.70*BuffPDEF;BuffMDEF=0.70*BuffMDEF}
//Slow
	if (d.gI("DESPEEDCHECK").checked==true)
		{var despeed=d.gI("DESPEED").value;BuffSPEED=BuffSPEED*SLOW[despeed]}
//Song of Earth
	if (d.gI("SOECHECK").checked==true)
		{var soea=d.gI("SOEA").value;BuffPDEF=1.25*BuffPDEF}
//Song of Hunter
	if (d.gI("SOHCHECK").checked==true)
		{var soh=d.gI("SOH").value;critical=critical*2}
//Song of Vitality
	if (d.gI("VITCHECK").checked==true)
		{var vit=d.gI("VIT").value;BuffHP=1.3*BuffHP}
//Song of Warding
	if (d.gI("WARDCHECK").checked==true)
		{var ward=d.gI("WARD").value;BuffMDEF=1.3*BuffMDEF}
//Song of Water
	if (d.gI("SWATCHECK").checked==true)
		{var swat=d.gI("SWAT").value;BuffEVA=BuffEVA+3}
//Song of Wind
	if (d.gI("SWINDCHECK").checked==true)
		{var swind=d.gI("SWIND").value;AddSPEED=AddSPEED+20}
//Tribunal
	var tribunal=d.gI("TRIBUNAL").value
	if (d.gI("TRIBUNALCHECK").checked==true)
		{var tribunal=d.gI("TRIBUNAL").value;
		critical=critical+(critical*TRIBUNAL[tribunal])}
//Ultimate Defense
	if (d.gI("UDCHECK").checked==true)
		{var ud=d.gI("UD").value
		AddPDEF=AddPDEF+UDPDEF[ud]
		AddMDEF=AddMDEF+UDMDEF[ud]}
//Wind Vortex
	if (d.gI("WVCHECK").checked==true)
		{var wv=d.gI("WV").value;BuffSPEED=BuffSPEED*0.9;BuffASPD=BuffASPD*0.9;BuffCAST=BuffCAST*0.7}
//Wind Walk
	if (d.gI("WWCHECK").checked==true)
		{var ww=d.gI("WW").value;AddSPEED=AddSPEED+WINDWALK[ww]}

//Lv
d.gI("LVL").innerHTML=(LVL)

//HP calculation
var hp=BaseHP*CONMOD*BuffHP+AddHP
if (hp<1)
	{hp=1}
var hp2=Math.floor(hp)
d.gI("HP").innerHTML=(hp2)

//MP calculation
var mp=BaseMP*MENMOD*BuffMP+AddMP
var mp2=Math.floor(mp)
d.gI("MP").innerHTML=mp2

//P.Atk. calculation
var patk=WpnPATK*STRMOD*LVLMOD*BuffPATK+AddPATK
var patk2=Math.floor(patk)
d.gI("PATK").innerHTML=patk2

//M.Atk. calculation
var matk=WpnMATK*(INTMOD*INTMOD)*(LVLMOD*LVLMOD)*BuffMATK+AddMATK
var matk2=Math.floor(matk)
d.gI("MATK").innerHTML=matk2

//P.Def. calculation
pdef=apdef*LVLMOD*BuffPDEF+AddPDEF
pdef2=Math.floor(pdef)
d.gI("PDEF").innerHTML=pdef2

//M.Def. calculation
mdef=jmdef*LVLMOD*MENMOD*BuffMDEF+AddMDEF
mdef2=Math.floor(mdef)
d.gI("MDEF").innerHTML=mdef2

//Accuracy Calculation
var Accuracy=(Math.sqrt(DEX)*6)+LVL+WpnAcc+BuffACC
var Accuracy2=Math.floor(Accuracy)
d.gI("ACCURACY").innerHTML=Accuracy2

//Evasion Calculation
var Evasion=(Math.sqrt(DEX)*6)+LVL+BuffEVA
var Evasion2=Math.floor(Evasion)
d.gI("EVASION").innerHTML=Evasion2

//Critical calculation
var critical=critical+AddCRIT
var critical2=Math.floor(critical)
if (critical2 > 500)
	{critical2=500}
d.gI("CRITICAL").innerHTML=critical2

//Speed calculation
var Speed=BaseRun*DEXMOD*BuffSPEED+AddSPEED
var Speed2=Math.floor(Speed)
var Speed60=BaseRun*DEXMOD*BuffSPEED*BuffSPEED60+AddSPEED+AddSPEED60
var Speed602=Math.floor(Speed60)
var Speed30=BaseRun*DEXMOD*BuffSPEED*BuffSPEED60*BuffSPEED30+AddSPEED+AddSPEED60+AddSPEED30
var Speed302=Math.floor(Speed30)
d.gI("SPEED").innerHTML=Speed2

//Atk. Spd. calculation
var atkspd=WpnSpd*DEXMOD*BuffASPD+AddASPD
var atkspd2=Math.floor(atkspd)
d.gI("ATKSPD").innerHTML=atkspd2

//Casting Spd. calculation
var castingspd=333*BuffCAST+AddCAST
var castingspd2=Math.floor(castingspd)
d.gI("CASTINGSPD").innerHTML=castingspd2

var timeset2=new Date();
var timer=timeset2.getTime() - timeset1.getTime();
var timer2=timer/1000
//d.gI("TEST").innerHTML=timer2
}