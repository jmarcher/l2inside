/** 
 * @version V1.2 02 Marzo 2007
 * Julian Lasso julian.lasso@gmail.com
 * Liberada bajo licencia GPL
 */
function crear_senal(senal,ancho,alto){
  document.getElementById(senal).style.visibility = 'hidden';
  document.getElementById(senal).style.position = 'absolute';
  document.getElementById(senal).style.top = ancho + 'px';
  document.getElementById(senal).style.left = alto + 'px';
  document.getElementById(senal).innerHTML = '<img align="right" src="imagenes/reloj_arena.gif" width="32" height="32" />';
}

/* Ejemplo para colocar en las p�ginas donde se quiere el reloj de arena
<div id="senal" style="visibility:hidden; position:absolute; top:100px; left:100px;">
<img src="imagenes/reloj_arena.gif" width="32" height="32" />
</div>
*/

/**
 * Funci�n para enviar informaci�n por el metodo GET o POST y
 * recibir una respuesta sin recargar la p�gina en un <div></div>
 * especifico.
 *
 * @param pagina - P�gina en donde se va a consultar
 * @param respuesta - Id de la etiqueta a donde va a llegar la respuesta <div> <spam>
 * @param metodo - Metodo por el cual se envian las variables GET o POST
 * @param variables - Conjunto de Variables a enviar EJ: 'var1=Julian' �
 *                    'nom=Julian&ape=Lasso&edad=21' � 'dir=Calle 2 # 36&tel=4451555'
 * @param senal - Es el ID de la que va a contener el satelite que indica el estado
 *                de la conecci�n al servidor [opcional]
 * @param ancho - Aqui pasamos el valor en pixeles de la posici�n a lo ancho,
 *                este valor es opcional si senal es null
 * @param alto - Aqui pasamos el valor en pixeles de la posici�n a lo largo,
 *                este valor es opcional si senal es null
 */
function consultar(pagina, respuesta, metodo, variables, senal){
  /* Podr�amos indicar que estamos cargando los datos.... que espere */
  // document.getElementById(respuesta).innerHTML = 'Cargando datos...';
  if(senal != null){
    alto = 0; // Number((Number(window.innerWidth) / Number(2))) - Number((Number(58) / Number(2)));
    ancho = 0; // Number((Number(window.innerHeight) / Number(2))) - Number((Number(96) / Number(2)));
    crear_senal(senal,ancho,alto);
    document.getElementById(senal).style.visibility = 'visible';
  }
  else{
    document.getElementById(respuesta).innerHTML = '<img src="imagenes/loading.gif" alt="" width="16" height="16" />';
  }
  /* Preparaci�n de la variable 'pagina' en caso de que se haga uso del metodo GET */
  if(metodo == 'get' || metodo == 'GET'){
    metodo = 'GET';
    pagina = pagina + '?' + variables;
  }
  else{
    if(metodo == 'post' || metodo == '' || metodo == null){
      metodo = 'POST';
    }
  }
  /* Compatibilidad con FireFox, Opera y cualquier otro BUEN navegador */
  if(typeof(XMLHttpRequest) != 'undefined'){
    try{
      var satelite = new XMLHttpRequest();
    }
    catch(e){ }
  }
  else{
    /* Compatibilidad para el navegador m�s ASQUEROSO del planeta [ IE ] */
    try{
      var satelite = new ActiveXObject('Microsoft.XMLHTTP');
    }
    catch(e){
      var satelite = new ActiveXObject('Msxml2.XMLHTTP');
    }
  }
  /* Una vez incializado el objeto y definido en el contexto de nuestro script
     intentaremos abrir la conexi�n a la direcci�n indicada por la variable 'pagina' */
  try{
    /* esto bien podr�a cambiar a "satelite.open('GET',pagina,true);" si se desea pasar
       los datos mediante m�todo GET */
    satelite.open(metodo,pagina,true);
  }
  catch(e){
    return false;
  }
  /* Esta variable env�a una cabecera indicando que enviaremos los datos m�todo 'post' en
     forma urlencoded ejemplo ("mi_variable=cosa&otra_variable=otra_cosa"); */
  satelite.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  /* Enviamos los datos 'elementos' */
  satelite.send(variables);
  try{
    /* Justo cuando el estatus de la petici�n cambia esperamos a que sea igual a 4 que es
       igual a OK. */
    satelite.onreadystatechange = function(){
      if(satelite.readyState == 4){
        if(senal != null){
          document.getElementById(senal).style.visibility = 'hidden';
        }
        /* ��� Listo !!!, ahora ya tenemos el resultado y podemos acceder a �l desde
           'satelite.responseText' El resultado lo vamos a escribir en el elemento
           'respuesta' ... */
        /* Extracci�n de la vandera EVAL[--> */
        temp = (satelite.responseText).substring(0,8);
        if(temp == 'EVAL[-->'){
          /* Si la bandera existe entonces se extrae el script y se ejecuta */
          eval((satelite.responseText).substring(8));
        }
        else{
          /* Si la vandera no existe entonces se muestra el resultado en el div correspondiente */
          document.getElementById(respuesta).innerHTML = satelite.responseText;
        }
      }
    }
  }
  catch(e){
    return false;
  }
  return true;
} // fin consultar();