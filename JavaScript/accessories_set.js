function accessories(info)
{
	this.id = info[0];
	this.name = info[2];
	this.equip_slot = info[3];
	this.mdefpower = info[4];
	this.grade = first_to_upper(info[5]);
	this.no_crystal = info[6];
	this.weight = info[7];
	this.pic = new Image();
	this.description = info[9];
	
	this.gender = (info[10]) ? info[10] : false;
	
	this.screenshot = (this.equip_slot == 'special') ? 1 : -1;
	
	this.set_icon = function() { this.pic.src = '/images/item_icons/item_' + info[0] + '.jpg'; }
	
	this.row_data = make_row;
}

function make_row()
{
	return new Array(this.pic, this.name, this.grade, this.mdefpower, this.weight, this.no_crystal);
}

var my_header = new Array('Name', 'Grade', 'M.&nbsp;Def.', 'Weight', 'Crystal Count');
var categories_select = new Array('Earring', 'Necklace', 'Ring', 'Special');

// returns matches
function sort_pages(all_my_items, to_show)
{
	to_show = to_show.replace(/Earring/i, 'rear;lear');
	to_show = to_show.replace(/Necklace/i, 'neck');
	to_show = to_show.replace(/Ring/i, 'rfinger;lfinger');
	
	my_matches = new Array();
	for(i=0; i < all_my_items.length; i++)
	{
		if(all_my_items[i].equip_slot == to_show)
		{
			my_matches.push(all_my_items[i]);
		}
	}
	return my_matches;
}

function view_item(id)
{
	cur_gender = (show_items[id].gender) ? ((show_items[id].gender == 'm') ? 1 : 2) : 1;
	cur_race = 0;	
	document.location.hash = 'itemsTop';
	var container = document.getElementById('item_container');
	prev_content = container.innerHTML;
	
	var output = '<div id="itemWrapper">' +
				 '<div id="itemTitle"><div><img src="/images/item_icons/item_' + show_items[id].id + '.jpg" onerror="this.src=\'/images/item_icons/noicon.jpg\';" /></div><div style="margin-top: 7px; margin-left: 10px;">' + show_items[id].name + '</div></div>' +
				 '<div id="itemImage">' +
				 	'<div><a href="javascript: close_item(\'' + id + '\');"><img id="itemScreen" src="/images/item_screens/' + show_items[id].id + '_0_' + cur_gender + '.jpg" width="382" height="403" border="0" alt="Back" title="Back" ></a></div>' +
				 	'<div id="screenNav">' + gender_select(show_items[id].id, id) + ' ' + race_select(show_items[id].id) + '</div>' +
				 '</div>' +
				 '<div id="itemStats">' +
				 	'<table width="100%" cellpadding="0" cellspacing="0" border="0">' +
					'<tr>' +
					'<td class="statTitle">Grade</td>' +
					'</tr>' +
					'<tr>' +
					'<td class="statText">' + show_items[id].grade + '</td>' +
					'</tr>' +
					'<tr>' +
					'<td class="statTitle">M. Def.</td>' +
					'</tr>' +
					'<tr>' +
					'<td class="statText">' + show_items[id].mdefpower + '</td>' +
					'</tr>' +
					'<tr>' +
					'<td class="statTitle">Weight</td>' +
					'</tr>' +
					'<tr>' +
					'<td class="statText">' + show_items[id].weight + '</td>' +
					'</tr>' +
					'<tr>' +
					'<td class="statTitle">Crystal Count</td>' +
					'</tr>' +
					'<tr>' +
					'<td class="statText">' + show_items[id].no_crystal + '</td>' +
					'</tr>' +
					((show_items[id].description.length > 2) ? 
					'<tr>' +
					'<td class="statTitle">Description</td>' +
					'</tr>' +
					'<tr>' +
					'<td class="statText"><div style="text-align: left;">' + show_items[id].description + '</div></td>' +
					'</tr>' : '') +
					'</table>' +
				 '</div>' +
				 '</div>' +
				 '<div id="itemBack">[ <a href="javascript: close_item(\'' + id + '\');">Back to Armor</a> ]</div>';
				 
	container.innerHTML = output;
}

function gender_select(id, item_id)
{
	var output = '<select name="genderSelect" onchange="swap_screen(\'' + id + '\', this.options[this.selectedIndex].value, cur_race);">' +
				 ((show_items[item_id].gender && show_items[item_id].gender == 'f') ? '' : '<option value="1">Male</option>') +
				 ((show_items[item_id].gender && show_items[item_id].gender == 'm') ? '' : '<option value="2">Female</option>') +
				 '</select>';
	return output;
}

function race_select(id)
{
	var output = '<select name="raceSelect" onchange="swap_screen(\'' + id + '\', cur_gender, this.options[this.selectedIndex].value);">' +
				 '<option value="0">Human</option>' +
				 '<option value="5">Human Mystic</option>' +
				 '<option value="1">Elf</option>' +
				 '<option value="2">Dark Elf</option>' +
				 '<option value="3">Orc</option>' +
				 '<option value="6">Orc Mystic</option>' +
				 '<option value="4">Dwarf</option>' +
				 '</select>';
	return output;
}

function swap_screen(id, gender, race)
{
	cur_gender = new String(gender);
	cur_race = new String(race);
	document.getElementById("itemScreen").src = '/images/item_screens/' + id + '_' + cur_race + '_' + cur_gender + '.jpg';
}
// show screens column
var show_screens = true;
var cur_gender;
var cur_race;