BLANK_IMAGE = 'images/b.gif';

var STYLE = {
	css:{
		ON:"clsCMOn",		// CSS class for items
		OVER:"clsCMOver"	// CSS class  for item which is under mouse
	}
};

var MENU_ITEMS = [
	{pos:[10,10], itemoff:[21,0], leveloff:[0,99], style:STYLE, size:[22,100]},
	{code:"Item 1",
		sub:[
			{},
			{code:"SubItem 1", url:"http://www.l2inside.org/", target:"_self"},
			{code:"SubItem 2"},
			{code:"SubItem 3"}
		]
	}
];
