function armor(info)
{
	this.id = info[0];
	this.name = info[1];
	this.type = info[3];
	this.pdef = info[5];
	this.shielddef = info[6];
	this.grade = info[4];
	this.no_crystal = info[9];
	this.mp_bonus = info[8];
	this.weight = info[7];
	this.screenshot = info[11];
	this.description = info[10];
	this.pic = new Image();
	
	this.set_icon = function() { this.pic.src = '/images/item_icons/item_' + info[0] + '.jpg'; }
	
	this.row_data = make_row;
}

function make_row()
{
	return new Array(this.pic, this.name, this.grade, this.pdef, this.weight, this.mp_bonus, this.no_crystal);
}

var my_header = new Array('Name', 'Grade', 'P.&nbsp;Def.','Weight', 'MP Bonus', 'Crystal Count');
var categories_select = new Array('Light Upper', 'Light Lower', 'Light Up/Low', 'Heavy Upper', 'Heavy Lower', 'Heavy Up/Low', 'Robe Upper', 'Robe Lower', 'Robe Up/Low', 'Shield', 'Feet', 'Hands', 'Head', 'Pet');

// returns matches
function sort_pages(all_my_items, to_show)
{
	my_matches = new Array();
	for(i=0; i < all_my_items.length; i++)
	{
		if((all_my_items[i].type.toLowerCase()).replace(/upper\/lower/, 'up/low') == to_show.toLowerCase())
		{
			my_matches.push(all_my_items[i]);
		}
	}
	return my_matches;
}
function view_item(id)
{
	cur_gender = 1;
	cur_race = 0;	
	document.location.hash = 'itemsTop';
	var container = document.getElementById('item_container');
	prev_content = container.innerHTML;
	var output = '<div id="itemWrapper">' +
				 '<div id="itemTitle"><div><img src="/images/item_icons/item_' + show_items[id].id + '.jpg" onerror="this.src=\'/images/item_icons/noicon.jpg\';" /></div><div style="margin-top: 7px; margin-left: 10px;">' + show_items[id].name + '</div></div>' +
				 '<div id="itemImage">' +
				 	'<div><a href="javascript: close_item(\'' + id + '\');"><img id="itemScreen" src="/images/item_screens/' + show_items[id].id + '_0_1.jpg" width="382" height="403" border="0" alt="Back" title="Back" onError="this.src=\'/images/item_screens/nopic.jpg\';" ></a></div>' +
				 	'<div id="screenNav">' + gender_select(show_items[id].id) + ' ' + race_select(show_items[id].id) + '</div>' +
				 '</div>' +
				 '<div id="itemStats">' +
				 	'<table width="100%" cellpadding="0" cellspacing="0" border="0">' +
					'<tr>' +
					'<td class="statTitle">Grade</td>' +
					'</tr>' +
					'<tr>' +
					'<td class="statText">' + show_items[id].grade + '</td>' +
					'</tr>' +
					'<tr>' +
					'<td class="statTitle">Worn Slot</td>' +
					'</tr>' +
					'<tr>' +
					'<td class="statText">' + show_items[id].type + '</td>' +
					'</tr>' +
					'<tr>' +
					'<td class="statTitle">P. Def.</td>' +
					'</tr>' +
					'<tr>' +
					'<td class="statText">' + show_items[id].pdef + '</td>' +
					'</tr>' +
					'<tr>' +
					'<td class="statTitle">Weight</td>' +
					'</tr>' +
					'<tr>' +
					'<td class="statText">' + show_items[id].weight + '</td>' +
					'</tr>' +
					'<tr>' +
					'<td class="statTitle">MP Bonus</td>' +
					'</tr>' +
					'<tr>' +
					'<td class="statText">' + show_items[id].mp_bonus + '</td>' +
					'</tr>' +
					'<tr>' +
					'<td class="statTitle">Crystal Count</td>' +
					'</tr>' +
					'<tr>' +
					'<td class="statText">' + show_items[id].no_crystal + '</td>' +
					'</tr>' +
					((show_items[id].description.length > 2) ? 
					'<tr>' +
					'<td class="statTitle">Description</td>' +
					'</tr>' +
					'<tr>' +
					'<td class="statText"><div style="text-align: left;">' + show_items[id].description + '</div></td>' +
					'</tr>' : '') +
					'</table>' +
				 '</div>' +
				 '</div>' +
				 '<div id="itemBack">[ <a href="javascript: close_item(\'' + id + '\');">Back to Armor</a> ]</div>';
				 
	container.innerHTML = output;
}

function gender_select(id)
{
	var output = '<select name="genderSelect" onchange="swap_screen(\'' + id + '\', this.options[this.selectedIndex].value, cur_race);">' +
				 '<option value="1">Male</option>' +
				 '<option value="2">Female</option>' +
				 '</select>';
	return output;
}

function race_select(id)
{
	var output = '<select name="raceSelect" onchange="swap_screen(\'' + id + '\', cur_gender, this.options[this.selectedIndex].value);">' +
				 '<option value="0">Human</option>' +
				 '<option value="5">Human Mystic</option>' +
				 '<option value="1">Elf</option>' +
				 '<option value="2">Dark Elf</option>' +
				 '<option value="3">Orc</option>' +
				 '<option value="6">Orc Mystic</option>' +
				 '<option value="4">Dwarf</option>' +
				 '</select>';
	return output;
}

