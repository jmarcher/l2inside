// JavaScript Document

var moving = false;
var small_map_x;
var small_map_y;
var x, y, px, py;
var show = "default";
var ie = false;

function init() {
	// quit if this function has already been called
    if (arguments.callee.done) return;

    // flag this function so we don't do the same thing twice
    arguments.callee.done = true;
	
	if(document.getElementById("smallMapField").addEventListener){
		document.getElementById("smallMapField").addEventListener("mousemove", onPointerMouseMove, false);
		document.getElementById("smallMapField").addEventListener("mousedown", onPointerMouseDown, false);
	} else {
		document.getElementById("smallMapField").attachEvent("onmousemove", onPointerMouseMove);
		document.getElementById("smallMapField").attachEvent("onmousedown", onPointerMouseDown);
	}
}

function showinfo(id) {
	if(id == show)
		return false;
	document.getElementById(id).style.display = "block";
	if(show != null)
		document.getElementById(show).style.display = "none";
	show = id;
	return false;
}

function onPointerMouseDown(e) {
	moving = true;
	small_map_x = getX('smallMap');
	small_map_y = getY('smallMap');
	onPointerMouseMove(e);
}

function onPointerMouseMove(e) {
	if(moving){
		var mapEvent = (e)?e:event;
		var p_x = ((mapEvent.pageX)?mapEvent.pageX:mapEvent.x) - small_map_x;
		var p_y = ((mapEvent.pageY)?mapEvent.pageY:mapEvent.y) - small_map_y;
		var x = Math.round(p_x*6.4);
		var y = Math.round(p_y*6.4);
		move_map(x,y,p_x,p_y);
	}	
}

function move_map(left,top,p_left,p_top) {
	x=left;
	y=top;
	px=p_left;
	py=p_top;
	if(!ie) {
		if(left<=225)left=225;
		if(left>=1556)left=1556;
		if(top<=200)top=200;
		if(top>=2415)top=2415;
		if(p_left<=36)p_left=36;
		if(p_left>=245)p_left=245;
		if(p_top<=32)p_top=32;
		if(p_top>=375)p_top=375;	
		left=left-225;
		top=top-205;
		p_left=p_left-36;
		p_top = p_top - 32;
	} else {
		if(left<=225)left=225;
		if(left>=1556)left=1556;
		if(top<=-250)top=-250;
		if(top>=1950)top=1950;
		if(p_left<=36)p_left=36;
		if(p_left>=245)p_left=245;
		if(p_top<=-50)p_top=-50;
		if(p_top>=293)p_top=293;	
		left = left - 225;
		top = top + 260;
		p_left=p_left - 36;
		p_top = p_top + 50;	
	}
	document.getElementById("bigMapContainer").scrollLeft = left;
	document.getElementById("bigMapContainer").scrollTop  = top;
	document.getElementById("map_pointer").style.left     = p_left+"px";
	document.getElementById("map_pointer").style.top      = p_top+"px";
	}

function onPointerMouseUp() {
	moving = false;	
}

function getX(div){
	var x = 0;
	obj = document.getElementById(div);
	while(obj){
		x+=obj.offsetLeft;
		obj=obj.offsetParent;
	} 
	return x;
}

function getY(div){
	var y = 0;
	obj = document.getElementById(div);
	while(obj){
		y+=obj.offsetTop;
		obj=obj.offsetParent;
	}
	return y;
}