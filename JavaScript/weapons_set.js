function weapon(info)
{
	this.id = info[0];
	this.name = (info[3] == "Dual") ? info[1].replace(/-/i, ' * ') : info[1];
	this.worn_slot = info[5];
	this.speed = info[6];
	this.type = info[3];
	this.patk = info[7];
	this.matk = info[8];
	this.grade = info[4];
	this.no_crystal = info[9];
	this.screenshot = info[13];
	this.pic = new Image();
	
	this.set_icon = function() { this.pic.src = '/images/item_icons/item_' + info[0] + '.jpg'; }
	
	this.row_data = make_row;
	this.soul_crystal = info[12];
	this.enchant_desc = info[11];
	this.enchantment = info[10];
	
	this.row_title = (this.enchantment.length > 1) ? this.name + ' --' + this.enchantment + '--' : this.name;
}

function make_row()
{
	return new Array(this.pic, this.row_title, this.grade, this.worn_slot, this.patk, this.matk, this.no_crystal, make_color(this.soul_crystal));
}

var my_header = new Array('Name', 'Grade', 'Worn Slot', 'P. Atk.', 'M. Atk.', 'Crystal Count', 'Soul Crystal');
var categories_select = new Array('Sword', 'Blunt', 'Dagger', 'Bow', 'Fist', 'Dual', 'Polearm', 'Pet', 'Misc.');

// returns matches
function sort_pages(all_my_items, to_show)
{
	my_matches = new Array();
	for(i=0; i < all_my_items.length; i++)
	{
		tmp_compare = (all_my_items[i].type.replace(/Etc\./i, 'misc')).toLowerCase()
		if(tmp_compare == to_show)
		{
			my_matches.push(all_my_items[i]);
		}
	}
	return my_matches;
}
function make_color(color)
{
	if(color.length > 2)
	{
		var output = '<span style="color: #';
		switch(color)
		{
			case 'Red':
			output += 'ab1a39';
			break;
			
			case 'Green':
			output += '457836';
			break;
			
			case 'Blue':
			output += '008bcc';
			break;
		}
		output += ';">' + color + '</span>';
		return output;
	}
	return '-';
}
function view_item(id)
{
	document.location.hash = 'itemsTop';
	var container = document.getElementById('item_container');
	prev_content = container.innerHTML;
	var output = '<div id="itemWrapper">' +
				 '<div id="itemTitle"><div><img src="/images/item_icons/item_' + show_items[id].id + '.jpg" /></div><div style="margin-top: 7px; margin-left: 10px;">' + show_items[id].name + '</div></div>' +
				 '<div id="itemImage"><a href="javascript: close_item(\'' + id + '\');"><img src="/images/item_screens/' + show_items[id].id + '.jpg" width="382" height="403" border="0" onerror="this.src=\'/images/item_screens/nopic.jpg\';" alt="Back" title="Back" /></a></div>' +
				 '<div id="itemStats">' +
				 	'<table width="100%" cellpadding="0" cellspacing="0" border="0">' +
					'<tr>' +
					'<td class="statTitle">Grade</td>' +
					'</tr>' +
					'<tr>' +
					'<td class="statText">' + show_items[id].grade + '</td>' +
					'</tr>' +
					'<tr>' +
					'<td class="statTitle">Worn Slot</td>' +
					'</tr>' +
					'<tr>' +
					'<td class="statText">' + show_items[id].worn_slot + '</td>' +
					'</tr>' +
					'<tr>' +
					'<td class="statTitle">P. Atk.</td>' +
					'</tr>' +
					'<tr>' +
					'<td class="statText">' + show_items[id].patk + '</td>' +
					'</tr>' +
					'<tr>' +
					'<td class="statTitle">M. Atk.</td>' +
					'</tr>' +
					'<tr>' +
					'<td class="statText">' + show_items[id].matk + '</td>' +
					'</tr>' +
					'<tr>' +
					'<td class="statTitle">Crystal Count</td>' +
					'</tr>' +
					'<tr>' +
					'<td class="statText">' + show_items[id].no_crystal + '</td>' +
					'</tr>' +
					((show_items[id].soul_crystal.length > 2) ? 
					'<tr>' +
					'<td class="statTitle">Soul Crystal</td>' +
					'</tr>' +
					'<tr>' +
					'<td class="statText">' + make_color(show_items[id].soul_crystal) + '</td>' +
					'</tr>' : '') +
					((show_items[id].enchantment.length > 2) || (show_items[id].enchant_desc.length > 2) ? 
					'<tr>' +
					'<td class="statTitle">Enhancement</td>' +
					'</tr>' : '') +
					'<tr>' +
					'<td class="statText"><span style="text-decoration: underline;">' + show_items[id].enchantment + 
						'</span><div style="text-align: left; padding-top: 3px; font-size: 12px;">' + show_items[id].enchant_desc + '</div>' +
					'</td>' +
					'</tr>' +
					'</table>' +
				 '</div>' +
				 '</div>' + 
				'<div id="itemBack">[ <a href="javascript: close_item(\'' + id + '\');">Back to Weapons</a> ]</div>';
				 
	container.innerHTML = output;
}
// show screens column
var show_screens = true;