function etc(info)
{
	this.id = info[0];
	this.name = info[2].replace(/\(Grade (.*)\)/i, '(Grade&nbsp;$1)');
	this.grade = first_to_upper(info[3]);
	this.type = info[4];
	this.obtain = first_to_upper(info[7]);
	this.level_req = (this.name.indexOf("High Degree") > 0) ? 40 : ((this.name.indexOf("Dye of") >= 0) ? 20 : info[6]);
	this.weight = info[5];
	this.pic = new Image();
	
	this.set_icon = function() { this.pic.src = '/images/item_icons/item_' + info[0] + '.jpg'; }
	
	this.row_data = make_row;
}

function make_row()
{
	return new Array(this.pic, this.name, this.grade, this.weight);
}

var my_header = new Array('Name', 'Grade', 'Weight');
var categories_select = new Array('Spellbooks', 'Amulets', 'Blueprints', 'Consumables', 'Scrolls', 'Dyes', 'Manor', 'Fishing', 'Misc.');

// returns matches
function sort_pages(all_my_items, to_show)
{
	my_matches = new Array();
	for(i=0; i < all_my_items.length; i++)
	{
		if(my_type(all_my_items[i].type) == to_show.toLowerCase())
		{
			my_matches.push(all_my_items[i]);
		}
	}
	return my_matches;
}

function my_type(text)
{
	switch(text.toLowerCase())
	{
		
		case 'shot':
		return 'etc';
		break;
		
		case 'spell_book':
		return 'spellbooks';
		break;
		
		case 'talisman':
		return 'amulets';
		break;
		
		case 'seed':
		return 'manor';
		break;
		
		case 'dye':
		return 'dyes';
		break;
		
		case 'potion':
		return 'consumables';
		break;
		
		case 'blueprint':
		return 'blueprints';
		break;
		
		case 'scroll':
		return 'scrolls';
		break;
		
		default:
		return text.toLowerCase();
	}
}