  var boxText = "Loading...";   // dialog box message
  var boxFont = "bold 14px Arial,Helvetica";  // dialog box font (CSS spec: "style size family")
  var boxFontColor = "#FFCC00";               // dialog box font color
  var boxWidth = 150;                         // dialog box width (pixels)
  var boxHeight = 100;                        // dialog box height (pixels)
  var boxBGColor = "#1D1915";                 // dialog box background color
  var boxBorder = "1px outset #383835";       // dialog box border (CSS spec: "size style color")

  var barLength = 120;                        // progress bar length (pixels)
  var barHeight = 5;                         // progress bar height (pixels)
  var barColor = "#0F4283";                   // progress bar color
  var barBGColor = "#191B0D";                 // progress bar background color

  var fadeInSpeed = 20;                       // content fade-in speed (0 - 30; 0 = no fading)*
  var safari = (navigator.userAgent.indexOf('Safari') != -1) ? true : false;

  if((document.all || document.getElementById) && !safari) {
    document.write('<style> .clsBox { ' +
                   'position:absolute; top:50%; left:50%; ' +
                   'width:' + boxWidth + 'px; ' +
                   'height:' + boxHeight + 'px; ' +
                   'margin-top:-' + Math.round(boxHeight / 2) + 'px; ' +
                   'margin-left:-' + Math.round(boxWidth / 2) + 'px; ' +
                   (boxBGColor ? 'background-color:' + boxBGColor + '; ' : '') +
                   (boxBorder ? 'border:' + boxBorder + '; ' : '') +
                   'z-index:69; ' +
                   '} .clsBarBG { ' +
                   'width:' + (barLength + 4) + 'px; ' +
                   'height:' + (barHeight + 4) + 'px; ' +
                   'background-color:' + barBGColor + '; ' +
                   'border-top:1px solid black; border-left:1px solid black; ' +
                   'border-bottom:1px solid silver; border-right:1px solid silver; ' +
                   'margin:0px; padding:0px; ' +
                   '} .clsBar { ' +
                   'width:0px; height:' + barHeight + 'px; ' +
                   'background-color:' + barColor + '; ' +
                   'border-top:1px solid silver; border-left:1px solid silver; ' +
                   'border-bottom:1px solid black; border-right:1px solid black; ' +
                   'margin:1px; padding:0px; ' +
                   'font-size:1px; ' +
                   '} .clsText { ' +
                   'font:' + boxFont + '; ' +
                   'color:' + boxFontColor + '; ' +
                   '} </style> ' +
                   '<div id="divBox" class="clsBox">' +
                   '<table border=0 cellspacing=0 cellpadding=0><tr>' +
                   '<td width=' + boxWidth + ' height=' + boxHeight + ' align=center>' +
                   (boxText ? '<p class="clsText" align=center>' + boxText + '</p>' : '') +
                   '<table border=0 cellspacing=0 cellpadding=0><tr><td width=' + barLength + '>' +
                   '<div id="divBarBG" class="clsBarBG"><div id="divBar" class="clsBar"></div></div>' +
                   '</td></tr></table>' +
                   '</td></tr></table></div>' +
                   '<div id="Content" style="width:100%; visibility:hidden">');
  }

//----------------------------------------------------------------------------------------------------
