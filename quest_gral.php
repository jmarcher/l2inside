<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Documento sin t&iacute;tulo</title>
<style type="text/css">
<!--
body,td,th {
	color: #FFFFFF;
}
body {
	background-color: #000000;
}
.Estilo1 {
	font-size: 18px;
	font-weight: bold;
}
.Estilo4 {color: #F09850}
.Estilo5 {color: #A0C4C0}
-->
</style></head>

<body>
<table width="70%" border="1" align="center" style="border-collapse:collapse">
  <tr>
    <td colspan="2"><div align="center" class="Estilo1">Nombre de la Quest </div></td>
  </tr>
  <tr>
    <td width="27%" bgcolor="#333333"><div align="center"><strong>Level</strong></div></td>
    <td width="73%"><div align="center">35</div></td>
  </tr>
  <tr>
    <td><div align="center"><strong>Start Location - NPC </strong></div></td>
    <td bgcolor="#333333"><div align="center">Dion Castle Town - Kash </div></td>
  </tr>
  <tr>
    <td bgcolor="#333333"><div align="center"><strong>Type</strong></div></td>
    <td><div align="center"><span><img src="http://lineage2.stratics.com/images/db/quests/quest_one_time.png" alt="One Time Only" title="One Time Only" /></span><strong>  <span class="Estilo4"> One Time Only</span> <img src="http://lineage2.stratics.com/images/db/quests/quest_solo.png" alt="Solo" title="Solo" /> <span class="Estilo5">Solo</span> </strong></div></td>
  </tr>
  <tr>
    <td><div align="center"><strong>Race</strong></div></td>
    <td bgcolor="#333333"><div align="center">Dark Elf, Elf, Human, Orc</div></td>
  </tr>
  <tr>
    <td bgcolor="#333333"><div align="center"><strong>Classes</strong></div></td>
    <td><div align="center">Bladedancer, Destroyer, Gladiator, Swordsinger, Tyrant, Warlord</div></td>
  </tr>
  <tr>
    <td><div align="center"><strong>Rewards</strong></div></td>
    <td bgcolor="#333333"><div align="center">Mark of Challenger, 8 Dimen. Diamonds, 72,394xp &amp; 11,250 SP</div></td>
  </tr>
  <tr>
    <td colspan="2"><div align="center"><strong>Desctription</strong></div>      
      <div align="center"></div>
    <div align="center"></div>      <div align="center"></div>
    <div align="center"></div>    <div align="center"></div>
    <div align="center"></div>    <div align="center"></div>
    <div align="center"></div>    <div align="center"></div></td>
  </tr>
  
  <tr>
    <td height="37" colspan="2"><div align="center">
      <p align="center"><strong>Trial of the Challenger <br />
        By Mame</strong></p>
      <p>Description: Quest to acquire occupation change item, 'Mark of the  Challenger'. Kash says he will tell you who has the Mark of the  Challenger, if you manage to kill the medusa, Shyslassys, and bring  back the old parchment that she holds. She lives in a cave in the  Neutral Zone, which is between the Elven and the Dark Forests. He  advises you to go in parties, for she is a strong enemy who has lived  for hundreds of years. </p>
      <p><img src="http://lineage2.stratics.com/content/library/quests/images/c4-trialofthechallenger/trialofthechallenger1.jpg" alt="k" /></p>
      <p>1. The quest begins in Dion Town, with Kash, who is located opposite  the entrance to the WareHouse. He sends you to the Undine Waterfalls in  the Neutral Zone (actually to the South East of the Swamplands) to  defeat the monster know as 'Shyslyssys' (she is actually Shyslassys). </p>
      <p><img src="http://lineage2.stratics.com/content/library/quests/images/c4-trialofthechallenger/trialofthechallenger2.jpg" alt="l" /><br />
          <img src="http://lineage2.stratics.com/content/library/quests/images/c4-trialofthechallenger/trialofthechallenger3.jpg" alt="l" /></p>
      <p>2. The cave itself is behind the where the water falls(located on  the map by a marker), and once inside you must search for Shyslassys.  She is surrounded by a small group of Cave Basilisks. They are all easy  to kill, but take some HP potions in-case of difficulties. </p>
      <p><img src="http://lineage2.stratics.com/content/library/quests/images/c4-trialofthechallenger/trialofthechallenger4.jpg" alt="l" /><br />
          <img src="http://lineage2.stratics.com/content/library/quests/images/c4-trialofthechallenger/trialofthechallenger5.jpg" alt="l" /></p>
      <p>3. Once you have killed Shyslassys you will earn the Scroll of  Shyslassys and also a broken key. A chest will appear and you can open  it with the key. The reward is either a small amount of adena or a  random piece of fabric. </p>
      <p><img src="http://lineage2.stratics.com/content/library/quests/images/c4-trialofthechallenger/trialofthechallenger6.jpg" alt="l" /></p>
      <p>4. Return to Kash with the Scroll, and you will receive the Letter  of Kash from him. Travel next to the Giran Town, to speak to Martien  (he is located on the map with pointer). </p>
      <p><img src="http://lineage2.stratics.com/content/library/quests/images/c4-trialofthechallenger/trialofthechallenger7.jpg" alt="l" /><img src="http://lineage2.stratics.com/content/library/quests/images/c4-trialofthechallenger/trialofthechallenger8.jpg" alt="l" /></p>
      <p>5. Martien will send you to cave in the area of Floran Village, to defeat an evil monster called 'Gorr'. </p>
      <p><img src="http://lineage2.stratics.com/content/library/quests/images/c4-trialofthechallenger/trialofthechallenger9.jpg" alt="ll" /><br />
          <img src="http://lineage2.stratics.com/content/library/quests/images/c4-trialofthechallenger/trialofthechallenger10.jpg" alt="l" /></p>
      <p>6. Gorr's cave is (located on the map by a marker) around the area  of the Monster Race Track and isn't hard to find. Inside you will find  a monster similar to Shyslassys and you must slay it. </p>
      <p><img src="http://lineage2.stratics.com/content/library/quests/images/c4-trialofthechallenger/trialofthechallenger11.jpg" alt="l" /></p>
      <p>7. Upon killing Gorr you will receive the Mark of the Watchers Eye,  return to Martien and present it to him. Martien will then send you  back to the same are to hunt a monster called 'Baraham' who dwells in a  cave near to where Gorr was. </p>
      <p><img src="http://lineage2.stratics.com/content/library/quests/images/c4-trialofthechallenger/trialofthechallenger12.jpg" alt="l" /></p>
      <p>8. After defeating Baraham in its cave (located by the map by a  marker) you will receive Watchers 2nd Eye. At the same time Raldo will  appear in the cave, instead of returning to Martien speak to Raldo. </p>
      <p><img src="http://lineage2.stratics.com/content/library/quests/images/c4-trialofthechallenger/trialofthechallenger13.jpg" alt="l" /></p>
      <p>9. Raldo will tell inform you that you have been misled, and that he  alone has the right to offer the Mark of Challenger. Offer your help to  amend your mistake, then travel to the Dwarven Village at the request  of Raldo. </p>
      <p><img src="http://lineage2.stratics.com/content/library/quests/images/c4-trialofthechallenger/trialofthechallenger14.jpg" alt="l" /><br />
          <img src="http://lineage2.stratics.com/content/library/quests/images/c4-trialofthechallenger/trialofthechallenger15.jpg" alt="l" /><br />
          <img src="http://lineage2.stratics.com/content/library/quests/images/c4-trialofthechallenger/trialofthechallenger16.jpg" alt="l" /></p>
      <p>10. Speak to Filaur of the Grey Pillar Guild (who is located on the  map by marker), and then travel to the Mithril Mines to fight and  defeat the Succubus Queen. </p>
      <p><img src="http://lineage2.stratics.com/content/library/quests/images/c4-trialofthechallenger/trialofthechallenger17.jpg" alt="l" /><br />
          <img src="http://lineage2.stratics.com/content/library/quests/images/c4-trialofthechallenger/trialofthechallenger18.jpg" alt="l" /><br />
          <img src="http://lineage2.stratics.com/content/library/quests/images/c4-trialofthechallenger/trialofthechallenger19.jpg" alt="l" /></p>
      <p>11. Be aware that the majority of monsters in the Mines are  aggressive, and although low lvl, in high numbers can present a threat.  Work your way down the into the Mithril Mines, descending carefully.  Continue until you reach a very large round room, where you should make  your way to the lowest level and exit down the tunnel there. Continue  down until you reach an area resembling this (see picture) and make  your way to the last platform. There you will find the Succubus Queen  and her minions. They are relatively easy to defeat, but bring HP  potions in-case of difficulties. </p>
      <p><img src="http://lineage2.stratics.com/content/library/quests/images/c4-trialofthechallenger/trialofthechallenger20.jpg" alt="l" /><br />
          <img src="http://lineage2.stratics.com/content/library/quests/images/c4-trialofthechallenger/trialofthechallenger21.jpg" alt="l" /><br />
          <img src="http://lineage2.stratics.com/content/library/quests/images/c4-trialofthechallenger/trialofthechallenger22.jpg" alt="l" /></p>
      <p>12. After defeating the Succubus Queen, Raldo will appear again.  Speak to him and he will congratulate you and award you with the Mark  of Challenger. </p>
      <p><img src="http://lineage2.stratics.com/content/library/quests/images/c4-trialofthechallenger/trialofthechallenger23.jpg" alt="l" /></p>
    </div>      <div align="center"></div></td>
  </tr>
  <tr>
    <td><div align="center"></div></td>
    <td><div align="center"></div></td>
  </tr>
</table>
</body>
</html>
