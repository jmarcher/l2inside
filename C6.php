<? if (isset($_GET['file'])){?>
<p><strong>Download here, a complete image gallery, from l2inside.org:</strong> <a href="http://gloomex.com/C6.zip">Click here to begin the download.</a> </p>
<p><b>Source:</b> <a href="http://lin2.enun.co.kr/" target="_blank">http://lin2.enun.co.kr/</a><br>
   <br>
  <b>Translation:</b><br>
   <br>
  
  <b><font size="4"><font color="darkred">The Chaotic Throne</font></font></b><br>
   <br>
  <font size="2">This winter, the updates continue with 'Chaotic Throne', the first 'Interlude' update to follow the Chaotic Chronicles. This interlude will introduce a number of new systems as described in this article.</font><br>
   <br>
  <img src="image/C6/1.jpg" alt="" width="580" height="362" border="0"><br>
   <br>
   <br>
  <b><font color="darkred">New hunting grounds</font></b></p>
<ul><li><font color="white"><b>Fight against the Primeval Creatures</b></font><br>
<font color="white">Hunting in the new Interlude will be ladden with suspense. Adventures against the primeval monsters of a new territory awaits you.</font></li><li><font color="white"><b>New hunting concept</b></font><br>

<font color="white">A new hunting area will be introduced - a hybrid between an instanced dungeon and an outdoor field. </font></li></ul><img src="image/C6/2.jpg" alt="" width="580" height="362" border="0"><br>
<i>Primeval Creature: Jurassic Park?</i><br>
 <br>
<b><font color="#8b0000">Evolving Items</font></b><ul><li><font color="white"><b>Variated Items: Refinement System</b></font><br>
<font color="white">The new 'Refinement System' will allow players to add a number of different attributes to items. For example, these attributes include a skill that enlarges your head (hehe) or a skill that allows you to B-soe.</font><br>
 <br>
<img src="image/C6/3.jpg" alt="" width="580" height="362" border="0"><br>
<i>Item Refining (note: Gemstones needed - 940 A grade gemstones)</i><br>
 <br>

<img src="image/C6/4.jpg" alt="" width="580" height="362" border="0"><br>
<i>Items showing different applied refinements</i><br>
<i>(Refinement stats from left to right: </i><br>
<i>- Activates Skill Blessed Scroll of Escape. Cannot be exchanged or dropped.</i><br>
<i>- Strength +1, Accuracy +2. Cannot be exchanged or dropped.</i><br>
<i>- HP +91, Critical +2.8%, Increases monster's hate on attack. Cannot be exchanged or dropped.</i><br>
<i>- Evasion +2, Accuracy +2, Activates skill that makes you invulnerable. Cannot be exchanged or dropped.)</i><br>
_</li><li><b>Capes now in development</b><br>
They haven't been completed yet but they're coming.</li><li><b>Rank Items</b><br>

New items that show your rank will be added.</li><li><b>New weapons</b><br>
New weapons will be added. At this point we're only sure of items between A and S grade. It's not clear we will see weapons above S grade in this interlude.<br>
<br>
<img src="image/C6/5.jpg" alt="" width="540" height="416" border="0"><br>
</li><li><b>New item slot</b><br>
A new item slot will be added to your inventory.</li></ul><b><font color="#8b0000">Beyond Castle Sieges</font></b><ul><li><b>Fortresses Introduced</b><br>
Fortresses will take its place between clan halls and castles. Castle-owning clans cannot participate in Fortress battles. They were designed specifically for smaller clans. Fortress battles will combine PvP and NPC fighting. They seem to be variations on Devastated Castle. Many other updates for small/medium sized clans will follow.<br>
<img src="http://img235.imageshack.us/img235/5059/21205cw7.jpg" alt="" border="0"><br>
<i>Image of a Fortress</i><br>

_</li><li><b>National Wars</b><br>
National wars will be larger in scale that castle sieges</li><li><b>Air Ships</b><br>
New technologies to be used in national warfare include air ships. Air ships will be used to transport armies when striking at full force.</li><li><b>Marine, Aerial Combat</b><br>
Plans are made to add battles on sea and in air.</li><li><b>New NPC force introduced</b><br>
Large battles aren't only against players. The new NPC race will be enemy to all.</li><li><b>Cross-server National wars</b><br>
Wars against nations on other servers are in production</li><li><b>Claim your honor</b><br>

Participating in national wars will bring you honor and riches.</li><li><b>The outcome of national wars define your server</b><br>
National wars will affect the entire server. If the war is lost, the town of the warzone will be unusable. If the war is won, the rewards will take the form of new dungeons or new shops.</li></ul><b><font color="#8b0000">Taking a break</font></b><ul><li><b>New Mini-Game</b><br>
A player-customizable mini-game will be added. It looks to be a type of fun fair. Its content will vary depending on the organizer.<br>
<img src="image/C6/7.jpg" alt="" width="580" height="362" border="0"><br>
<i>Mini-game World</i><br>
_</li><li><b>Wedding Hall added</b><br>
A new area for large player events such as a wedding will be added.<br>

<img src="image/C6/8.jpg" alt="" width="580" height="362" border="0"><br>
<i>Wedding Hall</i></li></ul>
<p><b><font size="4"><font color="darkred">Q&amp;A</font></font></b><br>
   <br>
    <b>Q: Do you plan to add anything to wolves? Other pets buff and heal and become mounts.. all wolves do is bite.</b><br>
  A: There will be a patch for pets. We're thinking of decreasing the feed usage and add more abilities, as well as evolvements for existing pets.<br>
  <br>
   <b>Q:There should be additional effects for weapons at +10 or +16. For example a stronger acumen or focus.</b><br>
  A: The new refinement system will take this into consideration.<br>
  
  <br>
   <b>Q: Castle sieges can get too confusing. Sometimes people die to clans without any tags and some clans change their clan tags to match the siege tags. Atleast 6 to 20 clans get involved at every siege and it gets impossible to target properly.</b><br>
  A: Additional colour coding is in development.<br>
  <br>
   <b>Q:All fantasy stories have holy swords that stand against demonic swords. Where is the holy sword in L2?</b><br>
  A:That will come in time. At this interlude we'll be adding a Sword of Blood. Unlike the demonic sword this one will have an active skill. There can also be a battle between the Demonic and Blood swords.<br>
  <br>
   <b>Q: Can you please add more clan halls. Most of the clan halls are taken by castle-owning clans leaving none for the smaller clans.</b><br>
  A: We're adding 21 fortresses to be used by small/medium sized clans.<br>
  
  <br>
   <b>Q: Please add system announcements so that we can know which party succeeded in levelling the soul crystals.</b><br>
  A: We're working on this. It will be added soon.<br>
  <br>
   <b>Q: When will we see the character services such as changing names and moving servers?</b><br>
  A: Around december we should be able to provide name changes. Server changes will take a little longer but it is definitely coming.<br>
  <br>
   <b>Q: Do you think the refinement system will add to an influx of unwanted items, throwing the economy off balance? Can the refinement effects be removed? Are the effects random or predetermined? What's the success rate?</b><br>
  A: The effects can be removed. because of the cost of refinement, we dont' think people will be making too many weapons for the sake of refining. Refinement succeeds at 100% and the effects are random.<br>
  
  <font color="Red">(note: In another article on <a href="http://www.playforum.net/lineage2/column.comm?action=read&amp;iid=10031004&amp;kid=8419" target="_blank">Playforum</a> it's said that the effects are not all random. The options come in different grades - some of these options are static, some of the options are partially random.)</font><br>
  <br>
   <b>Q: What happened to the new race?</b><br>
  A: We don't have any further announcements prepared on a new race.<br>
  <br>
   <b>Q: What will be the grade of the new items and what will be their special abilities?</b><br>
  A: You will see new items between A and S grades<br>
  
  <br>
   <b>Q: The bug patches come too late. When will you get to work on the existing bugs (i.e.Devil's Isle)</b><br>
  A: Once we receive reports of the bug it is sent to the Q/A team to test, and then the test results are sent to the development team to fix. So it all takes quite a bit of time. The bug you mention is probably already being looked at.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><strong><u>Internlude Patch Notes</u></strong><br />
(The parts that have a &quot;?&quot; means it's probably not the most accurate translation) <br />
<br />
Sorry for the bad formatting  I'll try to fix it <br />
<br />
Source: <a href="http://www.mylineage2.com" target="_blank">www.mylineage2.com</a><br />
<br />
<strong><u>Refinement System</u></strong><br />
<strong>&bull;C Grade to S Grade weapons can be refined </strong><br />
<strong>&bull;Required Materials: Weapon, Life Orbs, Gemstones</strong><br />
<em>-Life Orbs can be obtained through mobs<br />
-Boss has a higher chance of dropping higher level of Life Orbs <br />
-&amp;C Grade Weapon uses D Grade gemstone,  A&amp;S Grade Weapon requires C Grade Gemstones </em>&bull;Refinement has a 100% success rate<br />
<strong>&bull;Refinement Effects:</strong><br />
<em>-Increases power in STR/CON/DEX/MEN/WIT/INT<br />
-Increases attacking effect (increases critical, power, landing rate and etc)<br />
-Increases battling effects ( increases hate, chance of poisonous, sleep, root and other negative effects) </em>-Using skills (instant warp, healing, and other positive buffs)<br />
<strong>&bull;When using a weapon which is a higher grade than the player, these refinement effects will have no effects<br />
&bull;Refinement can exist at the same time as SA&rsquo;s, they won&rsquo;t affect each other <br />
&bull;Weapons that had been refined, can not be traded or sold<br />
&bull;After refinement has been taken off, the weapon can once again be traded or sold</strong><br />
<br />
<strong><u>Skills</u></strong> (for more specific information in skills please read VHS' Translation in Korean Patch Translation (Skills aka read below) <br />
<strong><strong>-</strong>More higher level active and passive skills <br />
Combo skills</strong> <br />
- <em>Combo skills can be used through 2-4 people<br />
- There are 2 Energy? Power, which can be casted (p.atk+m.atk) onto party members</em><br />
<strong>-Combo Magic Skills (powerful and larger area)</strong><br />
<em> - Increase all attacking skills within a certain area (for example, significantly increase bow attacks)<br />
- Create a significant strong defense shield (for example, dispell all  debuffs, and creating a short period of invincible affect)<br />
- Special Battle Ground Skills (such as long period damaging enemy's summons, heal a bigger area of clan members etc) </em><br />
[b]<strong>-</strong> A new siege summon (Hellrose's Clarification: And the new siege summon.. koreans are calling it Swoop Cannon )B]<br />
<em>- level 68 warsmith can learn this skill<br />
- Only the attacking side of the siege can use this, if you are defending the castle you can't use it<br />
- The Swoop Cannon has a cerrtain skill to aim a certain target inside the castle (area attack)<br />
- Another skill is some kind of bomb, and can be tossed and attack, uses this bomb to atk any parts of the castle walls<br />
- with the above skill, it affects EVERYONE in the area the toss lands (meaning you + your enemy) <br />
- These bombs can be bought through Ivory Tower <br />
- When Swoop Cannon is summoned, A Grade Crystal is required, and B  grade gemstone is required continuously through out the whole summon  time</em><br />
<strong><strong>-</strong>Clan <strong>Armors skills: (NC has made this into the new Clan Skills)</strong></strong><br />
<em> - Tieras/Crowns of each castle: has a defense to shocking/stunning attacks, cool time 10 minutes<br />
- Castle Leader Crown: can call out to all of the clan members, cool time 60 minutes <br />
~ when calling, the castle/clan leader can not move, all clan members  will receive a message when the clan leader uses this skill<br />
~ within 2 minutes, clan members that have arrived the castle and talk  to the NPC will be teleported to the clan leader's side, number of clan  members is limited <br />
~All of the clan member's Tiaras and Crowns can be bought inside the  castle. Castle Leader's Crown generates automatically, and this can  only be used by the clan/castle leader<br />
~ Clan members in the Academy can not use any of these Tiaras/Crowns<br />
- 3rd Class skills can now be enchanted<br />
- Some fearing skills can now affect players<br />
- Some skills powers had been adjusted (some got stronger, some weaker)<br />
- All p.atk power has increased significantly with soulshot is used.</em><br />
<br />
<u><strong>Jerassic Park?</strong></u><br />
<strong>-</strong> Located south of Rune, East of the Olympid Area, can be teleported from Rune<br />
<strong>-</strong> this area is high level hunting area, including new raid boss <br />
<strong>-</strong> Boss: Siren / Seiren <br />
- Located inside the Siren Cave inside the Jerassic Par, it's located away from the hunting area<br />
- To enter this cave, quests need to be completed<br />
<strong>-</strong> Raid: Uruka <br />
<strong>-</strong> Mobs in this area can get very strong sometimes out of  berserk, however there is capturing system you can use to lower the  difficulties<br />
<strong>-</strong> Capturing System? It's not to capture and breed the mobs, but  it's to significantly weaken the mob, and this also gives other rewards<br />
<em> - To start this capturing system, quests need to be completed<br />
- Capturing System requires the use of items which is sold on the island.</em><br />
<br />
<strong><u>Monsters </u></strong><br />
-Antharas + Valarka difficulty has been lowered<br />
-A new way of killing Antharas<br />
<em>- The level of difficulty of Antharas will change according to the amount of people raiding it<br />
- Difference in level of difficulty will vary the difference in rewards<br />
- a new Antharas minion has been added when raiding Antharas</em><br />
<br />
<strong><u>Battling System</u></strong> <br />
(Thanks to Hellrose for clarifying this, Hellrose's explanation: The  'Battling system' is a duelling system and it's consentual. It's  separate from the freeform PvP system.. so yes you will still lose XP  from regular pvp.)<br />
<br />
- Dying from PVP will not have any penalties<br />
- This has to happen within a non-safe area (aka not in the towns or castles etc)<br />
- After PVP, those who anticipated will have restoration in CP100%, MP/HP50% (they didn't say whether it's the clan war or PVP)<br />
- During PVP, instant warp skills can not be used<br />
- During PVP, battle can be cacelled through NPC<br />
<strong>2 people PVP System </strong><br />
-Requirement 1 vs 1 (duh... not sure why they wrote it there)<br />
-Before PVP starts, one has to invite, and the opponent can refuse or agree to anticipate <br />
-PVP time is 2 minutes, until one's HP goes down to 1HP, the system will announce the winner (no death penalty, no death) <br />
<strong>Group PVP System</strong><br />
- one party can invite the other party to pvp, but the opponent party can refuse such invitation<br />
- PVP duration is 5 minutes, all party members can not be doing  something else? (siege/olympid/death/other battles?) &lt;-- this  doesn't make too much sense <br />
<br />
<br />
<strong><u>Items </u></strong><br />
-Top A Grade Weapons <br />
- Top A grade weapons uses level 12 Soul Crystals<br />
- Hair Accessories (there is a lot of them, not going to list them all :of_happy however they can be obtained by:<br />
<em>- raid bosses / ceremony of darkness / dimensional rift<br />
- Christmas Events <br />
- Giran Luxury Store </em><br />
<strong>Projection Weapons (what I indicated before as Illusionary Weapons)</strong><br />
- These weapons lasts within a time period <br />
- As soon as you equip them, the weapon life time will start to go down, as soon as you unequip, it will stop <br />
- When its life time goes to 0, this weapon will disappear<br />
- This can not be traded, however can be stored <br />
- can not be enchanted, can not be equipped with SA or refined<br />
- D and C grade weapons can be acquired after 2nd class transfer with some kind of tickets<br />
- C and B can be used with all chars above level 40, and these can be bought from NPC <br />
<strong>Cursed Weapon #2: Blood Sword</strong><br />
-the way this appears and disappears as well as killing wise is the same as the demon sword<br />
- Also cursed weapons will disappear 24 hours after PK<br />
- Any one of these 2 cursed weapons disppear / levels up/ appears will cause the other one level up<br />
- cursed weapons get their own active attack skills? <br />
<br />
<br />
<strong><u>Quests</u></strong> <br />
- New Quests: not listed but they are the similar to the other quests<br />
- new &quot;let's play poker&quot; and ... (not sure how to translate the other  one into English, Chinese names are pretty fancy and sophisticated and  they don't need to have a meaning), lasts 2 hours mini game <br />
- Clan Reputation quests, mainly killing mobs on the island (didn't indicate which island)<br />
- Capturing System quests <br />
- A grade weapon materials and recipes exchange, mainly killing mobs on  the island (I assume they mean the Jerassic park island?)<br />
- Before going into the Siren's Cave, 3 continuous quests are required  to be completed, one of them can be repeated in which you gain entrance  to the cave<br />
- Level 11 and 12 Soul Crystals can now be obtained through Siren as well as Uruka<br />
- When a level 11 or higher soul crystal is succefully obtained, all  players in the nearby area will receive a message from the system<br />
- After completion of 1st / 2nd class change, players will be able to obtain the ticket to exchange for projection weapons<br />
<br />
<br />
<strong><u>Other</u></strong> <br />
<strong>Command channel changes</strong><br />
- interface looks better, can view how the other parties are doing in detail<br />
- Alliances meet the following 2 requirements will have the first chance to pick up the drops (and you have 5 minutes to do so)<br />
<em> - raiding with 18 people or more, boss-raiding with 36 people or more <br />
- does the very first strike to the RAID/BOSS</em><br />
-5 minutes after the raid, normal players can pick up the drops <br />
-some pets food consumption had been lowered<br />
- Party interface has been changed<br />
- Map/Area information interface has been changed, and some maps for new players which I don't understand it myself <br />
<strong>Saven Sign </strong><br />
-When Dawn's Seal of Strife is activate, during siege Attacker can't use Wild hog, golem, and the siege frog</p>
<p><strong><u>*Passive Skill</u></strong><br />
  <br />
  <strong>Inner rhythm </strong>- Song/Dance MP consumption decreased<br />
  <br />
  <strong>Knighthood </strong>- When wearing Heavy Armor def is raised plus shield def increased.<br />
  <br />
  <strong>Master of Combat</strong> - When using (Dagger,Blunt,Polearm, Dual,and fist weapons) P atk and max CP increases.<br />
  <br />
  <strong>Archery </strong>- When using Bow type weapon your patk and max range increases,<br />
  <br />
  <strong>Assassination </strong>- When using Dagger type weapon your p atk is increased and chance to hit targets vital part increases.<br />
  <br />
  <strong>Arcane Roar </strong>- Resistance against elemental attributes and M atk increased. <br />
  <br />
  <strong>Necromancy </strong>- Resistance against Unholy attribute and m atk increased.<br />
  <br />
  <strong>Summon Roar</strong> - When wearing Robe/Light armor def and magic casting speed is raised. Magic skill MP consumption is decreased.<br />
  <br />
  <strong>Divine Roar</strong> - Mp consumption for magic skills are decreased.<br />
  <br />
  <strong><u>*Active skill </u></strong><br />
  <br />
  <strong>Brave Heart </strong>- by raising ones fighting spirit you regenerate CP <br />
  <strong><br />
Force Meditation</strong> : Uses force energy to recover HP/MP  while unable to move. During the meditation effect def is lowered and  will be disrupted when struck,<br />
<strong><br />
Sonic Barrier</strong> : Uses force energy to make barrier . Becomes invulnerable to normal attack , skill , buff/debuff<br />
<br />
<strong>Force Barrier</strong> : SAME AS SONIC BARRIER<br />
<br />
<strong>Mirage </strong>: Chance to push back a target that attacked you.<br />
<br />
<strong>Dodge Attack </strong>: Can dodge melee attacks momentarily.<br />
<br />
<strong>Counter Attack</strong> : Counters a melee attack and returns the dmg to the attacker.<br />
<br />
<strong>Summon King of Cat</strong> : Summons the King of cat.<br />
<br />
<strong>Summon Unicorn Magnus </strong>: Summons Unicorn Magnus<br />
<br />
<strong>Summon Spectral Lord </strong>: Summons spectral Lord<br />
<br />
<strong>Cleanse </strong>: Removes all the debuffs on a target.<br />
<br />
<strong>Salvation </strong>: Upon death you can resurrect in full health. Buff  and Debuff still remains after death. Except Blessing of Nobless and  Lucky charm disappears.<br />
<br />
<strong>Mystic Immunity</strong> : For short duration you are immune to buff / debuffs. <br />
<br />
<strong>Spell Turning </strong>: Disrupts the targets casting and cancels the magic.<br />
<br />
<strong>Chant of Magnus</strong> : The spirits of ancient magician temporarily possess one's party members. - uses soul ore <br />
<br />
<strong>Victory of Paagrio </strong>: The spirits of ancient heroes temporarily possess one's ally members. - uses soul ore <br />
<br />
<strong>Emblem of Paagrio </strong>: Temporarily increases ALLY members' resistance to buff cancel and de-buff attack<br />
<br />
<strong>Fist of Paagrio</strong> : Increases MAX CP for Ally members' and restores the cp that has been increased. - uses soul ore <br />
<br />
<strong>Chant of Gate </strong>: Summons a Party member (requires material for summoning )<br />
<br />
<strong>Summon Attract Cubic </strong>: Summons Attract Cubic<br />
<strong><br />
Banish Holy</strong> : Instills a fear in Angels and makes it  run away. Can Instant kill. Uses spirit ores.<br />
<br />
<strong>Break Duress :</strong> Channels force energy to break away from hold state.<br />
<br />
<strong>Sonic Move</strong> : Movement speed increased for short duration by exploding ones force energy.<br />
<br />
<strong>Shock Stomp </strong>: By stomping on the ground your enemy target will lose target and goes into shock status.<br />
<br />
<strong>Escape Shackle</strong> : Escapes from captured state<br />
<br />
<strong>Aura Flash</strong> : Attacks nearby enemy targets (non elemental) and their target canceled. - Uses soul ore<br />
<br />
<strong>Celestial Shield</strong> : Protects the target with holy power. (Target becomes invulnerable for duration)<br />
<br />
<strong>Invocation </strong>: MP regeneration speed will greatly increase when  you become immobile state. During the Duration Defense will greatly  increase and will cancel when hit.<br />
<br />
<strong><u>*Change of Current Skills</u></strong><br />
<br />
<strong>Stun Shot </strong>- Power has been greatly increased.<br />
<br />
<strong>Benediction </strong>- Requirements has been changed to (Hp 25% -&gt; MP 25%)<br />
<br />
<strong>Mana Burn / Mana Storm</strong> - Slept targets can be awoken up.<br />
<br />
<strong>Death Whisper/ Dance of fire</strong> - When doing critical dmg the crit attack power has been decreased.<br />
<br />
<strong>Recharge </strong>- Classes that knows recharge skills can not be recharged.<br />
<br />
Mages magic skill has been changed to where a certain percentage can be blocked by a shield.<br />
(Wind Strike, Aqua Wall, Twister, Blaze, Death Spike, Prominense, Ice dagger , and Hurricane)<br />
<strong><br />
Greater Heal and Chant of life/ Heart of Paagrio</strong> can stack. And it will be shown on buff slot.<br />
<br />
HP potions effects will be changed from buff slots to a desinated HP potion effect slot.<br />
<br />
<strong>Major Heal </strong>- For example as Bishop you will be able to learn 11 lvs of the skill and can still skill enchant it.<br />
<br />
<strong>Curse Fear , Horror , Fear </strong>- This has been changed so that it will also effect the PC.<br />
<strong><br />
Soul Shot</strong> - Effect of soul shot has been greatly increased.<br />
<br />
<strong>Magical Vampire </strong>- This skill's effect has been increased.<br />
<br />
The following skills power has been greatly increased.<br />
(Spinning Slasher, Thunderstorm, Hammer Crusher, Soul Breaker, Tribunal, Judgement, Armor Crush, Shock BLast )<br />
<br />
The following skills can be skill enchanted.<br />
(Tribunal, Judgement, Arrest, Shackle, Mass shackle, Banish undead,  Critical blow, Might Mortal, Stealth, Sand Boom?, Mass curse fear,<br />
Mass curse gloom, Trance, Erase, Magical Vampire, Mana burn, Mana  Storm, Turn undead, Major Heal, Group Heal, Summo Swoop Canon, Shock  stomp, Aura Flash, Banish holy.)<br />
<br />
WEAPONS<br />
<br />
Name of weapon, types of weapon, patk/matk, ss/sps SA (all lvl 12 crystal) (red, green, blue)<br />
<br />
Tongue of Themis &ndash; one handed sword, 202/161  1/1  mana up, magic shield, magic focus<br />
<br />
Tiphon Spear-pole, 251/121   1/1   Crt. Stun, Long blow, Wide blow<br />
<br />
Naga Storm- dagger, 220/121   1/1 focus, crt. Damage, back blow<br />
<br />
Sirr Blade- one handed sword, 251/121    1/1 haste, health, crt. Poison<br />
<br />
Barakiel Axe- blunt, 251/121   1/1 health, haste, focus<br />
<br />
Sword of ipos- two handed sword, 305/121    1/1 focus, haste, health<br />
<br />
Sobekk hurricane- fist, 305/121     1/1 rsk. Haste, haste, crt. Drain<br />
<br />
Shyid bow- bow, very slow, 570, 133    2/1 cheap shot, focus, quick recovery<br />
<br />
Hand of cabrio- blunt, updown( I think it&rsquo;s conversion) mana up, magic silence<br />
<br />
Crystal of demon- blunt, two handed sword, 245/161   1/1   mana up, acumen, magic shield<br />
<br />
Tuning fork of behemoth- blunt, two handed sword, 305/121   1/1   focus, health, anger</p>
<p>&nbsp;</p>
<p><strong><a name="new" id="new"></a>Weapon Augmentation</strong></p>
<p>In Interlude, you will be able to further refine your weapon of choice through a new system called augmentation. The Blacksmith will help you add new bonuses to your C Grade through S Grade weapon. All that's required is a Life Stone and Gemstones.</p>
<p>Over 150 new bonuses for your weapon will be at your disposal! The types of bonuses include:</p>
<p>Increased basic abilities, such as Strength and Intelligence<br />
  Increased battle prowess, such as attack, defense, or accuracy<br />
  Additional skills</p>
<p>Augmentations will allow even more weapon customization than ever before! These new bonuses can be added on top of any enchantment and special ability you have already placed on your weapon.<br />
</p>
<p><strong>Primeval Isle </strong></p>
<p>A lush, tropical island has been discovered to the south of Rune Township. Named the &quot;Primeval Isle,&quot; this new area can be reached by ferry from Rune Harbor.</p>
<p>Only high level and experienced players should venture to the island. Full of mysterious and fearsome creatures, the isle will be a test of your skill in battle.</p>
<p>But the challenge is worth the risk! On this isle you will find unique monsters, a clan reputation quest, a quest to find a new ingredient for an A Grade weapon, and more. You will also experience a new way of fighting when you use a trapping skill to assist in your battle against the mighty Tyrannosaurus Rex.</p>
<p>Discover the Primeval Isle and its fearsome inhabitants in Interlude!</p>
<p>&nbsp;</p>
<p><strong>Other Features</strong></p>
<p>Interlude also adds other exciting new features to Lineage II, including:</p>
<p>Over 50 new skills for high level players <br />
  New head accessories and A Grade weapons <br />
  A new demonic weapon for intense PvP combat <br />
  A dueling system to test your mettle against your friends <br />
  And more!</p>
<p>We hope you enjoy Interlude as you embark on a new journey in Lineage II: The Chaotic Throne.</p>
<? }else{
include "error.php";
}
?>