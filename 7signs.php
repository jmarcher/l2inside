<? if (isset($_GET['file'])){?>
<table width="100%" border="1" align="center" style="border-collapse:collapse ">
  <tr>
 
    <td width="32%" background="#1"><div align="center" class="Estilo26"><strong><span class="Estilo14"><a href="#1">Joining and Withdrawing from the Seven Signs Cabals</a></span></strong></div></td>
    <td width="12%"><div align="left" class="Estilo14 Estilo26 Estilo29">
      <div align="center"><a href="#2" class="Estilo33">Action of the Seals </a></div>
    </div></td>
    <td width="21%"><div align="center" class="Estilo30"><span class="Estilo14"><a href="#3"><span class="Estilo33">Catacomb and Necropolis, Oracle</span> </a></span></div></td>
    <td width="10%"><div align="center" class="Estilo30"><span class="Estilo14"><a href="#4">Other matters</a> </span></div></td>
    <td width="25%"><div align="center" class="Estilo30"><span class="Estilo32"><a href="index.php?file=unseal">Ancient Adena Manager (Unsealing) </a></span></div></td>
 
  </tr>
</table>
<p align="center">&nbsp;</p>
<p align="center" class="Estilo16"> Seven Signs At A Glance </p>
<p align="center"><img src="image/7signs.png" width="384" height="357" /></p>
<table width="100%" border="0" align="center">
  <tr>
    <td><div align="center"><span class="Estilo16">Joining and Withdrawing from the Seven Signs Cabals<a name="1" id="1"></a></span></div></td>
  </tr>
</table>
<table width="100%" border="0" align="center">
  <tr>
    <td width="980"><p align="center">The players who want to participate in the Seven Signs must join one of the Cabals, either the Revolutionary Army of Dusk ("Dusk") or the Lords of Dawn ("Dawn") for a certain period of time. Once they join the Cabals, it is impossible for them to withdraw until a new Seven Signs starts after two weeks, and, at the time of joining, they must choose the Seal they want to possess. </p>
      <p align="justify" class="Estilo18">1) Completed the second class transfer: </p>
      <blockquote>
        <p align="justify">Lords of Dawn: <br />
      Members of clans who own a castle and their alliances may join the Lords of Dawn. Also, players who are not members of the castle-holding clans or alliances but willing to pay 50,000 adena are able to choose Dawn through the Priests of Dawn in each village during the competition period. The Lord of each castle also has the privilege to allow an additional 300 characters to join. During the competition period, the Lords of the castles may purchase 300 of the Approval Certificates through the Chamberlain and transfer these Certificates to other characters. </p>
        <p align="justify"> Players who are not members of castle-owning clans need to get an Approval Certificate from the Lords to join Dawn, or pay 50,000 adena. The Approval Certificates will never expire and are valid for any event period of the Seven Signs. However, Approval Certificates will disappear after use. </p>
        <p align="justify">Revolutionary Army of Dusk: <br />
    Only players who are not members of the castle clans and guilds may join Dusk. During the registration period, they may join through the Priests of Dusk in each village. </p>
      </blockquote>      <p align="justify"><span class="Estilo18">2) Completed the first class transfer but not the second: </span><br />
  They may freely join either the Lords of Dawn or the Revolutionary Army of Dusk. <br />
  <br />
  <span class="Estilo18">3) Have not completed the first class transfer: </span><br />
  They may not participate in the Seven Signs. </p></td>
  </tr>
</table>
<p>&nbsp;</p>
<table width="100%" border="0" align="center">
  <tr>
    <td width="980"><p align="justify"> <span class="Estilo18">1) Collecting Seal stones (Blue, Green, and Red)</span> <br />
Each Cabal may hunt the monsters within the Catacomb and Necropolis to acquire Seal Stones. The items acquired in this way are entrusted to the Priest of each side , and then, based on the number of items, the scores contributed to Seven Signs by each player are accumulated and included in the total score of the whole Cabal. [Contribution points to the Cabal will be different based on stone color: Blue (1), Green (5), and Red (10)]. Seal Stones are the items that can be exchanged among the players, and even if the items are not entrusted to the Priest during the competition period, they do not disappear and can be used during the next competition period. </p>
    </td>
  </tr>
</table>
<p>&nbsp;</p>
<table width="100%" border="0" align="center">
  <tr>
    <td width="980"><p align="justify">&nbsp; </p>
      <p class="Estilo18">2) The Festival of Darkness </p>
      <ul>
        <li><strong>Overview </strong><br />
    One chosen party participates in this challenge for the victory of the Cabal. Of the players participating in the Festival of Darkness, the party who recorded the best score in each grade will contribute some points to the Cabal it belongs to. The Festival of Darkness is divided by level and any players belonging to the Cabals of Dawn and Dusk can participate in it. </li>
      </ul>
      <ul>
        <li><strong>How to participate </strong><br />
    Players may participate only during the competition period. The party who wants to participate should pay Seal Stones to the Guide of the Festival in the Oracle. Priests in towns will teleport you to Oracle for free. The entry fee varies depending on the level and will be paid by party leaders. The party must consist of minimum of 5 members belonging to the same Cabal and the members of the party must correspond to the grade of the Festival they want to participate in. The characters who correspond to the level of the Festival, but have higher level skills than the party members may not participate. This happens when the character is de-leveled, but his skills learned from the previous higher level remain. These challenges are always possible during the competition period and there is no limit in the number of challenges. </li>
        <li><strong>Festival of Darkness </strong><br />
    In the Oracle, where the Festival of Darkness occurs, items are not dropped even after death with the exception of Chaotic players. Experience loss is reduced to a quarter of the normal death penalty. If the competition is restarted within the Oracle, your party is moved to the nearby village when restarting. Within the center magic array in the Oracle MP regenerates more quickly than normal. Players can be teleported to the Oracle and, when coming out, return to the place they left free of charge through the Priest of each village. When players die, they will restart inside the room. Resurrection is not allowed within the Oracle. Archer monsters in the corners may pinpoint their attacks on healer classes. </li>
      </ul>
      <ul>
        <li><strong>Type of challenges </strong><br />
    The Festival of Darkness is divided into five grades, such as less than a 32 level, less than a 43 level, less than a 54 level, less than a 65 level and an unlimited level. </li>
      </ul></td>
  </tr>
</table>
<p>&nbsp;</p>
<table width="100%" border="0" align="center">
  <tr>
    <td width="980"><table cellspacing="0" cellpadding="0">
      <tr>
        <td class="Estilo16"><div align="center">Action of the Seals <a name="2" id="2"></a></div></td>
      </tr>
      <tr>
        <td><span class="Estilo18">I
      1) Seal of Avarice </span><br />
      <ul>
        <li> The teleport NPC at the entrance of the Necropolis moves only the winner inside the dungeon, and they can receive various kinds of buffs if they pay with Ancient Adena. If they don't own the Seal, they cannot enter. </li>
        <li> Able to enter all 8 Necropolis freely. </li>
        <li> In the Necropolis, monsters are generated for the winner to hunt. </li>
        <li> They can meet Anakim or Lilith in the Disciples Necropolis. Anakim and Lilith are related to the blessed Soul Crystal which gives the special ability to use A-grade weapons. </li>
        <li> The Trader of Mammon appears inside of the dungeon. They can purchase a variety of items that cannot be purchased in the general stores by paying with Ancient Adena. The Trader of Mammon is not staying in one place, but often moves around each dungeon. </li>
      </ul>
      <p align="left" class="Estilo18"> 2) Seal of Gnosis </p>
      <ul>
        <li> The teleport NPC at the entrance of the Catacombs moves only the winner to the inside of the dungeon. If they don't own the Seal, they cannot enter. </li>
        <li> Able to enter all 6 Catacombs. </li>
        <li> The winner can be teleported to the hunting field by paying the priests of the village with Ancient Adena at a cheaper price. </li>
        <li> A hostile NPC moves around each village, except the Beginner's Village, and randomly casts various kinds of harmful magic onto members of the losing side. </li>
        <li> A friendly NPC moves around each village, except the Beginner's Village, and randomly casts various kinds of support magic to the winner's side. </li>
        <li> The Blacksmith of Mammon appears inside the Catacombs. The Blacksmith of Mammon receives Ancient Adena for giving the special ability to use the A-grade weapons, removing the Seal on the armor, making it possible to upgrade the weapons to higher-level weapons, and exchanging it free of charge with other kinds of weapons at the same level. The Blacksmith of Mammon also moves around each dungeon continuously. </li>
      </ul>
      <p align="left" class="Estilo18"> 3) Seal of Strife </p>
      <p align="left"><strong> - If owned by Dawn </strong></p>
      <ul>
        <li> At the time of siege warfare, it becomes possible to hire the elite and Dawn mercenaries, in addition to the existing mercenaries. </li>
        <li> The cost required for the upgrade of the castle gates and walls is slightly reduced. </li>
        <li> The defensive power of the castle gates and walls is slightly increased. </li>
        <li> The maximum tax rate that the lord of each castle can establish is increased to 25%. </li>
      </ul>
      <p align="left"><strong> - If owned by Dusk </strong></p>
      <ul>
        <li> At the time of siege warfare, it becomes impossible to hire the existing mercenaries and possible to hire only low-level mercenaries. </li>
        <li> The cost required for the upgrade of the castle gates and walls is greatly increased. </li>
        <li> The defensive power of the castle gates and walls is greatly reduced. </li>
        <li> The maximum tax rate that the lord of each castle can establish is decreased to 5% . </li>
    </ul></td>
      </tr>
    </table>    <p align="justify">&nbsp;</p>
    </td>
  </tr>
</table>
<p>&nbsp;</p>
<table width="100%" border="0" align="center">
  <tr>
    <td width="980"><table cellspacing="0" cellpadding="0">
      <tr>
        <td class="Estilo16"><div align="center">Catacomb and Necropolis, Oracle <a name="3" id="3"></a></div></td>
      </tr>
      <tr>
        <td> Sixteen dungeons were added in relation to the Seven Signs. The Oracle is the space to hold the Festival of Darkness, and, in the Catacomb and Necropolis, the players can hunt the monsters and get Seal Stones necessary for the acquisition of the Seals. These monsters, represented by Lilim and Nephilim, drop Seal Stone, but, instead, do not drop the Adena. Access to these dungeons is limited based on the participation in the Cabal and ownership of the Seals. <br />
            <p class="Estilo18"> 1) Necropolis </p>
            <ul>
              <li> <strong>Necropolis of Sacrifice</strong> : level 20~30 monsters appear and it is located on the southern seashore of the Gludio Territory. </li>
              <li><strong> Pilgrims Necropolis</strong> : level 30~40 monsters appear and it is located near the Partisan's Hideaway in Dion Territory. </li>
              <li> <strong>Worshipers Necropolis</strong> : level 40~50 monsters appear and it is located near the Alligator Island in the Innadril Territory. </li>
              <li><strong> Patriots Necropolis</strong> : level 50~60 monsters appear and it is located above the Gludio Castle in the Gludio Territory. </li>
              <li> <strong>Ascetics Necropolis</strong> : level 60~70 monsters appear and it is located near the Altar of Rites in the Oren Territory. </li>
              <li> <strong>Martyrs Necropolis</strong> : level 60~70 monsters appear and it is located near the Giran Castle in the Giran Territory. </li>
              <li><strong> Saints Necropolis </strong>: level 70~80 monsters appear and it is located near the Field of Whispers in the Innadril Territory. </li>
              <li> <strong>Disciples Necropolis</strong> : level 70~80 monsters appear and it is located near the Devastated Castle in the Aden Territory. From here, the players can move to the place where Anakim or Lilith appear, based on the ownership of the Seals. </li>
            </ul>
            <p class="Estilo18"> 2) Catacomb </p>
            <ul>
              <li> <strong>Heretics Catacomb</strong> : level 30~40 monsters appear and it is located near the Execution Ground in the Dion Territory. </li>
              <li> <strong>Catacomb of the Brande</strong>d : level 40~50 monsters appear and it is located near the Giran Port in the Giran Territory. </li>
              <li> <strong>Catacomb of the Apostate</strong> : level 50~60 monsters appear and it is located near the Plains of the Lizardmen in the Oren Territory. </li>
              <li> <strong>Catacomb of the Witch</strong> : level 60~70 monsters appear and it is located near the Forest of Mirrors in the Aden Territory. </li>
              <li> <strong>Catacomb of Dark Omens</strong> : level 70~80 monsters appear and it is located near the Dark Elven Village in the Oren Territory. </li>
              <li><strong>Catacomb of the Forbidden Path</strong> : level 70~80 monsters appear and it is located near the Hunters Village in the Aden Territory. </li>
            </ul>
            <p class="Estilo18"> 3) Oracle </p>
            <ul>
              <li> <strong>Oracle of Dawn</strong> : located near the Orc Barracks in the Gludio Territory. </li>
              <li><strong> Oracle of Dusk</strong> : located near the Windy Hill in the Gludio Territory. </li>
              <li> One cannot initiate Private Store inside the Oracle. Inside Oracle is Peace Zones. </li>
            </ul>
            <p><span class="Estilo18">4) Access to the Dungeons </span><br />
        For the first week to acquire the Seals (hereinafter the "Competition Period"), any characters who joined a Cabal can access the dungeons. That is, the characters that did not join a Cabal cannot access the dungeons. After the competition period is over, for the next week when the Seals are effective (hereinafter the "Seal Effective Period"), only the Cabal who owns the appropriate Seal can have access. The loser, or even the winner who did not achieve the percentage necessary for the acquisition of the appropriate Seal, cannot access the dungeons for one week while the Seals are effective. <br />
            </p>
            <p><span class="Estilo18">5) The monsters in the Dungeons </span><br />
        The Cabals can acquire Rune and Mantra by hunting the monsters in the dungeons. Nephilim drops Rune necessary for Dusk and Lilim drops Mantra necessary for Dawn. In the Competition Period, the general monsters, Lilim and Nephilim all appear, but if the players acquired the Seal during the Seal Effective Period, the general monsters and the monsters who drop only the items necessary for the winner appear (if owned by Dawn, they drop Mantra and if owned by Dusk, they drop Rune). </p></td>
      </tr>
    </table>    <p align="center">&nbsp;</p>
    </td>
  </tr>
</table>
<p>&nbsp;</p>
<table width="100%" border="0" align="center">
  <tr>
    <td width="980"><table cellspacing="0" cellpadding="0">
      <tr>
        <td class="Estilo16"><div align="center">Other matters <a name="4" id="4"></a></div></td>
      </tr>
      <tr>
        <td><p><br />
            <span class="Estilo18">1) Seal Stones </span><br />
        Rune and Mantra both are exchangeable and can be acquired by hunting Lilim, Nephilim, Gigant, or Nephilim in the Catacomb and Necropolis. The value of Runes are higher in the order of Blue &lt; Green &lt; Red. If entrusted to Priests in town, it will add contribution points to your Cabal. Each of Blue, green, and Red contribute points as much as 3, 5, and 10 respectively. <br />
        <br />
          </p>
            <p><span class="Estilo18">2) Ancient Adena </span><br />
        Ancient Adena is an item that can be acquired through Seal Stones, converted to regular adena by 1:1 through the Black Market Trader of Mammon, and used to purchase the items from the Priests of Dawn or Dusk, Blacksmith of Mammon and Trader of Mammon or utilize various kinds of functions. </p>
            <p><span class="Estilo18">3) The Record of Seven Signs</span> <br />
        This is the item that allows you to see overall situations, main events and Seal states of the Seven Signs at a glance. Anyone can buy this item at 500 adena through the Priests of Dawn or Dusk. </p>
            <p><span class="Estilo18">4) Approval Certificates of the Lord </span><br />
        With this item, even the characters that are not members of castle-holding clans and completed the second change of occupation can join Dawn. </p>
            <p><span class="Estilo18">5) The Black Market Trader of Mammon</span> <br />
        They exchange Seal Stones and also exchange Ancient Adena with Adena. </p></td>
      </tr>
    </table>
    <p align="center">&nbsp;</p></td>
  </tr>
</table>
<? }else{
include "error.php";
}
?>