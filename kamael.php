<? if (isset($_GET['file'])){?>
<p><strong>Download the l2inside official picteres gallery of kamael here!!!: <a href="http://l2inside.org/Kamael.zip">http://l2inside.org/Kamael.zip</a></strong></p>
<p><strong>Caotic Throne 1 Patch Notes</strong><br />
  <br />
  <br />
  <strong>Fortresses</strong><br />
- 21 new fortresses added<br />
- each fortress will be part of castles ruling area, each fortress will  have the choice to choose whether or not the castle has the ruling powr  over them. <br />
<br />
Fortresses Wars<br />
- each lasts 1~ 4 hours<br />
- after declaration of siege, the siege starts in 60 minutes<br />
- fortresses all contain [allies, attackers, non-related people]<br />
- people within the alliance will be categorized on the same side <br />
- anyone that's not belong to the same alliance but is attending the siege is categorized as non-related people<br />
<br />
Declaration of Siege on Fortresses <br />
- when declaration is announced, each guild is treated as 1 unit<br />
- any guilds above level 4 can declare<br />
- when the first declaration is announced (by any guild), in 60 minutes  the siege starts, within the first 50 minutes other guilds can declare  as well. <br />
- the last 10 minutes is treated as &quot;preparation time&quot; no other guild is allowed to declare anymore. <br />
- When the guild wants to change/cancel the declaration/siege, talk to NPC to cancel<br />
- any guild that owned this fortresses will automatically be assigned as the def side <br />
<br />
Siege Starts<br />
- as soon as the siege starts, all areas around it will become battle ground. <br />
- siege starts, all doors to the fortress will be closed<br />
- If the def side has appointed def troops (NPC), these will appear at this moment. <br />
<br />
Ownage of Fortresses<br />
- according to each fortresses, there are between 3-5 different def points with 1 central command area<br />
- If the attackers take over 1 of the def points, within 10 minutes  they take over the other ones, the central command area door will be  opened<br />
- Take over as in take down the def NPCs at the def areas and destroy the def advice<br />
- After taking over 1 def area, if within 10 minutes the other def  areas are not taken over, then the NPC's at this area will respawn<br />
- After all the def areas are taken down, the central command door will be opened and this is where the &quot;flag&quot; game starts <br />
<br />
Flag Battle <br />
- After the Central Command Room has been opened, there will be 3 battle flags appear in the battle ground<br />
- When players obtain the flag, they will have the ability to place the flag <br />
- When the player dies, the flag will disappear and the battle ground will once again contain 3 flags. <br />
- When players get to the place within the central command area they will be able to place the flag in the specified spot <br />
- When the above is done, end of war and fortress is taken over<br />
<br />
Fortress rewards<br />
- teleport, buffs and return to fortress scroll <br />
- use the guild warehouse <br />
- can buy t-shirt, totem, bracelet, etc<br />
- can learn guild extended force skills <br />
- when siege is successful, clan points will be rewarded<br />
- when the fortress accepts the ruling from the castle, they will obtain more def and will be able to summon more def troops<br />
<strong>Siege</strong><br />
- Within battle ground, players can not Ctrl+atk <br />
- Within battle ground, players will be &quot;walk through-a-ble&quot; meaning  you can't block the others from going through by standing there<br />
- any players died within the battle ground that did not declare siege  will be penalized the same way as in normal pve environment<br />
- any players died within the battle ground that did delare siege will be penalized 1/4 the probability of normal cases<br />
- any players died within the battle ground that was not the one they  declare siege on will be penalized the same way as normal pve  environment</p>
<p><strong>Politics</strong><br />
- political activities is developed on the relationship in between of the castles and the fortresses<br />
- After taking ownership of the fortresses, a NPC will appear and allow  players to select whether or not to be part of the castle ruling.  Limited time of 1 hour<br />
<br />
Decision Period<br />
- During this Period of decision, the fortress would be under &ldquo;undecided&rdquo; stage<br />
- During this stage, fortresses def system can not be used<br />
<br />
Independence <br />
- If undecided after 1 hour of decision time, then the fortress will automatically goes independent <br />
- When independent, fortresses need not pay any tax to the castles<br />
- When independent, fortresses will not be able to use the &ldquo;strengthen def&rdquo; ability<br />
- When independent, the castle alliance / guilds can go siege war with the fortress<br />
- When independent, managing ability is limited<br />
- When independent, NPC will not be selling any healing items<br />
- When independent, fortress instant dungeon will be accessible<br />
- When independent, castle instant dungeon will not be accessible <br />
<br />
Submission<br />
- if submission to castle&rsquo;s ruling, tax is deducted every 6 hours<br />
- if there is not enough money in the guild warehouse, the fortress goes independent automatically right away<br />
- if submission, all managing abilities will be usable<br />
- if submission, NPC will be selling healing items<br />
- healing items are categorized in 6 levels, every 6 hours it will go up in 1 level <br />
- every time someone buys healing item, the clan points of the castle guild will go down by 10 points. <br />
- when the fortress ownership is over 36 hours, the healing item  reaches max, castle guild clan points will not be deducted at this  point <br />
- if there is not enough clan points (Guild that owns the castle), the  healing item will stop at the level which it was last raised. <br />
- if submission, fortress instant dungeon will not be accessible <br />
- if submission, castle instant dungeon will be accessible<br />
<br />
Other Rewards<br />
- when taking ownership of a NPC owned fortress, clan point goes up by 300<br />
- when the fortress is being taken away by other guilds, the 300 points  will change ownership too to the new guild that took over the fortress<br />
- After 6 hours of ownership, the guild can acquire the Oath of Blood from NPC <br />
- During fortress siege, the guild can also obtain Knight Badge from NPC <br />
<br />
- After taking ownership of the fortress, the guild will acquire  the ability to go home automatically (return portal type of thing) <br />
- Every fortress contains 2 special abilities, these abilities can be learned after taking ownership <br />
- If ownership of fortress is taken away, all of the above mentioned will disappear<br />
<br />
- During independence, guild can access monster instant dungeon<br />
- instant dungeon requires 2 or more people in a party to access<br />
- Every time only 1 instant dungeon can be accessed<br />
- When the instant dungeon has been activated, within 4 hours it can not be activated again<br />
- If disconnection occurs during this time, players will not be able to go back into the instant dungeon <br />
- After killing the boss in the instant dungeon, a quest item will be acquired which can exchange for knight badge. <br />
- the above mentioned &ldquo;knight badge&rdquo; can be used to trade for healing item in the fortress</p>
<p><strong>Castles</strong><br />
Production ability <br />
- can buy from palace NPC: bracelet, totem, t-shirt, accessories<br />
- hair accessories: 5 types<br />
- bracelets: 3<br />
- t-shirt: 5<br />
- totem: 52<br />
<br />
Other abilities <br />
- the ability to recover hp/mp<br />
- the ability to recover exp<br />
- the ability to buff<br />
- the abililty to teleport</p>
<p><strong>Skills </strong> (*Last Update July 16, 2007)<br />
Source: <br />
<a href="http://lineage2.gametsg.com/index.php?view=article&amp;k1=news&amp;articleid=1436" target="_blank">http://lineage2.gametsg.com/index.ph...articleid=1436</a><br />
<br />
&ndash; All 3rd Class skills (* please note: the names of the skills might be incorrectly translated) <br />
<l2inside src="http://lineage2.gametsg.com/pic/20070716/001.jpg" alt="asdasd" border="0" /> - Iron Shield, (all knight classes) - when being attacked, probability of raising shield def<br />
<br />
<l2inside src="http://lineage2.gametsg.com/pic/20070716/002.jpg" alt="as" border="0" /> - Shield Absorb- (All Knight Classes) - When party is being attacked,  the knight takes some dmg off on himself (it's like the skill in final  fantasy 7 where 1 party member blocks off the dmg) <br />
<br />
<l2inside src="http://lineage2.gametsg.com/pic/20070716/003.jpg" alt="dsa" border="0" /> - Song of Elements (SWS) raises def to 4 elemental effects for party<br />
<br />
<l2inside src="http://lineage2.gametsg.com/pic/20070716/004.jpg" alt="d" border="0" /> - Dance of Unity (BD) - increases party state resistence <br />
<br />
<l2inside src="http://lineage2.gametsg.com/pic/20070716/009.jpg" alt="sa" border="0" /> - Counter Chance (all archer classes) - when being attacked, probaility of raising self casting speed and lowers mp usage<br />
<br />
<l2inside src="http://lineage2.gametsg.com/pic/20070716/006.jpg" alt="dsa" border="0" /> - Counter Semsation - (all archer classes) - when being attacked, probaility of raising PARTY'S bow type of weapon attack speed<br />
<br />
<l2inside src="http://lineage2.gametsg.com/pic/20070716/008.jpg" alt="d" border="0" /> - Counter Speed - (all archers) - when being attacked, probability of raising party moving speed<br />
<br />
<l2inside src="http://lineage2.gametsg.com/pic/20070716/010.jpg" alt="das" border="0" /> - Physical Limit (Titan) - raising spirits, within a whort amount of time reaching physical limits <br />
<br />
<l2inside src="http://lineage2.gametsg.com/pic/20070716/011.jpg" alt="d" border="0" /> - ?? Boms (scav) - Turning all the &quot;blue mob bodies&quot; into a time bomb,  when the time's up, a big bombing attack will be carried out. <br />
<br />
<l2inside src="http://lineage2.gametsg.com/pic/20070716/005.jpg" alt="sa" border="0" /> - Critical Response - (class info not yet released) - raises critical power <br />
<br />
<l2inside src="http://lineage2.gametsg.com/pic/20070716/102.jpg" alt="d" border="0" /> - Revenge Rage - (wiz) - continuous fire attack <br />
<br />
<l2inside src="http://lineage2.gametsg.com/pic/20070716/104.jpg" alt="as" border="0" /> - Diamond Stardust - (sps) - area ice attack ~&gt; curse speed<br />
<br />
<l2inside src="http://lineage2.gametsg.com/pic/20070716/105.jpg" alt="d" border="0" /> - Rhyme of Ice - (sps) - continuous water attack<br />
<br />
<l2inside src="http://lineage2.gametsg.com/pic/20070716/108.jpg" alt="as" border="0" /> - Rhyme of Wind - (sh) - continuous wind attack<br />
<br />
<l2inside src="http://lineage2.gametsg.com/pic/20070716/107.jpg" alt="d" border="0" /> - Magic Echo - (class info not yet released) - increases mp usage + increases m.atk power <br />
<br />
<l2inside src="http://lineage2.gametsg.com/pic/20070716/109.jpg" alt="d" border="0" /> - Sacred Power - bishops - within a period of time increases the amount of hp recovery skill effect<br />
<br />
<l2inside src="http://lineage2.gametsg.com/pic/20070716/110.jpg" alt="sa" border="0" /> - EE/SE - within a short period of time increases the effect of mp recharge <br />
<br />
<l2inside src="http://lineage2.gametsg.com/pic/20070716/111.jpg" alt="d" border="0" /> - Song of Guardian (wc) - lowers party dmg when under critical attack <br />
<br />
<l2inside src="http://lineage2.gametsg.com/pic/20070716/112.jpg" alt="asd" border="0" /> - Seal of Incantation (dominator) - seal enemy's p.atk within an area<br />
<br />
3 Wiz Class continuous attacks <br />
<l2inside src="http://lineage2.gametsg.com/pic/20070716/101.jpg" alt="as" border="0" /> - Explosion ??? - wizard - when it's successfully landed on the target,  continuous strong fire attack before the effect is over. <br />
<br />
<l2inside src="http://lineage2.gametsg.com/pic/20070716/103.jpg" alt="asd" border="0" /> - Swirl Ice ?? - sps - when it's successfully landed on the target, continous strong water attack before the effect is over <br />
<br />
<l2inside src="http://lineage2.gametsg.com/pic/20070716/106.jpg" alt="as" border="0" /> - Storm ??? - sh - when it's successfully landed on the target continuous strong wind attack before the effect is over<br />
<br />
Other New Skills <br />
<l2inside src="http://lineage2.gametsg.com/pic/20070716/113.jpg" alt="sa" border="0" /> - Equipment of Fire?? - class info not yet released - incrases elemental attack effect<br />
<br />
<l2inside src="http://lineage2.gametsg.com/pic/20070716/114.jpg" alt="d" border="0" /> - Equipment of Ice??? - class info not yet released - increases elemental attack effect <br />
<br />
<l2inside src="http://lineage2.gametsg.com/pic/20070716/115.jpg" alt="as" border="0" /> - Equipment of Wind?? - class info not yet released - increases elemental attack effect<br />
<br />
<l2inside src="http://lineage2.gametsg.com/pic/20070716/116.jpg" alt="d" border="0" /> - Equipment of Earth?? - class info not yet released - increases elemental attack effect<br />
<br />
<l2inside src="http://lineage2.gametsg.com/pic/20070716/117.jpg" alt="as" border="0" /> - Meteorite - class info not yet released - fire elemental attack<br />
<br />
<l2inside src="http://lineage2.gametsg.com/pic/20070716/118.jpg" alt="d" border="0" /> - Meteor Shower - class info not yet released - non elemental attack<br />
<br />
July 13, 2007 Skill Correction / Adjustment <br />
- Revenge - skills to provoke substantially increase in effect<br />
- Agression / Strong Aggression skill effect substantially increases  effect, cooldown time increases, casting speed will not affect cooldown  time <br />
- All knights will get both the Iron Shield and Shield Absorb skills<br />
- Knight classes' Heavy Armor Mastery reduces critical dmg effect<br />
- all archer/dagger/orc fighters' Light Armor Mastery reduces critical dmg landing rate<br />
- The original skills that were shared by the alliance is now changed to guild only. (dominator skills etc) <br />
<br />
Skills Adjustment<br />
- adjustment to debuff time<br />
- DOT skills effectiveness goes up <br />
- UE: can evade p.atk <br />
- Erase: cool down time increased<br />
- shield block mastery increases more def<br />
- the effectiveness of Eva&rsquo;s Shield goes down <br />
- Shield Block skill goes down in blocking rate<br />
- Passive Critical Chance: (only applies to Plain Walker) goes up to level 4<br />
- Passive Critical Power (only applies to AW) goes up to level 7<br />
<br />
- Only 1 type of Elemental Attack Effect can exist, however elemental def can have a few different types co-existing. <br />
<br />
<strong>Skill Enchant </strong><br />
- When skill enchant fails, the original level will not go back to 0 <br />
- The amount of sp/exp used for skill enchant will be 3x as now <br />
- Need another Giant book to do this<br />
<br />
Skill Enchant exchange <br />
- After skills had been enchanted, players can exchange between mp usage / power <br />
- probability of losing 3 levels of this skill enchant <br />
- Need the same book as the above<br />
<br />
Cancelling Skil Enchant<br />
- skills enchants can be canceled<br />
-  After cancelation players will be returned with part of the sp used for enchanting the skill<br />
- Need to use another Giant book (to forget)<br />
<br />
Enchant types and categories <br />
- increased amount of skill enchant types <br />
- added enchant content, such as different elemental attacks, weakening target skills etc.<br />
<br />
Passive Skills can also be enchanted<br />
Critical Rate &ndash; effectiveness<br />
Shield Mastery  - increases def of shield<br />
Critical Effect &ndash; effectiveness<br />
??? &ndash; increases moving speed <br />
<br />
Scav skills 6 different types<br />
- when doing spoiling, can take away enemy&rsquo;s HP<br />
- when doing spoiling, can take away enemy&rsquo;s MP<br />
- increases self def to fire type atk<br />
- increases self def to water type atk<br />
- increases self def to light type atk<br />
- increases self def to dark type atk<br />
<br />
5 ways Bishop can enchant Body of Avatar <br />
- amount of time in effect<br />
- reduce MP usage<br />
- increases the amount of hp recovery<br />
- increases p.def<br />
- increases m.def<br />
<br />
Other Skills Examples<br />
Boost HP &ndash; increases CP regen rate<br />
Prayer &ndash; increases mp regen rate<br />
The Heart of Pa&rsquo;agrio &ndash; increases def to root <br />
<br />
<strong>Items</strong><br />
New items<br />
- level 80 armors<br />
- level 14 soul crystals - chance of succeeding in leveling to 14 with the boss at the new hunting ground <br />
- if failure, can be taken to the Blacksmith (in the tombs) to develop it into a level 14 crystal <br />
(the only thing is whether or not the soul crystal has to be taken to  do the raid boss first or can be directly produced into a level 14 by  the blacksmith) <br />
<br />
Bracelet (don't know the name) - summons <br />
- castle, fortresses, rainbow clan hall, + the clan hall  - all members all these clans can use<br />
<br />
bracelet that contains 6 elements? <br />
- NPC inside castle, fortresses are selling these<br />
- ranges from C~ S grade<br />
- Can be crystalized but can not be enchanted <br />
- After it's equipped, it can be equipped with some special totem<br />
- The higher grade the bracelet, the more totems can be equipped on<br />
- Totems are similar to projection weapons, they'll disappear after a period of time<br />
- Totem can enhance special skills<br />
<br />
T-shirt (adds def)<br />
- equipped onto the slot where it was originally designed for underwear<br />
- adds def<br />
- can be enchanted + crystalized<br />
- this is the reward for fortresses<br />
<br />
- nobless specified hair accessory<br />
- race specified hair accessories - reward from fortresses<br />
- class specified hair accessories<br />
<br />
level 80 special accessories<br />
- level 80 specified usage<br />
- these accessories adds def to different types of debuff<br />
<br />
<strong>Auction System</strong>- Giran, Aden, Rune <br />
- Every Mondays, Wdnesdays, Thursdays night time 8-12 <br />
- highest being 2,100,000,000<br />
- extension of 5-8 minutes for final competition <br />
- automatically goes to the person's inventory after auction is over<br />
<br />
*Updated July 16, 2007*<br />
Source: <a href="http://lineage2.gametsg.com/index.php?view=article&amp;k1=news&amp;articleid=1452" target="_blank">http://lineage2.gametsg.com/index.ph...articleid=1452</a><br />
<br />
<l2inside src="http://lineage2.gametsg.com/pic/20070717/item/744.jpg" alt="" border="0" /><br />
Bottom left corner to start the auction system <br />
<br />
<l2inside src="http://lineage2.gametsg.com/pic/20070717/item/756.jpg" alt="" border="0" /><br />
Seems like auctioning can go up by %. 50% increase, 10% increase and give up etc options<br />
<br />
It seems like from this picture, it's a level 14 soul crystal <br />
<br />
*end of update July 16, 2007*<br />
<br />
Others <br />
- Charm of courage - 20 minutes only<br />
- Within a certain period of time, buffs will not disappear<br />
- Can be used for fortresses siege<br />
<br />
<strong>Guilds</strong><br />
Guild levels<br />
- Level 9: requires clan points 40k, 140 members, <br />
- Level 10: requires clan points 40k, 140 members <br />
<br />
Residential Ability <br />
- All castles / fortresses members will automatically learn the new ability to go to residential area (?)<br />
- Academy level members will not be able to learn such ability <br />
<br />
Instant Dungeons<br />
- underground instant dungeon: castle specified instant dungeon<br />
Monster cell instant dungeon: fortresses specified instant dungeon<br />
<br />
<strong>User Interface</strong><br />
- UI is nicer, words are bigger<br />
- characters, skills, guild, map UI had been revised<br />
- the little Radar has been enhanced<br />
- Macro increased to 48<br />
- Party members can be re-arranged <br />
- Can check other people's armor / weapon by moving the mouse to the conversation spot<br />
- Players can now take movies (instead of watching them in game you can take them out as movies) <br />
<br />
*New Update July 16, 2007*<br />
Source: <a href="http://lineage2.gametsg.com/index.php?view=article&amp;k1=news&amp;articleid=1453" target="_blank">http://lineage2.gametsg.com/index.ph...articleid=1453</a><br />
<br />
<l2inside src="http://lineage2.gametsg.com/pic/20070717/item/759.jpg" alt="" border="0" /><br />
When you open your inventory, press shift then select the item to view,  then the name of that item will go into conversation box. <br />
<br />
A yellow underline + &quot;?&quot; <br />
<br />
At this point, does matter if it's the viewer or the reader, as long as  they move the cursor over to the &quot;?&quot; they'll be able to see the  specifications of this item. <br />
<br />
<l2inside src="http://lineage2.gametsg.com/pic/20070717/item/761.jpg" alt="" border="0" /><br />
Although this is perhaps very convenient, flood problem might occur ^_^;;;<br />
<br />
*End of July 16, 2007 Update*<br />
<br />
<strong>Assistance to New Players</strong><br />
- all characters under level 39 will get buffs<br />
- these buffs can be obtained from the adventure member <br />
<br />
<strong>Quests</strong><br />
- quest added to get top A grade weapon blades<br />
- quest added to get level 79-80 skill books<br />
- Kamael 1st class change quest added<br />
- level 50 transformation quest added</p>
<p><strong>Player Related</strong>: <br />
- When leveled, player becomes stronger (huh?)<br />
- After level 70, player's battle status goes up<br />
<br />
The following is PVE only, not sure about PVP<br />
Land rate + evasion <br />
- Above 70, land rate + evasion goes up<br />
- Above 78, the above goes up even more<br />
Meaning when there is a higher gap in between of levels there will be  more misses. When battling between 78 and below 78 will make a big  difference. <br />
<br />
Critical (both physical + magic) <br />
Player's level will affect critical rate. When hitting something low level more chance of doing a critical<br />
<br />
M.def<br />
Same as above <br />
<br />
EXP penalty<br />
- players below level 77 exp penalty applies when killing mobs 6 levels lower <br />
- players above 78, penalty applies to 4 levels lower<br />
<br />
Teams/Parties: <br />
When partying with members that are 20 levels lower, the lowest level player will not get any exp<br />
<br />
<strong>New Race</strong><br />
Kamael is categorized as dark based. When being healed, effectiveness goes down, max is 50% <br />
<br />
Subclass (Kamael)<br />
- can not choose other race to subclass<br />
- other races can not subclass Kamael <br />
<br />
After they finished 2 subclasses, the 3rd (hidden) subclass will be available<br />
(trade your time for this hidden subclass) <br />
<br />
Weapon Transfer: <br />
- can trade after 1st class quest<br />
- trade can go bothway, same rules apply (soul crystal / enchant / life ore) <br />
- Trade rule: <br />
2 hand sword -- ancient sword<br />
1 hand sword -- rapier<br />
bow -- crossbow<br />
<br />
For Kamael Skills, please read the 3rd page of this thread for more info, or click on the following link<br />
<a href="http://www.l2guru.com/forum/showthread.php?t=82829&amp;page=3" target="_blank">http://www.l2guru.com/forum/showthre...t=82829&amp;page=3</a><br />
<br />
<strong>Transformation </strong><br />
<br />
Transformation Types <br />
- battle type, non-battle type<br />
- cursed weapon transformation, very different from non-cursed weapon transformation <br />
<br />
Transformation <br />
- After getting a cursed weapon (blood sword, demon sword)<br />
- by using transformation skills<br />
- by equipping special transformation items <br />
<br />
When does Transformation Stop?<br />
- Death<br />
- After unequipping the special transformation item<br />
- When in water (happens only to non-cursed weapon transformation. This does not apply to cursed-weapon transformation <br />
- When the time's up <br />
<br />
Transformation Skills<br />
- level 50 new quest (after quest go to Ivory Tower to learn this skill) <br />
- different quests for different types of transformation <br />
- After doing part of the quest, can buy transformation items from npc<br />
- after doing part of the quest, can trade transformation items with ancient adena <br />
- After doing part of the quest, can trade with Life Crystal <br />
<br />
After Transformation <br />
- will not have any death side effects<br />
- can not take the boat <br />
- can not summon<br />
- can not pvp<br />
- buffs that were originally buffed will not reset<br />
- can still use the player's original skills<br />
- after cursed sword transformation, a message will show in alliance/guild/pm conversation <br />
<br />
<strong>Elemental Effects </strong>fire vs water<br />
earth vs wind <br />
dark vs light <br />
(pretty sure everyone knows that) <br />
By adding this to your weapon / armor it adds elemental effects to it.  Making yourself weak/strong against different elementalized attacks. <br />
<br />
Characteristics (like enchanting, this can fail too) <br />
- only S grade and above armors/weapons can be equipped with this<br />
- If a weapon is already equipped with a specific element ore, it can not be equipped again until it's removed <br />
- Armor includes - body, helmet, gloves, boots can be equipped with elemental effects<br />
- After weapon/armor has been equipped with elemental ores this can be viewed from the status window<br />
- elemental ores can be obtained by killing mobs at the 2 new islands,  players can exchange these ores for other elemental ores with NPC <br />
- all summons, mobs will be elementalized <br />
<br />
<strong>Olympiad</strong><br />
- battle ground has been reduced in size<br />
- 10 seconds before battle starts, player will be in a room and will not be able to see the oppenont until the battle starts<br />
<br />
<br />
<br />
<strong>Raids/Bosses</strong><br />
- antharas, baium &amp; frintezza: p.def, m.def, max hp, hp regen rate will all go up significantly<br />
- fire dragon: m.def, p.def, max hp goes up<br />
- no news on the water dragon / others<br />
Under Translation <br />
<br />
<strong>Pets/Summons </strong><br />
- wolf at level 55 can change class (?)<br />
- wolf food usage goes down<br />
- wolf is a type of battling pet, therefore can help out in battles<br />
- wolves will be able to learn new skills at level 60, 65, 70<br />
- pets exp will now come from a fix % instead of the original depending on contribution to battle<br />
- different pets have a different %<br />
- if the master and the pet's level differ by 6 levels and above, exp penalty will apply<br />
- summons can level up by enchanting skills (max level 85)<br />
- summons will be able to learn new skills after skill enchant<br />
<br />
<strong>Command Channel</strong><br />
- exp distribution will be equally distributed among all parties that are in a command channel <br />
- Within this channel, any players that are 6 levels above the mob will be penalized (exp penalty)<br />
- If a player is too far away from the mobs/parties, he/she will not be able to get any exp/sp/raid points<br />
- raid points will be 4x as now (what's the point of this?) <br />
<br />
<strong>S80 Weapon SA</strong> <em>* Last Update July 17, 2007*</em><br />
<br />
1 hand sword (can be exchanged for rapier) <br />
Focus, Health, Light <br />
<br />
2 hand sword (can be exchanged for ancient sword) <br />
Focus, Health, Light <br />
<br />
Bow (can be exchanged for crossbow)<br />
Chip Shot, Guidance, Evasion <br />
<br />
Magic Sword<br />
Acumen, mana up, Conversion <br />
<br />
Dagger<br />
Focus, Evasion, Crt.Damage <br />
<br />
Polearm<br />
Anger, Critical Stun, Light <br />
<br />
2 hand blunt <br />
Anger, Health, Risk Focus<br />
<br />
2 hand staff<br />
Mana up, Conversion, Acumen<br />
<br />
Fist <br />
Risk Haste, Risk Evasion, Haste<br />
<br />
Dual <br />
(Dark Legion X Dark Legion) <br />
Accuracy + 6 (when enchanted over 4)<br />
</p>
<? }else{
include "error.php";
}
?>