<ul class="tabselector" id="tabcontrol1">
  <!-- preloaded -->
  <li class="tab-selected" id="tab1">
    <?= link_to_function('Supplier', "tabselect($('tab1')); paneselect($('pane1'));") ?></li>
  <!-- preloaded -->
  <li class="tab-unselected" id="tab2">
    <?= link_to_function('Contact', "tabselect($('tab2')); paneselect($('pane2'));") ?></li>
  <!-- load on demand -->
  <li class="tab-unselected" id="tab3">
    <?= link_to_function('Parts', "if ($('pane3').innerHTML=='') { " .
                                   remote_function(array('update' => 'pane3',
                                                         'loading' => "$('pane3').innerHTML='" . 
                                                             image_tag('wait.gif') . ' Loading...' . "'",
                                                         'url' => array('controller' => 'parts',
                                                                        'action' => 'listing',
                                                                        'supplier' => $supplier->id))) .
                                  "} tabselect($('tab3')); paneselect($('pane3'))") ?></li>
  <!-- load on demand -->
  <li class="tab-unselected" id="tab4">
    <?= link_to_function('Notes', "if ($('pane4').innerHTML=='') { " .
                                   remote_function(array('update' => 'pane4',
                                                         'loading' => "$('pane4').innerHTML='" . 
                                                             image_tag('wait.gif') . ' Loading...' . "'",
                                                         'url' => array('controller' => 'notes',
                                                                        'action' => 'listing',
                                                                        'supplier' => $supplier->id))) .
                                  "} tabselect($('tab4')); paneselect($('pane4'))") ?></li></ul>
								  <script language="javascript">
								  function tabselect(tab) {
  var tablist = $('tabcontrol1').getElementsByTagName('li');
  var nodes = $A(tablist);

  nodes.each(function(node){
    if (node.id == tab.id) {
      tab.className='tab-selected';
		} else {
      node.className='tab-unselected';
		};
  });
}

function paneselect(pane) {
  var panelist = $('panecontrol1').getElementsByTagName('li');
  var nodes = $A(panelist);

  nodes.each(function(node){
    if (node.id == pane.id) {
      pane.className='pane-selected';
		} else {
      node.className='pane-unselected';
		};
  });
}
                    </script>