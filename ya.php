<table align="center" border="0" cellpadding="0" cellspacing="1" width="100%">
  <tbody>
    <tr>
      <td><strong>Your Account</strong></td>
    </tr>
    <tr>
      <td colspan="3"><ul>
        <li>The  use of third-party software to automate gameplay (including chat and  private store) is a bannable offense. Additionally, the purchase or  sale of in-game assets for real world currency is a violation of the  User Agreement and a punishable offense.</li>
        <li>Modifying the game client, even with other NCsoft content (such as textures from any forthcoming Chronicle), is not allowed.</li>
        <li>Characters take seven days to delete. This waiting period cannot be altered.</li>
        <li>Be careful when logging into Lineage II from any computer besides your own, as it is difficult to ensure they are secure.</li>
        <li>Character names cannot be changed, unless they are deemed blatantly offensive according to our content guidelines.</li>
        <li>Never share your account information, even with friends or family.</li>
      </ul></td>
    </tr>
  </tbody>
</table>
