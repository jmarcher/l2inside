<?php
require 'L2DP.php';
l2dp_header();
echo "<title>L2DP - Skills</title>";
l2dp_body();
$dbc = l2dp_connect( "lineage2" ); 

  $class1 = $_REQUEST['class1'];
  $class2 = $_REQUEST['class2'];

  if ( isset( $_REQUEST['hide'] ) )
  { $hide = $_REQUEST['hide']; }
  else
  { $hide = ""; }

  echo "<form method=\"GET\">\n";
  echo "<select name=\"class1\">\n";

$sql = "SELECT C.name, S.name AS name_lang
  FROM Classes AS C, SysStrings as S
  WHERE C.sys_id = S.id AND S.language = '$l2dp_lang' AND C.min_level > 0
  ORDER BY S.name;";

  //echo "<pre>" . $sql . "</pre>";
  $result = do_sql( $dbc, $sql ); 

  while ( $info = mysql_fetch_array( $result ) )
  {
    echo "<option value=\"" . $info['name'];
    if ( $info['name'] == $class1 )
      echo "\" selected>";
    else
      echo "\">";
    echo $info['name_lang'] . "\n";
  }

  echo "</select>\n";
  echo "<select name=\"class2\">\n";

$sql = "SELECT C.name, S.name AS name_lang
  FROM Classes AS C, SysStrings as S
  WHERE C.sys_id = S.id AND S.language = '$l2dp_lang' AND C.min_level > 0
  ORDER BY S.name;";

  //echo "<pre>" . $sql . "</pre>";
  $result = do_sql( $dbc, $sql ); 

  while ( $info = mysql_fetch_array( $result ) )
  {
    echo "<option value=\"" . $info['name'];
    if ( $info['name'] == $class2 )
      echo "\" selected>";
    else
      echo "\">";
    echo $info['name_lang'] . "\n";
  }

  echo "</select>\n";
  echo "<input type=\"checkbox\" name=\"hide\">Hide\n";
  echo "<input type=\"SUBMIT\" value=\"Compare\">\n";
  echo "</form>\n";

  if ( $class1 == "" || $class2 == "" )
  {
    echo "<p>Error - No classes selected</p>";
  }
  else
  {
$sql = "SELECT C.id, C.name, parents, min_level, max_level, S.name AS name_lang
  FROM Classes AS C, SysStrings as S
  WHERE C.name = '$class1' AND C.sys_id = S.id AND S.language = '$l2dp_lang';";

  //echo "<pre>" . $sql . "</pre>";
  $result = do_sql( $dbc, $sql ); 
  $class_info1 = mysql_fetch_array( $result );

  $ancestors = "";
  if ( strlen( $class_info1['parents'] ) > 0 )
  {
    $par = split( ";", $class_info1['parents'] );
    foreach ( $par as $p )
    {
      // lookup class_id
      $sql = "SELECT id FROM Classes WHERE name = '$p';";
      $result = do_sql( $dbc, $sql ); 
      $lookup = mysql_fetch_array( $result );
      $ancestors .= " OR class_id = " . $lookup['id'];
    }
  }

$sql = "SELECT N.name AS name_lang, MAX( S.level ) AS max
  FROM Skills as S, GainSkills as G, SkillNames as N
  WHERE S.id = G.skill_id AND S.level = G.skill_level
    AND ( class_id = $class_info1[id] $ancestors )
    AND S.id = N.id AND N.language = '$l2dp_lang'
  GROUP BY N.name
  ORDER BY N.name;";

  //echo "<pre>" . $sql . "</pre>";
  $result1 = do_sql( $dbc, $sql ); 

$sql = "SELECT C.id, C.name, parents, min_level, max_level, S.name AS name_lang
  FROM Classes AS C, SysStrings as S
  WHERE C.name = '$class2' AND C.sys_id = S.id AND S.language = '$l2dp_lang'";

  //echo "<pre>" . $sql . "</pre>";
  $result = do_sql( $dbc, $sql );
  $class_info2 = mysql_fetch_array( $result );

  $ancestors = "";
  if ( strlen( $class_info2['parents'] ) > 0 )
  {
    $par = split( ";", $class_info2['parents'] );
    foreach ( $par as $p )
    {
      // lookup class_id
      $sql = "SELECT id FROM Classes WHERE name = '$p';";
      $result = do_sql( $dbc, $sql ); 
      $lookup = mysql_fetch_array( $result );
      $ancestors .= " OR class_id = " . $lookup['id'];
    }
  }


$sql = "SELECT N.name AS name_lang, MAX( S.level ) AS max
  FROM Skills as S, GainSkills as G, SkillNames as N
  WHERE S.id = G.skill_id AND S.level = G.skill_level
    AND ( class_id = $class_info2[id] $ancestors )
    AND S.id = N.id AND N.language = '$l2dp_lang'
  GROUP BY N.name
  ORDER BY N.name;";

  //echo "<pre>" . $sql . "</pre>";
  $result2 = do_sql( $dbc, $sql );
  
  echo '<table border=1 cellpadding=3 cellspacing=0 width="100%">';
  echo '<tr><th>Skill';
  echo '<th bgcolor="#ffcccc">' . $class_info1['name_lang'];
  echo '<th bgcolor="#ccccff">' . $class_info2['name_lang'];
  echo "</tr>";

  $skill1 = mysql_fetch_array( $result1 );
  $skill2 = mysql_fetch_array( $result2 );

  $i = 0;
  while ( ( $skill1['name_lang'] != "" ) || ( $skill2['name_lang'] != "" ) )
  {
    //echo "*" . $skill1['name_lang'] . "**" . $skill2['name_lang'] . "*\n";

    if ( ( $skill2['name_lang'] == "" )
      || ( ( strcmp( $skill1['name_lang'], $skill2['name_lang'] ) < 0 )
      && ( $skill1['name_lang'] != "" ) ) )
    {
      echo "<tr bgcolor=\"#ffcccc\"><td>" . $skill1['name_lang'] . "</td>";
      echo "<td><b>" . $skill1['max'] . "</b></td>";
      echo "<td>&nbsp;</td></tr>\n";
      $skill1 = mysql_fetch_array( $result1 );
    }
    else if ( ( $skill1['name_lang'] == "" )
      || ( ( strcmp( $skill1['name_lang'], $skill2['name_lang'] ) > 0 )
      && ( $skill2['name_lang'] != "" ) ) )
    {
      echo "<tr bgcolor=\"#ccccff\"><td>" . $skill2['name_lang'] . "</td>";
      echo "<td>&nbsp;</td>\n";
      echo "<td><b>" . $skill2['max'] . "</b></td></tr>";
      $skill2 = mysql_fetch_array( $result2 );
    }
    else if
      ( strcmp( $skill1['name_lang'], $skill2['name_lang'] ) == 0 )
    {
      if ( $skill1['max'] == $skill2['max'] )
      {
        if ( $hide != "on" )
        {
          echo "<tr bgcolor=\"#666666\"><td>" . $skill1['name_lang'] . "</td>";
          echo "<td>" . $skill1['max'] . "</td>";
          echo "<td>" . $skill2['max'] . "</td></tr>\n";
        }
      }
      else
      {
        echo "<tr bgcolor=\"#cccccc\"><td>".$skill1['name_lang']."</td>";
        echo "<td><b>" . $skill1['max'] . "</b></td>";
        echo "<td><b>" . $skill2['max'] . "</b></td></tr>\n";
      }
      $skill1 = mysql_fetch_array( $result1 );
      $skill2 = mysql_fetch_array( $result2 );
    }
    else
    {
      echo "<tr bgcolor=\"#ff0000\"><td>Error</td></tr>";
    }

    $i++;
    if ( $i > 50 ) break;
  }

  echo "</table>\n";
  }

l2dp_disconnect( $dbc ); 
l2dp_footer();
?>
