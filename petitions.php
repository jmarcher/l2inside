<? if (isset($_GET['file'])){?><table align="center" border="0" cellpadding="0" cellspacing="1" width="100%">
  <tbody>
    <tr>
      <td><strong>Petitions</strong> </td>
    </tr>
    <tr>
      <td colspan="3"><ul>
        <li>All  in-game GM character names adhere to the following formats: &quot;=GM=Name&quot;  or &quot;=Name=&quot;. If someone claims to be a representative of NCsoft and  does not have these special symbols in their name, please petition  immediately.</li>
        <li>Make sure you have room left in your inventory before requesting item reimbursement.</li>
        <li>Unless  you were specifically advised to email support, you can petition with  the email reference number (as long as it is an in-game  emergency/violation type issue).</li>
        <li>If you observe behavior you  believe to be in violation of the rules, please write the player's name  down exactly and include it in your petition.</li>
        <li>If you feel you have gained karma because of an exploit, you may experience non-refundable XP or item losses if          you continue to play. To receive assistance, visit support or log in with a non-chaotic character on the same account        and submit a petition. &nbsp;</li>
        <li>If you have experienced a loss that can be demonstrated graphically (becoming stuck in a wall, etc.) please remember        to take a screenshot to better support your case.</li>
        <li>GMs  can only discuss issues pertaining to your account and character.  Please do not petition on behalf of anyone else, unless reporting a  game violation. Also, GMs cannot discuss the disciplinary status of  other accounts.</li>
        <li>GMs will never ask you for any items or account passwords under any circumstances.</li>
        <li>Please  do not petition with a misleading message in an effort to receive a  quicker response, as this can be considered an abuse of the petition  system and causes additional delays for other petitioners.</li>
        <li>When  petitioning, please give as detailed a description of your issue as  possible, to help the GM staff resolve your petition efficiently. For  example, please avoid: 'Hello', 'Help', 'Problem', 'Petition'.</li>
        <li>If you have any doubt someone is a GM, you can verify by using the /gmlist command.</li>
      </ul></td>
    </tr>
  </tbody>
</table>
<?
}else{
	include "error.php";
}
?>
