<table align="center" border="0" cellpadding="0" cellspacing="1" width="100%">
  <tbody>
    <tr>
      <td><strong>Castle Siege &amp; Taxes</strong></td>
    </tr>
    <tr>
      <td colspan="3"><ul>
        <li>The  competition period for the Seven Signs is Monday 6:00 PM CST - Monday  6:00 PM CST. The seal validation period is Monday 6:15 PM CST - Monday  5:45 PM CST.</li>
        <li>Deliberately attempting to interfere with sieges  by dropping excessive items on the ground is a punishable offense. If  this type of behavior is witnessed, please petition immediately with as  much detail as possible. Be aware that the Support Team will not  directly modify the outcome or function of any siege even if this  action occurs.</li>
        <li>If you are defending a castle during a siege and  are registered to the &ldquo;Dusk&rdquo; side in the Seven Signs Festival, upon  your death you will be unable to spawn inside the castle, whether or  not your clan was approved to defend. Only those players who are  registered to the &ldquo;Dawn&rdquo; faction and in an approved defender clan may  spawn inside the castle during a siege.</li>
        <li>If there is a server  restart during a siege, all siege participants will be frozen when they  log-in, until all siege NPCs have resumed their positions. The cost of  any siege golems lost during the restart will be refunded.</li>
        <li>Mercenaries  can be placed anywhere on the castle grounds, until a siege begins.  Unused mercenaries will remain available to the castle lord for use in  future sieges.</li>
        <li>Once a siege time is established, it cannot be reset.</li>
        <li>If a castle owner loses a castle to an attacker, the owner may place an encampment while attempting to re-take the castle.</li>
        <li>While  defending a castle, you will respawn in a room with a teleporter NPC if  you die. Depending upon the status of the Life Control Crystals, it may  take longer to teleport out of the room.</li>
        <li>If no one registers to attack a clan-owned castle, the current owner will keep the castle until the next siege occurs.</li>
      </ul></td>
    </tr>
  </tbody>
</table>
