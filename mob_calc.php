<? if(isset($_GET['file'])){?>
<script src="JavaScript/rawdata.js"></script>
<script src="JavaScript/summoncalc.js"></script>
<script>
window.onload=function() {
buffs();
debuffs();
calc();
boxgoaway()}
</script>
<link rel="stylesheet" type="text/css" href="e.css">
<body>
<form id="statcalculator">
<table border="1" cellpadding="0" width="1000" align="center">

<!----left---->

<!----middle---->
<td class="main" width=1000 valign=top >
<h3>L2Calc</h3>
<h4>Servitor Calculator</h4>
<a href="index.php?file=l2calc">To L2Calc</a><br>
<hr>
<p>The Servitor Calculator is currently undergoing testing. There are a few servitors who may have incorrect stats. It is up to the player base to locate these inaccuracies and help correct them.</p>
<hr>

<table class="statcalc">

<tr>
<td><fieldset>
<legend>Status Window</legend>
<table class="statcalc" width=275>
<tr><td colspan=4>
	Servitor 
		<select id="class" onChange="calc()">
			<option value=SG>Siege Golem
		</select></td></tr>

<tr><td colspan=4>
	Summoner Lv 
		<select id="LV" onChange="calc()">
		</select>
</td></tr>
	<tr><td>Lv</td><td id="LVL" align=right><strong>N/A</strong></td>
	</tr>
	<tr><td width=95>HP</td><td id="HP" width=35 align=right><strong>N/A</strong></td>
	<td width=15></td><td width=95>MP</td><td id="MP" width=35 align=right><strong>N/A</strong></td></tr>
</table>
<hr />
<table width=275>
	<tr><td>P.Atk.</td><td id="PATK" align=right><strong>N/A</strong></td>
	<td></td><td>M.Atk.</td><td id="MATK" align=right><strong>N/A</strong></td></tr>
	<tr><td>P.Def.</td><td id="PDEF" align=right><strong>N/A</strong></td>
	<td></td><td>M.Def.</td><td id="MDEF" align=right><strong>N/A</strong></td></tr>
	<tr><td>Accuracy</td><td id="ACCURACY" align=right><strong>N/A</strong></td>
	<td></td><td>Evasion</td><td id="EVASION" align=right><strong>N/A</strong></td></tr>

	<tr><td>Critical</td><td id="CRITICAL" align=right><strong>N/A</strong></td>
	<td></td><td>Speed</td><td id="SPEED" align=right><strong>N/A</strong></td></tr>
	<tr><Td>Atk. Spd.</td><td id="ATKSPD" align=right><strong>N/A</strong></td>
	<td></td><td>Casting Spd.</td><td id="CASTINGSPD" align=right><strong>N/A</strong></td></tr>
</table>
</fieldset></td>
<td valign=bottom>
<fieldset>
<legend>Base Stats</legend>

<table class="statcalc" cellpadding="5">
	<tr><td>STR</td><td><strong>40</strong></td>
	<td>DEX</td><td><strong>30</strong></td><td>CON</td><td><strong>43</strong></td></tr>
	<tr><td>INT</td><td><strong>21</strong></td>
	<td>WIT</td><td><strong>20</strong></td><td>MEN</td><td><strong>25</strong></td></tr>
</table>
</fieldset>
</td>
</tr>
</table>

<fieldset>
	<table class="statcalc"
<tr>			<td colspan=8>Turn the form display on or off with these:</td></tr>
<tr>			<td width=5><input type=checkbox id="BUFFSCHECK" onClick="boxgoaway()" checked></td>
			<td>Buffs</td>
			<td width=5><input type=checkbox id="DEBUFFSCHECK" onClick="boxgoaway()" checked></td>

			<td>Debuffs</td>
</tr>
	</table>
</fieldset>

<fieldset id="BUFFS">
<legend>Buffs</legend>
<table class="statcalc">
<tr>			<td colspan=2 align=right>Buffs Quickselect:</td>
				<td colspan=5><select id="SETBUFFS" onChange="buffs(true);calc()">

					<option>==============
					<option value="0">clear all buffs
					<option value="33">Lvl 78 Elemental Summoner
					<option value="34">Lvl 77 Elemental Summoner
					<option value="35">Lvl 56 Elemental Summoner
					<option value="36">Lvl 52 Elemental Summoner
					<option value="37">Lvl 48 Elemental Summoner
					<option value="38">Lvl 44 Elemental Summoner
					<option value="39">Lvl 40 Elemental Summoner
					<option value="0">clear all buffs
					<option value="40">Lvl 78 Phantom Summoner
					<option value="41">Lvl 77 Phantom Summoner
					<option value="42">Lvl 56 Phantom Summoner
					<option value="43">Lvl 52 Phantom Summoner
					<option value="44">Lvl 48 Phantom Summoner
					<option value="45">Lvl 44 Phantom Summoner
					<option value="46">Lvl 40 Phantom Summoner
					<option value="0">clear all buffs
					<option value="47">Lvl 78 Warlock
					<option value="48">Lvl 77 Warlock
					<option value="49">Lvl 56 Warlock
					<option value="50">Lvl 52 Warlock
					<option value="51">Lvl 48 Warlock
					<option value="52">Lvl 44 Warlock
					<option value="53">Lvl 40 Warlock
					<option value="0">clear all buffs
					<option value="1">Lvl 78 Hierophant
					<option value="2">Lvl 72 Prophet
					<option value="3">Lvl 66 Prophet
					<option value="4">Lvl 56 Prophet
					<option value="5">Lvl 52 Prophet
					<option value="6">Lvl 48 Prophet
					<option value="7">Lvl 44 Prophet
					<option value="8">Lvl 40 Prophet
					<option value="0">clear all buffs
					<option value="9">Lvl 78 Shillien's Saint
					<option value="10">Lvl 56 Shillien Elder
					<option value="11">Lvl 52 Shillien Elder
					<option value="12">Lvl 48 Shillien Elder
					<option value="13">Lvl 44 Shillien Elder
					<option value="14">Lvl 40 Shillien Elder
					<option value="0">clear all buffs
					<option value="15">Lvl 78 Doomcryer
					<option value="16">Lvl 68 Warcryer
					<option value="17">Lvl 66 Warcryer
					<option value="18">Lvl 64 Warcryer
					<option value="19">Lvl 58 Warcryer
					<option value="20">Lvl 56 Warcryer
					<option value="21">Lvl 52 Warcryer
					<option value="22">Lvl 48 Warcryer
					<option value="23">Lvl 44 Warcryer
					<option value="24">Lvl 40 Warcryer
					<option value="0">clear all buffs
					<option value="25">Lvl 77 Dominator
					<option value="26">Lvl 64 Overlord
					<option value="27">Lvl 58 Overlord
					<option value="28">Lvl 56 Overlord
					<option value="29">Lvl 52 Overlord
					<option value="30">Lvl 48 Overlord
					<option value="31">Lvl 44 Overlord
					<option value="32">Lvl 40 Overlord
					<option value="0">clear all buffs
				</select></td></tr>

<tr>	<td>HP</td>
		<td><input type=checkbox id="BTBCHECK" onClick="buffs();calc()"></td>
		<td><select id="BTB" onChange="calc()">
			<option value="1">Bless the Body 1
			<option value="2">Bless the Body 2
			<option value="3">Bless the Body 3
			<option value="4">Bless the Body 4
			<option value="5">Bless the Body 5
			<option value="6" selected>Bless the Body 6
			<option value="1">Body of Avatar 1
			<option value="2">Body of Avatar 2
			<option value="3">Body of Avatar 3
			<option value="4">Body of Avatar 4
			<option value="5">Body of Avatar 5
			<option value="6">Body of Avatar 6
		</select></td>

		<td><input type=checkbox id="VITCHECK" onClick="buffs();calc()"></td>
		<td><select id="VIT" onChange="calc()">
			<option value="SOV">Song of Vitality
		</select></td></tr>
<tr>
	<td>MP</td>
		<td><input type=checkbox id="BTSCHECK" onClick="buffs();calc()"></td>
		<td><select id="BTS" onChange="calc()">
			<option value="1">Bless the Soul 1
			<option value="2">Bless the Soul 2
			<option value="3">Bless the Soul 3
			<option value="4">Bless the Soul 4
			<option value="5">Bless the Soul 5
			<option value="6" selected>Bless the Soul 6
		</select></td></tr>

<tr>
	<td>P.Atk.</td>
		<td><input type=checkbox id="MIGHTCHECK" onClick="buffs();calc()"></td>
		<td><select id="MIGHT" onChange="calc()">
			<option value="1">Might 1
			<option value="2">Might 2
			<option value="3">Might 3
			<option value="3">Herb of Strength
			<option value="1">Chant of Battle 1
			<option value="2">Chant of Battle 2
			<option value="3">Chant of Battle 3
			<option value="1">Pa'agrian Gift 1
			<option value="2">Pa'agrian Gift 2
			<option value="3">Pa'agrian Gift 3
			<option value="1">Mighty Servitor 1
			<option value="2">Mighty Servitor 2
			<option value="3" selected>Mighty Servitor 3
		</select>


		<td><input type=checkbox id="DOWACHECK" onClick="buffs();calc()"></td>
		<td><select id="DOWA" onChange="calc()">
			<option value="DOW">Dance of Warrior
		</select></td>
		<td><input type=checkbox id="GREATERMIGHTCHECK" onClick="buffs();calc()"></td>
		<td><select id="GREATERMIGHT" onChange="calc()">
			<option value="1">Greater Might 1
			<option value="2">Greater Might 2
			<option value="3" selected>Greater Might 3
			<option value="1">War Chant 1
			<option value="2">War Chant 2
			<option value="3">War Chant 3
		</select></td>

</tr>
<tr>
	<td>M.Atk.</td>
		<td><input type=checkbox id="EMPCHECK" onClick="buffs();calc()"></td>
		<td><select id="EMP" onChange="calc()">
			<option value="1">Empower 1
			<option value="2">Empower 2
			<option value="3">Greater Empower 3
			<option value="3">Herb of Magic
			<option value="3">Soul of Pa'agrio
			<option value="1">Bright Servitor 1
			<option value="2">Bright Servitor 2
			<option value="3" selected>Bright Servitor 3
		</select></td>

		<td><input type=checkbox id="DOMYCHECK" onClick="buffs();calc()"></td>
		<td><select id="DOMY" onChange="calc()">
			<option value="DOM">Dance of Mystic
		</select></td></tr>
<tr>
	<td>P.Def.</td>
		<td><input type=checkbox id="SHIELDCHECK" onClick="buffs();calc()"></td>
		<td><select id="SHIELD" onChange="calc()">
			<option value="1">Shield 1
			<option value="2">Shield 2
			<option value="3">Shield 3
			<option value="1">Blessings of Pa'agrio 1
			<option value="2">Blessings of Pa'agrio 2
			<option value="3">Blessings of Pa'agrio 3
			<option value="1">Chant of Shielding 1
			<option value="2">Chant of Shielding 2
			<option value="3">Chant of Shielding 3
			<option value="1">Soul Shield 1
			<option value="2">Soul Shield 2
			<option value="3">Soul Shield 3
			<option value="1">Servitor Physical Shield 1
			<option value="2">Servitor Physical Shield 2
			<option value="3" selected>Servitor Physical Shield 3
		</select></td>

		<td><input type=checkbox id="SOECHECK" onClick="buffs();calc()"></td>
		<td><select id="SOEA" onChange="calc()">
			<option value="SOE">Song of Earth
		</select></td>
		<td><input type=checkbox id="GREATERSHIELDCHECK" onClick="buffs();calc()"></td>
		<td><select id="GREATERSHIELD" onChange="calc()">
			<option value="1">Greater Shield 1
			<option value="2">Greater Shield 2
			<option value="3" selected>Greater Shield 3
			<option value="1">Earth Chant 1
			<option value="2">Earth Chant 2
			<option value="3">Earth Chant 3
		</select></td>

</tr>
<tr>
	<td>M.Def.</td>
		<td><input type=checkbox id="MBCHECK" onClick="buffs();calc()"></td>
		<td><select id="MB" onChange="calc()">
			<option value="2">Magic Barrier 1
			<option value="3" selected>Magic Barrier 2
			<option value="1">Chant of Fire 1
			<option value="2">Chant of Fire 2
			<option value="3">Chant of Fire 3
			<option value="1">Glory of Pa'agrio 1
			<option value="2">Glory of Pa'agrio 2
			<option value="3">Glory of Pa'agrio 3
			<option value="2">Servitor Magic Shield 1
			<option value="3">Servitor Magic Shield 2
		</select>

		<td><input type=checkbox id="WARDCHECK" onClick="buffs();calc()"></td>
		<td><select id="WARD" onChange="calc()">
			<option value="SOW">Song of Warding
		</select></td></tr>
<tr>
	<td>Accuracy</td>
		<td><input type=checkbox id="GUIDCHECK" onClick="buffs();calc()"></td>
		<td><select id="GUID" onChange="calc()">
			<option value="1">Guidance 1
			<option value="2">Guidance 2
			<option value="3" selected>Guidance 3
			<option value="1">Chant of Eagle 1
			<option value="2">Chant of Eagle 2
			<option value="3">Chant of Eagle 3
			<option value="1">Vision of Pa'agrio 1
			<option value="2">Vision of Pa'agrio 2
			<option value="3">Vision of Pa'agrio 3
		</select></td>

		<td><input type=checkbox id="DOICHECK" onClick="buffs();calc()"></td>
		<td><select id="DOI" onChange="calc()">
			<option value="DOI">Dance of Inspiration
		</select></td>
</tr>
<tr>
	<td>Evasion</td>
		<td><input type=checkbox id="AGICHECK" onClick="buffs();calc()"></td>
		<td><select id="AGI" onChange="calc()">
			<option value="1">Agility 1
			<option value="2">Agility 2
			<option value="3" selected>Agility 3
			<option value="1">Chant of Evasion 1
			<option value="2">Chant of Evasion 2
			<option value="3">Chant of Evasion 3
			<option value="1">Tact of Pa'agrio 1
			<option value="2">Tact of Pa'agrio 2
			<option value="3">Tact of Pa'agrio 3
		</select></td>

		<td><input type=checkbox id="SWATCHECK" onClick="buffs();calc()"></td>
		<td><select id="SWAT" onChange="calc()">
			<option value="SOW">Song of Water
		</select></td>
</tr>
<tr>
	<td>Critical</td>
		<td><input type=checkbox id="FCSCHECK" onClick="buffs();calc()"></td>
		<td><select id="FCS" onChange="calc()">
			<option value="1">Focus 1
			<option value="2">Focus 2
			<option value="3" selected>Focus 3
			<option value="1">Chant of Predator 1
			<option value="2">Chant of Predator 2
			<option value="3">Chant of Predator 3
			<option value="3">Herb of Critical Attack
		</select></td>

		<td><input type=checkbox id="SOHCHECK" onClick="buffs();calc()"></td>
		<td><select id="SOH" onChange="calc()">
			<option value="SOH">Song of Hunter
		</select></td></tr>
<tr>
	<td>Speed</td>
		<td><input type=checkbox id="WWCHECK" onClick="buffs();calc()"></td>
		<td><select id="WW" onChange="calc()">
			<option value="1">Wind Walk 1
			<option value="2">Wind Walk 2
			<option value="1">Pa'agrian Haste 1
			<option value="2">Pa'agrian Haste 2
			<option value="1" id="Sprint1">Sprint 1
			<option value="2" id="Sprint2">Sprint 2
			<option value="1">Haste Potion
			<option value="2">Greater Quick Step Potion
			<option value="2">Herb of Speed
			<option value="1">Servitor Wind Walk 1
			<option value="2" selected>Servitor Wind Walk 2
		</select></td>

		<td><input type=checkbox id="SWINDCHECK" onClick="buffs();calc()"></td>
		<td><select id="SWIND" onChange="calc()">
			<option value="SOW">Song of Wind
		</select></td>
		<td><input type=checkbox id="DSHCHECK" onClick="buffs();calc()"></td>
		<td><select id="DSH" onChange="calc()">
			<option value="DOS">Dance of Shadow
		</select></td>
</tr>
<tr>
	<td>Atk. Spd.</td>

		<td><input type=checkbox id="HASTECHECK" onClick="buffs();calc()"></td>
		<td><select id="HASTE" onChange="calc()">
			<option value="1">Haste 1
			<option value="2">Haste 2
			<option value="1">Chant of Fury 1
			<option value="2">Chant of Fury 2
			<option value="1">Potion of Alacrity
			<option value="2">Greater Swift Attack Potion
			<option value="2">Herb of Atk. Speed
			<option value="1">Servitor Haste 1
			<option value="2" selected>Servitor Haste 2
		</select></td>
		<td><input type=checkbox id="DFURYCHECK" onClick="buffs();calc()"></td>

		<td><select id="DFURY" onChange="calc()">
			<option value="DOF">Dance of Fury
		</select></td>
</tr>
<tr>
	<td>Casting Spd.</td>
		<td><input type=checkbox id="ACUCHECK" onClick="buffs();calc()"></td>
		<td><select id="ACU" onChange="calc()">
			<option value="1">Acumen 1
			<option value="2">Acumen 2
			<option value="3" selected>Acumen 3
			<option value="1">Chant of Flame 1
			<option value="2">Chant of Flame 2
			<option value="3">Chant of Flame 3
			<option value="1">Wisdom of Pa'agrio 1
			<option value="2">Wisdom of Pa'agrio 2
			<option value="3">Wisdom of Pa'agrio 3
			<option value="2">Magic Haste Potion
			<option value="3">Greater Magic Haste Potion
			<option value="3">Herb of Casting Spd.
		</select></td>

		<td><input type=checkbox id="DCONCHECK" onClick="buffs();calc()"></td>
		<td><select id="DCON" onChange="calc()">
			<option value="DOC">Dance of Concentration
		</select></td></tr>
<tr>
	<td>Misc.</td>
		<td><input type=checkbox id="ZERKCHECK" onClick="buffs();calc()"></td>
		<td><select id="ZERK" onChange="calc()">
			<option value="1">Berserker Spirit 1
			<option value="2" selected>Berserker Spirit 2
			<option value="1">Rage of Pa'agrio 1
			<option value="2">Rage of Pa'agrio 2
		</select></td>

		<td><input type=checkbox id="QUEENCHECK" onClick="buffs();calc()"></td>
		<td><select id="QUEEN" onChange="calc()">
			<option value="4">Blessing of Queen 1
			<option value="5">Blessing of Queen 2
			<option value="6" selected>Blessing of Queen 3
			<option value="1">Gift of Queen 1
			<option value="2">Gift of Queen 2
			<option value="3">Gift of Queen 3
		</select></td>
		<td><input type=checkbox id="PROPHCHECK" onClick="buffs();calc()"></td>
		<td><select id="PROPH" onChange="calc()">
			<option value="1">Chant of Victory
			<option value="2">Prophecy of Fire
			<option value="3">Prophecy of Water
			<option value="4">Prophecy of Wind
			<option value="1" selected>Final Servitor
			<option value="2">Warrior Servitor
			<option value="3">Wizard Servitor
			<option value="4">Assassin Servitor
		</select></td>

		<td><input type=checkbox id="UDCHECK" onClick="buffs();calc()"></td>
		<td><select id="UD" onChange="calc()">
			<option value="1">Servitor Empowerment 1
			<option value="2" selected>Servitor Empowerment 2
		</select>
</tr></table>
</fieldset>
<fieldset id="DEBUFFS">
<legend>Debuffs</legend>
	<table class="statcalc">
<tr>	<td>P.Atk.</td>

		<td><input type=checkbox id="DEPATKCHECK" onClick="debuffs();calc()"></td>
		<td><select id="DEPATK" onChange="calc()">
			<option value="1">Curse: Weakness 1
			<option value="2">Curse: Weakness 2-5
			<option value="3">Curse: Weakness 6+
			<option value="3">Howl
			<option value="3">Poltergeist Cubic 1+
			<option value="2">Power Break 1-2
			<option value="3">Power Break 3+
		</select></td></tr>
<tr>
	<td>P.Def.</td>

		<td><input type=checkbox id="DEPDEFCHECK" onClick="debuffs();calc()"></td>
		<td><select id="DEPDEF" onChange="calc()">
			<option value="1">Hex
			<option value="1">Poltergeist Cubic
		</select></td>
		<td><input type=checkbox id="BLKSCHECK" onClick="buffs();calc()"></td>
		<td><select id="BLKS" onChange="calc()">
			<option value="BLKS">Block Shield
			<option value="MBLKS">Mass Shield Block
		</select></td></tr>
<tr>	<td>M.Def.</td>

		<td><input type=checkbox id="GLOOMCHECK" onClick="debuffs();calc()"></td>
		<td><select id="GLOOM" onChange="calc()">
			<option value="CG1">Curse Gloom
		</select></td></tr>
<tr>	<td>Accuracy
		<td><input type=checkbox id="DEACCCHECK" onClick="debuffs();calc()"></td>
		<td><select id="DEACC" onChange="calc()">
			<option value="1">Curse Chaos 1
			<option value="2">Curse Chaos 2+
			<option value="1">Seal of Chaos 1-2
			<option value="2">Seal of Chaos 3+
		</select></td>

		<td><input type=checkbox id="SANDBOMBCHECK" onClick="debuffs();calc()"></td>
		<td><select id="SANDBOMB" onChange="calc()">
			<option value="1">Sand Bomb 1-5
			<option value="3">Sand Bomb 10
		</select></td></tr>
<tr>	<td>Critical
		<td><input type=checkbox id="TRIBUNALCHECK" onClick="debuffs();calc()"></td>
		<td><select id="TRIBUNAL" onChange="calc()">
			<option value="1">Tribunal 1-6
			<option value="2">Tribunal 7-9
			<option value="3">Tribunal 10
		</select></td></tr>

<tr>	<td>Speed</td>
		<td><input type=checkbox id="DESPEEDCHECK" onClick="debuffs();calc()"></td>
		<td><select id="DESPEED" onChange="calc()">
			<option value="1">Slow 1
			<option value="2">Slow 2+
			<option value="2">Blizzard
			<option value="1">Cripple 1-5
			<option value="2">Cripple 6+
			<option value="1">Entangle 1
			<option value="2">Entangle 2+
			<option value="1">Freezing Strike
			<option value="1">Frost Bolt
			<option value="2">Hamstring
			<option value="2">Hamstring Shot
			<option value="1">Ice Bolt
			<option value="2">Mass Slow
			<option value="1">Seal of Slow 1
			<option value="2">Seal of Slow 2
		</select></td>

		<td><input type=checkbox id="BWWCHECK" onClick="buffs();calc()"></td>
		<td><select id="BWW" onChange="calc()">
			<option value="BWW">Block Wind Walk
			<option value="MBWW">Mass Block Wind Walk
		</select></td></tr>
<tr>	<td>Atk. Spd.
		<td><input type=checkbox id="DEASPDCHECK" onClick="debuffs();calc()"></td>
		<td><select id="DEASPD" onChange="calc()">
			<option value="1">Wind Shackle 1
			<option value="2">Wind Shackle 2-5
			<option value="3">Wind Shackle 6+
			<option value="3">Poltergeist Cubic
			<option value="3">Seal of Winter
			<option value="3">Spoil (Atk. Spd.)
			<option value="3">Spoil Festival (Atk. Spd.)
		</select></td></tr>

<tr>	<td>Misc.</td>
		<td><input type=checkbox id="ACCHECK" onClick="debuffs();calc()"></td>
		<td><select id="ARCR" onChange="calc()">
			<option value="ARC1">Armor Crush
		</select></td>
		<td><input type=checkbox id="COABYSSCHECK" onClick="debuffs();calc()"></td>
		<td><select id="COABYSS" onChange="calc()">
			<option value="COA1">Curse of Abyss
		</select></td>

		<td><input type=checkbox id="CODOOMCHECK" onClick="debuffs();calc()"></td>
		<td><select id="CODOOM" onChange="calc()">
			<option value="COD1">Curse of Doom
		</select></td>
		<td><input type=checkbox id="COSHADECHECK" onClick="debuffs();calc()"></td>
		<td><select id="COSHADE" onChange="calc()">
			<option value="1">Curse of Shade 1
			<option value="2">Curse of Shade 2
			<option value="3">Curse of Shade 3
			<option value="1">Mass Curse of Shade 1
			<option value="2">Mass Curse of Shade 2
			<option value="3">Mass Curse of Shade 3
		</select></td></tr>

<tr>	<td></td>
		<td><input type=checkbox id="DEMONICBDCHECK" onClick="debuffs();calc()"></td>
		<td><select id="DEMONICBD" onChange="calc()">
			<option value="2">Demonic Blade Dance 4-5
			<option value="5">Demonic Blade Dance 10
		</select></td>
		<td><input type=checkbox id="DVCHECK" onClick="debuffs();calc()"></td>
		<td><select id="DV" onChange="calc()">
			<option value="DV">Dark Vortex
		</select></td>

		<td><input type=checkbox id="FVCHECK" onClick="debuffs();calc()"></td>
		<td><select id="FV" onChange="calc()">
			<option value="FV">Fire Vortex
		</select></td>
		<td><input type=checkbox id="CHOLCHECK" onClick="debuffs();calc()"></td>
		<td><select id="CHOL" onChange="calc()">
			<option value="1">Hot Springs Cholera 1
			<option value="2">Hot Springs Cholera 2
			<option value="3">Hot Springs Cholera 3
			<option value="4">Hot Springs Cholera 4
			<option value="5">Hot Springs Cholera 5
			<option value="6">Hot Springs Cholera 6
			<option value="7">Hot Springs Cholera 7
			<option value="8">Hot Springs Cholera 8
			<option value="9">Hot Springs Cholera 9
			<option value="10">Hot Springs Cholera 10
		</select></td></tr>

<tr>	<td></td>
		<td><input type=checkbox id="MALCHECK" onClick="debuffs();calc()"></td>
		<td><select id="MAL" onChange="calc()">
			<option value="1">Hot Springs Malaria 1
			<option value="2">Hot Springs Malaria 2
			<option value="3">Hot Springs Malaria 3
			<option value="4">Hot Springs Malaria 4
			<option value="5">Hot Springs Malaria 5
			<option value="6">Hot Springs Malaria 6
			<option value="7">Hot Springs Malaria 7
			<option value="8">Hot Springs Malaria 8
			<option value="9">Hot Springs Malaria 9
			<option value="10">Hot Springs Malaria 10
		</select></td>

		<td><input type=checkbox id="IVCHECK" onClick="debuffs();calc()"></td>
		<td><select id="IV" onChange="calc()">
			<option value="IV">Ice Vortex
		</select></td>
		<td><input type=checkbox id="LVORCHECK" onClick="debuffs();calc()"></td>
		<td><select id="LVOR" onChange="calc()">
			<option value="LV">Light Vortex
		</select></td>
		<td><input type=checkbox id="PSYCHOCHECK" onClick="debuffs();calc()"></td>
		<td><select id="PSYCHO" onChange="calc()">

			<option value="1">Psycho Symphony 1-3
		</select></td></tr>
<tr>	<td></td>
		<td><input type=checkbox id="SBCHECK" onClick="debuffs();calc()"></td>
		<td><select id="SB" onChange="calc()">
			<option value="SB">Shock Blast
		</select></td>
		<td><input type=checkbox id="DESPAIRCHECK" onClick="debuffs();calc()"></td>
		<td><select id="DESPAIR" onChange="calc()">
			<option value="SOD1">Seal of Despair
		</select></td>

		<td><input type=checkbox id="WVCHECK" onClick="debuffs();calc()"></td>
		<td><select id="WV" onChange="calc()">
			<option value="WV">Wind Vortex
		</select></td></tr>
</table>
</form>

<!----right column---->

</table>

</body>
<?
exit();
}else{
	include "error.php";
}?>