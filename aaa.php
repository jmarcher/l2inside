<form action="script.php" method="post" enctype="multipart/form-data" id="form-demo">
	<fieldset>
		<legend>Select Files</legend>
		<p>
			Selected photos will be queued for upload, select all your files and start upload the upload.
		</p>
		<label for="demo-filedata">
			Upload Photos:
			<input type="file" name="Filedata" id="demo-filedata" />
		</label>
	</fieldset>
	<fieldset>
		<legend>Upload Queue</legend>
		<div id="demo-status">
			Check the selected files and start uploading.
		</div>
		<ul class="fancyupload-queue" id="demo-queue">
			<li style="display: none" />
		</ul>
		<p>
			<input type="submit" id="simple-submit" value="Start Upload"/>
		</p>
	</fieldset>
</form>