	<script type="text/javascript" src="SpryAssets/mootools-1.2-core.js"></script>
	<script type="text/javascript" src="SpryAssets/Swiff.Base.js"></script>
	<script type="text/javascript" src="SpryAssets/Swiff.Uploader.js"></script>

	<script type="text/javascript" src="SpryAssets/FancyUpload.js"></script>
<script language="javascript">
window.addEvent('load', function() {
	/**
	 * We take the first input with this class we can find ...
	 */
	var input = $('demo-filedata');
 
	/**
	 * Simple and easy
	 *
	 * swf: the path to the swf
	 * container: the object is embedded in this container (default: document.body)
	 *
	 * NOTE: container is only used for the first uploader u create, all others depend
	 * on the same swf in that container, so the container option for the other uploaders
	 * will be ignored.
	 *
	 */
	var upload = new FancyUpload(input, {
		swf:  'SpryAssets/Swiff.Uploader.swf',
		queueList: 'demo-queue'
	});
 
	/**
	 * We create the clear-queue link on-demand, since we don't know if the user has flash/javascript.
	 *
	 * You can also create the complete xhtml structure thats needed for the queue here, to be sure
	 * that its only in the document when the user has flash enabled.
	 */
	$('demo-status').adopt(new Element('a', {
		'href': '#',
		'events': {
			'click': (function(e) {
				e.stop();
				upload.clearList(false);
			}).bindWithEvent()
		}
	}).setHTML('Clear Completed'));
 
});
</script>
<form action="script.php" method="post" enctype="multipart/form-data" id="form-demo">
	<fieldset>
		<legend>Select Files</legend>
		<p>
			Selected photos will be queued for upload, select all your files and start upload the upload.
		</p>
		<label for="demo-filedata">
			Upload Photos:
			<input type="file" name="Filedata" id="demo-filedata" />
		</label>
	</fieldset>
	<fieldset>
		<legend>Upload Queue</legend>
		<div id="demo-status">
			Check the selected files and start uploading.
		</div>
		<ul class="fancyupload-queue" id="demo-queue">
			<li style="display: none" />
		</ul>
		<p>
			<input type="submit" id="simple-submit" value="Start Upload"/>
		</p>
	</fieldset>
</form>