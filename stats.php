<html>
<head>
<script src="rawdata.js"></script>
<script src="calculator.js"></script>
<title>stats</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
<!--
.Estilo1 {
	font-family: "MS Sans Serif";
	font-size: 10px;
	color: #FFFFFF;
}
-->
</style>
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<!-- ImageReady Slices (stats.psd) -->
Lv 
		<select id="LV" onChange="calc(false,false,false,false,false,true)"><option value="1">1<option value="2">2<option value="3">3<option value="4">4<option value="5">5<option value="6">6<option value="7">7<option value="8">8<option value="9">9<option value="10">10<option value="11">11<option value="12">12<option value="13">13<option value="14">14<option value="15">15<option value="16">16<option value="17">17<option value="18">18<option value="19">19<option value="20">20<option value="21">21<option value="22">22<option value="23">23<option value="24">24<option value="25">25<option value="26">26<option value="27">27<option value="28">28<option value="29">29<option value="30">30<option value="31">31<option value="32">32<option value="33">33<option value="34">34<option value="35">35<option value="36">36<option value="37">37<option value="38">38<option value="39">39<option value="40">40<option value="41">41<option value="42">42<option value="43">43<option value="44">44<option value="45">45<option value="46">46<option value="47">47<option value="48">48<option value="49">49<option value="50">50<option value="51">51<option value="52">52<option value="53">53<option value="54">54<option value="55">55<option value="56">56<option value="57">57<option value="58">58<option value="59">59<option value="60">60<option value="61">61<option value="62">62<option value="63">63<option value="64">64<option value="65">65<option value="66">66<option value="67">67<option value="68">68<option value="69">69<option value="70">70<option value="71">71<option value="72">72<option value="73">73<option value="74">74<option value="75">75<option value="76">76<option value="77">77<option value="78">78<option value="79">79<option value="80">80</select>

	Class 
		<select id="class" onChange="calc(true,false,false,false,false,false)">
			<option value="AW">Abyss Walker
			<option value="AR">Artisan
			<option value="AS">Assassin
			<option value="BI">Bishop
			<option value="BD">Bladedancer
			<option value="BH">Bounty Hunter
			<option value="CL">Cleric
			<option value="DA">Dark Avenger
			<option value="DF">Dark Fighter
			<option value="DM">Dark Mystic
			<option value="DW">Dark Wizard
			<option value="DE">Destroyer
			<option value="DO">Dwarven Fighter
			<option value="ELS">Elemental Summoner
			<option value="EE">Elven Elder
			<option value="EF">Elven Fighter
			<option value="EK">Elven Knight
			<option value="EM">Elven Mystic
			<option value="EO">Elven Oracle
			<option value="EFS">Elven Scout
			<option value="EW">Elven Wizard
			<option value="GL">Gladiator
			<option value="HE">Hawkeye
			<option value="HF">Human Fighter
			<option value="HK">Human Knight
			<option value="HM">Human Mystic
			<option value="MO">Monk
			<option value="NE">Necromancer
			<option value="OF">Orc Fighter
			<option value="OM">Orc Mystic
			<option value="OR">Orc Raider
			<option value="OS">Orc Shaman
			<option value="OL">Overlord
			<option value="PA">Paladin
			<option value="PK">Palus Knight
			<option value="PR">Phantom Ranger
			<option value="PS">Phantom Summoner
			<option value="PW">Plainswalker
			<option value="PP">Prophet
			<option value="RO">Rogue
			<option value="SC">Scavenger
			<option value="SHE">Shillien Elder
			<option value="SK">Shillien Knight
			<option value="SO">Shillien Oracle
			<option value="SR">Silver Ranger
			<option value="SOR">Sorcerer
			<option value="SPH">Spellhowler
			<option value="SPS">Spellsinger
			<option value="SW">Swordsinger
			<option value="TK">Temple Knight
			<option value="TH">Treasure Hunter
			<option value="TY">Tyrant
			<option value="WC">Warcryer
			<option value="WL">Warlock
			<option value="WD">Warlord
			<option value="WA">Warrior
			<option value="WS">Warsmith
			<option value="WI">Wizard
		</select>

</form><br>
<fieldset>
<legend>Base Stats</legend>
<table class="statcalc" cellpadding="5">
	<tr><td>STR</td><td id="STR">N/A</td><td>DEX</td><td id="DEX">N/A</td><td>CON</td><td id="CON">N/A</td></tr>

	<tr><td>INT</td><td id="INT">N/A</td><td>WIT</td><td id="WIT">N/A</td><td>MEN</td><td id="MEN">N/A</td></tr>
</table>
	<form name="TATTOO">
			Tattoo Slot 1
				<select name="tattooslot1" id="tattooslot1" onChange="calc()">
				<option value="NoTat">No Tattoo
				<option value="C1D1">CON+1 DEX-1
				<option value="C1D2">CON+1 DEX-2
				<option value="C1D3">CON+1 DEX-3
				<option value="C1S1">CON+1 STR-1
				<option value="C1S2">CON+1 STR-2
				<option value="C1S3">CON+1 STR-3
				<option value="C2D2">CON+2 DEX-2
				<option value="C2D3">CON+2 DEX-3
				<option value="C2D4">CON+2 DEX-4
				<option value="C2S2">CON+2 STR-2
				<option value="C2S3">CON+2 STR-3
				<option value="C2S4">CON+2 STR-4
				<option value="C3D3">CON+3 DEX-3
				<option value="C3D4">CON+3 DEX-4
				<option value="C3D5">CON+3 DEX-5
				<option value="C3S3">CON+3 STR-3
				<option value="C3S4">CON+3 STR-4
				<option value="C3S5">CON+3 STR-5
				<option value="C4D4">CON+4 DEX-4
				<option value="C4D5">CON+4 DEX-5
				<option value="C4D6">CON+4 DEX-6
				<option value="C4S4">CON+4 STR-4
				<option value="C4S5">CON+4 STR-5
				<option value="C4S6">CON+4 STR-6
				<option value="D1C1">DEX+1 CON-1
				<option value="D1C2">DEX+1 CON-2
				<option value="D1C3">DEX+1 CON-3
				<option value="D1S1">DEX+1 STR-1
				<option value="D1S2">DEX+1 STR-2
				<option value="D1S3">DEX+1 STR-3
				<option value="D2C2">DEX+2 CON-2
				<option value="D2C3">DEX+2 CON-3
				<option value="D2C4">DEX+2 CON-4
				<option value="D2S2">DEX+2 STR-2
				<option value="D2S3">DEX+2 STR-3
				<option value="D2S4">DEX+2 STR-4
				<option value="D3C3">DEX+3 CON-3
				<option value="D3C4">DEX+3 CON-4
				<option value="D3C5">DEX+3 CON-5
				<option value="D3S3">DEX+3 STR-3
				<option value="D3S4">DEX+3 STR-4
				<option value="D3S5">DEX+3 STR-5
				<option value="D4C4">DEX+4 CON-4
				<option value="D4C5">DEX+4 CON-5
				<option value="D4C6">DEX+4 CON-6
				<option value="D4S4">DEX+4 STR-4
				<option value="D4S5">DEX+4 STR-5
				<option value="D4S6">DEX+4 STR-6
				<option value="I1M1" id="1I1M1">INT+1 MEN-1
				<option value="I1M2" id="1I1M2">INT+1 MEN-2
				<option value="I1M3" id="1I1M3">INT+1 MEN-3
				<option value="I1W1" id="1I1W1">INT+1 WIT-1
				<option value="I1W2" id="1I1W2">INT+1 WIT-2
				<option value="I1W3" id="1I1W3">INT+1 WIT-3
				<option value="I2M2" id="1I2M2">INT+2 MEN-2
				<option value="I2M3" id="1I2M3">INT+2 MEN-3
				<option value="I2M4" id="1I2M4">INT+2 MEN-4
				<option value="I2W2" id="1I2W2">INT+2 WIT-2
				<option value="I2W3" id="1I2W3">INT+2 WIT-3
				<option value="I2W4" id="1I2W4">INT+2 WIT-4
				<option value="I3M3" id="1I3M3">INT+3 MEN-3
				<option value="I3M4" id="1I3M4">INT+3 MEN-4
				<option value="I3M5" id="1I3M5">INT+3 MEN-5
				<option value="I3W3" id="1I3W3">INT+3 WIT-3
				<option value="I3W4" id="1I3W4">INT+3 WIT-4
				<option value="I3W5" id="1I3W5">INT+3 WIT-5
				<option value="I4M4" id="1I4M4">INT+4 MEN-4
				<option value="I4M5" id="1I4M5">INT+4 MEN-5
				<option value="I4M6" id="1I4M6">INT+4 MEN-6
				<option value="I4W4" id="1I4W4">INT+4 WIT-4
				<option value="I4W4" id="1I4W5">INT+4 WIT-5
				<option value="I4W4" id="1I4W6">INT+4 WIT-6
				<option value="M1I1" id="1M1I1">MEN+1 INT-1
				<option value="M1I2" id="1M1I2">MEN+1 INT-2
				<option value="M1I3" id="1M1I3">MEN+1 INT-3
				<option value="M1W1" id="1M1W1">MEN+1 WIT-1
				<option value="M1W2" id="1M1W2">MEN+1 WIT-2
				<option value="M1W3" id="1M1W3">MEN+1 WIT-3
				<option value="M2I2" id="1M2I2">MEN+2 INT-2
				<option value="M2I3" id="1M2I3">MEN+2 INT-3
				<option value="M2I4" id="1M2I4">MEN+2 INT-4
				<option value="M2W2" id="1M2W2">MEN+2 WIT-2
				<option value="M2W3" id="1M2W3">MEN+2 WIT-3
				<option value="M2W4" id="1M2W4">MEN+2 WIT-4
				<option value="M3I3" id="1M3I3">MEN+3 INT-3
				<option value="M3I4" id="1M3I4">MEN+3 INT-4
				<option value="M3I5" id="1M3I5">MEN+3 INT-5
				<option value="M3W3" id="1M3W3">MEN+3 WIT-3
				<option value="M3W4" id="1M3W4">MEN+3 WIT-4
				<option value="M3W5" id="1M3W5">MEN+3 WIT-5
				<option value="M4I4" id="1M4I4">MEN+4 INT-4
				<option value="M4I5" id="1M4I5">MEN+4 INT-5
				<option value="M4I6" id="1M4I6">MEN+4 INT-6
				<option value="M4W4" id="1M4W4">MEN+4 WIT-4
				<option value="M4W5" id="1M4W5">MEN+4 WIT-5
				<option value="M4W6" id="1M4W6">MEN+4 WIT-6
				<option value="S1C1" id="S1C1">STR+1 CON-1
				<option value="S1C2">STR+1 CON-2
				<option value="S1C3">STR+1 CON-3
				<option value="S1D1">STR+1 DEX-1
				<option value="S1D1">STR+1 DEX-2
				<option value="S1D1">STR+1 DEX-3
				<option value="S2C2">STR+2 CON-2
				<option value="S2C2">STR+2 CON-3
				<option value="S2C2">STR+2 CON-4
				<option value="S2D2">STR+2 DEX-2
				<option value="S2D3">STR+2 DEX-3
				<option value="S2D4">STR+2 DEX-4
				<option value="S3C3">STR+3 CON-3
				<option value="S3C4">STR+3 CON-4
				<option value="S3C5">STR+3 CON-5
				<option value="S3D3">STR+3 DEX-3
				<option value="S3D4">STR+3 DEX-4
				<option value="S3D5">STR+3 DEX-5
				<option value="S4C4">STR+4 CON-4
				<option value="S4C5">STR+4 CON-5
				<option value="S4C6">STR+4 CON-6
				<option value="S4D4">STR+4 DEX-4
				<option value="S4D5">STR+4 DEX-5
				<option value="S4D6">STR+4 DEX-6
				<option value="W1I1" id="1W1I1">WIT+1 INT-1
				<option value="W1I2" id="1W1I2">WIT+1 INT-2
				<option value="W1I3" id="1W1I3">WIT+1 INT-3
				<option value="W1M1" id="1W1M1">WIT+1 MEN-1
				<option value="W1M2" id="1W1M2">WIT+1 MEN-2
				<option value="W1M3" id="1W1M3">WIT+1 MEN-3
				<option value="W2I2" id="1W2I2">WIT+2 INT-2
				<option value="W2I3" id="1W2I3">WIT+2 INT-3
				<option value="W2I4" id="1W2I4">WIT+2 INT-4
				<option value="W2M2" id="1W2M2">WIT+2 MEN-2
				<option value="W2M3" id="1W2M3">WIT+2 MEN-3
				<option value="W2M4" id="1W2M4">WIT+2 MEN-4
				<option value="W3I3" id="1W3I3">WIT+3 INT-3
				<option value="W3I4" id="1W3I4">WIT+3 INT-4
				<option value="W3I5" id="1W3I5">WIT+3 INT-5
				<option value="W3M3" id="1W3M3">WIT+3 MEN-3
				<option value="W3M4" id="1W3M4">WIT+3 MEN-4
				<option value="W3M5" id="1W3M5">WIT+3 MEN-5
				<option value="W4I4" id="1W4I4">WIT+4 INT-4
				<option value="W4I5" id="1W4I5">WIT+4 INT-5
				<option value="W4I6" id="1W4I6">WIT+4 INT-6
				<option value="W4M4" id="1W4M4">WIT+4 MEN-4
				<option value="W4M5" id="1W4M5">WIT+4 MEN-5
				<option value="W4M6" id="1W4M6">WIT+4 MEN-6
				</select>

<br>
			Tattoo Slot 2
				<select id="tattooslot2" onChange="calc()">
				<option value="NoTat">No Tattoo
				<option value="C1D1">CON+1 DEX-1
				<option value="C1D2">CON+1 DEX-2
				<option value="C1D3">CON+1 DEX-3
				<option value="C1S1">CON+1 STR-1
				<option value="C1S2">CON+1 STR-2
				<option value="C1S3">CON+1 STR-3
				<option value="C2D2">CON+2 DEX-2
				<option value="C2D3">CON+2 DEX-3
				<option value="C2D4">CON+2 DEX-4
				<option value="C2S2">CON+2 STR-2
				<option value="C2S3">CON+2 STR-3
				<option value="C2S4">CON+2 STR-4
				<option value="C3D3">CON+3 DEX-3
				<option value="C3D4">CON+3 DEX-4
				<option value="C3D5">CON+3 DEX-5
				<option value="C3S3">CON+3 STR-3
				<option value="C3S4">CON+3 STR-4
				<option value="C3S5">CON+3 STR-5
				<option value="C4D4">CON+4 DEX-4
				<option value="C4D5">CON+4 DEX-5
				<option value="C4D6">CON+4 DEX-6
				<option value="C4S4">CON+4 STR-4
				<option value="C4S5">CON+4 STR-5
				<option value="C4S6">CON+4 STR-6
				<option value="D1C1">DEX+1 CON-1
				<option value="D1C2">DEX+1 CON-2
				<option value="D1C3">DEX+1 CON-3
				<option value="D1S1">DEX+1 STR-1
				<option value="D1S2">DEX+1 STR-2
				<option value="D1S3">DEX+1 STR-3
				<option value="D2C2">DEX+2 CON-2
				<option value="D2C3">DEX+2 CON-3
				<option value="D2C4">DEX+2 CON-4
				<option value="D2S2">DEX+2 STR-2
				<option value="D2S3">DEX+2 STR-3
				<option value="D2S4">DEX+2 STR-4
				<option value="D3C3">DEX+3 CON-3
				<option value="D3C4">DEX+3 CON-4
				<option value="D3C5">DEX+3 CON-5
				<option value="D3S3">DEX+3 STR-3
				<option value="D3S4">DEX+3 STR-4
				<option value="D3S5">DEX+3 STR-5
				<option value="D4C4">DEX+4 CON-4
				<option value="D4C5">DEX+4 CON-5
				<option value="D4C6">DEX+4 CON-6
				<option value="D4S4">DEX+4 STR-4
				<option value="D4S5">DEX+4 STR-5
				<option value="D4S6">DEX+4 STR-6
				<option value="I1M1" id="2I1M1">INT+1 MEN-1
				<option value="I1M2" id="2I1M2">INT+1 MEN-2
				<option value="I1M3" id="2I1M3">INT+1 MEN-3
				<option value="I1W1" id="2I1W1">INT+1 WIT-1
				<option value="I1W2" id="2I1W2">INT+1 WIT-2
				<option value="I1W3" id="2I1W3">INT+1 WIT-3
				<option value="I2M2" id="2I2M2">INT+2 MEN-2
				<option value="I2M3" id="2I2M3">INT+2 MEN-3
				<option value="I2M4" id="2I2M4">INT+2 MEN-4
				<option value="I2W2" id="2I2W2">INT+2 WIT-2
				<option value="I2W3" id="2I2W3">INT+2 WIT-3
				<option value="I2W4" id="2I2W4">INT+2 WIT-4
				<option value="I3M3" id="2I3M3">INT+3 MEN-3
				<option value="I3M4" id="2I3M4">INT+3 MEN-4
				<option value="I3M5" id="2I3M5">INT+3 MEN-5
				<option value="I3W3" id="2I3W3">INT+3 WIT-3
				<option value="I3W4" id="2I3W4">INT+3 WIT-4
				<option value="I3W5" id="2I3W5">INT+3 WIT-5
				<option value="I4M4" id="2I4M4">INT+4 MEN-4
				<option value="I4M5" id="2I4M5">INT+4 MEN-5
				<option value="I4M6" id="2I4M6">INT+4 MEN-6
				<option value="I4W4" id="2I4W4">INT+4 WIT-4
				<option value="I4W4" id="2I4W5">INT+4 WIT-5
				<option value="I4W4" id="2I4W6">INT+4 WIT-6
				<option value="M1I1" id="2M1I1">MEN+1 INT-1
				<option value="M1I2" id="2M1I2">MEN+1 INT-2
				<option value="M1I3" id="2M1I3">MEN+1 INT-3
				<option value="M1W1" id="2M1W1">MEN+1 WIT-1
				<option value="M1W2" id="2M1W2">MEN+1 WIT-2
				<option value="M1W3" id="2M1W3">MEN+1 WIT-3
				<option value="M2I2" id="2M2I2">MEN+2 INT-2
				<option value="M2I3" id="2M2I3">MEN+2 INT-3
				<option value="M2I4" id="2M2I4">MEN+2 INT-4
				<option value="M2W2" id="2M2W2">MEN+2 WIT-2
				<option value="M2W3" id="2M2W3">MEN+2 WIT-3
				<option value="M2W4" id="2M2W4">MEN+2 WIT-4
				<option value="M3I3" id="2M3I3">MEN+3 INT-3
				<option value="M3I4" id="2M3I4">MEN+3 INT-4
				<option value="M3I5" id="2M3I5">MEN+3 INT-5
				<option value="M3W3" id="2M3W3">MEN+3 WIT-3
				<option value="M3W4" id="2M3W4">MEN+3 WIT-4
				<option value="M3W5" id="2M3W5">MEN+3 WIT-5
				<option value="M4I4" id="2M4I4">MEN+4 INT-4
				<option value="M4I5" id="2M4I5">MEN+4 INT-5
				<option value="M4I6" id="2M4I6">MEN+4 INT-6
				<option value="M4W4" id="2M4W4">MEN+4 WIT-4
				<option value="M4W5" id="2M4W5">MEN+4 WIT-5
				<option value="M4W6" id="2M4W6">MEN+4 WIT-6
				<option value="S1C1">STR+1 CON-1
				<option value="S1C2">STR+1 CON-2
				<option value="S1C3">STR+1 CON-3
				<option value="S1D1">STR+1 DEX-1
				<option value="S1D1">STR+1 DEX-2
				<option value="S1D1">STR+1 DEX-3
				<option value="S2C2">STR+2 CON-2
				<option value="S2C2">STR+2 CON-3
				<option value="S2C2">STR+2 CON-4
				<option value="S2D2">STR+2 DEX-2
				<option value="S2D3">STR+2 DEX-3
				<option value="S2D4">STR+2 DEX-4
				<option value="S3C3">STR+3 CON-3
				<option value="S3C4">STR+3 CON-4
				<option value="S3C5">STR+3 CON-5
				<option value="S3D3">STR+3 DEX-3
				<option value="S3D4">STR+3 DEX-4
				<option value="S3D5">STR+3 DEX-5
				<option value="S4C4">STR+4 CON-4
				<option value="S4C5">STR+4 CON-5
				<option value="S4C6">STR+4 CON-6
				<option value="S4D4">STR+4 DEX-4
				<option value="S4D5">STR+4 DEX-5
				<option value="S4D6">STR+4 DEX-6
				<option value="W1I1" id="2W1I1">WIT+1 INT-1
				<option value="W1I2" id="2W1I2">WIT+1 INT-2
				<option value="W1I3" id="2W1I3">WIT+1 INT-3
				<option value="W1M1" id="2W1M1">WIT+1 MEN-1
				<option value="W1M2" id="2W1M2">WIT+1 MEN-2
				<option value="W1M3" id="2W1M3">WIT+1 MEN-3
				<option value="W2I2" id="2W2I2">WIT+2 INT-2
				<option value="W2I3" id="2W2I3">WIT+2 INT-3
				<option value="W2I4" id="2W2I4">WIT+2 INT-4
				<option value="W2M2" id="2W2M2">WIT+2 MEN-2
				<option value="W2M3" id="2W2M3">WIT+2 MEN-3
				<option value="W2M4" id="2W2M4">WIT+2 MEN-4
				<option value="W3I3" id="2W3I3">WIT+3 INT-3
				<option value="W3I4" id="2W3I4">WIT+3 INT-4
				<option value="W3I5" id="2W3I5">WIT+3 INT-5
				<option value="W3M3" id="2W3M3">WIT+3 MEN-3
				<option value="W3M4" id="2W3M4">WIT+3 MEN-4
				<option value="W3M5" id="2W3M5">WIT+3 MEN-5
				<option value="W4I4" id="2W4I4">WIT+4 INT-4
				<option value="W4I5" id="2W4I5">WIT+4 INT-5
				<option value="W4I6" id="2W4I6">WIT+4 INT-6
				<option value="W4M4" id="2W4M4">WIT+4 MEN-4
				<option value="W4M5" id="2W4M5">WIT+4 MEN-5
				<option value="W4M6" id="2W4M6">WIT+4 MEN-6
				</select>

<br>
			Tattoo Slot 3
				<select id="tattooslot3" onChange="calc()">
				<option value="NoTat">No Tattoo
				<option value="C1D1">CON+1 DEX-1
				<option value="C1D2">CON+1 DEX-2
				<option value="C1D3">CON+1 DEX-3
				<option value="C1S1">CON+1 STR-1
				<option value="C1S2">CON+1 STR-2
				<option value="C1S3">CON+1 STR-3
				<option value="C2D2">CON+2 DEX-2
				<option value="C2D3">CON+2 DEX-3
				<option value="C2D4">CON+2 DEX-4
				<option value="C2S2">CON+2 STR-2
				<option value="C2S3">CON+2 STR-3
				<option value="C2S4">CON+2 STR-4
				<option value="C3D3">CON+3 DEX-3
				<option value="C3D4">CON+3 DEX-4
				<option value="C3D5">CON+3 DEX-5
				<option value="C3S3">CON+3 STR-3
				<option value="C3S4">CON+3 STR-4
				<option value="C3S5">CON+3 STR-5
				<option value="C4D4">CON+4 DEX-4
				<option value="C4D5">CON+4 DEX-5
				<option value="C4D6">CON+4 DEX-6
				<option value="C4S4">CON+4 STR-4
				<option value="C4S5">CON+4 STR-5
				<option value="C4S6">CON+4 STR-6
				<option value="D1C1">DEX+1 CON-1
				<option value="D1C2">DEX+1 CON-2
				<option value="D1C3">DEX+1 CON-3
				<option value="D1S1">DEX+1 STR-1
				<option value="D1S2">DEX+1 STR-2
				<option value="D1S3">DEX+1 STR-3
				<option value="D2C2">DEX+2 CON-2
				<option value="D2C3">DEX+2 CON-3
				<option value="D2C4">DEX+2 CON-4
				<option value="D2S2">DEX+2 STR-2
				<option value="D2S3">DEX+2 STR-3
				<option value="D2S4">DEX+2 STR-4
				<option value="D3C3">DEX+3 CON-3
				<option value="D3C4">DEX+3 CON-4
				<option value="D3C5">DEX+3 CON-5
				<option value="D3S3">DEX+3 STR-3
				<option value="D3S4">DEX+3 STR-4
				<option value="D3S5">DEX+3 STR-5
				<option value="D4C4">DEX+4 CON-4
				<option value="D4C5">DEX+4 CON-5
				<option value="D4C6">DEX+4 CON-6
				<option value="D4S4">DEX+4 STR-4
				<option value="D4S5">DEX+4 STR-5
				<option value="D4S6">DEX+4 STR-6
				<option value="I1M1" id="3I1M1">INT+1 MEN-1
				<option value="I1M2" id="3I1M2">INT+1 MEN-2
				<option value="I1M3" id="3I1M3">INT+1 MEN-3
				<option value="I1W1" id="3I1W1">INT+1 WIT-1
				<option value="I1W2" id="3I1W2">INT+1 WIT-2
				<option value="I1W3" id="3I1W3">INT+1 WIT-3
				<option value="I2M2" id="3I2M2">INT+2 MEN-2
				<option value="I2M3" id="3I2M3">INT+2 MEN-3
				<option value="I2M4" id="3I2M4">INT+2 MEN-4
				<option value="I2W2" id="3I2W2">INT+2 WIT-2
				<option value="I2W3" id="3I2W3">INT+2 WIT-3
				<option value="I2W4" id="3I2W4">INT+2 WIT-4
				<option value="I3M3" id="3I3M3">INT+3 MEN-3
				<option value="I3M4" id="3I3M4">INT+3 MEN-4
				<option value="I3M5" id="3I3M5">INT+3 MEN-5
				<option value="I3W3" id="3I3W3">INT+3 WIT-3
				<option value="I3W4" id="3I3W4">INT+3 WIT-4
				<option value="I3W5" id="3I3W5">INT+3 WIT-5
				<option value="I4M4" id="3I4M4">INT+4 MEN-4
				<option value="I4M5" id="3I4M5">INT+4 MEN-5
				<option value="I4M6" id="3I4M6">INT+4 MEN-6
				<option value="I4W4" id="3I4W4">INT+4 WIT-4
				<option value="I4W4" id="3I4W5">INT+4 WIT-5
				<option value="I4W4" id="3I4W6">INT+4 WIT-6
				<option value="M1I1" id="3M1I1">MEN+1 INT-1
				<option value="M1I2" id="3M1I2">MEN+1 INT-2
				<option value="M1I3" id="3M1I3">MEN+1 INT-3
				<option value="M1W1" id="3M1W1">MEN+1 WIT-1
				<option value="M1W2" id="3M1W2">MEN+1 WIT-2
				<option value="M1W3" id="3M1W3">MEN+1 WIT-3
				<option value="M2I2" id="3M2I2">MEN+2 INT-2
				<option value="M2I3" id="3M2I3">MEN+2 INT-3
				<option value="M2I4" id="3M2I4">MEN+2 INT-4
				<option value="M2W2" id="3M2W2">MEN+2 WIT-2
				<option value="M2W3" id="3M2W3">MEN+2 WIT-3
				<option value="M2W4" id="3M2W4">MEN+2 WIT-4
				<option value="M3I3" id="3M3I3">MEN+3 INT-3
				<option value="M3I4" id="3M3I4">MEN+3 INT-4
				<option value="M3I5" id="3M3I5">MEN+3 INT-5
				<option value="M3W3" id="3M3W3">MEN+3 WIT-3
				<option value="M3W4" id="3M3W4">MEN+3 WIT-4
				<option value="M3W5" id="3M3W5">MEN+3 WIT-5
				<option value="M4I4" id="3M4I4">MEN+4 INT-4
				<option value="M4I5" id="3M4I5">MEN+4 INT-5
				<option value="M4I6" id="3M4I6">MEN+4 INT-6
				<option value="M4W4" id="3M4W4">MEN+4 WIT-4
				<option value="M4W5" id="3M4W5">MEN+4 WIT-5
				<option value="M4W6" id="3M4W6">MEN+4 WIT-6
				<option value="S1C1">STR+1 CON-1
				<option value="S1C2">STR+1 CON-2
				<option value="S1C3">STR+1 CON-3
				<option value="S1D1">STR+1 DEX-1
				<option value="S1D1">STR+1 DEX-2
				<option value="S1D1">STR+1 DEX-3
				<option value="S2C2">STR+2 CON-2
				<option value="S2C2">STR+2 CON-3
				<option value="S2C2">STR+2 CON-4
				<option value="S2D2">STR+2 DEX-2
				<option value="S2D3">STR+2 DEX-3
				<option value="S2D4">STR+2 DEX-4
				<option value="S3C3">STR+3 CON-3
				<option value="S3C4">STR+3 CON-4
				<option value="S3C5">STR+3 CON-5
				<option value="S3D3">STR+3 DEX-3
				<option value="S3D4">STR+3 DEX-4
				<option value="S3D5">STR+3 DEX-5
				<option value="S4C4">STR+4 CON-4
				<option value="S4C5">STR+4 CON-5
				<option value="S4C6">STR+4 CON-6
				<option value="S4D4">STR+4 DEX-4
				<option value="S4D5">STR+4 DEX-5
				<option value="S4D6">STR+4 DEX-6
				<option value="W1I1" id="3W1I1">WIT+1 INT-1
				<option value="W1I2" id="3W1I2">WIT+1 INT-2
				<option value="W1I3" id="3W1I3">WIT+1 INT-3
				<option value="W1M1" id="3W1M1">WIT+1 MEN-1
				<option value="W1M2" id="3W1M2">WIT+1 MEN-2
				<option value="W1M3" id="3W1M3">WIT+1 MEN-3
				<option value="W2I2" id="3W2I2">WIT+2 INT-2
				<option value="W2I3" id="3W2I3">WIT+2 INT-3
				<option value="W2I4" id="3W2I4">WIT+2 INT-4
				<option value="W2M2" id="3W2M2">WIT+2 MEN-2
				<option value="W2M3" id="3W2M3">WIT+2 MEN-3
				<option value="W2M4" id="3W2M4">WIT+2 MEN-4
				<option value="W3I3" id="3W3I3">WIT+3 INT-3
				<option value="W3I4" id="3W3I4">WIT+3 INT-4
				<option value="W3I5" id="3W3I5">WIT+3 INT-5
				<option value="W3M3" id="3W3M3">WIT+3 MEN-3
				<option value="W3M4" id="3W3M4">WIT+3 MEN-4
				<option value="W3M5" id="3W3M5">WIT+3 MEN-5
				<option value="W4I4" id="3W4I4">WIT+4 INT-4
				<option value="W4I5" id="3W4I5">WIT+4 INT-5
				<option value="W4I6" id="3W4I6">WIT+4 INT-6
				<option value="W4M4" id="3W4M4">WIT+4 MEN-4
				<option value="W4M5" id="3W4M5">WIT+4 MEN-5
				<option value="W4M6" id="3W4M6">WIT+4 MEN-6
				</select>

	</form>
</fieldset>
<table id="Tabla_01" width="255" height="401" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td rowspan="26">
			<img src="Images/stats_01.gif" width="9" height="400" alt=""></td>
		<td>
			<img src="Images/stats_02.gif" width="6" height="20" alt=""></td>
		<td colspan="8">
			<img src="Images/stats_03.gif" width="76" height="20" alt=""></td>
		<td colspan="13" rowspan="2">
			<img src="Images/stats_04.gif" width="164" height="75" alt=""></td>
	</tr>
	<tr>
		<td colspan="9">
			<img src="Images/stats_05.gif" width="82" height="55" alt=""></td>
	</tr>
	<tr>
		<td colspan="6">
			<img src="Images/stats_06.gif" width="52" height="14" alt=""></td>
		<td colspan="16">
			<img src="Images/stats_07.gif" width="194" height="14" alt=""></td>
	</tr>
	<tr>
		<td colspan="22">
			<img src="Images/stats_08.gif" width="246" height="15" alt=""></td>
	</tr>
	<tr>
		<td colspan="22">
			<img src="Images/stats_09.gif" width="246" height="9" alt=""></td>
	</tr>
	<tr>
		<td colspan="2">
			<img src="Images/stats_10.gif" width="23" height="11" alt=""></td>
		<td colspan="3">
			<img src="Images/stats_11.gif" width="17" height="11" alt=""></td>
		<td colspan="8">
			<img src="Images/stats_12.gif" width="83" height="11" alt=""></td>
		<td colspan="9" rowspan="2">
			<img src="Images/stats_13.gif" width="123" height="12" alt=""></td>
	</tr>
	<tr>
		<td colspan="13">
			<img src="Images/stats_14.gif" width="123" height="1" alt=""></td>
	</tr>
	<tr>
		<td colspan="3" rowspan="2">
			<img src="Images/stats_15.gif" width="26" height="13" alt=""></td>
		<td colspan="9">
			<img src="Images/stats_16.gif" width="85" height="12" alt=""></td>
		<td rowspan="2">
			<img src="Images/stats_17.gif" width="12" height="13" alt=""></td>
		<td rowspan="7">
			<img src="Images/stats_18.gif" width="24" height="45" alt=""></td>
		<td colspan="6">
			<img src="Images/stats_19.gif" width="85" height="12" alt=""></td>
		<td colspan="2" rowspan="7">
			<img src="Images/stats_20.gif" width="14" height="45" alt=""></td>
	</tr>
	<tr>
		<td colspan="9">
			<img src="Images/stats_21.gif" width="85" height="1" alt=""></td>
		<td colspan="6" rowspan="6">
			<img src="Images/stats_22.gif" width="85" height="33" alt=""></td>
	</tr>
	<tr>
		<td colspan="13">
			<img src="Images/stats_23.gif" width="123" height="1" alt=""></td>
	</tr>
	<tr>
		<td colspan="3">
			<img src="Images/stats_24.gif" width="26" height="12" alt=""></td>
		<td colspan="9">
			<img src="Images/stats_25.gif" width="85" height="12" alt=""></td>
		<td>
			<img src="Images/stats_26.gif" width="12" height="12" alt=""></td>
	</tr>
	<tr>
		<td colspan="13">
			<img src="Images/stats_27.gif" width="123" height="2" alt=""></td>
	</tr>
	<tr>
		<td colspan="3">
			<img src="Images/stats_28.gif" width="26" height="12" alt=""></td>
		<td colspan="9">
			<img src="Images/stats_29.gif" width="85" height="12" alt=""></td>
		<td rowspan="2">
			<img src="Images/stats_30.gif" width="12" height="17" alt=""></td>
	</tr>
	<tr>
		<td colspan="12">
			<img src="Images/stats_31.gif" width="111" height="5" alt=""></td>
	</tr>
	<tr>
		<td colspan="22">
			<img src="Images/stats_32.gif" width="246" height="27" alt=""></td>
	</tr>
	<tr>
		<td colspan="7">
			<img src="Images/stats_33.gif" width="55" height="14" alt=""></td>
		<td colspan="4" background="Images/stats_34.gif">
			<div align="right"><span class="Estilo1">986</span></div></td>
		<td rowspan="5">
			<img src="Images/stats_35.gif" width="5" height="77" alt=""></td>
		<td colspan="4">
			<img src="Images/stats_36.gif" width="55" height="14" alt=""></td>
		<td colspan="3">
			<img src="Images/stats_37.gif" width="61" height="14" alt=""></td>
		<td colspan="2" rowspan="5">
			<img src="Images/stats_38.gif" width="13" height="77" alt=""></td>
		<td rowspan="10">
			<img src="Images/stats_39.gif" width="6" height="195" alt=""></td>
	</tr>
	<tr>
		<td colspan="7">
			<img src="Images/stats_40.gif" width="55" height="16" alt=""></td>
		<td colspan="4">
			<img src="Images/stats_41.gif" width="51" height="16" alt=""></td>
		<td colspan="4">
			<img src="Images/stats_42.gif" width="55" height="16" alt=""></td>
		<td colspan="3">
			<img src="Images/stats_43.gif" width="61" height="16" alt=""></td>
	</tr>
	<tr>
		<td colspan="7">
			<img src="Images/stats_44.gif" width="55" height="15" alt=""></td>
		<td colspan="4">
			<img src="Images/stats_45.gif" width="51" height="15" alt=""></td>
		<td colspan="4">
			<img src="Images/stats_46.gif" width="55" height="15" alt=""></td>
		<td colspan="3">
			<img src="Images/stats_47.gif" width="61" height="15" alt=""></td>
	</tr>
	<tr>
		<td colspan="7">
			<img src="Images/stats_48.gif" width="55" height="15" alt=""></td>
		<td colspan="4">
			<img src="Images/stats_49.gif" width="51" height="15" alt=""></td>
		<td colspan="4">
			<img src="Images/stats_50.gif" width="55" height="15" alt=""></td>
		<td colspan="3">
			<img src="Images/stats_51.gif" width="61" height="15" alt=""></td>
	</tr>
	<tr>
		<td colspan="7">
			<img src="Images/stats_52.gif" width="55" height="17" alt=""></td>
		<td colspan="4">
			<img src="Images/stats_53.gif" width="51" height="17" alt=""></td>
		<td colspan="5">
			<img src="Images/stats_54.gif" width="79" height="17" alt=""></td>
		<td colspan="2">
			<img src="Images/stats_55.gif" width="37" height="17" alt=""></td>
	</tr>
	<tr>
		<td colspan="21">
			<img src="Images/stats_56.gif" width="240" height="23" alt=""></td>
	</tr>
	<tr>
		<td colspan="4">
			<img src="Images/stats_57.gif" width="36" height="19" alt=""></td>
		<td colspan="4">
			<img src="Images/stats_58.gif" width="41" height="19" alt=""></td>
		<td colspan="2" rowspan="2">
			<img src="Images/stats_59.gif" width="7" height="36" alt=""></td>
		<td colspan="2">
			<img src="Images/stats_60.gif" width="27" height="19" alt=""></td>
		<td colspan="3">
			<img src="Images/stats_61.gif" width="48" height="19" alt=""></td>
		<td colspan="3">
			<img src="Images/stats_62.gif" width="38" height="19" alt=""></td>
		<td colspan="3">
			<img src="Images/stats_63.gif" width="43" height="19" alt=""></td>
	</tr>
	<tr>
		<td colspan="4">
			<img src="Images/stats_64.gif" width="36" height="17" alt=""></td>
		<td colspan="4">
			<img src="Images/stats_65.gif" width="41" height="17" alt=""></td>
		<td colspan="2">
			<img src="Images/stats_66.gif" width="27" height="17" alt=""></td>
		<td colspan="3">
			<img src="Images/stats_67.gif" width="48" height="17" alt=""></td>
		<td colspan="3">
			<img src="Images/stats_68.gif" width="38" height="17" alt=""></td>
		<td colspan="3">
			<img src="Images/stats_69.gif" width="43" height="17" alt=""></td>
	</tr>
	<tr>
		<td colspan="21">
			<img src="Images/stats_70.gif" width="240" height="23" alt=""></td>
	</tr>
	<tr>
		<td colspan="21">
			<img src="Images/stats_71.gif" width="240" height="36" alt=""></td>
	</tr>
	<tr>
		<td colspan="22">
			<img src="Images/stats_72.gif" width="246" height="8" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="Images/espacio.gif" width="9" height="1" alt=""></td>
		<td>
			<img src="Images/espacio.gif" width="6" height="1" alt=""></td>
		<td>
			<img src="Images/espacio.gif" width="17" height="1" alt=""></td>
		<td>
			<img src="Images/espacio.gif" width="3" height="1" alt=""></td>
		<td>
			<img src="Images/espacio.gif" width="10" height="1" alt=""></td>
		<td>
			<img src="Images/espacio.gif" width="4" height="1" alt=""></td>
		<td>
			<img src="Images/espacio.gif" width="12" height="1" alt=""></td>
		<td>
			<img src="Images/espacio.gif" width="3" height="1" alt=""></td>
		<td>
			<img src="Images/espacio.gif" width="22" height="1" alt=""></td>
		<td>
			<img src="Images/espacio.gif" width="5" height="1" alt=""></td>
		<td>
			<img src="Images/espacio.gif" width="2" height="1" alt=""></td>
		<td>
			<img src="Images/espacio.gif" width="22" height="1" alt=""></td>
		<td>
			<img src="Images/espacio.gif" width="5" height="1" alt=""></td>
		<td>
			<img src="Images/espacio.gif" width="12" height="1" alt=""></td>
		<td>
			<img src="Images/espacio.gif" width="24" height="1" alt=""></td>
		<td>
			<img src="Images/espacio.gif" width="12" height="1" alt=""></td>
		<td>
			<img src="Images/espacio.gif" width="7" height="1" alt=""></td>
		<td>
			<img src="Images/espacio.gif" width="24" height="1" alt=""></td>
		<td>
			<img src="Images/espacio.gif" width="7" height="1" alt=""></td>
		<td>
			<img src="Images/espacio.gif" width="30" height="1" alt=""></td>
		<td>
			<img src="Images/espacio.gif" width="5" height="1" alt=""></td>
		<td>
			<img src="Images/espacio.gif" width="8" height="1" alt=""></td>
		<td>
			<img src="Images/espacio.gif" width="6" height="1" alt=""></td>
	</tr>
</table>
<br>
<table>
<tr>
<td>
<fieldset>
<input type=checkbox id="WALK" onchange=calc()>Walk Mode<br>
HP:
<form>
<input type="radio" name="HPMODIFIER" id="HP1" onclick=calc() checked>100%<br>
<input type="radio" name="HPMODIFIER" id="HP6" onclick=calc()>60%<br>
<input type="radio" name="HPMODIFIER" id="HP3" onclick=calc()>30%<br>
</fieldset>

</td>
</tr>
</table>

</form>
<fieldset>
	<table class="statcalc"
		<form>
<tr>			<td colspan=8>Turn the form display on or off with these:</td></tr>
<tr>			<td width=5><input type=checkbox id="ITEMSCHECK" onClick="calc()" checked></td>
			<td>Equipment</td>
			<td width=5><input type=checkbox id="PASSIVESCHECK" onClick="calc()" checked></td>		
			<td>Passives</td>

			<td width=5><input type=checkbox id="BUFFSCHECK" onClick="calc()" checked></td>
			<td>Buffs</td>
			<td width=5><input type=checkbox id="TOGGLESCHECK" onClick="calc()" checked></td>
			<td>Toggles</td>
			<td width=5><input type=checkbox id="DEBUFFSCHECK" onClick="calc()" checked></td>
			<td>Debuffs</td>
</tr>
		</form>

	</table>
</fieldset>

<fieldset id="ITEMS">
<legend>Equipment</legend>
	<table class="statcalc">
<tr>
	<form id="EQUIPMENT">
			<td>Weapon</td>
				<td><select id="WEAPONENCHANT" onChange="calc()">

					<option value="0">+0<option value="1">+1<option value="2">+2<option value="3">+3<option value="4">+4<option value="5">+5<option value="6">+6<option value="7">+7<option value="8">+8<option value="9">+9<option value="10">+10<option value="11">+11<option value="12">+12<option value="13">+13<option value="14">+14<option value="15">+15<option value="16">+16<option value="17">+17<option value="18">+18<option value="19">+19<option value="20">+20
				</select></td>

				<td><select id="WEAPONGRADE" onChange="calc()">
					<option value="S">S Grade
					<option value="A">A Grade
					<option value="B">B Grade
					<option value="C">C Grade
					<option value="D">D Grade
					<option value="N">Non Grade
					<option value="H">Hero
				</select></td>
				<td>
				<select id="WPNS" onChange="calc(false,true,false,false,false,false)">
					<option value="0">Unequipped
					<option value="1">Angel Slayer
					<option value="2">Arcana Mace
					<option value="3">Basalt Battlehammer
					<option value="4">Demon Splinter
					<option value="5">Draconic Bow
					<option value="6">Dragon Hunter Axe
					<option value="7">Forgotten Blade
					<option value="8">Heavens Divider
					<option value="9">Imperial Staff
					<option value="10">Saint Spear
					<option value="11">Tallum Blade*Dark Legion's Edge (HP/CP)
				</select>

				<select id="WPNA" onChange="calc(false,true,false,false,false,false)">
					<option value="0">Unequipped
					<option value="12">Blood Tornado
					<option value="13">Bloody Orchid
					<option value="14">Branch of the Mother Tree
					<option value="15">Carnage Bow
					<option value="16">Damascus*Damascus (Guidance)
					<option value="17">Dark Legion's Edge
					<option value="18">Dasparion's Staff
					<option value="19">Destroyer Hammer
					<option value="20">Doom Crusher
					<option value="21">Dragon Grinder
					<option value="22">Dragon Slayer
					<option value="23">Elemental Sword
					<option value="24">Elysian Axe
					<option value="25">Flaming Dragon Skull
					<option value="26">Halberd
					<option value="27">Infernal Master
					<option value="28">Keshanberk*Damascus (Health)
					<option value="29">Keshanberk*Keshanberk (Haste)
					<option value="30">Meteor Shower
					<option value="31">Soul Bow
					<option value="32">Soul Seperator
					<option value="33">Spiritual Eye
					<option value="34">Sword of Miracles
					<option value="35">Tallum Blade
					<option value="36">Tallum Glaive
				</select>

				<select id="WPNB" onChange="calc(false,true,false,false,false,false)">
					<option value="0">Unequipped
					<option value="37">Art of Battle Axe
					<option value="38">Arthro Nail
					<option value="39">Bellion Cestus
					<option value="40">Bow of Peril
					<option value="41">Dark Elven Long Bow
					<option value="42">Deadman's Glory
					<option value="43">Demon's Sword
					<option value="44">Great Axe
					<option value="45">Great Sword
					<option value="46">Guardian Sword
					<option value="47">Heavy War Axe
					<option value="48">Kaim Vanul's Bones
					<option value="49">Hell Knife
					<option value="50">Ice Storm Hammer
					<option value="51">Keshanberk
					<option value="52">Kris
					<option value="53">Lance
					<option value="54">Spell Breaker
					<option value="55">Sprite's Staff
					<option value="56">Staff of Evil Spirit
					<option value="57">Star Buster
					<option value="58">Sword of Damascus
					<option value="59">Sword of Valhalla
					<option value="60">Wizard's Tear
					<option value="61">236/99 Samurai Long Sword*Samurai Long Sword (Haste)
					<option value="62">228/97 Caliburs*Samurai Long Sword (Haste)
					<option value="63">228/97 Sword of Delusion*Samurai Long Sword (Critical Damage)
					<option value="64">228/97 Sword of Limit*Samurai Long Sword (Guidance)
					<option value="65">228/97 Sword of Nightmare*Samurai Long Sword (Health)
					<option value="66">228/97 Tsurugi*Samurai Long Sword (Focus)
					<option value="67">220/94 Katana*Samurai Long Sword (Critical Damage)
					<option value="68">220/94 Raid Sword*Samurai Long Sword (Focus)
					<option value="69">220/94 Shamshir*Samurai Long Sword (Guidance)
					<option value="70">220/94 Spirit Sword*Samurai Long Sword (Health)
					<option value="71">213/91 Caliburs*Caliburs (Critical Damage)
					<option value="72">213/91 Caliburs*Sword of Delusion (Focus)
					<option value="73">213/91 Caliburs*Sword of Limit (Health)
					<option value="74">213/91 Caliburs*Sword of Nightmare (Haste)
					<option value="75">213/91 Caliburs*Tsurugi (Guidance)
					<option value="76">213/91 Stormbringer*Samurai Long Sword (Guidance)
					<option value="77">213/91 Sword of Delusion*Sword of Delusion (Guidance)
					<option value="78">213/91 Sword of Delusion*Sword of Nightmare (Critical Damage)
					<option value="79">213/91 Sword of Delusion*Tsurugi (Health)
					<option value="80">213/91 Sword of Limit*Sword of Delusion (Health)
					<option value="81">213/91 Sword of Limit*Sword of Limit (Critical Damage)
					<option value="82">213/91 Sword of Limit*Sword of Nightmare (Focus)
					<option value="83">213/91 Sword of Limit*Tsurugi (Haste)
					<option value="84">213/91 Sword of Nightmare*Sword of Nightmare (Focus)
					<option value="85">213/91 Sword of Nightmare*Tsurugi (Health)
					<option value="86">213/91 Tsurugi*Tsurugi (Focus)
					<option value="87">204/89 Katana*Caliburs (Guidance)
					<option value="88">204/89 Katana*Sword of Delusion (Health)
					<option value="89">204/89 Katana*Sword of Limit (Critical Damage)
					<option value="90">204/89 Katana*Sword of Nightmare (Focus)
					<option value="91">204/89 Katana*Tsurugi (Haste)
					<option value="92">204/89 Raid Sword*Caliburs (Guidance)
					<option value="93">204/89 Raid Sword*Sword of Delusion (Health)
					<option value="94">204/89 Raid Sword*Sword of Limit (Critical Damage)
					<option value="95">204/89 Raid Sword*Sword of Nightmare (Focus)
					<option value="96">204/89 Raid Sword*Tsurugi (Haste)
					<option value="97">204/89 Shamshir*Caliburs (Guidance)
					<option value="98">204/89 Shamshir*Sword of Delusion (Health)
					<option value="99">204/89 Shamshir*Sword of Limit (Critical Damage)
					<option value="100">204/89 Shamshir*Sword of Nightmare (Focus)
					<option value="101">204/89 Shamshir*Tsurugi (Haste)
					<option value="102">204/89 Spirit Sword*Caliburs (Guidance)
					<option value="103">204/89 Spirit Sword*Sword of Delusion (Health)
					<option value="104">204/89 Spirit Sword*Sword of Limit (Critical Damage)
					<option value="105">204/89 Spirit Sword*Sword of Nightmare (Focus)
					<option value="106">204/89 Spirit Sword*Tsurugi (Haste)
					<option value="107">197/86 Stormbringer*Caliburs (Guidance)
					<option value="108">197/86 Stormbringer*Sword of Delusion (Health)
					<option value="109">197/86 Stormbringer*Sword of Limit (Critical Damage)
					<option value="110">197/86 Stormbringer*Sword of Nightmare (Focus)
					<option value="111">197/86 Stormbringer*Tsurugi (Haste)
				</select>

				<select id="WPNC" onChange="calc(false,true,false,false,false,false)">
					<option value="0">Unequipped
					<option value="0">====Blunt====
					<option value="112">156/83 Yaksa Mace
					<option value="113">139/76 War Axe
					<option value="114">122/68 Dwarven War Hammer
					<option value="115">107/61 Battle Axe
					<option value="116">107/61 Big Hammer
					<option value="117">107/61 Silver Axe
					<option value="118">107/61 Skull Graver
					<option value="0">====Bow====
					<option value="119">323/83 Eminence Bow
					<option value="120">316/84 Akat Long Bow
					<option value="121">277/75 Elemental Bow
					<option value="122">252/68 Noble Elven Bow
					<option value="123">220/61 Crystallized Ice Bow
					<option value="0">====Dagger====
					<option value="124">136/83 Crystal Dagger
					<option value="125">122/76 Dark Screamer
					<option value="126">122/76 Grace Dagger
					<option value="127">107/68 Stiletto
					<option value="128">94/61 Cursed Dagger
					<option value="129">94/61 Dark Elven Dagger
					<option value="0">====Fist====
					<option value="130">190/83 Great Pata
					<option value="131">169/76 Fist Blade
					<option value="132">148/68 Knuckle Duster
					<option value="133">130/61 Chakram
					<option value="0">====Mystic Weapon (1H)====
					<option value="134">125/111 Ecliptic Axe
					<option value="135">125/111 Ecliptic Sword
					<option value="136">111/101 Club of Nature
					<option value="137">111/101 Heathen's Book
					<option value="138">111/101 Homunkulus's Sword
					<option value="139">111/101 Mace of the Underworld
					<option value="140">111/101 Nirvana Axe
					<option value="141">111/101 Stick of Eternity
					<option value="142">111/101 Sword of Whispering Death
					<option value="143">98/91 Horn of Glory
					<option value="144">86/91 Soulfire Dirk
					<option value="145">85/81 Mysterious Sword
					<option value="146">85/81 Stick of Faith
					<option value="0">====Mystic Weapon (2h)====
					<option value="147">151/111 Deadman's Staff
					<option value="148">151/111 Demon's Staff
					<option value="149">152/111 Ghoul's Staff
					<option value="150">141/104 Paagrio Axe
					<option value="151">135/101 Paagrio Hammer
					<option value="152">135/101 Paradia Staff
					<option value="153">135/101 Sage's Staff
					<option value="154">119/91 Cursed Staff
					<option value="155">103/81 Crystal Staff
					<option value="156">103/81 Heavy Doom Axe
					<option value="157">103/81 Heavy Doom Hammer
					<option value="0">====Polearm====
					<option value="158">156/83 Orcish Poleaxe
					<option value="159">144/78 Scorpion
					<option value="160">144/78 Widow Maker
					<option value="161">122/68 Bec de Corbin
					<option value="162">107/61 Body Slasher
					<option value="163">107/61 Orcish Glaive
					<option value="164">107/61 Scythe
					<option value="0">====Sword====
					<option value="165">156/83 Samurai Longsword
					<option value="166">139/76 Caliburs
					<option value="167">139/76 Sword of Delusion
					<option value="168">139/76 Sword of Limit
					<option value="169">139/76 Sword of Nightmare
					<option value="170">139/76 Tsurugi
					<option value="171">122/68 Katana
					<option value="172">122/68 Raid Sword
					<option value="173">122/68 Shamshir
					<option value="174">122/68 Spirit Sword
					<option value="175">107/61 Stormbringer
					<option value="0">====Two-Handed Blunt====
					<option value="176">190/83 Dwarven Hammer
					<option value="177">169/76 Karik Horn
					<option value="0">====Two-Handed Sword====
					<option value="178">190/83 Berserker Blade
					<option value="179">169/76 Pa'agrian Sword
					<option value="180">130/61 Flamberge
					<option value="0">====Dual Sword====
					<option value="181">190/83 Katana*Katana (Haste)
					<option value="182">190/83 Katana*Raid Sword (Critical Damage)
					<option value="183">190/83 Katana*Spirit Sword (Guidance)
					<option value="184">190/83 Raid Sword*Raid Sword (Haste)
					<option value="185">190/83 Shamshir*Katana (Critical Damage)
					<option value="186">190/83 Shamshir*Raid Sword (Health)
					<option value="187">190/83 Shamshir*Shamshir (Guidance)
					<option value="188">190/83 Shamshir*Spirit Sword (Health)
					<option value="189">190/83 Spirit Sword*Raid Sword (Focus)
					<option value="190">190/83 Spirit Sword*Spirit Sword (Health)
					<option value="191">183/81 Stormbringer*Katana (Health)
					<option value="192">183/81 Stormbringer*Raid Sword (Haste)
					<option value="193">183/81 Stormbringer*Shamshir (Critical Damage)
					<option value="194">183/81 Stormbringer*Spirit Sword (Focus)
					<option value="195">175/78 Stormbringer*Stormbringer (Guidance)
					<option value="196">162/73 Elven Long Sword*Elven Long Sword (Haste)
					<option value="197">155/70 Sword of Revolution*Elven Long Sword (Focus)
					<option value="198">148/68 Crimson Sword*Elven Long Sword (Guidance)
					<option value="199">148/68 Elven Sword*Elven Long Sword (Critical Damage)
					<option value="200">148/68 Sword of Revolution*Sword of Revolution (Health)
					<option value="201">136/63 Artisan's Sword*Elven Long Sword
					<option value="201">136/63 Bastard Sword*Elven Long Sword
					<option value="201">136/63 Crimson Sword*Sword of Revolution
					<option value="201">136/63 Elven Sword*Sword of Revolution
					<option value="201">136/63 Knight's Sword*Elven Long Sword
					<option value="201">136/63 Spinebone Sword*Elven Long Sword
					<option value="202">130/61 Saber*Elven Long Sword
					<option value="203">124/58 Artisan's Sword*Sword of Revolution
					<option value="203">124/58 Bastard Sword*Sword of Revolution
					<option value="203">124/58 Knight's Sword*Elven Long Sword
					<option value="203">124/58 Spinebone Sword*Sword of Revolution
					<option value="204">118/56 Crimson Sword*Crimson Sword
					<option value="204">118/56 Crimson Sword*Elven Sword
					<option value="204">118/56 Elven Sword*Elven Sword
					<option value="204">118/56 Saber*Sword of Revolution
				</select>

				<select id="WPND" onChange="calc(false,true,false,false,false,false)">
					<option value="0">Unequipped
					<option value="0">====Blunt====
					<option value="205">92/54 Bonebreaker
					<option value="206">79/47 Heavy Bone Club
					<option value="206">79/47 Morning Star
					<option value="206">79/47 Skull Breaker
					<option value="206">79/47 Tarbar
					<option value="207">64/39 Spiked Club
					<option value="208">51/32 Tomahawk
					<option value="208">51/32 Traveler's Tomahawk
					<option value="209">40/26 Hand Axe
					<option value="209">40/26 Heavy Mace
					<option value="209">40/26 Scalpel
					<option value="209">40/26 Work Hammer
					<option value="0">====Bow====
					<option value="210">191/54 Light Crossbow
					<option value="211">179/51 Strengthened Long Bow
					<option value="212">132/39 Gastraphetes
					<option value="213">114/35 Long Bow
					<option value="213">114/35 Traveler's Long Bow
					<option value="214">105/32 Dark Elven Bow
					<option value="214">105/32 Elven Bow
					<option value="214">105/32 Traveler's Dark Elven Bow
					<option value="215">82/26 Strengthened Bow
					<option value="0">====Dagger====
					<option value="216">80/54 Mithril Dagger
					<option value="217">69/47 Maingauche
					<option value="218">62/42 Cursed Maingauche
					<option value="219">56/39 Kukuri
					<option value="220">45/32 Poniard Dagger
					<option value="220">45/32 Traveler's Poniard Dagger
					<option value="221">35/26 Assassin Knife
					<option value="221">35/26 Crafted Dagger
					<option value="0">====Fist====
					<option value="222">112/54 Scallop Jamadhar
					<option value="223">96/47 Bich'Hwa
					<option value="224">78/39 Triple-Edged_Jamadhar
					<option value="225">62/32 Single-Edged Jamadhar
					<option value="225">62/32 Traveler's Jamadhar
					<option value="226">49/26 Bagh-Nakh
x					<option value="0">====Mystic Weapon (1h)====
					<option value="227">74/72 Staff of Life
					<option value="228">67/66 Demon Fangs
					<option value="229">63/63 Crucifix of Blood
					<option value="230">63/63 Priest Mace
					<option value="231">51/52 Blood of Saints
					<option value="232">45/52 Conjurer's Knife
					<option value="232">45/52 Dagger of Mana
					<option value="232">45/52 Mystic Knife
					<option value="232">45/52 Shillien Knife
					<option value="233">51/52 Tome of Blood
					<option value="234">43/45 Sword of Magic
					<option value="234">43/45 Sword of Mystic
					<option value="234">43/45 Sword of Occult
					<option value="235">41/43 Divine Tome
					<option value="236">41/43 Doom Hammer
					<option value="236">41/43 Mace of Judgment
					<option value="236">41/43 Mace of Miracle
					<option value="236">41/43 Mace of Prayer
					<option value="236">41/43 Traveler's Mace
					<option value="237">41/43 Traveler's Spellbook
					<option value="238">32/35 Branch of Life
					<option value="239">32/35 Priest Sword
					<option value="240">32/35 Proof of Revenge
					<option value="240">32/35 Scroll of Wisdom
					<option value="240">32/35 Tempation of Abyss
					<option value="0">====Mystic Weapon (2h)====
					<option value="241">90/72 Atuba Hammer
					<option value="241">90/72 Atuba Mace
					<option value="241">90/72 Ghost Staff
					<option value="242">77/63 Goat Head Staff
					<option value="243">62/52 Staff of Magic
					<option value="244">50/43 Conjurors_Staff
					<option value="244">50/43 Mystic Staff
					<option value="244">50/43 Staff of Mana
					<option value="244">50/43 Traveler's Staff
					<option value="245">39/35 Bone Staff
					<option value="0">====Polearm====
					<option value="246">92/54 Glaive
					<option value="247">79/47 War Pick
					<option value="247">79/47 Winged Spear
					<option value="248">64/39 Dwarven Pike
					<option value="248">64/39 War Hammer
					<option value="249">51/32 Dwarven Trident
					<option value="249">51/32 Pike
					<option value="249">51/32 Traveler's Pike
					<option value="250">40/26 Trident
					<option value="0">====Sword====
					<option value="251">92/54 Elven Long Sword
					<option value="252">79/47 Sword of Revolution
					<option value="253">64/39 Crimson Sword
					<option value="253">64/39 Elven Sword
					<option value="254">51/32 Artisan's Sword
					<option value="254">51/32 Bastard Sword
					<option value="254">51/32 Knight's Sword
					<option value="254">51/32 Spinebone Sword
					<option value="254">51/32 Traveler's Bastard Sword
					<option value="255">40/26 Saber
					<option value="0">====Two-Handed Blunt====
					<option value="256">96/47 Titan Hammer
					<option value="0">====Two-Handed Sword====
					<option value="257">112/54 Claymore
					<option value="258">96/47 Titan Sword
					<option value="259">78/39 Two-Handed Sword
					<option value="260">49/26 Heavy Sword
					<option value="0">====Dual Sword====
					<option value="261">107/51 Artisan's Sword*Crimson Sword
					<option value="261">107/51 Artisan's Sword*Elven Sword
					<option value="261">107/51 Bastard Sword*Crimson Sword
					<option value="261">107/51 Bastard Sword*Elven Sword
					<option value="261">107/51 Knight's Sword*Crimson Sword
					<option value="261">107/51 Knight's Sword*Elven Sword
					<option value="261">107/51 Spinebone Sword*Crimson Sword
					<option value="261">107/51 Spinebone Sword*Elven Sword
					<option value="262">96/47 Artisan's Sword*Artisan's Sword
					<option value="262">96/47 Artisan's Sword*Knight's Sword
					<option value="262">96/47 Bastard Sword*Artisan's Sword
					<option value="262">96/47 Bastard Sword*Bastard Sword
					<option value="262">96/47 Bastard Sword*Knight's Sword
					<option value="262">96/47 Bastard Sword*Spinebone Sword
					<option value="262">96/47 Knight's Sword*Knight's Sword
					<option value="262">96/47 Saber*Crimson Sword
					<option value="262">96/47 Saber*Elven Sword
					<option value="262">96/47 Spinebone Sword*Artisan's Sword
					<option value="262">96/47 Spinebone Sword*Knight's Sword
					<option value="262">96/47 Spinebone Sword*Spinebone Sword
					<option value="263">83/41 Saber*Artisan's Sword
					<option value="263">83/41 Saber*Bastard Sword
					<option value="263">83/41 Saber*Knight's Sword
					<option value="263">83/41 Saber*Spinebone Sword
					<option value="264">73/37 Saber*Saber
				</select>

				<select id="WPNN" onChange="calc(false,true,false,false,false,false)">
					<option value="0">Unequipped
					<option value="0">====Blunt====
					<option value="265">31/21 Buzdygan
					<option value="265">31/21 Iron Hammer
					<option value="266">24/17 Apprentice Adventurer's Bone Club
					<option value="266">24/17 Bone Club
					<option value="267">17/12 Dwarven Mace
					<option value="268">13/10 Silversmith Hammer
					<option value="269">12/9 Sickle
					<option value="270">10/9 Mace
					<option value="271">10/8 Heavy Chisel
					<option value="272">6/6 Club
					<option value="273">6/5 Guild Member's Club
					<option value="0">====Bow====
					<option value="274">64/21 Composition Bow
					<option value="275">49/17 Apprentice Adventurer's Bow
					<option value="275">49/17 Forest Bow
					<option value="276">45/16 Neti's Bow
					<option value="277">34/12 Crescent Moon Bow
					<option value="277">34/12 Hunting Bow
					<option value="278">23/9 Bow
					<option value="279">16/6 Short Bow
					<option value="0">====Dagger====
					<option value="280">27/21 Sword Breaker
					<option value="281">21/17 Apprentice Adventurer's Knife
					<option value="281">21/17 Shining Knife
					<option value="281">21/17 Throwing Knife
					<option value="282">19/16 Neti's Dagger
					<option value="283">15/12 Dirk
					<option value="284">11/10 Eldritch Dagger
					<option value="285">10/9 Cybellin's Dagger
					<option value="285">10/9 Doom Dagger
					<option value="285">10/9 Knife
					<option value="285">10/9 Pipette Knife
					<option value="286">7/6 Bone Dagger
					<option value="287">5/5 Dagger
					<option value="0">====Fist====
					<option value="288">38/21 Viper's Fang
					<option value="289">29/17 Apprentice Adventurer's Cestus
					<option value="289">29/17 Cestus
					<option value="290">21/12 Fox Claw Gloves
					<option value="291">16/10 Fist of Butcher
					<option value="292">13/9 Iron Gloves
					<option value="293">10/6 Spiked Gloves
					<option value="294">7/5 Training Gloves
					<option value="295">1/1 Chrono Unitus
					<option value="0">====Mystic Weapon (1h)====
					<option value="296">25/28 Crucifix of Blessing
					<option value="296">25/28 Voodoo Doll
					<option value="297">19/22 Relic of the Saints
					<option value="297">19/22 Tears of Eva
					<option value="298">11/13 Wand of Adept
					<option value="299">9/12 Apprentice's Spellbook
					<option value="300">6/8 Apprentice's Rod
					<option value="300">6/8 Buffalo's Horn
					<option value="301">5/7 Apprentice's Wand
					<option value="0">====Mystic Weapon (2h)====
					<option value="302">30/28 Mage Staff
					<option value="303">23/22 Apprentice Adventurer's Staff
					<option value="303">23/22 Journeyman's Staff
					<option value="304">16/16 Cedar Staff
					<option value="305">13/14 Staff of Sentinel
					<option value="306">13/13 Red Sunset Staff
					<option value="307">11/12 Willow Staff
					<option value="308">10/10 Gallint's Oak Wand
					<option value="309">1/1 Chrono Darbuka
					<option value="0">====Polearm====
					<option value="310">31/21 Long Spear
					<option value="311">24/17 Talins Spear
					<option value="312">21/17 Short Spear
					<option value="313">1/1 Chrono Campana
					<option value="0">====Sword====
					<option value="314">31/21 Falchion
					<option value="315">24/17 Apprentice Adventurer's Long Sword
					<option value="315">24/17 Long Sword
					<option value="315">24/17 Sword of Reflection
					<option value="315">24/17 Sword of Watershadow
					<option value="316">18/21 Rusted Bronze Sword
					<option value="317">17/12 Gladius
					<option value="317">17/12 Handmade Sword
					<option value="317">17/12 Orcish Sword
					<option value="317">17/12 Sword of Binding
					<option value="318">14/11 Blood Saber
					<option value="318">14/11 Sword of Sentinel
					<option value="319">13/10 Butcher's Sword
					<option value="320">12/9 Sword of Solidarity
					<option value="321">11/9 Broadsword
					<option value="322">8/6 Short Sword
					<option value="323">6/5 Squire's Sword
					<option value="324">1/1 Chrono Cithara
					<option value="0">====Two-Handed Sword====
					<option value="325">38/21 Zweihander
					<option value="326">29/17 Old Knight's Sword
					<option value="327">21/12 Brandish
					<option value="328">16/10 Red Sunset Sword
					<option value="0">====Duals====
					<option value="329">Chrono Maracas
				</select>

				<select id="WPNH" onChange="calc(false,true,false,false,false,false)">
					<option value="0">Unequipped
					<option value="330">Infinity Axe
					<option value="331">Infinity Blade
					<option value="332">Infinity Bow
					<option value="333">Infinity Cleaver
					<option value="334">Infinity Crusher
					<option value="335">Infinity Fang
					<option value="336">Infinity Rod
					<option value="337">Infinity Scepter
					<option value="338">Infinity Spear
					<option value="339">Infinity Stinger
					<option value="340">Infinity Wing
				</select>
				</td>

				<td>
				<select id="WEAPONSA" onchange=calc()>
					<option value="none">no SA
					<option value="none">no SA
					<option value="none">no SA
					<option value="none">no SA
					<option value="none">no SA
				</select>
				</td></tr>
<tr>			<td colspan=3 align=right>Quickselect an armor set:</td>
				<td><select id="FULLSET" onchange=calc(false,false,true,false,false,false)>

					<option>====S Grade====
					<option value="Draconic">Draconic
					<option value="Imperial_Crusader">Imperial Crusader
					<option value="Major_Arcana">Major Arcana
					<option>====A Grade====
					<option value="Apella_Heavy">(Hvy) Apella
					<option value="Dark_Crystal_Heavy">(Hvy) Dark Crystal
					<option value="Majestic_Heavy">(Hvy) Majestic
					<option value="Nightmare_Heavy">(Hvy) Nightmare
					<option value="Tallum_Heavy">(Hvy) Tallum
					<option value="Apella_Light">(Lgt) Apella
					<option value="Dark_Crystal_Light">(Lgt) Dark Crystal
					<option value="Majestic_Light">(Lgt) Majestic
					<option value="Nightmare_Light">(Lgt) Nightmare
					<option value="Tallum_Light">(Lgt) Tallum
					<option value="Apella_Robe">(Rb) Apella
					<option value="Dark_Crystal_Robe">(Rb) Dark Crystal
					<option value="Majestic_Robe">(Rb) Majestic
					<option value="Nightmare_Robe">(Rb) Nightmare
					<option value="Tallum_Robe">(Rb) Tallum
					<option>====B Grade====
					<option value="Avadon_Heavy">(Hvy) Avadon
					<option value="Blue_Wolf_Heavy">(Hvy) Blue Wolf
					<option value="Doom_Heavy">(Hvy) Doom
					<option value="Zubei_Heavy">(Hvy) Zubei
					<option value="Avadon_Light">(Lgt) Avadon
					<option value="Blue_Wolf_Light">(Lgt) Blue Wolf
					<option value="Doom_Light">(Lgt) Doom
					<option value="Zubei_Light">(Lgt) Zubei
					<option value="Avadon_Robe">(Rb) Avadon
					<option value="Blue_Wolf_Robe">(Rb) Blue Wolf
					<option value="Doom_Robe">(Rb) Doom
					<option value="Zubei_Robe">(Rb) Zubei
					<option>====C Grade====
					<option value="Chain_Mail">Chain Mail
					<option value="Composite">Composite
					<option value="Demon">Demon
					<option value="Divine">Divine
					<option value="Drake">Drake
					<option value="Full_Plate">Full Plate
					<option value="Karmian">Karmian
					<option value="Mithril_Light">Mithril (Light)
					<option value="Plated_Leather">Plated Leather
					<option value="Theca">Theca
					<option>====D Grade====
					<option value="Brigandine">Brigandine
					<option value="Clan_Oath_Heavy">Clan Oath (Heavy)
					<option value="Clan_Oath_Light">Clan Oath (Light)
					<option value="Clan_Oath_Robe">Clan Oath (Robe)
					<option value="Elven_Mithril">Elven Mithril
					<option value="Knowledge">Knowledge
					<option value="Manticore">Manticore
					<option value="Mithril_Heavy">Mithril (Heavy)
					<option value="Reinforced_Leather">Reinforced Leather
					<option>====No Grade====
					<option value="Devotion">Devotion
					<option value="Wooden">Wooden
				</select></td>

</tr>
<tr id="shieldstuff">
			<td>Shield</td>
				<td><select id="SHIELDENCHANT" onChange="calc()">
					<option value="0">+0<option value="1">+1<option value="2">+2<option value="3">+3<option value="4">+4<option value="5">+5<option value="6">+6<option value="7">+7<option value="8">+8<option value="9">+9<option value="10">+10<option value="11">+11<option value="12">+12<option value="13">+13<option value="14">+14<option value="15">+15<option value="16">+16<option value="17">+17<option value="18">+18<option value="19">+19<option value="20">+20
				</select></td>

				<td><select id="SHIELDGRADE" onChange="calc()">
					<option value="S">S Grade
					<option value="A">A Grade
					<option value="B">B Grade
					<option value="C">C Grade
					<option value="D">D Grade
					<option value="N">Non Grade
				</select></td>
				<td><select id="SHIELDS" onChange="calc()">
					<option value="0">Unequipped
					<option value="14">Imperial Crusader Shield
				</select>
				<select id="SHIELDA" onChange="calc()">

					<option value="0">Unequipped
					<option value="8">Dark Crystal Shield
					<option value="22">Shield of Nightmare
				</select>
				<select id="SHIELDB" onChange="calc()">
					<option value="0">Unequipped
					<option value="2">Avadon Shield
					<option value="9">Doom Shield
					<option value="27">Zubei's Shield
				</select>
				<select id="SHIELDC" onChange="calc()">
					<option value="0">Unequipped
					<option value="7">Chain Shield
					<option value="28">Composite Shield
					<option value="10">Dwarven Chain Shield
					<option value="11">Eldarake
					<option value="12">Full Plate Shield
					<option value="16">Knight Shield
					<option value="26">Tower Shield
				</select>

				<select id="SHIELDD" onChange="calc()">
					<option value="0">Unequipped
					<option value="1">Aspis
					<option value="4">Brigandine Shield
					<option value="5">Bronze Shield
					<option value="13">Hoplon
					<option value="15">Kite Shield
					<option value="18">Plate Shield
					<option value="25">Square Shield
				</select>
				<select id="SHIELDN" onChange="calc()">
					<option value="0">Unequipped
					<option value="3">Bone Shield
					<option value="6">Buckler
					<option value="17">Leather Shield
					<option value="19">Pledge Shield
					<option value="20">Round Shield
					<option value="21">Shield of Castle Pledge
					<option value="23">Skeleton Buckler
					<option value="24">Small Shield
				</select>

				</td></tr>
<tr>
			<td>Helmet</td>
				<td><select id="HELMETENCHANT" onChange="calc()">
					<option value="0">+0<option value="1">+1<option value="2">+2<option value="3">+3<option value="4">+4<option value="5">+5<option value="6">+6<option value="7">+7<option value="8">+8<option value="9">+9<option value="10">+10<option value="11">+11<option value="12">+12<option value="13">+13<option value="14">+14<option value="15">+15<option value="16">+16<option value="17">+17<option value="18">+18<option value="19">+19<option value="20">+20
				</select></td>

				<td><select id="HELMETGRADE" onChange="calc()">
					<option value="S">S Grade
					<option value="A">A Grade
					<option value="B">B Grade
					<option value="C">C Grade
					<option value="D">D Grade
					<option value="N">Non Grade
				</select></td>
				<td><select id="HELMETS" onChange="calc()">
					<option value="0">Unequipped
					<option value="1">Draconic Leather Helmet
					<option value="2">Imperial Crusader Helmet
					<option value="3">Major Arcana Circlet
				</select>

				<select id="HELMETA" onChange="calc()">
					<option value="0">Unequipped
					<option value="4">Apella Cap (R)
					<option value="5">Apella Great Helm (H)
					<option value="6">Apella Helm (L)
					<option value="7">Dark Crystal Helmet
					<option value="8">Helm of Nightmare
					<option value="9">Majestic Circlet
					<option value="10">Tallum Helm
				</select>
				<select id="HELMETB" onChange="calc()">
					<option value="0">Unequipped
					<option value="11">Avadon Circlet
					<option value="12">Blue Wolf Helmet
					<option value="13">Doom Helmet
					<option value="14">Zubei's Helmet
				</select>

				<select id="HELMETC" onChange="calc()">
					<option value="0">Unequipped
					<option value="15">Chain Hood
					<option value="16">Composite Helmet
					<option value="17">Full Plate Helmet
					<option value="18">Great Helmet
					<option value="19">Shining Circlet
				</select>
				<select id="HELMETD" onChange="calc()">
					<option value="0">Unequipped
					<option value="20">Bone Helmet
					<option value="21">Brigandine Helmet
					<option value="22">Bronze Helmet
					<option value="23">Clan Oath Cap (R)
					<option value="24">Clan Oath Great Helm (H)
					<option value="25">Clan Oath Helm (L)
					<option value="26">Helmet
					<option value="27">Plate Helmet
				</select>

				<select id="HELMETN" onChange="calc()">
					<option value="0">Unequipped
					<option value="28">Cloth Cap
					<option value="29">Hard Leather Helmet
					<option value="30">Leather Cap
					<option value="31">Leather Helmet
					<option value="32">Wooden Helmet
				</select>
				</td></tr>
<tr>
			<td>Body - Upper</td>
				<td><select id="UPPERENCHANT" onChange="calc()">

					<option value="0">+0<option value="1">+1<option value="2">+2<option value="3">+3<option value="4">+4<option value="5">+5<option value="6">+6<option value="7">+7<option value="8">+8<option value="9">+9<option value="10">+10<option value="11">+11<option value="12">+12<option value="13">+13<option value="14">+14<option value="15">+15<option value="16">+16<option value="17">+17<option value="18">+18<option value="19">+19<option value="20">+20
				</select></td>

				<td><select id="UPPERGRADE" onChange="calc()">
					<option value="S">S Grade
					<option value="A">A Grade
					<option value="B">B Grade
					<option value="C">C Grade
					<option value="D">D Grade
					<option value="N">Non Grade
				</select></td>
				<td><select id="UPPERS" onChange="calc()">
					<option value="0">Unequipped
					<option value="31">Draconic Leather Armor
					<option value="40">Imperial Crusader Breastplate
					<option value="49">Major Arcana Robe
				</select>

				<select id="UPPERA" onChange="calc()">
					<option value="0">Unequipped
					<option value="1">Apella Brigandine
					<option value="2">Apella Doublet
					<option value="3">Apella Plate Armor
					<option value="5">Armor of Nightmare
					<option value="25">Dark Crystal Leather Armor
					<option value="26">Dark Crystal Breastplate
					<option value="27">Dark Crystal Robe
					<option value="46">Majestic Leather Armor
					<option value="47">Majestic Plate Armor
					<option value="48">Majestic Robe
					<option value="56">Nightmarish Leather Armor
					<option value="63">Robe of Nightmare
					<option value="70">Tallum Leather Armor
					<option value="71">Tallum Plate Armor
					<option value="72">Tallum Tunic
				</select>

				<select id="UPPERB" onChange="calc()">
					<option value="0">Unequipped
					<option value="6">Avadon Breastplate
					<option value="7">Avadon Leather Armor
					<option value="8">Avadon Robe
					<option value="10">Blue Wolf Breastplate
					<option value="11">Blue Wolf Leather Armor
					<option value="12">Blue Wolf Tunic
					<option value="30">Doom Plate Armor
					<option value="42">Leather Armor of Doom
					<option value="76">Tunic of Doom
					<option value="79">Tunic of Zubei
					<option value="82">Zubei's Breastplate
					<option value="83">Zubei's Leather Shirt
				</select>

				<select id="UPPERC" onChange="calc()">
					<option value="0">Unequipped
					<option value="16">Chain Mail Shirt
					<option value="20">Composite Armor
					<option value="28">Demon's Tunic
					<option value="29">Divine Tunic
					<option value="32">Drake Leather Armor
					<option value="33">Dwarven Chain Mail Shirt
					<option value="37">Full Plate Armor
					<option value="41">Karmian Tunic
					<option value="53">Mithril Shirt
					<option value="58">Plated Leather
					<option value="64">Robe of Seal
					<option value="61">Rind Leather Armor
					<option value="73">Theca Leather Armor
				</select>

				<select id="UPPERD" onChange="calc()">
					<option value="0">Unequipped
					<option value="9">Blast Plate
					<option value="14">Brigandine Tunic
					<option value="17">Clan Oath Aketon
					<option value="18">Clan Oath Brigandine
					<option value="19">Clan Oath Plate Armor
					<option value="21">Compound Scale Mail
					<option value="24">Cursed Tunic
					<option value="34">Dwarven Scale Mail
					<option value="35">Elven Tunic
					<option value="38">Half Plate Armor
					<option value="45">Lion Skin Shirt
					<option value="50">Manticore Skin Shirt
					<option value="51">Mithril Banded Mail
					<option value="52">Mithril Breastplate
					<option value="54">Mithril Tunic
					<option value="55">Mystic's Tunic
					<option value="59">Puma Skin Shirt
					<option value="60">Reinforced Leather Shirt
					<option value="62">Ring Mail Breastplate
					<option value="65">Sage's Rag
					<option value="66">Salamander Skin Mail
					<option value="67">Scale Mail
					<option value="77">Tunic of Knowledge
					<option value="80">White Tunic
				</select>

				<select id="UPPERN" onChange="calc()">
					<option value="0">Unequipped
					<option value="4">Apprentice's Tunic
					<option value="13">Bone Breastplate
					<option value="15">Bronze Breastplate
					<option value="22">Cotton Shirt
					<option value="23">Cotton Tunic
					<option value="36">Feriotic Tunic
					<option value="39">Hard Leather Shirt
					<option value="43">Leather Shirt
					<option value="44">Leather Tunic
					<option value="57">Piece Bone Breastplate
					<option value="68">Shirt
					<option value="69">Squire's Shirt
					<option value="74">Tunic
					<option value="75">Tunic of Devotion
					<option value="78">Tunic of Magic
					<option value="81">Wooden Breastplate
				</select>

				</td></tr>
<tr>
			<td>Body - Lower</td>
				<td><select id="LOWERENCHANT" onChange="calc()">
					<option value="0">+0<option value="1">+1<option value="2">+2<option value="3">+3<option value="4">+4<option value="5">+5<option value="6">+6<option value="7">+7<option value="8">+8<option value="9">+9<option value="10">+10<option value="11">+11<option value="12">+12<option value="13">+13<option value="14">+14<option value="15">+15<option value="16">+16<option value="17">+17<option value="18">+18<option value="19">+19<option value="20">+20
				</select></td>

				<td><select id="LOWERGRADE" onChange="calc()">
					<option value="S">S Grade
					<option value="A">A Grade
					<option value="B">B Grade
					<option value="C">C Grade
					<option value="D">D Grade
					<option value="N">Non Grade
				</select></td>
				<td><select id="LOWERS" onChange="calc()">
					<option value="0">Unequipped
					<option value="24">Imperial Crusader Gaiters
				</select>
				<select id="LOWERA" onChange="calc()">

					<option value="0">Unequipped
					<option value="13">Dark Crystal Gaiters
					<option value="14">Dark Crystalline Leggings
					<option value="46">Tallum Stockings
				</select>
				<select id="LOWERB" onChange="calc()">
					<option value="0">Unequipped
					<option value="2">Avadon Gaiters
					<option value="3">Blue Wolf Gaiters
					<option value="4">Blue Wolf Stockings
					<option value="51">Stockings of Doom
					<option value="53">Stockings of Zubei
					<option value="55">Zubei's Gaiters
					<option value="56">Zubei's Leather Gaiters
				</select>

				<select id="LOWERC" onChange="calc()">
					<option value="0">Unequipped
					<option value="8">Chain Gaiters
					<option value="16">Demon's Stockings
					<option value="17">Divine Stockings
					<option value="18">Dwarven Chain Gaiters
					<option value="26">Karmian Stockings
					<option value="39">Plated Leather Gaiters
					<option value="42">Rind Leather Gaiters
					<option value="47">Tempered Mithril Gaiters
					<option value="48">Theca Leather Gaiters
				</select>
				<select id="LOWERD" onChange="calc()">
					<option value="0">Unequipped
					<option value="6">Brigandine Gaiters
					<option value="9">Compound Scale Gaiters
					<option value="12">Cursed Stockings
					<option value="15">Dark Stockings
					<option value="19">Dwarven Scale Gaiters
					<option value="20">Elven Stockings
					<option value="25">Iron Plate Gaiters
					<option value="29">Lion Skin Gaiters
					<option value="30">Manticore Skin Gaiters
					<option value="31">Mithril Banded Gaiters
					<option value="32">Mithril Gaiters
					<option value="33">Mithril Scale Gaiters
					<option value="34">Mithril Stockings
					<option value="35">Mystic's Stockings
					<option value="38">Plate Gaiters
					<option value="40">Puma Skin Gaiters
					<option value="41">Reinforced Leather Gaiters
					<option value="49">Scale Gaiters
					<option value="52">Stockings of Knowledge
				</select>

				<select id="LOWERN" onChange="calc()">
					<option value="0">Unequipped
					<option value="1">Apprentice's Stockings
					<option value="5">Bone Gaiters
					<option value="7">Bronze Gaiters
					<option value="10">Cotton Pants
					<option value="11">Cotton Stockings
					<option value="21">Feriotic Stockings
					<option value="22">Hard Leather Gaiters
					<option value="23">Hard Leather Pants
					<option value="27">Leather Pants
					<option value="28>Leather Stockings
					<option value="36">Pants
					<option value="37">Piece Bone Gaiters
					<option value="43">Squire's Pants
					<option value="50">Stockings
					<option value="44">Stockings of Devotion
					<option value="45">Stockings of Magic
					<option value="54">Wooden Gaiters
				</select>

				</td></tr>
<tr>
			<td>Gloves</td>
				<td><select id="GLOVEENCHANT" onChange="calc()">
					<option value="0">+0<option value="1">+1<option value="2">+2<option value="3">+3<option value="4">+4<option value="5">+5<option value="6">+6<option value="7">+7<option value="8">+8<option value="9">+9<option value="10">+10<option value="11">+11<option value="12">+12<option value="13">+13<option value="14">+14<option value="15">+15<option value="16">+16<option value="17">+17<option value="18">+18<option value="19">+19<option value="20">+20
				</select></td>

				<td><select id="GLOVEGRADE" onChange="calc()">
					<option value="S">S Grade
					<option value="A">A Grade
					<option value="B">B Grade
					<option value="C">C Grade
					<option value="D">D Grade
					<option value="N">Non Grade
				</select></td>
				<td><select id="GLOVES" onChange="calc()">
					<option value="0">Unequipped
					<option value="16">Draconic Leather Gloves
					<option value="27">Imperial Crusader Gauntlets
					<option value="32">Major Arcana Gloves
				</select>

				<select id="GLOVEA" onChange="calc()">
					<option value="0">Unequipped
					<option value="1">Apella Gauntlet (H)
					<option value="2">Apella Leather Gloves (L)
					<option value="3">Apella Silk Gloves (R)
					<option value="12">Dark Crystal Gloves
					<option value="25">Gloves of Nightmare
					<option value="31">Majestic Gloves
					<option value="45">Tallum Gloves
				</select>
				<select id="GLOVEB" onChange="calc()">
					<option value="0">Unequipped
					<option value="4">Avadon Gloves
					<option value="5">Blue Wolf Gloves
					<option value="15">Doom Gloves
					<option value="47">Zubei's Gauntlets
				</select>

				<select id="GLOVEC" onChange="calc()">
					<option value="0">Unequipped
					<option value="8">Chain Gloves
					<option value="13">Demon's Gloves
					<option value="14">Divine Gloves
					<option value="17">Drake Leather Gloves
					<option value="18">Dwarven Chain Gloves
					<option value="21">Full Plate Gauntlets
					<option value="26">Gloves of Seal
					<option value="28">Karmian Gloves
					<option value="34">Mithril Gauntlets
					<option value="37">Plated Leather Gloves
					<option value="39">Reinforced Mithril Gloves
					<option value="40">Rind Leather Gloves
					<option value="46">Theca Leather Gloves
				</select>

				<select id="GLOVED" onChange="calc()">
					<option value="0">Unequipped
					<option value="7">Brigandine Gauntlets
					<option value="9">Clan Oath Gauntlets (H)
					<option value="10">Clan Oath Leather Gloves (L)
					<option value="11">Clan Oath Padded Gloves (R)
					<option value="19">Elven Mithril Gloves
					<option value="20">Excellent Leather Gloves
					<option value="22">Gauntlets
					<option value="24">Gloves of Knowledge
					<option value="29">Leather Gauntlets
					<option value="33">Manticore Skin Gloves
					<option value="35">Mithril Gloves
					<option value="36">Ogre Power Gauntlets
					<option value="38">Reinforced Leather Gloves
					<option value="41">Rip Gauntlets
					<option value="42">Sage's Worn Gloves
				</select>

				<select id="GLOVEN" onChange="calc()">
					<option value="0">Unequipped
					<option value="6">Bracer
					<option value="23">Gloves
					<option value="30">Leather Gloves
					<option value="43">Short Gloves
					<option value="44">Short Leather Gloves
				</select>
				</td></tr>
<tr>
			<td>Boots</td>
				<td><select id="BOOTENCHANT" onChange="calc()">

					<option value="0">+0<option value="1">+1<option value="2">+2<option value="3">+3<option value="4">+4<option value="5">+5<option value="6">+6<option value="7">+7<option value="8">+8<option value="9">+9<option value="10">+10<option value="11">+11<option value="12">+12<option value="13">+13<option value="14">+14<option value="15">+15<option value="16">+16<option value="17">+17<option value="18">+18<option value="19">+19<option value="20">+20
				</select></td>

				<td><select id="BOOTGRADE" onChange="calc()">
					<option value="S">S Grade
					<option value="A">A Grade
					<option value="B">B Grade
					<option value="C">C Grade
					<option value="D">D Grade
					<option value="N">Non Grade
				</select></td>
				<td><select id="BOOTS" onChange="calc()">
					<option value="0">Unequipped
					<option value="28">Draconic Leather Boots
					<option value="33">Imperial Crusader Boots
					<option value="41">Major Arcana Boots
				</select>

				<select id="BOOTA" onChange="calc()">
					<option value="0">Unequipped
					<option value="1">Apella Boots (L)
					<option value="2">Apella Sandals (R)
					<option value="3">Apella Solleret (H)
					<option value="11">Boots of Nightmare
					<option value="24">Dark Crystal Boots
					<option value="40">Majestic Boots
					<option value="50">Tallum Boots
				</select>
				<select id="BOOTB" onChange="calc()">
					<option value="0">Unequipped
					<option value="6">Avadon Boots
					<option value="8">Blue Wolf Boots
					<option value="27">Doom Boots
					<option value="52">Zubei's Boots
				</select>

				<select id="BOOTC" onChange="calc()">
					<option value="0">Unequipped
					<option value="13">Boots of Seal
					<option value="15">Chain Boots
					<option value="20">Composite Boots
					<option value="22">Crimson Boots
					<option value="25">Demon's Boots
					<option value="26">Divine Boots
					<option value="29">Drake Leather Boots
					<option value="30">Dwarven Chain Boots
					<option value="32">Full Plate Boots
					<option value="35">Karmian Boots
					<option value="43">Mithril Boots
					<option value="45">Plated Leather Boots
					<option value="47">Rind Leather Boots
					<option value="51">Theca Leather Boots
				</select>

				<select id="BOOTD" onChange="calc()">
					<option value="0">Unequipped
					<option value="5">Assault Boots
					<option value="7">Blue Buckskin Boots
					<option value="10">Boots of Knowledge
					<option value="12">Boots of Power
					<option value="14">Brigandine Boots
					<option value="16">Clan Oath Boots (L)
					<option value="17">Clan Oath Sabaton (H)
					<option value="18">Clan Oath Sandals (R)
					<option value="31">Elven Mithril Boots
					<option value="34">Iron Boots
					<option value="36">Leather Boots
					<option value="42">Manticore Boots
					<option value="44">Plate Boots
					<option value="46">Reinforced Leather Boots
					<option value="48">Salamander Skin Boots
				</select>

				<select id="BOOTN" onChange="calc()">
					<option value="0">Unequipped
					<option value="4">Apprentice's Shoes
					<option value="9">Boots
					<option value="19">Cloth Shoes
					<option value="21">Cotton Shoes
					<option value="23">Crude Leather Shoes
					<option value="37">Leather Sandals
					<option value="38">Leather Shoes
					<option value="39">Low Boots
					<option value="49">Squeaking Shoes
				</select>
				</td></tr>
<tr>			<td colspan=3 align=right>Quickselect a jewelry set:</td>

				<td><select id="FULLMDEF" onchange=calc(false,false,false,true,false,false)>
					<option>=================
					<option value="Unsealed_Tateossian">Unsealed Tateossian
					<option value="Sealed_Tateossian">Sealed Tateossian
					<option value="Unsealed_Majestic">Unsealed Majestic
					<option value="Sealed_Majestic">Sealed Majestic
					<option value="Black_Ore">Black Ore
					<option value="Top_C">Top C
					<option value="Top_Lux">Top Luxury Shop
					<option value="Top_D">Top D
					<option value="Elven">Elven
					<option value="Top_Non">Top Non Grade
				</select></td></tr>
<tr>			<td>Necklace</td>

				<td><select id="NECKLACEENCHANT" onChange="calc()">
					<option value="0">+0<option value="1">+1<option value="2">+2<option value="3">+3<option value="4">+4<option value="5">+5<option value="6">+6<option value="7">+7<option value="8">+8<option value="9">+9<option value="10">+10<option value="11">+11<option value="12">+12<option value="13">+13<option value="14">+14<option value="15">+15<option value="16">+16<option value="17">+17<option value="18">+18<option value="19">+19<option value="20">+20
				</select></td>

				<td><select id="NECKLACEGRADE" onChange="calc()">
					<option value="S">S Grade
					<option value="A">A Grade
					<option value="B">B Grade
					<option value="C">C Grade
					<option value="D">D Grade
					<option value="N">Non Grade
				</select></td>
				<td><select id="NECKLACES" onChange="calc()">
					<option value="0">Unequipped
					<option value="20">Necklace of Valakas
					<option value="26">Sealed Tateossian Necklace
					<option value="27">Tateossian Necklace
				</select>

				<select id="NECKLACEA" onChange="calc()">
					<option value="0">Unequipped
					<option value="6">Majestic Necklace
					<option value="23">Phoenix Necklace
					<option value="24">Sealed Majestic Necklace
					<option value="25">Sealed Phoenix Necklace
				</select>
				<select id="NECKLACEB" onChange="calc()">
					<option value="0">Unequipped
					<option value="1">Adamantite Necklace
					<option value="10">Necklace of Black Ore
					<option value="11">Necklace of Blessing
					<option value="15">Necklace of Grace
				</select>

				<select id="NECKLACEC" onChange="calc()">
					<option value="0">Unequipped
					<option value="2">Aquastone Necklace
					<option value="9">Necklace of Binding
					<option value="17">Necklace of Mermaid
					<option value="19">Necklace of Protection
				</select>
				<select id="NECKLACED" onChange="calc()">
					<option value="0">Unequipped
					<option value="4">Elven Necklace
					<option value="5">Enchanted Necklace
					<option value="7">Near Forest Necklace
					<option value="13">Necklace of Darkness
					<option value="14">Necklace of Devotion
				</select>

				<select id="NECKLACEN" onChange="calc()">
					<option value="0">Unequipped
					<option value="3">Blue Diamond Necklace
					<option value="8">Necklace of Anguish
					<option value="12">Necklace of Courage
					<option value="16">Necklace of Magic
					<option value="18">Necklace of Knowledge
					<option value="21">Necklace of Valor
					<option value="22">Necklace of Wisdom
				</td></tr>
<tr>			<td>Earring</td>
				<td><select id="EARRING1ENCHANT" onchange="calc()">

					<option value="0">+0<option value="1">+1<option value="2">+2<option value="3">+3<option value="4">+4<option value="5">+5<option value="6">+6<option value="7">+7<option value="8">+8<option value="9">+9<option value="10">+10<option value="11">+11<option value="12">+12<option value="13">+13<option value="14">+14<option value="15">+15<option value="16">+16<option value="17">+17<option value="18">+18<option value="19">+19<option value="20">+20
				</select></td>

				<td><select id="EARRING1GRADE" onChange="calc()">
					<option value="S">S Grade
					<option value="A">A Grade
					<option value="B">B Grade
					<option value="C">C Grade
					<option value="D">D Grade
					<option value="N">Non Grade
				</select></td>
				<td><select id="EARRING1S" onChange="calc()">
					<option value="0">Unequipped
					<option value="5">Earring of Antharas
					<option value="24">Sealed Tateossian Earring
					<option value="25">Tateossian Earring
					<option value="27">Zaken's Earring
				</select>

				<select id="EARRING1A" onChange="calc()">
					<option value="0">Unequipped
					<option value="9">Earring of Orfen
					<option value="15">Majestic Earring
					<option value="21">Phoenix Earring
					<option value="22">Sealed Majestic Earring
					<option value="23">Sealed Phoenix Earring
				</select>
				<select id="EARRING1B" onChange="calc()">
					<option value="0">Unequipped
					<option value="1">Adamantite Earring
					<option value="7">Earring of Black Ore
					<option value="8">Earring of Blessing
				</select>

				<select id="EARRING1C" onChange="calc()">
					<option value="0">Unequipped
					<option value="6">Earring of Binding
					<option value="10">Earring of Protection
					<option value="16">Moonstone Earring
					<option value="18">Nassen's Earring
				</select>
				<select id="EARRING1D" onChange="calc()">
					<option value="0">Unequipped
					<option value="13">Elven Earring
					<option value="14">Enchanted Earring
					<option value="19">Omen Beast's Eye Earring
					<option value="20">Red Crescent Earring
					<option value="26">Tiger's Eye Earring
				</select>

				<select id="EARRING1N" onChange="calc()">
					<option value="0">Unequipped
					<option value="2">Apprentice's Earring
					<option value="3">Cat's Eye Earring
					<option value="4">Coral Earring
					<option value="11">Earring of Strength
					<option value="12">Earring of Wisdom
					<option value="17">Mystic's Earring
				</select>
				</td></tr>
<tr>			<td>Earring</td>
				<td><select id="EARRING2ENCHANT" onChange="calc()">

					<option value="0">+0<option value="1">+1<option value="2">+2<option value="3">+3<option value="4">+4<option value="5">+5<option value="6">+6<option value="7">+7<option value="8">+8<option value="9">+9<option value="10">+10<option value="11">+11<option value="12">+12<option value="13">+13<option value="14">+14<option value="15">+15<option value="16">+16<option value="17">+17<option value="18">+18<option value="19">+19<option value="20">+20
				</select></td>

				<td><select id="EARRING2GRADE" onChange="calc()">
					<option value="S">S Grade
					<option value="A">A Grade
					<option value="B">B Grade
					<option value="C">C Grade
					<option value="D">D Grade
					<option value="N">Non Grade
				</select></td>
				<td><select id="EARRING2S" onChange="calc()">
					<option value="0">Unequipped
					<option value="5">Earring of Antharas
					<option value="24">Sealed Tateossian Earring
					<option value="25">Tateossian Earring
					<option value="27">Zaken's Earring
				</select>

				<select id="EARRING2A" onChange="calc()">
					<option value="0">Unequipped
					<option value="9">Earring of Orfen
					<option value="15">Majestic Earring
					<option value="21">Phoenix Earring
					<option value="22">Sealed Majestic Earring
					<option value="23">Sealed Phoenix Earring
				</select>
				<select id="EARRING2B" onChange="calc()">
					<option value="0">Unequipped
					<option value="1">Adamantite Earring
					<option value="7">Earring of Black Ore
					<option value="8">Earring of Blessing
				</select>

				<select id="EARRING2C" onChange="calc()">
					<option value="0">Unequipped
					<option value="6">Earring of Binding
					<option value="10">Earring of Protection
					<option value="16">Moonstone Earring
					<option value="18">Nassen's Earring
				</select>
				<select id="EARRING2D" onChange="calc()">
					<option value="0">Unequipped
					<option value="13">Elven Earring
					<option value="14">Enchanted Earring
					<option value="19">Omen Beast's Eye Earring
					<option value="20">Red Crescent Earring
					<option value="26">Tiger's Eye Earring
				</select>

				<select id="EARRING2N" onChange="calc()">
					<option value="0">Unequipped
					<option value="2">Apprentice's Earring
					<option value="3">Cat's Eye Earring
					<option value="4">Coral Earring
					<option value="11">Earring of Strength
					<option value="12">Earring of Wisdom
					<option value="17">Mystic's Earring
				</select>
				</td></tr>
<tr>			<td>Ring</td>
				<td><select id="RING1ENCHANT" onChange="calc()">

					<option value="0">+0<option value="1">+1<option value="2">+2<option value="3">+3<option value="4">+4<option value="5">+5<option value="6">+6<option value="7">+7<option value="8">+8<option value="9">+9<option value="10">+10<option value="11">+11<option value="12">+12<option value="13">+13<option value="14">+14<option value="15">+15<option value="16">+16<option value="17">+17<option value="18">+18<option value="19">+19<option value="20">+20
				</select></td>

				<td><select id="RING1GRADE" onChange="calc()">
					<option value="S">S Grade
					<option value="A">A Grade
					<option value="B">B Grade
					<option value="C">C Grade
					<option value="D">D Grade
					<option value="N">Non Grade
				</select></td>
				<td><select id="RING1S" onChange="calc()">
					<option value="0">Unequipped
					<option value="13">Ring of Baium
					<option value="27">Sealed Tateossian Ring
					<option value="28">Tateossian Ring
				</select>

				<select id="RING1A" onChange="calc()">
					<option value="0">Unequipped
					<option value="8">Majestic Ring
					<option value="10">Phoenix Ring
					<option value="17">Ring of Core
					<option value="25">Sealed Majestic Ring
					<option value="26">Sealed Phoenix Ring
				</select>
				<select id="RING1B" onChange="calc()">
					<option value="0">Unequipped
					<option value="1">Adamantite Ring
					<option value="15">Ring of Black Ore
					<option value="16">Ring of Blessing
					<option value="22">Ring of Queen Ant
				</select>

				<select id="RING1C" onChange="calc()">
					<option value="0">Unequipped
					<option value="2">Aquastone Ring
					<option value="11">Ring of Ages
					<option value="14">Ring of Binding
					<option value="21">Ring of Protection
				</select>
				<select id="RING1D" onChange="calc()">
					<option value="0">Unequipped
					<option value="3">Black Pearl Ring
					<option value="5">Elven Ring
					<option value="6">Enchanted Ring
					<option value="9">Mithril Ring
					<option value="18">Ring of Devotion
				</select>

				<select id="RING1N" onChange="calc()">
					<option value="0">Unequipped
					<option value="4">Blue Coral Ring
					<option value="7">Magic Ring
					<option value="12">Ring of Anguish
					<option value="19">Ring of Firefly
					<option value="20">Ring of Knowledge
					<option value="23">Ring of Raccoon
					<option value="24">Ring of Wisdom
				</select>
				</td></tr>
<tr>			<td>Ring</td>

				<td><select id="RING2ENCHANT" onChange="calc()">
					<option value="0">+0<option value="1">+1<option value="2">+2<option value="3">+3<option value="4">+4<option value="5">+5<option value="6">+6<option value="7">+7<option value="8">+8<option value="9">+9<option value="10">+10<option value="11">+11<option value="12">+12<option value="13">+13<option value="14">+14<option value="15">+15<option value="16">+16<option value="17">+17<option value="18">+18<option value="19">+19<option value="20">+20
				</select></td>

				<td><select id="RING2GRADE" onChange="calc()">
					<option value="S">S Grade
					<option value="A">A Grade
					<option value="B">B Grade
					<option value="C">C Grade
					<option value="D">D Grade
					<option value="N">Non Grade
				</select></td>
				<td><select id="RING2S" onChange="calc()">
					<option value="0">Unequipped
					<option value="13">Ring of Baium
					<option value="27">Sealed Tateossian Ring
					<option value="28">Tateossian Ring
				</select>

				<select id="RING2A" onChange="calc()">
					<option value="0">Unequipped
					<option value="8">Majestic Ring
					<option value="10">Phoenix Ring
					<option value="17">Ring of Core
					<option value="25">Sealed Majestic Ring
					<option value="26">Sealed Phoenix Ring
				</select>
				<select id="RING2B" onChange="calc()">
					<option value="0">Unequipped
					<option value="1">Adamantite Ring
					<option value="15">Ring of Black Ore
					<option value="16">Ring of Blessing
					<option value="22">Ring of Queen Ant
				</select>

				<select id="RING2C" onChange="calc()">
					<option value="0">Unequipped
					<option value="2">Aquastone Ring
					<option value="11">Ring of Ages
					<option value="14">Ring of Binding
					<option value="21">Ring of Protection
				</select>
				<select id="RING2D" onChange="calc()">
					<option value="0">Unequipped
					<option value="3">Black Pearl Ring
					<option value="5">Elven Ring
					<option value="6">Enchanted Ring
					<option value="9">Mithril Ring
					<option value="18">Ring of Devotion
				</select>

				<select id="RING2N" onChange="calc()">
					<option value="0">Unequipped
					<option value="4">Blue Coral Ring
					<option value="7">Magic Ring
					<option value="12">Ring of Anguish
					<option value="19">Ring of Firefly
					<option value="20">Ring of Knowledge
					<option value="23">Ring of Raccoon
					<option value="24">Ring of Wisdom
				</select>
				</td></tr>
		</form>
	</table>

</fieldset>

<fieldset id="PASSIVES">
<legend>Passives</legend>
<table class="statcalc"><tr>
<tr>		<td>Weapon Mastery</td>
			<td><input type=checkbox id="WMFCHECK" onClick="calc()">
			<input type=checkbox id="WMMCHECK" onClick="calc()"></td>
			<td>
			<select id="WMF" onChange="calc()">

<option value="1">Weapon Mastery 1 (5)
<option value="2">Weapon Mastery 2 (10)
<option value="3">Weapon Mastery 3 (15)
</select>
			<select id="WMM" onChange="calc()">
<option value="1" id="Weapon_Mastery1">Weapon Mastery 1 (7)
<option value="2" id="Weapon_Mastery2">Weapon Mastery 2 (14)
<option value="3" id="Weapon_Mastery3">Weapon Mastery 3 (20)
<option value="4" id="Weapon_Mastery4">Weapon Mastery 4 (25)
<option value="5" id="Weapon_Mastery5">Weapon Mastery 5 (25)
<option value="6" id="Weapon_Mastery6">Weapon Mastery 6 (30)
<option value="7" id="Weapon_Mastery7">Weapon Mastery 7 (30)
<option value="8" id="Weapon_Mastery8">Weapon Mastery 8 (35)
<option value="9" id="Weapon_Mastery9">Weapon Mastery 9 (35)
<option value="10" id="Weapon_Mastery10">Weapon Mastery 10 (40)
<option value="11" id="Weapon_Mastery11">Weapon Mastery 11 (40)

<option value="12" id="Weapon_Mastery12">Weapon Mastery 12 (40)
<option value="13" id="Weapon_Mastery13">Weapon Mastery 13 (44)
<option value="14" id="Weapon_Mastery14">Weapon Mastery 14 (44)
<option value="15" id="Weapon_Mastery15">Weapon Mastery 15 (44)
<option value="16" id="Weapon_Mastery16">Weapon Mastery 16 (48)
<option value="17" id="Weapon_Mastery17">Weapon Mastery 17 (48)
<option value="18" id="Weapon_Mastery18">Weapon Mastery 18 (48)
<option value="19" id="Weapon_Mastery19">Weapon Mastery 19 (52)
<option value="20" id="Weapon_Mastery20">Weapon Mastery 20 (52)
<option value="21" id="Weapon_Mastery21">Weapon Mastery 21 (52)
<option value="22" id="Weapon_Mastery22">Weapon Mastery 22 (56)
<option value="23" id="Weapon_Mastery23">Weapon Mastery 23 (56)
<option value="24" id="Weapon_Mastery24">Weapon Mastery 24 (56)
<option value="25" id="Weapon_Mastery25">Weapon Mastery 25 (58)
<option value="26" id="Weapon_Mastery26">Weapon Mastery 26 (58)
<option value="27" id="Weapon_Mastery27">Weapon Mastery 27 (60)
<option value="28" id="Weapon_Mastery28">Weapon Mastery 28 (60)

<option value="29" id="Weapon_Mastery29">Weapon Mastery 29 (62)
<option value="30" id="Weapon_Mastery30">Weapon Mastery 30 (62)
<option value="31" id="Weapon_Mastery31">Weapon Mastery 31 (64)
<option value="32" id="Weapon_Mastery32">Weapon Mastery 32 (64)
<option value="33" id="Weapon_Mastery33">Weapon Mastery 33 (66)
<option value="34" id="Weapon_Mastery34">Weapon Mastery 34 (66)
<option value="35" id="Weapon_Mastery35">Weapon Mastery 35 (68)
<option value="36" id="Weapon_Mastery36">Weapon Mastery 36 (68)
<option value="37" id="Weapon_Mastery37">Weapon Mastery 37 (70)
<option value="38" id="Weapon_Mastery38">Weapon Mastery 38 (70)
<option value="39" id="Weapon_Mastery39">Weapon Mastery 39 (72)
<option value="40" id="Weapon_Mastery40">Weapon Mastery 40 (72)
<option value="41" id="Weapon_Mastery41">Weapon Mastery 41 (74)
<option value="42" id="Weapon_Mastery42">Weapon Mastery 42 (74)
</select></td>
			<td>

			<input type=checkbox id="WSBMCHECK" onClick="calc()">
			<input type=checkbox id="BLUNTMCHECK" onClick="calc()">
			<input type=checkbox id="FISTMCHECK" onClick="calc()">
			<input type=checkbox id="KSBMCHECK" onClick="calc()">
			<input type=checkbox id="DAGGERMCHECK" onClick="calc()"></td>
			<td>
			<select id="WSBM" onChange="calc()">
<option value="1" id="WSwordBlunt1">Sword/Blunt Mastery 1 (20)
<option value="2" id="WSwordBlunt2">Sword/Blunt Mastery 2 (24)
<option value="3" id="WSwordBlunt3">Sword/Blunt Mastery 3 (28)

<option value="4" id="WSwordBlunt4">Sword/Blunt Mastery 4 (28)
<option value="5" id="WSwordBlunt5">Sword/Blunt Mastery 5 (32)
<option value="6" id="WSwordBlunt6">Sword/Blunt Mastery 6 (32)
<option value="7" id="WSwordBlunt7">Sword/Blunt Mastery 7 (36)
<option value="8" id="WSwordBlunt8">Sword/Blunt Mastery 8 (36)
<option value="9" id="WSwordBlunt9">Sword/Blunt Mastery 9 (40)
<option value="10" id="WSwordBlunt10">Sword/Blunt Mastery 10 (40)
<option value="11" id="WSwordBlunt11">Sword/Blunt Mastery 11 (40)
<option value="12" id="WSwordBlunt12">Sword/Blunt Mastery 12 (43)
<option value="13" id="WSwordBlunt13">Sword/Blunt Mastery 13 (43)
<option value="14" id="WSwordBlunt14">Sword/Blunt Mastery 14 (43)
<option value="15" id="WSwordBlunt15">Sword/Blunt Mastery 15 (46)
<option value="16" id="WSwordBlunt16">Sword/Blunt Mastery 16 (46)
<option value="17" id="WSwordBlunt17">Sword/Blunt Mastery 17 (46)
<option value="18" id="WSwordBlunt18">Sword/Blunt Mastery 18 (49)
<option value="19" id="WSwordBlunt19">Sword/Blunt Mastery 19 (49)
<option value="20" id="WSwordBlunt20">Sword/Blunt Mastery 20 (49)

<option value="21" id="WSwordBlunt21">Sword/Blunt Mastery 21 (52)
<option value="22" id="WSwordBlunt22">Sword/Blunt Mastery 22 (52)
<option value="23" id="WSwordBlunt23">Sword/Blunt Mastery 23 (52)
<option value="24" id="WSwordBlunt24">Sword/Blunt Mastery 24 (55)
<option value="25" id="WSwordBlunt25">Sword/Blunt Mastery 25 (55)
<option value="26" id="WSwordBlunt26">Sword/Blunt Mastery 26 (55)
<option value="27" id="WSwordBlunt27">Sword/Blunt Mastery 27 (58)
<option value="28" id="WSwordBlunt28">Sword/Blunt Mastery 28 (58)
<option value="29" id="WSwordBlunt29">Sword/Blunt Mastery 29 (58)
<option value="30" id="WSwordBlunt30">Sword/Blunt Mastery 30 (60)
<option value="31" id="WSwordBlunt31">Sword/Blunt Mastery 31 (60)
<option value="32" id="WSwordBlunt32">Sword/Blunt Mastery 32 (62)
<option value="33" id="WSwordBlunt33">Sword/Blunt Mastery 33 (62)
<option value="34" id="WSwordBlunt34">Sword/Blunt Mastery 34 (64)
<option value="35" id="WSwordBlunt35">Sword/Blunt Mastery 35 (64)
<option value="36" id="WSwordBlunt36">Sword/Blunt Mastery 36 (66)
<option value="37" id="WSwordBlunt37">Sword/Blunt Mastery 37 (66)

<option value="38" id="WSwordBlunt38">Sword/Blunt Mastery 38 (68)
<option value="39" id="WSwordBlunt39">Sword/Blunt Mastery 39 (68)
<option value="40" id="WSwordBlunt40">Sword/Blunt Mastery 40 (70)
<option value="41" id="WSwordBlunt41">Sword/Blunt Mastery 41 (70)
<option value="42" id="WSwordBlunt42">Sword/Blunt Mastery 42 (72)
<option value="43" id="WSwordBlunt43">Sword/Blunt Mastery 43 (72)
<option value="44" id="WSwordBlunt44">Sword/Blunt Mastery 44 (74)
<option value="45" id="WSwordBlunt45">Sword/Blunt Mastery 45 (74)</select>
			<select id="BLUNTM" onChange="calc()">
<option value="1">Blunt Mastery 1 (20)
<option value="2">Blunt Mastery 2 (24)
<option value="3">Blunt Mastery 3 (28)
<option value="4">Blunt Mastery 4 (28)
<option value="5">Blunt Mastery 5 (32)
<option value="6">Blunt Mastery 6 (32)

<option value="7">Blunt Mastery 7 (36)
<option value="8">Blunt Mastery 8 (36)
<option value="9">Blunt Mastery 9 (40)
<option value="10">Blunt Mastery 10 (40)
<option value="11">Blunt Mastery 11 (40)
<option value="12">Blunt Mastery 12 (43)
<option value="13">Blunt Mastery 13 (43)
<option value="14">Blunt Mastery 14 (43)
<option value="15">Blunt Mastery 15 (46)
<option value="16">Blunt Mastery 16 (46)
<option value="17">Blunt Mastery 17 (46)
<option value="18">Blunt Mastery 18 (49)
<option value="19">Blunt Mastery 19 (49)
<option value="20">Blunt Mastery 20 (49)
<option value="21">Blunt Mastery 21 (52)
<option value="22">Blunt Mastery 22 (52)
<option value="23">Blunt Mastery 23 (52)

<option value="24">Blunt Mastery 24 (55)
<option value="25">Blunt Mastery 25 (55)
<option value="26">Blunt Mastery 26 (55)
<option value="27">Blunt Mastery 27 (58)
<option value="28">Blunt Mastery 28 (58)
<option value="29">Blunt Mastery 29 (58)
<option value="30">Blunt Mastery 30 (60)
<option value="31">Blunt Mastery 31 (60)
<option value="32">Blunt Mastery 32 (62)
<option value="33">Blunt Mastery 33 (62)
<option value="34">Blunt Mastery 34 (64)
<option value="35">Blunt Mastery 35 (64)
<option value="36">Blunt Mastery 36 (66)
<option value="37">Blunt Mastery 37 (66)
<option value="38">Blunt Mastery 38 (68)
<option value="39">Blunt Mastery 39 (68)
<option value="40">Blunt Mastery 40 (70)

<option value="41">Blunt Mastery 41 (70)
<option value="42">Blunt Mastery 42 (72)
<option value="43">Blunt Mastery 43 (72)
<option value="44">Blunt Mastery 44 (74)
<option value="45">Blunt Mastery 45 (74)
<option value="76">Blunt Mastery 45 +1 (Power)
<option value="77">Blunt Mastery 45 +2 (Power)
<option value="78">Blunt Mastery 45 +3 (Power)
<option value="79">Blunt Mastery 45 +4 (Power)
<option value="80">Blunt Mastery 45 +5 (Power)
<option value="81">Blunt Mastery 45 +6 (Power)
<option value="82">Blunt Mastery 45 +7 (Power)
<option value="83">Blunt Mastery 45 +8 (Power)
<option value="84">Blunt Mastery 45 +9 (Power)
<option value="85">Blunt Mastery 45 +10 (Power)
<option value="86">Blunt Mastery 45 +11 (Power)
<option value="87">Blunt Mastery 45 +12 (Power)

<option value="88">Blunt Mastery 45 +13 (Power)
<option value="89">Blunt Mastery 45 +14 (Power)
<option value="90">Blunt Mastery 45 +15 (Power)
<option value="91">Blunt Mastery 45 +16 (Power)
<option value="92">Blunt Mastery 45 +17 (Power)
<option value="93">Blunt Mastery 45 +18 (Power)
<option value="94">Blunt Mastery 45 +19 (Power)
<option value="95">Blunt Mastery 45 +20 (Power)
<option value="96">Blunt Mastery 45 +21 (Power)
<option value="97">Blunt Mastery 45 +22 (Power)
<option value="98">Blunt Mastery 45 +23 (Power)
<option value="99">Blunt Mastery 45 +24 (Power)
<option value="100">Blunt Mastery 45 +25 (Power)
<option value="101">Blunt Mastery 45 +26 (Power)
<option value="102">Blunt Mastery 45 +27 (Power)
<option value="103">Blunt Mastery 45 +28 (Power)
<option value="104">Blunt Mastery 45 +29 (Power)

<option value="105">Blunt Mastery 45 +30 (Power)</select>
			<select id="FISTM" onChange="calc()">
<option value="1">Fist Mastery 1 (20)
<option value="2">Fist Mastery 2 (24)
<option value="3">Fist Mastery 3 (28)
<option value="4">Fist Mastery 4 (28)
<option value="5">Fist Mastery 5 (32)
<option value="6">Fist Mastery 6 (32)
<option value="7">Fist Mastery 7 (36)
<option value="8">Fist Mastery 8 (36)
<option value="9">Fist Mastery 9 (40)
<option value="10">Fist Mastery 10 (40)
<option value="11">Fist Mastery 11 (40)
<option value="12">Fist Mastery 12 (43)
<option value="13">Fist Mastery 13 (43)

<option value="14">Fist Mastery 14 (43)
<option value="15">Fist Mastery 15 (46)
<option value="16">Fist Mastery 16 (46)
<option value="17">Fist Mastery 17 (46)
<option value="18">Fist Mastery 18 (49)
<option value="19">Fist Mastery 19 (49)
<option value="20">Fist Mastery 20 (49)
<option value="21">Fist Mastery 21 (52)
<option value="22">Fist Mastery 22 (52)
<option value="23">Fist Mastery 23 (52)
<option value="24">Fist Mastery 24 (55)
<option value="25">Fist Mastery 25 (55)
<option value="26">Fist Mastery 26 (55)
<option value="27">Fist Mastery 27 (58)
<option value="28">Fist Mastery 28 (58)
<option value="29">Fist Mastery 29 (58)
<option value="30">Fist Mastery 30 (60)

<option value="31">Fist Mastery 31 (60)
<option value="32">Fist Mastery 32 (62)
<option value="33">Fist Mastery 33 (62)
<option value="34">Fist Mastery 34 (64)
<option value="35">Fist Mastery 35 (64)
<option value="36">Fist Mastery 36 (66)
<option value="37">Fist Mastery 37 (66)
<option value="38">Fist Mastery 38 (68)
<option value="39">Fist Mastery 39 (68)
<option value="40">Fist Mastery 40 (70)
<option value="41">Fist Mastery 41 (70)
<option value="42">Fist Mastery 42 (72)
<option value="43">Fist Mastery 43 (72)
<option value="44">Fist Mastery 44 (74)
<option value="45">Fist Mastery 45 (74)
</select>
			<select id="KSBM" onChange="calc()">

<option value="1">Sword/Blunt Mastery 1 (20)
<option value="2">Sword/Blunt Mastery 2 (24)
<option value="3">Sword/Blunt Mastery 3 (28)
<option value="4">Sword/Blunt Mastery 4 (28)
<option value="5">Sword/Blunt Mastery 5 (32)
<option value="6">Sword/Blunt Mastery 6 (32)
<option value="7">Sword/Blunt Mastery 7 (36)
<option value="8">Sword/Blunt Mastery 8 (36)
<option value="9">Sword/Blunt Mastery 9 (40)
<option value="10">Sword/Blunt Mastery 10 (40)
<option value="11">Sword/Blunt Mastery 11 (40)
<option value="12">Sword/Blunt Mastery 12 (43)
<option value="13">Sword/Blunt Mastery 13 (43)
<option value="14">Sword/Blunt Mastery 14 (43)
<option value="15">Sword/Blunt Mastery 15 (46)
<option value="16">Sword/Blunt Mastery 16 (46)
<option value="17">Sword/Blunt Mastery 17 (46)

<option value="18">Sword/Blunt Mastery 18 (49)
<option value="19">Sword/Blunt Mastery 19 (49)
<option value="20">Sword/Blunt Mastery 20 (49)
<option value="21">Sword/Blunt Mastery 21 (52)
<option value="22">Sword/Blunt Mastery 22 (52)
<option value="23">Sword/Blunt Mastery 23 (52)
<option value="24">Sword/Blunt Mastery 24 (55)
<option value="25">Sword/Blunt Mastery 25 (55)
<option value="26">Sword/Blunt Mastery 26 (55)
<option value="27">Sword/Blunt Mastery 27 (58)
<option value="28">Sword/Blunt Mastery 28 (58)
<option value="29">Sword/Blunt Mastery 29 (58)
<option value="30">Sword/Blunt Mastery 30 (60)
<option value="31">Sword/Blunt Mastery 31 (60)
<option value="32">Sword/Blunt Mastery 32 (62)
<option value="33">Sword/Blunt Mastery 33 (62)
<option value="34">Sword/Blunt Mastery 34 (64)

<option value="35">Sword/Blunt Mastery 35 (64)
<option value="36">Sword/Blunt Mastery 36 (66)
<option value="37">Sword/Blunt Mastery 37 (66)
<option value="38">Sword/Blunt Mastery 38 (68)
<option value="39">Sword/Blunt Mastery 39 (68)
<option value="40">Sword/Blunt Mastery 40 (70)
<option value="41">Sword/Blunt Mastery 41 (70)
<option value="42">Sword/Blunt Mastery 42 (72)
<option value="43">Sword/Blunt Mastery 43 (72)
<option value="44">Sword/Blunt Mastery 44 (74)
<option value="45">Sword/Blunt Mastery 45 (74)
</select>
			<select id="DAGGERM" onChange="calc()">
<option value="1">Dagger Mastery 1 (20)
<option value="2">Dagger Mastery 2 (24)
<option value="3">Dagger Mastery 3 (28)

<option value="4">Dagger Mastery 4 (28)
<option value="5">Dagger Mastery 5 (32)
<option value="6">Dagger Mastery 6 (32)
<option value="7">Dagger Mastery 7 (36)
<option value="8">Dagger Mastery 8 (36)
<option value="9">Dagger Mastery 9 (40)
<option value="10">Dagger Mastery 10 (40)
<option value="11">Dagger Mastery 11 (40)
<option value="12">Dagger Mastery 12 (43)
<option value="13">Dagger Mastery 13 (43)
<option value="14">Dagger Mastery 14 (43)
<option value="15">Dagger Mastery 15 (46)
<option value="16">Dagger Mastery 16 (46)
<option value="17">Dagger Mastery 17 (46)
<option value="18">Dagger Mastery 18 (49)
<option value="19">Dagger Mastery 19 (49)
<option value="20">Dagger Mastery 20 (49)

<option value="21">Dagger Mastery 21 (52)
<option value="22">Dagger Mastery 22 (52)
<option value="23">Dagger Mastery 23 (52)
<option value="24">Dagger Mastery 24 (55)
<option value="25">Dagger Mastery 25 (55)
<option value="26">Dagger Mastery 26 (55)
<option value="27">Dagger Mastery 27 (58)
<option value="28">Dagger Mastery 28 (58)
<option value="29">Dagger Mastery 29 (58)
<option value="30">Dagger Mastery 30 (60)
<option value="31">Dagger Mastery 31 (60)
<option value="32">Dagger Mastery 32 (62)
<option value="33">Dagger Mastery 33 (62)
<option value="34">Dagger Mastery 34 (64)
<option value="35">Dagger Mastery 35 (64)
<option value="36">Dagger Mastery 36 (66)
<option value="37">Dagger Mastery 37 (66)

<option value="38">Dagger Mastery 38 (68)
<option value="39">Dagger Mastery 39 (68)
<option value="40">Dagger Mastery 40 (70)
<option value="41">Dagger Mastery 41 (70)
<option value="42">Dagger Mastery 42 (72)
<option value="43">Dagger Mastery 43 (72)
<option value="44">Dagger Mastery 44 (74)
<option value="45">Dagger Mastery 45 (74)
</select></td>
			<td><input type=checkbox id="POLEMCHECK" onClick="calc()">
			<input type=checkbox id="BOWMCHECK" onClick="calc()"></td>
			<td><select id="POLEM" onChange="calc()"><option value="45">Polearm Mastery 45 (74)<option value="44">Polearm Mastery 44 (74)<option value="43">Polearm Mastery 43 (72)<option value="42">Polearm Mastery 42 (72)<option value="41">Polearm Mastery 41 (70)<option value="40">Polearm Mastery 40 (70)<option value="39">Polearm Mastery 39 (68)<option value="38">Polearm Mastery 38 (68)<option value="37">Polearm Mastery 37 (66)<option value="36">Polearm Mastery 36 (66)<option value="35">Polearm Mastery 35 (64)<option value="34">Polearm Mastery 34 (64)<option value="33">Polearm Mastery 33 (62)<option value="32">Polearm Mastery 32 (62)<option value="31">Polearm Mastery 31 (60)<option value="30">Polearm Mastery 30 (60)<option value="29">Polearm Mastery 29 (58)<option value="28">Polearm Mastery 28 (58)<option value="27">Polearm Mastery 27 (58)<option value="26">Polearm Mastery 26 (55)<option value="25">Polearm Mastery 25 (55)<option value="24">Polearm Mastery 24 (55)<option value="23">Polearm Mastery 23 (52)<option value="22">Polearm Mastery 22 (52)<option value="21">Polearm Mastery 21 (52)<option value="20">Polearm Mastery 20 (49)<option value="19">Polearm Mastery 19 (49)<option value="18">Polearm Mastery 18 (49)<option value="17">Polearm Mastery 17 (46)<option value="16">Polearm Mastery 16 (46)<option value="15">Polearm Mastery 15 (46)<option value="14">Polearm Mastery 14 (43)<option value="13">Polearm Mastery 13 (43)<option value="12">Polearm Mastery 12 (43)<option value="11">Polearm Mastery 11 (40)<option value="10">Polearm Mastery 10 (40)<option value="9">Polearm Mastery 9 (40)<option value="8">Polearm Mastery 8 (36)<option value="7">Polearm Mastery 7 (36)<option value="6">Polearm Mastery 6 (32)<option value="5">Polearm Mastery 5 (32)<option value="4">Polearm Mastery 4 (28)<option value="3">Polearm Mastery 3 (28)<option value="2">Polearm Mastery 2 (24)<option value="1">Polearm Mastery 1 (20)</select>

			<select id="BOWM" onChange="calc()"><option value="52">Bow Mastery 52<option value="51">Bow Mastery 51<option value="50">Bow Mastery 50<option value="49">Bow Mastery 49<option value="48">Bow Mastery 48<option value="47">Bow Mastery 47<option value="46">Bow Mastery 46<option value="45">Bow Mastery 45<option value="44">Bow Mastery 44<option value="43">Bow Mastery 43<option value="42">Bow Mastery 42<option value="41">Bow Mastery 41<option value="40">Bow Mastery 40<option value="39">Bow Mastery 39<option value="38">Bow Mastery 38<option value="37">Bow Mastery 37<option value="36">Bow Mastery 36<option value="35">Bow Mastery 35<option value="34">Bow Mastery 34<option value="33">Bow Mastery 33<option value="32">Bow Mastery 32<option value="31">Bow Mastery 31<option value="30">Bow Mastery 30<option value="29">Bow Mastery 29<option value="28">Bow Mastery 28<option value="27">Bow Mastery 27<option value="26">Bow Mastery 26<option value="25">Bow Mastery 25<option value="24">Bow Mastery 24<option value="23">Bow Mastery 23<option value="22">Bow Mastery 22<option value="21">Bow Mastery 21<option value="20">Bow Mastery 20<option value="19">Bow Mastery 19<option value="18">Bow Mastery 18<option value="17">Bow Mastery 17<option value="16">Bow Mastery 16<option value="15">Bow Mastery 15<option value="14">Bow Mastery 14<option value="13">Bow Mastery 13<option value="12">Bow Mastery 12<option value="11">Bow Mastery 11<option value="10">Bow Mastery 10<option value="9">Bow Mastery 9<option value="8">Bow Mastery 8<option value="7">Bow Mastery 7<option value="6">Bow Mastery 6<option value="5">Bow Mastery 5<option value="4">Bow Mastery 4<option value="3">Bow Mastery 3<option value="2">Bow Mastery 2<option value="1">Bow Mastery 1</select></td>

			<td><input type=checkbox id="DUALMCHECK" onClick="calc()">
			<input type=checkbox id="THMCHECK" onClick="calc()"></td>
			<td><select id="DUALM" onChange="calc()"><option value="37">Dual Weapon Mastery 37<option value="36">Dual Weapon Mastery 36<option value="35">Dual Weapon Mastery 35<option value="34">Dual Weapon Mastery 34<option value="33">Dual Weapon Mastery 33<option value="32">Dual Weapon Mastery 32<option value="31">Dual Weapon Mastery 31<option value="30">Dual Weapon Mastery 30<option value="29">Dual Weapon Mastery 29<option value="28">Dual Weapon Mastery 28<option value="27">Dual Weapon Mastery 27<option value="26">Dual Weapon Mastery 26<option value="25">Dual Weapon Mastery 25<option value="24">Dual Weapon Mastery 24<option value="23">Dual Weapon Mastery 23<option value="22">Dual Weapon Mastery 22<option value="21">Dual Weapon Mastery 21<option value="20">Dual Weapon Mastery 20<option value="19">Dual Weapon Mastery 19<option value="18">Dual Weapon Mastery 18<option value="17">Dual Weapon Mastery 17<option value="16">Dual Weapon Mastery 16<option value="15">Dual Weapon Mastery 15<option value="14">Dual Weapon Mastery 14<option value="13">Dual Weapon Mastery 13<option value="12">Dual Weapon Mastery 12<option value="11">Dual Weapon Mastery 11<option value="10">Dual Weapon Mastery 10<option value="9">Dual Weapon Mastery 9<option value="8">Dual Weapon Mastery 8<option value="7">Dual Weapon Mastery 7<option value="6">Dual Weapon Mastery 6<option value="5">Dual Weapon Mastery 5<option value="4">Dual Weapon Mastery 4<option value="3">Dual Weapon Mastery 3<option value="2">Dual Weapon Mastery 2<option value="1">Dual Weapon Mastery 1</select>

			<select id="THM" onChange="calc()"><option value="20">Two Handed Weapon Mastery 20<option value="19">Two Handed Weapon Mastery 19<option value="18">Two Handed Weapon Mastery 18<option value="17">Two Handed Weapon Mastery 17<option value="16">Two Handed Weapon Mastery 16<option value="15">Two Handed Weapon Mastery 15<option value="14">Two Handed Weapon Mastery 14<option value="13">Two Handed Weapon Mastery 13<option value="12">Two Handed Weapon Mastery 12<option value="11">Two Handed Weapon Mastery 11<option value="10">Two Handed Weapon Mastery 10<option value="9">Two Handed Weapon Mastery 9<option value="8">Two Handed Weapon Mastery 8<option value="7">Two Handed Weapon Mastery 7<option value="6">Two Handed Weapon Mastery 6<option value="5">Two Handed Weapon Mastery 5<option value="4">Two Handed Weapon Mastery 4<option value="3">Two Handed Weapon Mastery 3<option value="2">Two Handed Weapon Mastery 2<option value="1">Two Handed Weapon Mastery 1</select></td></tr>

<tr>		<td>Armor Mastery</td>
			<td><input type=checkbox id="AMFCHECK" onClick="calc()">
			<input type=checkbox id="AMMCHECK" onClick="calc()"></td>
			<td><select id="AMF" onChange="calc()"><option value="5">Armor Mastery 5<option value="4">Armor Mastery 4<option value="3">Armor Mastery 3<option value="2">Armor Mastery 2<option value="1">Armor Mastery 1</select>
			<select id="AMM" onChange="calc()"><option value="3">Armor Mastery 3<option value="2">Armor Mastery 2<option value="1">Armor Mastery 1</select></td>

			<td><input type=checkbox id="HMKCHECK" onClick="calc()">
			<input type=checkbox id="HMWCHECK" onClick="calc()">
			<input type=checkbox id="HMOCHECK" onClick="calc()">
			<input type=checkbox id="HMPCHECK" onClick="calc()"></td>
			<td><select id="HMK" onChange="calc()"><option value="52">Heavy Armor Mastery 52<option value="51">Heavy Armor Mastery 51<option value="50">Heavy Armor Mastery 50<option value="49">Heavy Armor Mastery 49<option value="48">Heavy Armor Mastery 48<option value="47">Heavy Armor Mastery 47<option value="46">Heavy Armor Mastery 46<option value="45">Heavy Armor Mastery 45<option value="44">Heavy Armor Mastery 44<option value="43">Heavy Armor Mastery 43<option value="42">Heavy Armor Mastery 42<option value="41">Heavy Armor Mastery 41<option value="40">Heavy Armor Mastery 40<option value="39">Heavy Armor Mastery 39<option value="38">Heavy Armor Mastery 38<option value="37">Heavy Armor Mastery 37<option value="36">Heavy Armor Mastery 36<option value="35">Heavy Armor Mastery 35<option value="34">Heavy Armor Mastery 34<option value="33">Heavy Armor Mastery 33<option value="32">Heavy Armor Mastery 32<option value="31">Heavy Armor Mastery 31<option value="30">Heavy Armor Mastery 30<option value="29">Heavy Armor Mastery 29<option value="28">Heavy Armor Mastery 28<option value="27">Heavy Armor Mastery 27<option value="26">Heavy Armor Mastery 26<option value="25">Heavy Armor Mastery 25<option value="24">Heavy Armor Mastery 24<option value="23">Heavy Armor Mastery 23<option value="22">Heavy Armor Mastery 22<option value="21">Heavy Armor Mastery 21<option value="20">Heavy Armor Mastery 20<option value="19">Heavy Armor Mastery 19<option value="18">Heavy Armor Mastery 18<option value="17">Heavy Armor Mastery 17<option value="16">Heavy Armor Mastery 16<option value="15">Heavy Armor Mastery 15<option value="14">Heavy Armor Mastery 14<option value="13">Heavy Armor Mastery 13<option value="12">Heavy Armor Mastery 12<option value="11">Heavy Armor Mastery 11<option value="10">Heavy Armor Mastery 10<option value="9">Heavy Armor Mastery 9<option value="8">Heavy Armor Mastery 8<option value="7">Heavy Armor Mastery 7<option value="6">Heavy Armor Mastery 6<option value="5">Heavy Armor Mastery 5<option value="4">Heavy Armor Mastery 4<option value="3">Heavy Armor Mastery 3<option value="2">Heavy Armor Mastery 2<option value="1">Heavy Armor Mastery 1</select>

			<select id="HMW" onChange="calc()"><option value="50">Heavy Armor Mastery 50<option value="49">Heavy Armor Mastery 49<option value="48">Heavy Armor Mastery 48<option value="47">Heavy Armor Mastery 47<option value="46">Heavy Armor Mastery 46<option value="45">Heavy Armor Mastery 45<option value="44">Heavy Armor Mastery 44<option value="43">Heavy Armor Mastery 43<option value="42">Heavy Armor Mastery 42<option value="41">Heavy Armor Mastery 41<option value="40">Heavy Armor Mastery 40<option value="39">Heavy Armor Mastery 39<option value="38">Heavy Armor Mastery 38<option value="37">Heavy Armor Mastery 37<option value="36">Heavy Armor Mastery 36<option value="35">Heavy Armor Mastery 35<option value="34">Heavy Armor Mastery 34<option value="33">Heavy Armor Mastery 33<option value="32">Heavy Armor Mastery 32<option value="31">Heavy Armor Mastery 31<option value="30">Heavy Armor Mastery 30<option value="29">Heavy Armor Mastery 29<option value="28">Heavy Armor Mastery 28<option value="27">Heavy Armor Mastery 27<option value="26">Heavy Armor Mastery 26<option value="25">Heavy Armor Mastery 25<option value="24">Heavy Armor Mastery 24<option value="23">Heavy Armor Mastery 23<option value="22">Heavy Armor Mastery 22<option value="21">Heavy Armor Mastery 21<option value="20">Heavy Armor Mastery 20<option value="19">Heavy Armor Mastery 19<option value="18">Heavy Armor Mastery 18<option value="17">Heavy Armor Mastery 17<option value="16">Heavy Armor Mastery 16<option value="15">Heavy Armor Mastery 15<option value="14">Heavy Armor Mastery 14<option value="13">Heavy Armor Mastery 13<option value="12">Heavy Armor Mastery 12<option value="11">Heavy Armor Mastery 11<option value="10">Heavy Armor Mastery 10<option value="9">Heavy Armor Mastery 9<option value="8">Heavy Armor Mastery 8<option value="7">Heavy Armor Mastery 7<option value="6">Heavy Armor Mastery 6<option value="5">Heavy Armor Mastery 5<option value="4">Heavy Armor Mastery 4<option value="3">Heavy Armor Mastery 3<option value="2">Heavy Armor Mastery 2<option value="1">Heavy Armor Mastery 1</select>

			<select id="HMO" onChange="calc()"><option value="43">Heavy Armor Mastery 43<option value="42">Heavy Armor Mastery 42<option value="41">Heavy Armor Mastery 41<option value="40">Heavy Armor Mastery 40<option value="39">Heavy Armor Mastery 39<option value="38">Heavy Armor Mastery 38<option value="37">Heavy Armor Mastery 37<option value="36">Heavy Armor Mastery 36<option value="35">Heavy Armor Mastery 35<option value="34">Heavy Armor Mastery 34<option value="33">Heavy Armor Mastery 33<option value="32">Heavy Armor Mastery 32<option value="31">Heavy Armor Mastery 31<option value="30">Heavy Armor Mastery 30<option value="29">Heavy Armor Mastery 29<option value="28">Heavy Armor Mastery 28<option value="27">Heavy Armor Mastery 27<option value="26">Heavy Armor Mastery 26<option value="25">Heavy Armor Mastery 25<option value="24">Heavy Armor Mastery 24<option value="23">Heavy Armor Mastery 23<option value="22">Heavy Armor Mastery 22<option value="21">Heavy Armor Mastery 21<option value="20">Heavy Armor Mastery 20<option value="19">Heavy Armor Mastery 19<option value="18">Heavy Armor Mastery 18<option value="17">Heavy Armor Mastery 17<option value="16">Heavy Armor Mastery 16<option value="15">Heavy Armor Mastery 15<option value="14">Heavy Armor Mastery 14<option value="13">Heavy Armor Mastery 13<option value="12">Heavy Armor Mastery 12<option value="11">Heavy Armor Mastery 11<option value="10">Heavy Armor Mastery 10<option value="9">Heavy Armor Mastery 9<option value="8">Heavy Armor Mastery 8<option value="7">Heavy Armor Mastery 7<option value="6">Heavy Armor Mastery 6<option value="5">Heavy Armor Mastery 5<option value="4">Heavy Armor Mastery 4<option value="3">Heavy Armor Mastery 3<option value="2">Heavy Armor Mastery 2<option value="1">Heavy Armor Mastery 1</select>

			<select id="HMP" onChange="calc()"><option value="33">Heavy Armor Mastery 33<option value="32">Heavy Armor Mastery 32<option value="31">Heavy Armor Mastery 31<option value="30">Heavy Armor Mastery 30<option value="29">Heavy Armor Mastery 29<option value="28">Heavy Armor Mastery 28<option value="27">Heavy Armor Mastery 27<option value="26">Heavy Armor Mastery 26<option value="25">Heavy Armor Mastery 25<option value="24">Heavy Armor Mastery 24<option value="23">Heavy Armor Mastery 23<option value="22">Heavy Armor Mastery 22<option value="21">Heavy Armor Mastery 21<option value="20">Heavy Armor Mastery 20<option value="19">Heavy Armor Mastery 19<option value="18">Heavy Armor Mastery 18<option value="17">Heavy Armor Mastery 17<option value="16">Heavy Armor Mastery 16<option value="15">Heavy Armor Mastery 15<option value="14">Heavy Armor Mastery 14<option value="13">Heavy Armor Mastery 13<option value="12">Heavy Armor Mastery 12<option value="11">Heavy Armor Mastery 11<option value="10">Heavy Armor Mastery 10<option value="9">Heavy Armor Mastery 9<option value="8">Heavy Armor Mastery 8<option value="7">Heavy Armor Mastery 7<option value="6">Heavy Armor Mastery 6<option value="5">Heavy Armor Mastery 5<option value="4">Heavy Armor Mastery 4<option value="3">Heavy Armor Mastery 3<option value="2">Heavy Armor Mastery 2<option value="1">Heavy Armor Mastery 1</select></td>

			<td><input type=checkbox id="LMWCHECK" onClick="calc()">
			<input type=checkbox id="LMRCHECK" onClick="calc()">
			<input type=checkbox id="LMOCHECK" onClick="calc()">
			<input type=checkbox id="LMHCHECK" onClick="calc()">
			<input type=checkbox id="LMSCHECK" onClick="calc()"></td>
			<td><select id="LMW" onChange="calc()"><option value="50">Light Armor Mastery 50<option value="49">Light Armor Mastery 49<option value="48">Light Armor Mastery 48<option value="47">Light Armor Mastery 47<option value="46">Light Armor Mastery 46<option value="45">Light Armor Mastery 45<option value="44">Light Armor Mastery 44<option value="43">Light Armor Mastery 43<option value="42">Light Armor Mastery 42<option value="41">Light Armor Mastery 41<option value="40">Light Armor Mastery 40<option value="39">Light Armor Mastery 39<option value="38">Light Armor Mastery 38<option value="37">Light Armor Mastery 37<option value="36">Light Armor Mastery 36<option value="35">Light Armor Mastery 35<option value="34">Light Armor Mastery 34<option value="33">Light Armor Mastery 33<option value="32">Light Armor Mastery 32<option value="31">Light Armor Mastery 31<option value="30">Light Armor Mastery 30<option value="29">Light Armor Mastery 29<option value="28">Light Armor Mastery 28<option value="27">Light Armor Mastery 27<option value="26">Light Armor Mastery 26<option value="25">Light Armor Mastery 25<option value="24">Light Armor Mastery 24<option value="23">Light Armor Mastery 23<option value="22">Light Armor Mastery 22<option value="21">Light Armor Mastery 21<option value="20">Light Armor Mastery 20<option value="19">Light Armor Mastery 19<option value="18">Light Armor Mastery 18<option value="17">Light Armor Mastery 17<option value="16">Light Armor Mastery 16<option value="15">Light Armor Mastery 15<option value="14">Light Armor Mastery 14<option value="13">Light Armor Mastery 13<option value="12">Light Armor Mastery 12<option value="11">Light Armor Mastery 11<option value="10">Light Armor Mastery 10<option value="9">Light Armor Mastery 9<option value="8">Light Armor Mastery 8<option value="7">Light Armor Mastery 7<option value="6">Light Armor Mastery 6<option value="5">Light Armor Mastery 5<option value="4">Light Armor Mastery 4<option value="3">Light Armor Mastery 3<option value="2">Light Armor Mastery 2<option value="1">Light Armor Mastery 1</select>

			<select id="LMR" onChange="calc()"><option value="47">Light Armor Mastery 47<option value="46">Light Armor Mastery 46<option value="45">Light Armor Mastery 45<option value="44">Light Armor Mastery 44<option value="43">Light Armor Mastery 43<option value="42">Light Armor Mastery 42<option value="41">Light Armor Mastery 41<option value="40">Light Armor Mastery 40<option value="39">Light Armor Mastery 39<option value="38">Light Armor Mastery 38<option value="37">Light Armor Mastery 37<option value="36">Light Armor Mastery 36<option value="35">Light Armor Mastery 35<option value="34">Light Armor Mastery 34<option value="33">Light Armor Mastery 33<option value="32">Light Armor Mastery 32<option value="31">Light Armor Mastery 31<option value="30">Light Armor Mastery 30<option value="29">Light Armor Mastery 29<option value="28">Light Armor Mastery 28<option value="27">Light Armor Mastery 27<option value="26">Light Armor Mastery 26<option value="25">Light Armor Mastery 25<option value="24">Light Armor Mastery 24<option value="23">Light Armor Mastery 23<option value="22">Light Armor Mastery 22<option value="21">Light Armor Mastery 21<option value="20">Light Armor Mastery 20<option value="19">Light Armor Mastery 19<option value="18">Light Armor Mastery 18<option value="17">Light Armor Mastery 17<option value="16">Light Armor Mastery 16<option value="15">Light Armor Mastery 15<option value="14">Light Armor Mastery 14<option value="13">Light Armor Mastery 13<option value="12">Light Armor Mastery 12<option value="11">Light Armor Mastery 11<option value="10">Light Armor Mastery 10<option value="9">Light Armor Mastery 9<option value="8">Light Armor Mastery 8<option value="7">Light Armor Mastery 7<option value="6">Light Armor Mastery 6<option value="5">Light Armor Mastery 5<option value="4">Light Armor Mastery 4<option value="3">Light Armor Mastery 3<option value="2">Light Armor Mastery 2<option value="1">Light Armor Mastery 1</select>

			<select id="LMO" onChange="calc()"><option value="45">Light Armor Mastery 45<option value="44">Light Armor Mastery 44<option value="43">Light Armor Mastery 43<option value="42">Light Armor Mastery 42<option value="41">Light Armor Mastery 41<option value="40">Light Armor Mastery 40<option value="39">Light Armor Mastery 39<option value="38">Light Armor Mastery 38<option value="37">Light Armor Mastery 37<option value="36">Light Armor Mastery 36<option value="35">Light Armor Mastery 35<option value="34">Light Armor Mastery 34<option value="33">Light Armor Mastery 33<option value="32">Light Armor Mastery 32<option value="31">Light Armor Mastery 31<option value="30">Light Armor Mastery 30<option value="29">Light Armor Mastery 29<option value="28">Light Armor Mastery 28<option value="27">Light Armor Mastery 27<option value="26">Light Armor Mastery 26<option value="25">Light Armor Mastery 25<option value="24">Light Armor Mastery 24<option value="23">Light Armor Mastery 23<option value="22">Light Armor Mastery 22<option value="21">Light Armor Mastery 21<option value="20">Light Armor Mastery 20<option value="19">Light Armor Mastery 19<option value="18">Light Armor Mastery 18<option value="17">Light Armor Mastery 17<option value="16">Light Armor Mastery 16<option value="15">Light Armor Mastery 15<option value="14">Light Armor Mastery 14<option value="13">Light Armor Mastery 13<option value="12">Light Armor Mastery 12<option value="11">Light Armor Mastery 11<option value="10">Light Armor Mastery 10<option value="9">Light Armor Mastery 9<option value="8">Light Armor Mastery 8<option value="7">Light Armor Mastery 7<option value="6">Light Armor Mastery 6<option value="5">Light Armor Mastery 5<option value="4">Light Armor Mastery 4<option value="3">Light Armor Mastery 3<option value="2">Light Armor Mastery 2<option value="1">Light Armor Mastery 1</select>

			<select id="LMH" onChange="calc()"><option value="41">Light Armor Mastery 41<option value="40">Light Armor Mastery 40<option value="39">Light Armor Mastery 39<option value="38">Light Armor Mastery 38<option value="37">Light Armor Mastery 37<option value="36">Light Armor Mastery 36<option value="35">Light Armor Mastery 35<option value="34">Light Armor Mastery 34<option value="33">Light Armor Mastery 33<option value="32">Light Armor Mastery 32<option value="31">Light Armor Mastery 31<option value="30">Light Armor Mastery 30<option value="29">Light Armor Mastery 29<option value="28">Light Armor Mastery 28<option value="27">Light Armor Mastery 27<option value="26">Light Armor Mastery 26<option value="25">Light Armor Mastery 25<option value="24">Light Armor Mastery 24<option value="23">Light Armor Mastery 23<option value="22">Light Armor Mastery 22<option value="21">Light Armor Mastery 21<option value="20">Light Armor Mastery 20<option value="19">Light Armor Mastery 19<option value="18">Light Armor Mastery 18<option value="17">Light Armor Mastery 17<option value="16">Light Armor Mastery 16<option value="15">Light Armor Mastery 15<option value="14">Light Armor Mastery 14<option value="13">Light Armor Mastery 13<option value="12">Light Armor Mastery 12<option value="11">Light Armor Mastery 11<option value="10">Light Armor Mastery 10<option value="9">Light Armor Mastery 9<option value="8">Light Armor Mastery 8<option value="7">Light Armor Mastery 7<option value="6">Light Armor Mastery 6<option value="5">Light Armor Mastery 5<option value="4">Light Armor Mastery 4<option value="3">Light Armor Mastery 3<option value="2">Light Armor Mastery 2<option value="1">Light Armor Mastery 1</select>

			<select id="LMS" onChange="calc()"><option value="33">Light Armor Mastery 33<option value="32">Light Armor Mastery 32<option value="31">Light Armor Mastery 31<option value="30">Light Armor Mastery 30<option value="29">Light Armor Mastery 29<option value="28">Light Armor Mastery 28<option value="27">Light Armor Mastery 27<option value="26">Light Armor Mastery 26<option value="25">Light Armor Mastery 25<option value="24">Light Armor Mastery 24<option value="23">Light Armor Mastery 23<option value="22">Light Armor Mastery 22<option value="21">Light Armor Mastery 21<option value="20">Light Armor Mastery 20<option value="19">Light Armor Mastery 19<option value="18">Light Armor Mastery 18<option value="17">Light Armor Mastery 17<option value="16">Light Armor Mastery 16<option value="15">Light Armor Mastery 15<option value="14">Light Armor Mastery 14<option value="13">Light Armor Mastery 13<option value="12">Light Armor Mastery 12<option value="11">Light Armor Mastery 11<option value="10">Light Armor Mastery 10<option value="9">Light Armor Mastery 9<option value="8">Light Armor Mastery 8<option value="7">Light Armor Mastery 7<option value="6">Light Armor Mastery 6<option value="5">Light Armor Mastery 5<option value="4">Light Armor Mastery 4<option value="3">Light Armor Mastery 3<option value="2">Light Armor Mastery 2<option value="1">Light Armor Mastery 1</select></td>

			<td><input type=checkbox id="RMNCHECK" onClick="calc()">
			<input type=checkbox id="RMHCHECK" onClick="calc()">
			<input type=checkbox id="RMOCHECK" onClick="calc()"></td>
			<td><select id="RMN" onChange="calc()"><option value="41">Robe Mastery 41<option value="40">Robe Mastery 40<option value="39">Robe Mastery 39<option value="38">Robe Mastery 38<option value="37">Robe Mastery 37<option value="36">Robe Mastery 36<option value="35">Robe Mastery 35<option value="34">Robe Mastery 34<option value="33">Robe Mastery 33<option value="32">Robe Mastery 32<option value="31">Robe Mastery 31<option value="30">Robe Mastery 30<option value="29">Robe Mastery 29<option value="28">Robe Mastery 28<option value="27">Robe Mastery 27<option value="26">Robe Mastery 26<option value="25">Robe Mastery 25<option value="24">Robe Mastery 24<option value="23">Robe Mastery 23<option value="22">Robe Mastery 22<option value="21">Robe Mastery 21<option value="20">Robe Mastery 20<option value="19">Robe Mastery 19<option value="18">Robe Mastery 18<option value="17">Robe Mastery 17<option value="16">Robe Mastery 16<option value="15">Robe Mastery 15<option value="14">Robe Mastery 14<option value="13">Robe Mastery 13<option value="12">Robe Mastery 12<option value="11">Robe Mastery 11<option value="10">Robe Mastery 10<option value="9">Robe Mastery 9<option value="8">Robe Mastery 8<option value="7">Robe Mastery 7<option value="6">Robe Mastery 6<option value="5">Robe Mastery 5<option value="4">Robe Mastery 4<option value="3">Robe Mastery 3<option value="2">Robe Mastery 2<option value="1">Robe Mastery 1</select>

			<select id="RMH" onChange="calc()"><option value="41">Robe Mastery 41<option value="40">Robe Mastery 40<option value="39">Robe Mastery 39<option value="38">Robe Mastery 38<option value="37">Robe Mastery 37<option value="36">Robe Mastery 36<option value="35">Robe Mastery 35<option value="34">Robe Mastery 34<option value="33">Robe Mastery 33<option value="32">Robe Mastery 32<option value="31">Robe Mastery 31<option value="30">Robe Mastery 30<option value="29">Robe Mastery 29<option value="28">Robe Mastery 28<option value="27">Robe Mastery 27<option value="26">Robe Mastery 26<option value="25">Robe Mastery 25<option value="24">Robe Mastery 24<option value="23">Robe Mastery 23<option value="22">Robe Mastery 22<option value="21">Robe Mastery 21<option value="20">Robe Mastery 20<option value="19">Robe Mastery 19<option value="18">Robe Mastery 18<option value="17">Robe Mastery 17<option value="16">Robe Mastery 16<option value="15">Robe Mastery 15<option value="14">Robe Mastery 14<option value="13">Robe Mastery 13<option value="12">Robe Mastery 12<option value="11">Robe Mastery 11<option value="10">Robe Mastery 10<option value="9">Robe Mastery 9<option value="8">Robe Mastery 8<option value="7">Robe Mastery 7<option value="6">Robe Mastery 6<option value="5">Robe Mastery 5<option value="4">Robe Mastery 4<option value="3">Robe Mastery 3<option value="2">Robe Mastery 2<option value="1">Robe Mastery 1</select>

			<select id="RMO" onChange="calc()"><option value="45">Robe Mastery 45<option value="44">Robe Mastery 44<option value="43">Robe Mastery 43<option value="42">Robe Mastery 42<option value="41">Robe Mastery 41<option value="40">Robe Mastery 40<option value="39">Robe Mastery 39<option value="38">Robe Mastery 38<option value="37">Robe Mastery 37<option value="36">Robe Mastery 36<option value="35">Robe Mastery 35<option value="34">Robe Mastery 34<option value="33">Robe Mastery 33<option value="32">Robe Mastery 32<option value="31">Robe Mastery 31<option value="30">Robe Mastery 30<option value="29">Robe Mastery 29<option value="28">Robe Mastery 28<option value="27">Robe Mastery 27<option value="26">Robe Mastery 26<option value="25">Robe Mastery 25<option value="24">Robe Mastery 24<option value="23">Robe Mastery 23<option value="22">Robe Mastery 22<option value="21">Robe Mastery 21<option value="20">Robe Mastery 20<option value="19">Robe Mastery 19<option value="18">Robe Mastery 18<option value="17">Robe Mastery 17<option value="16">Robe Mastery 16<option value="15">Robe Mastery 15<option value="14">Robe Mastery 14<option value="13">Robe Mastery 13<option value="12">Robe Mastery 12<option value="11">Robe Mastery 11<option value="10">Robe Mastery 10<option value="9">Robe Mastery 9<option value="8">Robe Mastery 8<option value="7">Robe Mastery 7<option value="6">Robe Mastery 6<option value="5">Robe Mastery 5<option value="4">Robe Mastery 4<option value="3">Robe Mastery 3<option value="2">Robe Mastery 2<option value="1">Robe Mastery 1</select></td>

</tr>
<tr>		<td>M.Def.</td>
			<td><input type=checkbox id="MRCHECK" onClick="calc()">
			<input type=checkbox id="AMCHECK" onClick="calc()"></td>
			<td><select id="MR" onChange="calc()"><option value="1">Magic Resistance 1<option value="2">Magic Resistance 2<option value="3">Magic Resistance 3<option value="4">Magic Resistance 4<option value="5">Magic Resistance 5<option value="6">Magic Resistance 6<option value="7">Magic Resistance 7<option value="8">Magic Resistance 8<option value="9">Magic Resistance 9<option value="10">Magic Resistance 10<option value="11">Magic Resistance 11<option value="12">Magic Resistance 12<option value="13">Magic Resistance 13<option value="14">Magic Resistance 14<option value="15">Magic Resistance 15<option value="16">Magic Resistance 16<option value="17">Magic Resistance 17<option value="18">Magic Resistance 18<option value="19">Magic Resistance 19<option value="20">Magic Resistance 20<option value="21">Magic Resistance 21<option value="22">Magic Resistance 22<option value="23">Magic Resistance 23<option value="24">Magic Resistance 24<option value="25">Magic Resistance 25<option value="26">Magic Resistance 26<option value="27">Magic Resistance 27<option value="28">Magic Resistance 28<option value="29">Magic Resistance 29<option value="30">Magic Resistance 30<option value="31">Magic Resistance 31<option value="32">Magic Resistance 32<option value="33">Magic Resistance 33<option value="34">Magic Resistance 34<option value="35">Magic Resistance 35<option value="36">Magic Resistance 36<option value="37">Magic Resistance 37<option value="38">Magic Resistance 38<option value="39">Magic Resistance 39<option value="40">Magic Resistance 40<option value="41">Magic Resistance 41<option value="42">Magic Resistance 42<option value="43">Magic Resistance 43<option value="44">Magic Resistance 44<option value="45">Magic Resistance 45<option value="46">Magic Resistance 46<option value="47">Magic Resistance 47<option value="48">Magic Resistance 48<option value="49">Magic Resistance 49<option value="50">Magic Resistance 50<option value="51">Magic Resistance 51</select>

			<select id="AM" onChange="calc()"><option value="1">Anti-Magic 1<option value="2">Anti-Magic 2<option value="3">Anti-Magic 3<option value="4">Anti-Magic 4<option value="5">Anti-Magic 5<option value="6">Anti-Magic 6<option value="7">Anti-Magic 7<option value="8">Anti-Magic 8<option value="9">Anti-Magic 9<option value="10">Anti-Magic 10<option value="11">Anti-Magic 11<option value="12">Anti-Magic 12<option value="13">Anti-Magic 13<option value="14">Anti-Magic 14<option value="15">Anti-Magic 15<option value="16">Anti-Magic 16<option value="17">Anti-Magic 17<option value="18">Anti-Magic 18<option value="19">Anti-Magic 19<option value="20">Anti-Magic 20<option value="21">Anti-Magic 21<option value="22">Anti-Magic 22<option value="23">Anti-Magic 23<option value="24">Anti-Magic 24<option value="25">Anti-Magic 25<option value="26">Anti-Magic 26<option value="27">Anti-Magic 27<option value="28">Anti-Magic 28<option value="29">Anti-Magic 29<option value="30">Anti-Magic 30<option value="31">Anti-Magic 31<option value="32">Anti-Magic 32<option value="33">Anti-Magic 33<option value="34">Anti-Magic 34<option value="35">Anti-Magic 35<option value="36">Anti-Magic 36<option value="37">Anti-Magic 37<option value="38">Anti-Magic 38<option value="39">Anti-Magic 39<option value="40">Anti-Magic 40<option value="41">Anti-Magic 41<option value="42">Anti-Magic 42<option value="43">Anti-Magic 43<option value="44">Anti-Magic 44<option value="45">Anti-Magic 45</select>

			</td></tr>
<tr>		<td>Misc.</td>
			<td><input type=checkbox id="BOOSTHPCHECK" onClick="calc()">
			<input type=checkbox id="QUICKSTEPCHECK" onClick="calc()">
			<input type=checkbox id="FINALFORTRESSCHECK" onClick="calc()"></td>

			<td><select id="BOOSTHP" onChange="calc()">
<option value="1">Boost HP 1
<option value="2">Boost HP 2
<option value="3">Boost HP 3

<option value="4">Boost HP 4
<option value="5">Boost HP 5
<option value="6">Boost HP 6
<option value="7">Boost HP 7
<option value="8">Boost HP 8
<option value="9">Boost HP 9
<option value="10">Boost HP 10
<option value="11">Boost HP 10 +1 (HP)
<option value="41">Boost HP 10 +1 (CP)
<option value="12">Boost HP 10 +2 (HP)
<option value="42">Boost HP 10 +2 (CP)
<option value="13">Boost HP 10 +3 (HP)
<option value="43">Boost HP 10 +3 (CP)
<option value="14">Boost HP 10 +4 (HP)
<option value="44">Boost HP 10 +4 (CP)
<option value="15">Boost HP 10 +5 (HP)
<option value="45">Boost HP 10 +5 (CP)

<option value="16">Boost HP 10 +6 (HP)
<option value="46">Boost HP 10 +6 (CP)
<option value="17">Boost HP 10 +7 (HP)
<option value="47">Boost HP 10 +7 (CP)
<option value="18">Boost HP 10 +8 (HP)
<option value="48">Boost HP 10 +8 (CP)
<option value="19">Boost HP 10 +9 (HP)
<option value="49">Boost HP 10 +9 (CP)
<option value="20">Boost HP 10 +10 (HP)
<option value="50">Boost HP 10 +10 (CP)
<option value="21">Boost HP 10 +11 (HP)
<option value="51">Boost HP 10 +11 (CP)
<option value="22">Boost HP 10 +12 (HP)
<option value="52">Boost HP 10 +12 (CP)
<option value="23">Boost HP 10 +13 (HP)
<option value="53">Boost HP 10 +13 (CP)
<option value="24">Boost HP 10 +14 (HP)

<option value="54">Boost HP 10 +14 (CP)
<option value="25">Boost HP 10 +15 (HP)
<option value="55">Boost HP 10 +15 (CP)
<option value="26">Boost HP 10 +16 (HP)
<option value="56">Boost HP 10 +16 (CP)
<option value="27">Boost HP 10 +17 (HP)
<option value="57">Boost HP 10 +17 (CP)
<option value="28">Boost HP 10 +18 (HP)
<option value="58">Boost HP 10 +18 (CP)
<option value="29">Boost HP 10 +19 (HP)
<option value="59">Boost HP 10 +19 (CP)
<option value="30">Boost HP 10 +20 (HP)
<option value="60">Boost HP 10 +20 (CP)
<option value="31">Boost HP 10 +21 (HP)
<option value="61">Boost HP 10 +21 (CP)
<option value="32">Boost HP 10 +22 (HP)
<option value="62">Boost HP 10 +22 (CP)

<option value="33">Boost HP 10 +23 (HP)
<option value="63">Boost HP 10 +23 (CP)
<option value="34">Boost HP 10 +24 (HP)
<option value="64">Boost HP 10 +24 (CP)
<option value="35">Boost HP 10 +25 (HP)
<option value="65">Boost HP 10 +25 (CP)
<option value="36">Boost HP 10 +26 (HP)
<option value="66">Boost HP 10 +26 (CP)
<option value="37">Boost HP 10 +27 (HP)
<option value="67">Boost HP 10 +27 (CP)
<option value="38">Boost HP 10 +28 (HP)
<option value="68">Boost HP 10 +28 (CP)
<option value="39">Boost HP 10 +29 (HP)
<option value="69">Boost HP 10 +29 (CP)
<option value="40">Boost HP 10 +30 (HP)
<option value="70">Boost HP 10 +30 (CP)
</select>

			<select id="QUICKSTEP" onChange="calc()"><option value="1">Quick Step 1<option value="2">Quick Step 2</select>
			<select id="FINALFORTRESS" onChange="calc()"><option value="1">Final Fortress 1<option value="2">Final Fortress 2<option value="3">Final Fortress 3<option value="4">Final Fortress 4<option value="5">Final Fortress 5<option value="6">Final Fortress 6<option value="7">Final Fortress 7<option value="8">Final Fortress 8<option value="9">Final Fortress 9<option value="10">Final Fortress 10<option value="11">Final Fortress 11</select></td>

			<td><input type=checkbox id="BOOSTMPCHECK" onClick="calc()">
			<input type=checkbox id="BOOSTEVASIONCHECK" onClick="calc()">
			<input type=checkbox id="FINALFRENZYCHECK" onClick="calc()"></td>

			<td><select id="BOOSTMP" onChange="calc()"><option value="1">Boost MP 1<option value="2">Boost MP 2<option value="3">Boost MP 3<option value="4">Boost MP 4<option value="5">Boost MP 5<option value="6">Boost MP 6<option value="7">Boost MP 7<option value="8">Boost MP 8</select>

			<select id="BOOSTEVASION" onChange="calc()"><option value="1">Boost Evasion 1<option value="2">Boost Evasion 2<option value="3">Boost Evasion 3</select>
			<select id="FINALFRENZY" onChange="calc()"><option value="1">Final Frenzy 1<option value="2">Final Frenzy 2<option value="3">Final Frenzy 3<option value="4">Final Frenzy 4<option value="5">Final Frenzy 5<option value="6">Final Frenzy 6<option value="7">Final Frenzy 7<option value="8">Final Frenzy 8<option value="9">Final Frenzy 9<option value="10">Final Frenzy 10<option value="11">Final Frenzy 11<option value="12">Final Frenzy 12<option value="13">Final Frenzy 13<option value="14">Final Frenzy 14</select></td>

			<td><input type=checkbox id="SHADOWSENSECHECK" onClick="calc()">
			<input type=checkbox id="AGILEMOVEMENTCHECK" onClick="calc()">
			<input type=checkbox id="CRITICALCHANCECHECK" onClick="calc()">
			<input type=checkbox id="FASTCASTCHECK" onClick="calc()"></td>

			<td><select id="SHADOWSENSE" onChange="calc()"><option value="1">Shadow Sense</select>
			<select id="AGILEMOVEMENT" onChange="calc()"><option value="1">Agile Movement 1<option value="2">Agile Movement 2</select>

			<select id="CRITICALCHANCE" onChange="calc()"><option value="1">Critical Chance 1<option value="2">Critical Chance 2<option value="3">Critical Chance 3</select>
			<select id="FASTCAST" onChange="calc()"><option value="1">Fast Spell Casting 1<option value="2">Fast Spell Casting 2<option value="3">Fast Spell Casting 3</select></td>

			<td><input type=checkbox id="BOOSTASPDCHECK" onChange="calc()"></td>
			<td><select id="BOOSTASPD" onChange="calc()"><option value="1">Boost Attack Speed 1<option value="2">Boost Attack Speed 2<option value="3">Boost Attack Speed 3</select>

			</td></tr>
<tr>		<td>Clan Skills</td>
			<td><input type=checkbox id="CLANHPCHECK" onClick="calc()"></td>
			<td><select id="CLANHP" onChange="calc()"><option value="1">Clan Vitality 1 (HP)<option value="2">Clan Vitality 2 (HP)</select></td>
			<td><input type=checkbox id="CLANCPCHECK" onClick="calc()"></td>
			<td><select id="CLANCP" onChange="calc()"><option value="1">Clan Spirituality 1 (CP)</select></td>
</tr>

<tr>		<td></td>
			<td><input type=checkbox id="CLANPATKCHECK" onClick="calc()"></td>
			<td><select id="CLANPATK" onChange="calc()"><option value="1">Clan Might 1 (PATK)</select></td>
			<td><input type=checkbox id="CLANPDEFCHECK" onClick="calc()"></td>
			<td><select id="CLANPDEF" onChange="calc()"><option value="1">Clan Aegis 1 (PDEF)<option value="2">Clan Aegis 2 (PDEF)</select></td>
			<td><input type=checkbox id="CLANMDEFCHECK" onClick="calc()"></td>
			<td><select id="CLANMDEF" onChange="calc()"><option value="1">Clan Mag. Prot. 1 (MDEF)<option value="2">Clan Mag. Prot. 2 (MDEF)</select></td>

</tr>
</table>
</fieldset>

<fieldset id="BUFFS">
<legend>Buffs</legend>
<table class="statcalc">
<tr>			<td colspan=2 align=right>Buffs Quickselect:</td>
				<td colspan=5><select id="SETBUFFS" onchange=calc(false,false,false,false,true,false)>
					<option>==============
					<option value="clear">clear all buffs
					<option value="78_PP">Lvl 78 Prophet
					<option value="74_PP">Lvl 74 Prophet
					<option value="72_PP">Lvl 72 Prophet
					<option value="66_PP">Lvl 66 Prophet
					<option value="58_PP">Lvl 58 Prophet
					<option value="56_PP">Lvl 56 Prophet
					<option value="52_PP">Lvl 52 Prophet
					<option value="48_PP">Lvl 48 Prophet
					<option value="44_PP">Lvl 44 Prophet
					<option value="40_PP">Lvl 40 Prophet
					<option value="56_SE">Lvl 56 Shillien Elder
					<option value="52_SE">Lvl 52 Shillien Elder
					<option value="48_SE">Lvl 48 Shillien Elder
					<option value="44_SE">Lvl 44 Shillien Elder
					<option value="40_SE">Lvl 40 Shillien Elder
					<option value="clear">clear all buffs
					<option value="78_WC">Lvl 78 Warcryer
					<option value="74_WC">Lvl 74 Warcryer
					<option value="68_WC">Lvl 68 Warcryer
					<option value="66_WC">Lvl 66 Warcryer
					<option value="64_WC">Lvl 64 Warcryer
					<option value="58_WC">Lvl 58 Warcryer
					<option value="56_WC">Lvl 56 Warcryer
					<option value="52_WC">Lvl 52 Warcryer
					<option value="48_WC">Lvl 48 Warcryer
					<option value="44_WC">Lvl 44 Warcryer
					<option value="40_WC">Lvl 40 Warcryer
					<option value="77_OL">Lvl 77 Overlord
					<option value="64_OL">Lvl 64 Overlord
					<option value="58_OL">Lvl 58 Overlord
					<option value="56_OL">Lvl 56 Overlord
					<option value="52_OL">Lvl 52 Overlord
					<option value="48_OL">Lvl 48 Overlord
					<option value="44_OL">Lvl 44 Overlord
					<option value="40_OL">Lvl 40 Overlord
					<option value="clear">clear all buffs
				</select></td></tr>

<tr>	<td>HP</td>
		<td><input type=checkbox id="BTBCHECK" onClick="calc()"></td>
		<td><select id="BTB" onChange="calc()">
			<option value="1">Bless the Body 1
			<option value="2">Bless the Body 2
			<option value="3">Bless the Body 3
			<option value="4">Bless the Body 4
			<option value="5">Bless the Body 5
			<option value="6">Bless the Body 6
			<option value="1" id="Battle_Roar1">Battle Roar 1
			<option value="2" id="Battle_Roar2">Battle Roar 2
			<option value="3" id="Battle_Roar3">Battle Roar 3
			<option value="4" id="Battle_Roar4">Battle Roar 4
			<option value="5" id="Battle_Roar5">Battle Roar 5
			<option value="6" id="Battle_Roar6">Battle Roar 6
			<option value="1">Body of Avatar 1
			<option value="2">Body of Avatar 2
			<option value="3">Body of Avatar 3
			<option value="4">Body of Avatar 4
			<option value="5">Body of Avatar 5
			<option value="6">Body of Avatar 6
		</select></td>

		<td><input type=checkbox id="VITCHECK" onClick="calc()"></td>
		<td><select id="VIT" onChange="calc()">
			<option value="SOV">Song of Vitality
		</select></td></tr>
<tr>
	<td>MP</td>
		<td><input type=checkbox id="BTSCHECK" onClick="calc()"></td>
		<td><select id="BTS" onChange="calc()">
			<option value="1">Bless the Soul 1
			<option value="2">Bless the Soul 2
			<option value="3">Bless the Soul 3
			<option value="4">Bless the Soul 4
			<option value="5">Bless the Soul 5
			<option value="6">Bless the Soul 6
		<td><input type=checkbox id="SAGCHECK" onClick="calc()"></td>

		<td><select id="SAG" onchange="calc()">
			<option value="1">Soul of Sagitarrius 1
			<option value="2">Soul of Sagitarrius 2
			<option value="3">Soul of Sagitarrius 3
			<option value="4">Soul of Sagitarrius 4
		</select></td></tr>
<tr>
	<td>P.Atk.</td>
		<td><input type=checkbox id="MIGHTCHECK" onClick="calc()"></td>
		<td><select id="MIGHT" onChange="calc()">
			<option value="1">Might 1
			<option value="2">Might 2
			<option value="3">Might 3
			<option value="3">Herb of Strength
			<option value="1" id="Attack_Aura1">Attack Aura 1
			<option value="2" id="Attack_Aura2">Attack Aura 2
			<option value="1">Chant of Battle 1
			<option value="2">Chant of Battle 2
			<option value="3">Chant of Battle 3
			<option value="1">Power of Paagrio 1
			<option value="2">Power of Paagrio 2
			<option value="3">Power of Paagrio 3
		</select></td>

		<td><input type=checkbox id="DOWACHECK" onClick="calc()"></td>
		<td><select id="DOWA" onChange="calc()">
			<option value="DOW">Dance of Warrior
		</select></td>
		<td><input type=checkbox id="GREATERMIGHTCHECK" onClick="calc()"></td>
		<td><select id="GREATERMIGHT" onChange="calc()">
			<option value="1">Greater Might 1
			<option value="2">Greater Might 2
			<option value="3">Greater Might 3
			<option value="1">War Chant 1
			<option value="2">War Chant 2
			<option value="3">War Chant 3
		</select></td>

		<td><input type=checkbox id="FRENZYCHECK" onClick="calc()">
		<input type=checkbox id="WARCRYCHECK" onClick="calc()"></td>
		<td><select id="FRENZY" onChange="calc()">
			<option value="1">Frenzy 1
			<option value="2" id="Frenzy2">Frenzy 2
			<option value="3" id="Frenzy3">Frenzy 3
		</select>
		<select id="WARCRY" onChange="calc()">
			<option value="1">War Cry 1
			<option value="2" id="Warcry2">War Cry 2
		</select></td>

</tr>
<tr>	<td></td>
		<td><input type=checkbox id="RAGECHECK" onClick="calc()"></td>
		<td><select id="RAGE" onChange="calc()">
			<option value="1">Rage 1
			<option value="2" id="Rage2">Rage 2
		</select></td></tr>
<tr>
	<td>M.Atk.</td>
		<td><input type=checkbox id="EMPCHECK" onClick="calc()"></td>

		<td><select id="EMP" onChange="calc()">
			<option value="1">Empower 1
			<option value="2">Empower 2
			<option value="3">Greater Empower 3
			<option value="3">Herb of Magic
			<option value="3">Soul of Paagrio
		</select></td>
		<td><input type=checkbox id="DOMYCHECK" onClick="calc()"></td>
		<td><select id="DOMY" onChange="calc()">
			<option value="DOM">Dance of Mystic
		</select></td></tr>
<tr>

	<td>P.Def.</td>
		<td><input type=checkbox id="SHIELDCHECK" onClick="calc()"></td>
		<td><select id="SHIELD" onChange="calc()">
			<option value="1">Shield 1
			<option value="2">Shield 2
			<option value="3">Shield 3
			<option value="1">Blessing of Paagrio 1
			<option value="2">Blessing of Paagrio 2
			<option value="3">Blessing of Paagrio 3
			<option value="1">Chant of Shielding 1
			<option value="2">Chant of Shielding 2
			<option value="3">Chant of Shielding 3
			<option value="1" id="DefenseAura1">Defense Aura 1
			<option value="2" id="DefenseAura2">Defense Aura 2
			<option value="1">Soul Shield 1
			<option value="2">Soul Shield 2
			<option value="3">Soul Shield 3
		</select></td>

		<td><input type=checkbox id="SOECHECK" onClick="calc()"></td>
		<td><select id="SOEA" onChange="calc()">
			<option value="SOE">Song of Earth
		</select></td>
		<td><input type=checkbox id="GREATERSHIELDCHECK" onClick="calc()"></td>
		<td><select id="GREATERSHIELD" onChange="calc()">
			<option value="1">Greater Shield 1
			<option value="2">Greater Shield 2
			<option value="3">Greater Shield 3
			<option value="1">Earth Chant 1
			<option value="2">Earth Chant 2
			<option value="3">Earth Chant 3
		</select></td>

		<td><input type=checkbox id="GUTSCHECK" onClick="calc()">
		<input type=checkbox id="MAJCHECK" onClick="calc()"></td>
		<td><select id="GUTS" onChange="calc()">
			<option value="1">Guts 1
			<option value="2" id="Guts2">Guts 2
			<option value="3" id="Guts3">Guts 3
		</select>
		<select id="MAJ" onChange="calc()">
			<option value="1">Majesty 1
			<option value="2" id="Majesty2">Majesty 2
			<option value="3" id="Majesty3">Majesty 3
		</select></td></tr>

<tr>
	<td>M.Def.</td>
		<td><input type=checkbox id="MBCHECK" onClick="calc()"></td>
		<td><select id="MB" onChange="calc()">
			<option value="2">Magic Barrier 1
			<option value="3">Magic Barrier 2
			<option value="1">Chant of Fire 1
			<option value="2">Chant of Fire 2
			<option value="3">Chant of Fire 3
			<option value="1">Glory of Paagrio 1
			<option value="2">Glory of Paagrio 2
			<option value="3">Glory of Paagrio 3
			<option value="1" id="Iron_Will1">Iron Will 1
			<option value="2" id="Iron_Will2">Iron Will 2
			<option value="3" id="Iron_Will3">Iron Will 3
			<option value="1" id="Spirit_Barrier1">Spirit Barrier 1
			<option value="2" id="Spirit_Barrier2">Spirit Barrier 2
			<option value="3" id="Spirit_Barrier3">Spirit Barrier 3
		</select></td>

		<td><input type=checkbox id="WARDCHECK" onClick="calc()"></td>
		<td><select id="WARD" onChange="calc()">
			<option value="SOW">Song of Warding
		</select></td></tr>
<tr>
	<td>Accuracy</td>
		<td><input type=checkbox id="GUIDCHECK" onClick="calc()"></td>
		<td><select id="GUID" onChange="calc()">
			<option value="1">Guidance 1
			<option value="2">Guidance 2
			<option value="3">Guidance 3
			<option value="1">Chant of Eagle 1
			<option value="2">Chant of Eagle 2
			<option value="3">Chant of Eagle 3
			<option value="1">Sight of Paagrio 1
			<option value="2">Sight of Paagrio 2
			<option value="3">Sight of Paagrio 3
		</select></td>

		<td><input type=checkbox id="DOICHECK" onClick="calc()"></td>
		<td><select id="DOI" onChange="calc()">
			<option value="DOI">Dance of Inspiration
		</select></td>
		<td><input type=checkbox id="HECHECK" onClick="calc()"></td>
		<td><select id="HE" onChange="calc()">
			<option value="1">Hawk Eye 1
			<option value="2">Hawk Eye 2
			<option value="3">Hawk Eye 3
		</select></td></tr>
<tr>

	<td>Evasion</td>
		<td><input type=checkbox id="AGICHECK" onClick="calc()"></td>
		<td><select id="AGI" onChange="calc()">
			<option value="1">Agility 1
			<option value="2">Agility 2
			<option value="3">Agility 3
			<option value="1">Chant of Evasion 1
			<option value="2">Chant of Evasion 2
			<option value="3">Chant of Evasion 3
			<option value="1">Tact of Paagrio 1
			<option value="2">Tact of Paagrio 2
			<option value="3">Tact of Paagrio 3
		</select></td>

		<td><input type=checkbox id="SWATCHECK" onClick="calc()"></td>
		<td><select id="SWAT" onChange="calc()">
			<option value="SOW">Song of Water
		</select></td>
		<td><input type=checkbox id="UECHECK" onClick="calc()"></td>
		<td><select id="UE" onChange="calc()">
			<option value="1">Ultimate Evasion 1
			<option value="2" id="Ultimate_Evasion2">Ultimate Evasion 2
		</select></td>
		<td><input type=checkbox id="EVSHTCHECK" onClick="calc()"></td>

		<td><select id="EVSHT" onChange="calc()">
			<option value="ES">Evasion Shot
		</select></td></tr>
<tr>
	<td>Critical</td>
		<td><input type=checkbox id="FCSCHECK" onClick="calc()"></td>
		<td><select id="FCS" onChange="calc()">
			<option value="1">Focus 1
			<option value="2">Focus 2
			<option value="3">Focus 3
			<option value="1">Chant of Predator 1
			<option value="2">Chant of Predator 2
			<option value="3">Chant of Predator 3
			<option value="3">Herb of Critical Attack
		</select></td>

		<td><input type=checkbox id="SOHCHECK" onClick="calc()"></td>
		<td><select id="SOH" onChange="calc()">
			<option value="SOH">Song of Hunter
		</select></td></tr>
<tr>
	<td>Speed</td>
		<td><input type=checkbox id="WWCHECK" onClick="calc()"></td>
		<td><select id="WW" onChange="calc()">
			<option value="1">Wind Walk 1
			<option value="2">Wind Walk 2
			<option value="1">Speed of Paagrio 1
			<option value="2">Speed of Paagrio 2
			<option value="1" id="Sprint1">Sprint 1
			<option value="2" id="Sprint2">Sprint 2
			<option value="1">Haste Potion
			<option value="2">Greater Quick Step Potion
			<option value="2">Herb of Speed
		</select></td>

		<td><input type=checkbox id="SWINDCHECK" onClick="calc()"></td>
		<td><select id="SWIND" onChange="calc()">
			<option value="SOW">Song of Wind
		</select></td>
		<td><input type=checkbox id="DSHCHECK" onClick="calc()"></td>
		<td><select id="DSH" onChange="calc()">
			<option value="DOS">Dance of Shadow
		</select></td>
		<td><input type=checkbox id="DASHCHECK" onClick="calc()">
		<input type=checkbox id="BLINDINGCHECK" onClick="calc()"></td>

		<td><select id="DASH" onChange="calc()">
			<option value="1">Dash 1
			<option value="2" id="Dash2">Dash 2
		</select>
		<select id="BLINDING" onChange="calc()">
			<option value="BB">Blinding Blow
		</select></td></tr>
<tr>
	<td>Atk. Spd.</td>
		<td><input type=checkbox id="HASTECHECK" onClick="calc()"></td>

		<td><select id="HASTE" onChange="calc()">
			<option value="1">Haste 1
			<option value="2">Haste 2
			<option value="1">Chant of Fury 1
			<option value="2">Chant of Fury 2
			<option value="1">Potion of Alacrity
			<option value="2">Greater Swift Attack Potion
			<option value="2">Herb of Atk. Speed
			</select></td>
		<td><input type=checkbox id="DFURYCHECK" onClick="calc()"></td>
		<td><select id="DFURY" onChange="calc()">
			<option value="DOF">Dance of Fury
		</select></td>

		<td><input type=checkbox id="RAPIDCHECK" onChange="calc()">
		<input type=checkbox id="DUELCHECK" onClick="calc()">
		<input type=checkbox id="TFCHECK" onClick="calc()"></td>
		<td><select id="RAPID" onChange="calc()">
			<option value="1">Rapid Shot 1
			<option value="2" id="Rapid_Shot2">Rapid Shot 2
		</select>
		<select id="DUEL" onChange="calc()">
			<option value="1">Duelist Spirit 1
			<option value="2">Duelist Spirit 2
		</select>

		<select id="TF" onChange="calc()">
			<option value="1">Thrill Fight 1
			<option value="2">Thrill Fight 2
		</select></td></tr>
<tr>
	<td>Casting Spd.</td>
		<td><input type=checkbox id="ACUCHECK" onClick="calc()"></td>
		<td><select id="ACU" onChange="calc()">
			<option value="1">Acumen 1
			<option value="2">Acumen 2
			<option value="3">Acumen 3
			<option value="1">Chant of Flame 1
			<option value="2">Chant of Flame 2
			<option value="3">Chant of Flame 3
			<option value="1">Wisdom of Paagrio 1
			<option value="2">Wisdom of Paagrio 2
			<option value="3">Wisdom of Paagrio 3
			<option value="2">Magic Haste Potion
			<option value="3">Greater Magic Haste Potion
			<option value="3">Herb of Casting Spd.
		</select></td>

		<td><input type=checkbox id="DCONCHECK" onClick="calc()"></td>
		<td><select id="DCON" onChange="calc()">
			<option value="DOC">Dance of Concentration
		</select></td></tr>
<tr>
	<td>Misc.</td>
		<td><input type=checkbox id="ZERKCHECK" onClick="calc()"></td>
		<td><select id="ZERK" onChange="calc()">
			<option value="1">Berserker Spirit 1
			<option value="2">Berserker Spirit 2
			<option value="1">Rage of Paagrio 1
			<option value="2">Rage of Paagrio 2
		</select></td>

		<td><input type=checkbox id="QUEENCHECK" onClick="calc()"></td>
		<td><select id="QUEEN" onChange="calc()">
			<option value="1">Blessing of Queen 1
			<option value="2">Blessing of Queen 2
			<option value="3">Blessing of Queen 3
			<option value="4">Gift of Queen 1
			<option value="5">Gift of Queen 2
			<option value="6">Gift of Queen 3
		</select></td>
		<td><input type=checkbox id="PROPHCHECK" onClick="calc()"></td>
		<td><select id="PROPH" onChange="calc()">
			<option value="COV">Chant of Victory
			<option value="POF">Prophecy of Fire
			<option value="PWA">Prophecy of Water
			<option value="PWI">Prophecy of Wind
		</select></td>

		<td><input type=checkbox id="UDCHECK" onClick="calc()">
		<input type=checkbox id="TOTEMCHECK" onClick="calc()">
		<input type=checkbox id="SNIPECHECK" onClick="calc()">
		<input type=checkbox id="FACHECK" onClick="calc()">
		<input type=checkbox id="STEALTHCHECK" onClick="calc()"></td>
		<td><select id="UD" onChange="calc()">
			<option value="1">Ultimate Defense 1
			<option value="2" id="Ultimate_Defense2">Ultimate Defense 2
			<option value="3" id="Vengeance">Vengeance
			<option value="4" id="Ultimate_Defense3">Ultimate Defense 2 +1
			<option value="5" id="Ultimate_Defense4">Ultimate Defense 2 +2
			<option value="6" id="Ultimate_Defense5">Ultimate Defense 2 +3
			<option value="7" id="Ultimate_Defense6">Ultimate Defense 2 +4
			<option value="8" id="Ultimate_Defense7">Ultimate Defense 2 +5
			<option value="9" id="Ultimate_Defense8">Ultimate Defense 2 +6
			<option value="10" id="Ultimate_Defense9">Ultimate Defense 2 +7
			<option value="11" id="Ultimate_Defense10">Ultimate Defense 2 +8
			<option value="12" id="Ultimate_Defense11">Ultimate Defense 2 +9
			<option value="13" id="Ultimate_Defense12">Ultimate Defense 2 +10
			<option value="14" id="Ultimate_Defense13">Ultimate Defense 2 +11
			<option value="15" id="Ultimate_Defense14">Ultimate Defense 2 +12
			<option value="16" id="Ultimate_Defense15">Ultimate Defense 2 +13
			<option value="17" id="Ultimate_Defense16">Ultimate Defense 2 +14
			<option value="18" id="Ultimate_Defense17">Ultimate Defense 2 +15
			<option value="19" id="Ultimate_Defense18">Ultimate Defense 2 +16
			<option value="20" id="Ultimate_Defense19">Ultimate Defense 2 +17
			<option value="21" id="Ultimate_Defense20">Ultimate Defense 2 +18
			<option value="22" id="Ultimate_Defense21">Ultimate Defense 2 +19
			<option value="23" id="Ultimate_Defense22">Ultimate Defense 2 +20
			<option value="24" id="Ultimate_Defense23">Ultimate Defense 2 +21
			<option value="25" id="Ultimate_Defense24">Ultimate Defense 2 +22
			<option value="26" id="Ultimate_Defense25">Ultimate Defense 2 +23
			<option value="27" id="Ultimate_Defense26">Ultimate Defense 2 +24
			<option value="28" id="Ultimate_Defense27">Ultimate Defense 2 +25
			<option value="29" id="Ultimate_Defense28">Ultimate Defense 2 +26
			<option value="30" id="Ultimate_Defense29">Ultimate Defense 2 +27
			<option value="31" id="Ultimate_Defense30">Ultimate Defense 2 +28
			<option value="32" id="Ultimate_Defense31">Ultimate Defense 2 +29
			<option value="33" id="Ultimate_Defense32">Ultimate Defense 2 +30
		</select>

		<select id="TOTEM" onChange="calc()">
			<option value="BEAR">Bear Spirit Totem
			<option value="BISON" id="Bison">Bison Spirit Totem
			<option value="HAWK" id="Hawk">Hawk Spirit Totem
			<option value="OGRE" id="Ogre">Ogre Spirit Totem
			<option value="PUMA" id="Puma">Puma Spirit Totem
			<option value="RABBIT" id="Rabbit">Rabbit Spirit Totem
			<option value="WOLF">Wolf Spirit Totem
		</select>
		<select id="SNIPE" onChange="calc()">
			<option value="1">Snipe 1
			<option value="2">Snipe 2
			<option value="3">Snipe 3
			<option value="4">Snipe 4
			<option value="5">Snipe 5
			<option value="6">Snipe 6
			<option value="7">Snipe 7
			<option value="8">Snipe 8
			<option value="9">Snipe 8 +1
			<option value="10">Snipe 8 +2
			<option value="11">Snipe 8 +3
			<option value="12">Snipe 8 +4
			<option value="13">Snipe 8 +5
			<option value="14">Snipe 8 +6
			<option value="15">Snipe 8 +7
			<option value="16">Snipe 8 +8
			<option value="17">Snipe 8 +9
			<option value="18">Snipe 8 +10
			<option value="19">Snipe 8 +11
			<option value="20">Snipe 8 +12
			<option value="21">Snipe 8 +13
			<option value="22">Snipe 8 +14
			<option value="23">Snipe 8 +15
			<option value="24">Snipe 8 +16
			<option value="25">Snipe 8 +17
			<option value="26">Snipe 8 +18
			<option value="27">Snipe 8 +19
			<option value="28">Snipe 8 +20
			<option value="29">Snipe 8 +21
			<option value="30">Snipe 8 +22
			<option value="31">Snipe 8 +23
			<option value="32">Snipe 8 +24
			<option value="33">Snipe 8 +25
			<option value="34">Snipe 8 +26
			<option value="35">Snipe 8 +27
			<option value="36">Snipe 8 +28
			<option value="37">Snipe 8 +29
			<option value="38">Snipe 8 +30
		</select>

		<select id="FA" onChange="calc()">
			<option value="FA">Focus Attack</select>
		<select id="STEALTH" onChange="calc()">
			<option value="1">Stealth 1
			<option value="3">Stealth 3</select></td></tr>
<tr>		<td></td>
		<td><input type=checkbox id="ZEALOTCHECK" onClick="calc()"></td>
		<td><select id="ZEALOT" onChange="calc()">

			<option value="2">Zealot 2
		</select></td>
		<td><input type=checkbox id="HEROICCHECK" onClick="calc()"></td>
		<td><select id="HEROIC" onChange="calc()">
			<option value="1">Heroic Berserker
			<option value="2">Heroic Miracle
			<option value="3">Heroic Valor
		</select></td>
		</tr></table>
</fieldset>
<fieldset id="TOGGLES">
<legend>Toggles</legend>

	<table class="statcalc">
<tr>
		<td><input type=checkbox id="SOULCHECK" onClick="calc()">
		<input type=checkbox id="ARPCHECK" onClick="calc()">
		<input type=checkbox id="GSCHECK" onClick="calc()">
		<input type=checkbox id="SGCHECK" onClick="calc()">
		<input type=checkbox id="ACCTCHECK" onClick="calc()">
		<input type=checkbox id="FOCDCHECK" onClick="calc()">
		<input type=checkbox id="SMCHECK" onClick="calc()">

		<input type=checkbox id="FFURYCHECK" onClick="calc()">
		<input type=checkbox id="ARAGICHECK" onClick="calc()">
		<input type=checkbox id="ARWISCHECK" onClick="calc()">
		<input type=checkbox id="PARRYCHECK" onClick="calc()">
		<input type=checkbox id="RIPOSTECHECK" onClick="calc()">
		<input type=checkbox id="POLEACCCHECK" onClick="calc()"></td>
		<td><select id="SOUL" onChange="calc()">
			<option value="1">Soul Cry 1
			<option value="2">Soul Cry 2
			<option value="3">Soul Cry 3
			<option value="4">Soul Cry 4
			<option value="5">Soul Cry 5
			<option value="6">Soul Cry 6
			<option value="7">Soul Cry 7
			<option value="8">Soul Cry 8
			<option value="9">Soul Cry 9
			<option value="10">Soul Cry 10
		</select>

		<select id="ARP" onChange="calc()">
			<option value="AP">Arcane Power
		</select>
		<select id="GS" onChange="calc()">
			<option value="1">Guard Stance 1
			<option value="2">Guard Stance 2
			<option value="3">Guard Stance 3
			<option value="4">Guard Stance 4
			<option value="5">Guard Stance 4 +1 (Power)
			<option value="6">Guard Stance 4 +2 (Power)
			<option value="7">Guard Stance 4 +3 (Power)
			<option value="8">Guard Stance 4 +4 (Power)
			<option value="9">Guard Stance 4 +5 (Power)
			<option value="10">Guard Stance 4 +6 (Power)
			<option value="11">Guard Stance 4 +7 (Power)
			<option value="12">Guard Stance 4 +8 (Power)
			<option value="13">Guard Stance 4 +9 (Power)
			<option value="14">Guard Stance 4 +10 (Power)
			<option value="15">Guard Stance 4 +11 (Power)
			<option value="16">Guard Stance 4 +12 (Power)
			<option value="17">Guard Stance 4 +13 (Power)
			<option value="18">Guard Stance 4 +14 (Power)
			<option value="19">Guard Stance 4 +15 (Power)
			<option value="20">Guard Stance 4 +16 (Power)
			<option value="21">Guard Stance 4 +17 (Power)
			<option value="22">Guard Stance 4 +18 (Power)
			<option value="23">Guard Stance 4 +19 (Power)
			<option value="24">Guard Stance 4 +20 (Power)
			<option value="25">Guard Stance 4 +21 (Power)
			<option value="26">Guard Stance 4 +22 (Power)
			<option value="27">Guard Stance 4 +23 (Power)
			<option value="28">Guard Stance 4 +24 (Power)
			<option value="29">Guard Stance 4 +25 (Power)
			<option value="30">Guard Stance 4 +26 (Power)
			<option value="31">Guard Stance 4 +27 (Power)
			<option value="32">Guard Stance 4 +28 (Power)
			<option value="33">Guard Stance 4 +29 (Power)
			<option value="34">Guard Stance 4 +30 (Power)
		</select>

		<select id="SG" onChange="calc()">
			<option value="1">Soul Guard 1
			<option value="2">Soul Guard 2
			<option value="3">Soul Guard 3
			<option value="4">Soul Guard 4
			<option value="5">Soul Guard 5
			<option value="6">Soul Guard 6
			<option value="7">Soul Guard 7
			<option value="8">Soul Guard 8
			<option value="9">Soul Guard 9
			<option value="10">Soul Guard 10
			<option value="11">Soul Guard 11
			<option value="12">Soul Guard 12
			<option value="13">Soul Guard 13
		</select>

		<select id="ACCT" onChange="calc()">
			<option value="ACC">Accuracy
		</select>
		<select id="FOCD" onChange="calc()">
			<option value="FD">Focus Death
		</select>
		<select id="SM" onChange="calc()">
			<option value="SM">Silent Move
		</select>
		<select id="FFURY" onChange="calc()">

			<option value="FF">Fist Fury
		</select>
		<select id="ARAGI" onChange="calc()">
			<option value="AA">Arcane Agility
		</select>
		<select id="ARWIS" onChange="calc()">
			<option value="AW">Arcane Wisdom
		</select>
		<select id="PARRY" onChange="calc()">
			<option value="PS">Parry Stance
		</select>

		<select id="RIPOSTE" onChange="calc()">
			<option value="RP">Riposte Stance
		</select>
		<select id="POLEACC" onChange="calc()">
			<option value="1">Polearm Accuracy 1
			<option value="2">Polearm Accuracy 2
			<option value="3">Polearm Accuracy 3</select>
		</td></tr>
	</table>
</fieldset>

<fieldset id="DEBUFFS">
<legend>Debuffs</legend>
	<table class="statcalc">
<tr>	<td>CP</td>
		<td><input type=checkbox id="TODCHECK" onClick="calc()"></td>
		<td><select id="TOD" onChange="calc()">
			<option value="TOD">Touch of Death
		</select></td></tr>
<tr>	<td>P.Atk.</td>

		<td><input type=checkbox id="DEPATKCHECK" onClick="calc()"></td>
		<td><select id="DEPATK" onChange="calc()">
			<option value="1">Curse: Weakness 1
			<option value="2">Curse: Weakness 2-5
			<option value="3">Curse: Weakness 6+
			<option value="3">Howl
			<option value="3">Poltergeist Cubic 1+
			<option value="2">Power Break 1-2
			<option value="3">Power Break 3+
		</select></td></tr>
<tr>
	<td>P.Def.</td>

		<td><input type=checkbox id="DEPDEFCHECK" onClick="calc()"></td>
		<td><select id="DEPDEF" onChange="calc()">
			<option value="1">Hex
			<option value="1">Poltergeist Cubic
		</select></td>
		<td><input type=checkbox id="BLKSCHECK" onClick="calc()"></td>
		<td><select id="BLKS" onChange="calc()">
			<option value="BLKS">Block Shield
			<option value="MBLKS">Mass Shield Block
		</select></td></tr>
<tr>	<td>M.Def.</td>

		<td><input type=checkbox id="GLOOMCHECK" onClick="calc()"></td>
		<td><select id="GLOOM" onChange="calc()">
			<option value="CG1">Curse Gloom
		</select></td></tr>
<tr>	<td>Accuracy
		<td><input type=checkbox id="DEACCCHECK" onClick="calc()"></td>
		<td><select id="DEACC" onChange="calc()">
			<option value="1">Curse Chaos 1
			<option value="2">Curse Chaos 2+
			<option value="1">Seal of Chaos 1-2
			<option value="2">Seal of Chaos 3+
		</select></td></tr>

<tr>	<td>Speed</td>
		<td><input type=checkbox id="DESPEEDCHECK" onClick="calc()"></td>
		<td><select id="DESPEED" onChange="calc()">
			<option value="1">Slow 1
			<option value="2">Slow 2+
			<option value="2">Blizzard
			<option value="1">Cripple 1-5
			<option value="2">Cripple 6+
			<option value="1">Entangle 1
			<option value="2">Entangle 2+
			<option value="1">Freezing Strike
			<option value="1">Frost Bolt
			<option value="2">Hamstring
			<option value="2">Hamstring Shot
			<option value="1">Ice Bolt
			<option value="2">Mass Slow
			<option value="1">Seal of Slow 1
			<option value="2">Seal of Slow 2
		</select></td>

		<td><input type=checkbox id="BWWCHECK" onClick="calc()"></td>
		<td><select id="BWW" onChange="calc()">
			<option value="BWW">Block Wind Walk
			<option value="MBWW">Mass Block Wind Walk
		</select></td></tr>
<tr>	<td>Atk. Spd.
		<td><input type=checkbox id="DEASPDCHECK" onClick="calc()"></td>
		<td><select id="DEASPD" onChange="calc()">
			<option value="1">Wind Shackle 1
			<option value="2">Wind Shackle 2-5
			<option value="3">Wind Shackle 6+
			<option value="3">Poltergeist Cubic
			<option value="3">Seal of Winter
			<option value="3">Spoil (Atk. Spd.)
			<option value="3">Spoil Festival (Atk. Spd.)
		</select></td></tr>

<tr>	<td>Misc.</td>
		<td><input type=checkbox id="ACCHECK" onClick="calc()"></td>
		<td><select id="ARCR" onChange="calc()">
			<option value="ARC1">Armor Crush
		</select></td>
		<td><input type=checkbox id="COABYSSCHECK" onClick="calc()"></td>
		<td><select id="COABYSS" onChange="calc()">
			<option value="COA1">Curse of Abyss
		</select></td>

		<td><input type=checkbox id="CODOOMCHECK" onClick="calc()"></td>
		<td><select id="CODOOM" onChange="calc()">
			<option value="COD1">Curse of Doom
		</select></td>
		<td><input type=checkbox id="COSHADECHECK" onClick="calc()"></td>
		<td><select id="COSHADE" onChange="calc()">
			<option value="1">Curse of Shade 1
			<option value="2">Curse of Shade 2
			<option value="3">Curse of Shade 3
			<option value="1">Mass Curse of Shade 1
			<option value="2">Mass Curse of Shade 2
			<option value="3">Mass Curse of Shade 3
		</select></td></tr>

<tr>	<td></td>
		<td><input type=checkbox id="FVCHECK" onClick="calc()"></td>
		<td><select id="FV" onChange="calc()">
			<option value="FV">Fire Vortex
		</select></td>
		<td><input type=checkbox id="CHOLCHECK" onClick="calc()"></td>
		<td><select id="CHOL" onChange="calc()">
			<option value="1">Hot Springs Cholera 1
			<option value="2">Hot Springs Cholera 2
			<option value="3">Hot Springs Cholera 3
			<option value="4">Hot Springs Cholera 4
			<option value="5">Hot Springs Cholera 5
			<option value="6">Hot Springs Cholera 6
			<option value="7">Hot Springs Cholera 7
			<option value="8">Hot Springs Cholera 8
			<option value="9">Hot Springs Cholera 9
			<option value="10">Hot Springs Cholera 10
		</select></td>

		<td><input type=checkbox id="MALCHECK" onClick="calc()"></td>
		<td><select id="MAL" onChange="calc()">
			<option value="1">Hot Springs Malaria 1
			<option value="2">Hot Springs Malaria 2
			<option value="3">Hot Springs Malaria 3
			<option value="4">Hot Springs Malaria 4
			<option value="5">Hot Springs Malaria 5
			<option value="6">Hot Springs Malaria 6
			<option value="7">Hot Springs Malaria 7
			<option value="8">Hot Springs Malaria 8
			<option value="9">Hot Springs Malaria 9
			<option value="10">Hot Springs Malaria 10
		</select></td>
		<td><input type=checkbox id="IVCHECK" onClick="calc()"></td>

		<td><select id="IV" onChange="calc()">
			<option value="IV">Ice Vortex
		</select></td></tr>
<tr>	<td></td>
		<td><input type=checkbox id="LVORCHECK" onClick="calc()"></td>
		<td><select id="LVOR" onChange="calc()">
			<option value="LV">Light Vortex
		</select></td>
		<td><input type=checkbox id="SBCHECK" onClick="calc()"></td>
		<td><select id="SB" onChange="calc()">

			<option value="SB">Shock Blast
		</select></td>
		<td><input type=checkbox id="DESPAIRCHECK" onClick="calc()"></td>
		<td><select id="DESPAIR" onChange="calc()">
			<option value="SOD1">Seal of Despair
		</select></td>
		<td><input type=checkbox id="WVCHECK" onClick="calc()"></td>
		<td><select id="WV" onChange="calc()">
			<option value="WV">Wind Vortex
		</select></td></tr>
		</table>
</body>
</html>