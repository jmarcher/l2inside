<? if(isset($_GET['file'])){?><style type="text/css">
<!--
.2364463246454236446237489792374238744dsafdf5 {font-size: 12px; font-weight: bold; font-family: Arial, Helvetica, sans-serif; }
.2364463246454236446237489792374238744dsafdf6 {
	font-size: 14px;
	font-weight: bold;
	font-family: Arial, Helvetica, sans-serif;
}
.2364463246454236446237489792374238744dsafdf11 {font-size: 12; font-family: Arial, Helvetica, sans-serif; }
.2364463246454236446237489792374238744dsafdf12 {font-size: 12}
.2364463246454236446237489792374238744dsafdf13 {
	font-size: 18px;
	font-weight: bold;
	font-family: Arial, Helvetica, sans-serif;
}
-->
</style>
<div align="center" class="2364463246454236446237489792374238744dsafdf13">Manor System</div>
<table width="90%" border="1" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse">
  <tr>
    <th colspan="12"><span class="2364463246454236446237489792374238744dsafdf6">Seeds</span></th>
  </tr>
  <tr>
    <th><span class="2364463246454236446237489792374238744dsafdf5">Seed Name</span></th>
    <th width="55"><span class="2364463246454236446237489792374238744dsafdf5">Levels</span></th>
    <th width="55"><span class="2364463246454236446237489792374238744dsafdf5">Reward 1</span></th>
    <th width="55"><span class="2364463246454236446237489792374238744dsafdf5">Reward 2</span></th>
    <th width="55"><span class="2364463246454236446237489792374238744dsafdf5">Gludio</span></th>
    <th width="55"><span class="2364463246454236446237489792374238744dsafdf5">Dion</span></th>
    <th width="55"><span class="2364463246454236446237489792374238744dsafdf5">Giran</span></th>
    <th width="55"><span class="2364463246454236446237489792374238744dsafdf5">Oren</span></th>
    <th width="55"><span class="2364463246454236446237489792374238744dsafdf5">Aden</span></th>
    <th width="55"><span class="2364463246454236446237489792374238744dsafdf5">Heine</span></th>
    <th width="55"><span class="2364463246454236446237489792374238744dsafdf5">Goddard</span></th>
    <th width="55"><span class="2364463246454236446237489792374238744dsafdf5">Rune</span></th>
  </tr>
  <tr>
    <td><span class="2364463246454236446237489792374238744dsafdf11">Alternative Dark Coda</span></td>
    <td align="center" width="55"><span class="2364463246454236446237489792374238744dsafdf11">5 - 15</span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1864" title="Stem" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_branch_gold_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1878" title="Braided Hemp" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_braided_hemp_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5650" title="Alternative Dark Coda Seed" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_coda_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5676" title="Alternative Dark Coda Seed" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_coda_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
  </tr>
  <tr>
    <td><span class="2364463246454236446237489792374238744dsafdf11">Dark Coda</span></td>
    <td align="center" width="55"><span class="2364463246454236446237489792374238744dsafdf11">5 - 15</span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1864" title="Stem" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_branch_gold_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1878" title="Braided Hemp" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_braided_hemp_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5016" title="Seed: Dark Coda" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_coda_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5042" title="Seed: Dark Coda" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_coda_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
  </tr>
  <tr>
    <td><span class="2364463246454236446237489792374238744dsafdf11">Alternative Red Coda</span></td>
    <td align="center" width="55"><span class="2364463246454236446237489792374238744dsafdf11">8 - 18</span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1865" title="Varnish" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_pouch_yellow_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1879" title="Cokes" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_oil_pot_black_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5651" title="Alternative Red Coda Seed" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_coda_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5677" title="Alternative Red Coda Seed" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_coda_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
  </tr>
  <tr>
    <td><span class="2364463246454236446237489792374238744dsafdf11">Red Coda</span></td>
    <td align="center" width="55"><span class="2364463246454236446237489792374238744dsafdf11">8 - 18</span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1865" title="Varnish" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_pouch_yellow_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1879" title="Cokes" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_oil_pot_black_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5017" title="Seed: Red Coda" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_coda_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5043" title="Seed: Red Coda" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_coda_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
  </tr>
  <tr>
    <td><span class="2364463246454236446237489792374238744dsafdf11">Alternative Chilly Coda</span></td>
    <td align="center" width="55"><span class="2364463246454236446237489792374238744dsafdf11">11 - 21</span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1866" title="Suede" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_suede_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1874" title="Oriharukon Ore" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_oriharukon_ore_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5652" title="Alternative Chilly Coda Seed" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_coda_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=7034" title="Seed: Alternative Chilly Coda" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_coda_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
  </tr>
  <tr>
    <td><span class="2364463246454236446237489792374238744dsafdf11">Chilly Coda</span></td>
    <td align="center" width="55"><span class="2364463246454236446237489792374238744dsafdf11">11 - 21</span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1866" title="Suede" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_suede_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1874" title="Oriharukon Ore" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_oriharukon_ore_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5018" title="Seed: Chilly Coda" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_coda_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=7020" title="Seed: Chilly Coda" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_coda_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
  </tr>
  <tr>
    <td><span class="2364463246454236446237489792374238744dsafdf11">Alternative Blue Coda</span></td>
    <td align="center" width="55"><span class="2364463246454236446237489792374238744dsafdf11">14 - 24</span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1867" title="Animal Skin" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_leather_brown_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1894" title="Crafted Leather" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_crafted_leather_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5653" title="Alternative Blue Coda Seed" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_coda_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5658" title="Alternative Blue Coda Seed" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_coda_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5678" title="Alternative Blue Coda Seed" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_coda_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
  </tr>
  <tr>
    <td><span class="2364463246454236446237489792374238744dsafdf11">Blue Coda</span></td>
    <td align="center" width="55"><span class="2364463246454236446237489792374238744dsafdf11">14 - 24</span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1867" title="Animal Skin" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_leather_brown_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1894" title="Crafted Leather" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_crafted_leather_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5019" title="Seed: Blue Coda" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_coda_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5024" title="Seed: Blue Coda" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_coda_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5044" title="Seed: Blue Coda" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_coda_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
  </tr>
  <tr>
    <td><span class="2364463246454236446237489792374238744dsafdf11">Alternative Golden Coda</span></td>
    <td align="center" width="55"><span class="2364463246454236446237489792374238744dsafdf11">17 - 27</span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1868" title="Thread" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_skein_white_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1876" title="Mithril Ore" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_mithril_ore_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5654" title="Alternative Golden Coda Seed" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_coda_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5659" title="Alternative Golden Coda Seed" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_coda_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=7040" title="Seed: Alternative Golden Coda" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_coda_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
  </tr>
  <tr>
    <td><span class="2364463246454236446237489792374238744dsafdf11">Golden Coda</span></td>
    <td align="center" width="55"><span class="2364463246454236446237489792374238744dsafdf11">17 - 27</span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1868" title="Thread" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_skein_white_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1876" title="Mithril Ore" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_mithril_ore_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5020" title="Seed: Golden Coda" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_coda_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5025" title="Seed: Golden Coda" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_coda_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=7026" title="Seed: Golden Coda" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_coda_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
  </tr>
  <tr>
    <td><span class="2364463246454236446237489792374238744dsafdf11">Alternative Lute Coda</span></td>
    <td align="center" width="55"><span class="2364463246454236446237489792374238744dsafdf11">20 - 30</span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1869" title="Iron Ore" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_lump_gray_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1895" title="Metallic Fiber" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_metallic_fiber_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5655" title="Alternative Lute Coda Seed" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_coda_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5660" title="Alternative Lute Coda Seed" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_coda_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
  </tr>
  <tr>
    <td><span class="2364463246454236446237489792374238744dsafdf11">Lute Coda</span></td>
    <td align="center" width="55"><span class="2364463246454236446237489792374238744dsafdf11">20 - 30</span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1869" title="Iron Ore" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_lump_gray_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1895" title="Metallic Fiber" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_metallic_fiber_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5021" title="Seed: Lute Coda" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_coda_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5026" title="Seed: Lute Coda" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_coda_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
  </tr>
  <tr>
    <td><span class="2364463246454236446237489792374238744dsafdf11">Alternative Desert Coda</span></td>
    <td align="center" width="55"><span class="2364463246454236446237489792374238744dsafdf11">23 - 33</span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1870" title="Coal" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_coal_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1877" title="Adamantite Nugget" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_adamantite_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5656" title="Alternative Desert Coda Seed" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_coda_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5661" title="Alternative Desert Coda Seed" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_coda_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
  </tr>
  <tr>
    <td><span class="2364463246454236446237489792374238744dsafdf11">Desert Coda</span></td>
    <td align="center" width="55"><span class="2364463246454236446237489792374238744dsafdf11">23 - 33</span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1870" title="Coal" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_coal_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1877" title="Adamantite Nugget" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_adamantite_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5022" title="Seed: Desert Coda" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_coda_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5027" title="Seed: Desert Coda" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_coda_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
  </tr>
  <tr>
    <th><span class="2364463246454236446237489792374238744dsafdf11">Name</span></th>
    <th width="55"><span class="2364463246454236446237489792374238744dsafdf11">Levels</span></th>
    <th width="55"><span class="2364463246454236446237489792374238744dsafdf11">Reward 1</span></th>
    <th width="55"><span class="2364463246454236446237489792374238744dsafdf11">Reward 2</span></th>
    <th width="55"><span class="2364463246454236446237489792374238744dsafdf11">Gludio</span></th>
    <th width="55"><span class="2364463246454236446237489792374238744dsafdf11">Dion</span></th>
    <th width="55"><span class="2364463246454236446237489792374238744dsafdf11">Giran</span></th>
    <th width="55"><span class="2364463246454236446237489792374238744dsafdf11">Oren</span></th>
    <th width="55"><span class="2364463246454236446237489792374238744dsafdf11">Aden</span></th>
    <th width="55"><span class="2364463246454236446237489792374238744dsafdf11">Heine</span></th>
    <th width="55"><span class="2364463246454236446237489792374238744dsafdf11">Goddard</span></th>
    <th width="55"><span class="2364463246454236446237489792374238744dsafdf11">Rune</span></th>
  </tr>
  <tr>
    <td><span class="2364463246454236446237489792374238744dsafdf11">Alternative Red Cobol</span></td>
    <td align="center" width="55"><span class="2364463246454236446237489792374238744dsafdf11">26 - 36</span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1871" title="Charcoal" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_charcoal_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=4042" title="Enria" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_gem_red_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=7044" title="Seed: Alternative Red Cobol" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_cobol_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5662" title="Alternative Red Cobol Seed" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_cobol_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5667" title="Alternative Red Cobol Seed" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_cobol_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5679" title="Alternative Red Cobol Seed" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_cobol_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
  </tr>
  <tr>
    <td><span class="2364463246454236446237489792374238744dsafdf11">Red Cobol</span></td>
    <td align="center" width="55"><span class="2364463246454236446237489792374238744dsafdf11">26 - 36</span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1871" title="Charcoal" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_charcoal_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=4042" title="Enria" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_gem_red_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=7051" title="Seed: Red Cobol" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_cobol_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5028" title="Seed: Red Cobol" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_cobol_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5033" title="Seed: Red Cobol" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_cobol_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5045" title="Seed: Red Cobol" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_cobol_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
  </tr>
  <tr>
    <td><span class="2364463246454236446237489792374238744dsafdf11">Alternative Chilly Cobol</span></td>
    <td align="center" width="55"><span class="2364463246454236446237489792374238744dsafdf11">29 - 39</span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1872" title="Animal Bone" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_piece_bone_white_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1880" title="Steel" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_lump_black_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=7033" title="Seed: Alternative Chilly Cobol" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_cobol_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5663" title="Alternative Chilly Cobol Seed" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_cobol_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5668" title="Alternative Chilly Cobol Seed" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_cobol_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5680" title="Alternative Chilly Cobol Seed" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_cobol_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5696" title="Alternative Chilly Cobol Seed" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_cobol_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
  </tr>
  <tr>
    <td><span class="2364463246454236446237489792374238744dsafdf11">Chilly Cobol</span></td>
    <td align="center" width="55"><span class="2364463246454236446237489792374238744dsafdf11">29 - 39</span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1872" title="Animal Bone" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_piece_bone_white_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1880" title="Steel" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_lump_black_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=7019" title="Seed: Chilly Cobol" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_cobol_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5029" title="Seed: Chilly Cobol" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_cobol_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5034" title="Seed: Chilly Cobol" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_cobol_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5046" title="Seed: Chilly Cobol" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_cobol_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5221" title="Seed: Chilly Cobol" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_cobol_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
  </tr>
  <tr>
    <td><span class="2364463246454236446237489792374238744dsafdf11">Alternative Blue Cobol</span></td>
    <td align="center" width="55"><span class="2364463246454236446237489792374238744dsafdf11">32 - 42</span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1873" title="Silver Nugget" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_silver_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=4044" title="Thons" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_gem_clear_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5657" title="Alternative Blue Cobol Seed" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_cobol_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=7030" title="Seed: Alternative Blue Cobol" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_cobol_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5669" title="Alternative Blue Cobol Seed" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_cobol_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5681" title="Alternative Blue Cobol Seed" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_cobol_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5697" title="Alternative Blue Cobol Seed" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_cobol_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
  </tr>
  <tr>
    <td><span class="2364463246454236446237489792374238744dsafdf11">Blue Cobol</span></td>
    <td align="center" width="55"><span class="2364463246454236446237489792374238744dsafdf11">32 - 42</span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1873" title="Silver Nugget" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_silver_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=4044" title="Thons" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_gem_clear_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5023" title="Seed: Blue Cobol" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_cobol_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=7016" title="Seed: Blue Cobol" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_cobol_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5035" title="Seed: Blue Cobol" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_cobol_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5047" title="Seed: Blue Cobol" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_cobol_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5222" title="Seed: Blue Cobol" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_cobol_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
  </tr>
  <tr>
    <td><span class="2364463246454236446237489792374238744dsafdf11">Alternative Thorn Cobol</span></td>
    <td align="center" width="55"><span class="2364463246454236446237489792374238744dsafdf11">35 - 45</span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1864" title="Stem" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_branch_gold_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5549" title="Metallic Thread" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_skein_gray_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5664" title="Alternative Thorn Cobol Seed" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_cobol_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5670" title="Alternative Thorn Cobol Seed" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_cobol_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5682" title="Alternative Thorn Cobol Seed" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_cobol_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5687" title="Alternative Thorn Cobol Seed" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_cobol_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5698" title="Alternative Thorn Cobol Seed" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_cobol_seed_i00_0.png" alt="L2InsideL2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
  </tr>
  <tr>
    <td><span class="2364463246454236446237489792374238744dsafdf11">Thorn Cobol</span></td>
    <td align="center" width="55"><span class="2364463246454236446237489792374238744dsafdf11">35 - 45</span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1864" title="Stem" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_branch_gold_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5549" title="Metallic Thread" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_skein_gray_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5030" title="Seed: Thorn Cobol" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_cobol_seed_i00_0.png" alt="L2InsideL2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5036" title="Seed: Thorn Cobol" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_cobol_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5048" title="Seed: Thorn Cobol" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_cobol_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5053" title="Seed: Thorn Cobol" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_cobol_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5223" title="Seed: Thorn Cobol" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_cobol_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
  </tr>
  <tr>
    <td><span class="2364463246454236446237489792374238744dsafdf11">Alternative Golden Cobol</span></td>
    <td align="center" width="55"><span class="2364463246454236446237489792374238744dsafdf11">38 - 48</span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1865" title="Varnish" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_pouch_yellow_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=4039" title="Mold Glue" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_reagent_red_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5665" title="Alternative Golden Cobol Seed" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_cobol_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5671" title="Alternative Golden Cobol Seed" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_cobol_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=7039" title="Seed: Alternative Golden Cobol" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_cobol_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5688" title="Alternative Golden Cobol Seed" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_cobol_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5699" title="Alternative Golden Cobol Seed" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_cobol_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
  </tr>
  <tr>
    <td><span class="2364463246454236446237489792374238744dsafdf11">Golden Cobol</span></td>
    <td align="center" width="55"><span class="2364463246454236446237489792374238744dsafdf11">38 - 48</span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1865" title="Varnish" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_pouch_yellow_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=4039" title="Mold Glue" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_reagent_red_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5031" title="Seed: Golden Cobol" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_cobol_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5037" title="Seed: Golden Cobol" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_cobol_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=7025" title="Seed: Golden Cobol" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_cobol_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5054" title="Seed: Golden Cobol" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_cobol_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5224" title="Seed: Golden Cobol" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_cobol_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
  </tr>
  <tr>
    <td><span class="2364463246454236446237489792374238744dsafdf11">Alternative Great Cobol</span></td>
    <td align="center" width="55"><span class="2364463246454236446237489792374238744dsafdf11">41 - 51</span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1866" title="Suede" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_suede_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1885" title="High Grade Suede" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_pouch_yellow_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5666" title="Alternative Great Cobol Seed" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_cobol_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5672" title="Alternative Great Cobol Seed" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_cobol_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=7041" title="Seed: Alternative Great Cobol" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_cobol_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5689" title="Alternative Great Cobol Seed" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_cobol_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5700" title="Alternative Great Cobol Seed" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_cobol_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
  </tr>
  <tr>
    <td><span class="2364463246454236446237489792374238744dsafdf11">Great Cobol</span></td>
    <td align="center" width="55"><span class="2364463246454236446237489792374238744dsafdf11">41 - 51</span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1866" title="Suede" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_suede_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1885" title="High Grade Suede" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_pouch_yellow_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5032" title="Seed: Great Cobol" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_cobol_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5038" title="Seed: Great Cobol" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_cobol_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=7027" title="Seed: Great Cobol" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_cobol_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5055" title="Seed: Great Cobol" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_cobol_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5225" title="Seed: Great Cobol" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_cobol_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
  </tr>
  <tr>
    <th><span class="2364463246454236446237489792374238744dsafdf11">Name</span></th>
    <th width="55"><span class="2364463246454236446237489792374238744dsafdf11">Levels</span></th>
    <th width="55"><span class="2364463246454236446237489792374238744dsafdf11">Reward 1</span></th>
    <th width="55"><span class="2364463246454236446237489792374238744dsafdf11">Reward 2</span></th>
    <th width="55"><span class="2364463246454236446237489792374238744dsafdf11">Gludio</span></th>
    <th width="55"><span class="2364463246454236446237489792374238744dsafdf11">Dion</span></th>
    <th width="55"><span class="2364463246454236446237489792374238744dsafdf11">Giran</span></th>
    <th width="55"><span class="2364463246454236446237489792374238744dsafdf11">Oren</span></th>
    <th width="55"><span class="2364463246454236446237489792374238744dsafdf11">Aden</span></th>
    <th width="55"><span class="2364463246454236446237489792374238744dsafdf11">Heine</span></th>
    <th width="55"><span class="2364463246454236446237489792374238744dsafdf11">Goddard</span></th>
    <th width="55"><span class="2364463246454236446237489792374238744dsafdf11">Rune</span></th>
  </tr>
  <tr>
    <td><span class="2364463246454236446237489792374238744dsafdf11">Alternative Red Codran</span></td>
    <td align="center" width="55"><span class="2364463246454236446237489792374238744dsafdf11">44 - 54</span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1867" title="Animal Skin" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_leather_brown_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=4043" title="Asofe" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_gem_blue_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=7045" title="Seed: Alternative Red Codran" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_codran_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5673" title="Alternative Red Codran Seed" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_codran_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=7046" title="Seed: Alternative Red Codran" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_codran_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5690" title="Alternative Red Codran Seed" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_codran_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5701" title="Alternative Red Codran Seed" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_codran_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
  </tr>
  <tr>
    <td><span class="2364463246454236446237489792374238744dsafdf11">Red Codran</span></td>
    <td align="center" width="55"><span class="2364463246454236446237489792374238744dsafdf11">44 - 54</span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1867" title="Animal Skin" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_leather_brown_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=4043" title="Asofe" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_gem_blue_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=7052" title="Seed: Red Codran" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_codran_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5039" title="Seed: Red Codran" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_codran_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=7053" title="Seed: Red Codran" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_codran_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5056" title="Seed: Red Codran" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_codran_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5226" title="Seed: Red Codran" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_codran_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
  </tr>
  <tr>
    <td><span class="2364463246454236446237489792374238744dsafdf11">Alternative Sea Codran</span></td>
    <td align="center" width="55"><span class="2364463246454236446237489792374238744dsafdf11">45 - 55</span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1868" title="Thread" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_skein_white_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1884" title="Cord" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_skein_gray_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=7035" title="Seed: Alternative Chilly Codran" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_codran_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=7036" title="Seed: Alternative Chilly Codran" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_codran_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5684" title="Alternative Chilly Codran Seed" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_codran_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5691" title="Alternative Chilly Codran Seed" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_codran_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
  </tr>
  <tr>
    <td><span class="2364463246454236446237489792374238744dsafdf11">Sea Codran</span></td>
    <td align="center" width="55"><span class="2364463246454236446237489792374238744dsafdf11">45 - 55</span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1868" title="Thread" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_skein_white_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1884" title="Cord" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_skein_gray_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=7054" title="Seed: Sea Codran" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_codran_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=7055" title="Seed: Sea Codran" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_codran_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5049" title="Seed: Sea Codran" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_codran_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=7056" title="Seed: Sea Codran" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_codran_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5227" title="Seed: Sea Codran" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_codran_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
  </tr>
  <tr>
    <td><span class="2364463246454236446237489792374238744dsafdf11">Alternative Chilly Codran</span></td>
    <td align="center" width="55"><span class="2364463246454236446237489792374238744dsafdf11">47 - 57</span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1869" title="Iron Ore" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_lump_gray_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=4041" title="Mold Hardener" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_reagent_blue_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=7035" title="Seed: Alternative Chilly Codran" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_codran_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=7036" title="Seed: Alternative Chilly Codran" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_codran_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5684" title="Alternative Chilly Codran Seed" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_codran_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5691" title="Alternative Chilly Codran Seed" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_codran_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
  </tr>
  <tr>
    <td><span class="2364463246454236446237489792374238744dsafdf11">Chilly Codran</span></td>
    <td align="center" width="55"><span class="2364463246454236446237489792374238744dsafdf11">47 - 57</span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1869" title="Iron Ore" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_lump_gray_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=4041" title="Mold Hardener" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_reagent_blue_i00_0.png" alt="L2InsideL2InsideL2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=7021" title="Seed: Chilly Codran" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_codran_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=7022" title="Seed: Chilly Codran" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_codran_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5050" title="Seed: Chilly Codran" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_codran_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5057" title="Seed: Chilly Codran" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_codran_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
  </tr>
  <tr>
    <td><span class="2364463246454236446237489792374238744dsafdf11">Alternative Blue Codran</span></td>
    <td align="center" width="55"><span class="2364463246454236446237489792374238744dsafdf11">50 - 60</span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1870" title="Coal" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_coal_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1882" title="Leather" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_leather_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=7031" title="Seed: Alternative Blue Codran" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_codran_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=7032" title="Seed: Alternative Blue Codran" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_codran_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5685" title="Alternative Blue Codran Seed" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_codran_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5692" title="Alternative Blue Codran Seed" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_codran_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
  </tr>
  <tr>
    <td><span class="2364463246454236446237489792374238744dsafdf11">Blue Codran</span></td>
    <td align="center" width="55"><span class="2364463246454236446237489792374238744dsafdf11">50 - 60</span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1870" title="Coal" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_coal_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1882" title="Leather" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_leather_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=7017" title="Seed: Blue Codran" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_codran_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=7018" title="Seed: Blue Codran" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_codran_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5051" title="Seed: Blue Codran" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_codran_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5058" title="Seed: Blue Codran" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_codran_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
  </tr>
  <tr>
    <td><span class="2364463246454236446237489792374238744dsafdf11">Alternative Twin Codran</span></td>
    <td align="center" width="55"><span class="2364463246454236446237489792374238744dsafdf11">53 - 63</span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1871" title="Charcoal" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_charcoal_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=4040" title="Mold Lubricant" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_reagent_gold_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=7050" title="Seed: Alternative Twin Codran" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_codran_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5674" title="Alternative Twin Codran Seed" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_codran_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5686" title="Alternative Twin Codran Seed" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_codran_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5693" title="Alternative Twin Codran Seed" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_codran_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
  </tr>
  <tr>
    <td><span class="2364463246454236446237489792374238744dsafdf11">Twin Codran</span></td>
    <td align="center" width="55"><span class="2364463246454236446237489792374238744dsafdf11">53 - 63</span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1871" title="Charcoal" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_charcoal_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=4040" title="Mold Lubricant" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_reagent_gold_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=7057" title="Seed: Twin Codran" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_codran_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5052" title="Seed: Twin Codran" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_codran_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5059" title="Seed: Twin Codran" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_codran_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
  </tr>
  <tr>
    <td><span class="2364463246454236446237489792374238744dsafdf11">Alternative Great Codran</span></td>
    <td align="center" width="55"><span class="2364463246454236446237489792374238744dsafdf11">56 - 66</span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1872" title="Animal Bone" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_piece_bone_white_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1881" title="Coarse Bone Powder" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_powder_white_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=7042" title="Seed: Alternative Great Codran" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_codran_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=7043" title="Seed: Alternative Great Codran" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_codran_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5694" title="Alternative Great Codran Seed" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_codran_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
  </tr>
  <tr>
    <td><span class="2364463246454236446237489792374238744dsafdf11">Great Codran</span></td>
    <td align="center" width="55"><span class="2364463246454236446237489792374238744dsafdf11">56 - 66</span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1872" title="Animal Bone" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_piece_bone_white_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1881" title="Coarse Bone Powder" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_powder_white_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=7028" title="Seed: Great Codran" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_codran_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=7029" title="Seed: Great Codran" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_codran_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5060" title="Seed: Great Codran" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_codran_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
  </tr>
  <tr>
    <td><span class="2364463246454236446237489792374238744dsafdf11">Alternative Desert Codran</span></td>
    <td align="center" width="55"><span class="2364463246454236446237489792374238744dsafdf11">61 - 71</span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1873" title="Silver Nugget" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_silver_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5550" title="Durable Metal Plate" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_squares_wood_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=7037" title="Seed: Alternative Desert Codran" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_codran_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5675" title="Alternative Desert Codran Seed" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_codran_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5695" title="Alternative Desert Codran Seed" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_codran_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=7038" title="Seed: Alternative Desert Codran" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_codran_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
  </tr>
  <tr>
    <td><span class="2364463246454236446237489792374238744dsafdf11">Desert Codran</span></td>
    <td align="center" width="55"><span class="2364463246454236446237489792374238744dsafdf11">61 - 71</span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1873" title="Silver Nugget" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_silver_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5550" title="Durable Metal Plate" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_squares_wood_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=7023" title="Seed: Desert Codran" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_codran_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5041" title="Seed: Desert Codran" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_codran_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5061" title="Seed: Desert Codran" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_codran_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=7024" title="Seed: Desert Codran" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_codran_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
  </tr>
  <tr>
    <th><span class="2364463246454236446237489792374238744dsafdf11">Name</span></th>
    <th width="55"><span class="2364463246454236446237489792374238744dsafdf11">Levels</span></th>
    <th width="55"><span class="2364463246454236446237489792374238744dsafdf11">Reward 1</span></th>
    <th width="55"><span class="2364463246454236446237489792374238744dsafdf11">Reward 2</span></th>
    <th width="55"><span class="2364463246454236446237489792374238744dsafdf11">Gludio</span></th>
    <th width="55"><span class="2364463246454236446237489792374238744dsafdf11">Dion</span></th>
    <th width="55"><span class="2364463246454236446237489792374238744dsafdf11">Giran</span></th>
    <th width="55"><span class="2364463246454236446237489792374238744dsafdf11">Oren</span></th>
    <th width="55"><span class="2364463246454236446237489792374238744dsafdf11">Aden</span></th>
    <th width="55"><span class="2364463246454236446237489792374238744dsafdf11">Heine</span></th>
    <th width="55"><span class="2364463246454236446237489792374238744dsafdf11">Goddard</span></th>
    <th width="55"><span class="2364463246454236446237489792374238744dsafdf11">Rune</span></th>
  </tr>
  <tr>
    <td><span class="2364463246454236446237489792374238744dsafdf11">Alternative Blue Coba</span></td>
    <td align="center" width="55"><span class="2364463246454236446237489792374238744dsafdf11">62 - 72</span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1894" title="Crafted Leather" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_crafted_leather_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=4039" title="Mold Glue" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_reagent_red_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=6742" title="Seed: Alternative Blue Coba" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_blue_coba_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=6743" title="Seed: Alternative Blue Coba" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_blue_coba_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=6744" title="Seed: Alternative Blue Coba" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_blue_coba_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=6745" title="Seed: Alternative Blue Coba" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_blue_coba_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=6746" title="Seed: Alternative Blue Coba" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_blue_coba_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
  </tr>
  <tr>
    <td><span class="2364463246454236446237489792374238744dsafdf11">Blue Coba</span></td>
    <td align="center" width="55"><span class="2364463246454236446237489792374238744dsafdf11">62 - 72</span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1894" title="Crafted Leather" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_crafted_leather_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=4039" title="Mold Glue" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_reagent_red_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=6768" title="Seed: Blue Coba" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_blue_coba_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=6769" title="Seed: Blue Coba" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_blue_coba_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=6770" title="Seed: Blue Coba" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_blue_coba_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=6771" title="Seed: Blue Coba" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_blue_coba_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=6772" title="Seed: Blue Coba" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_blue_coba_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
  </tr>
  <tr>
    <td><span class="2364463246454236446237489792374238744dsafdf11">Alternative Red Coba</span></td>
    <td align="center" width="55"><span class="2364463246454236446237489792374238744dsafdf11">65 - 75</span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1888" title="Synthetic Cokes" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_synthetic_cokes_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=4040" title="Mold Lubricant" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_reagent_gold_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=6731" title="Seed: Alternative Red Coba" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_red_coba_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=6732" title="Seed: Alternative Red Coba" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_red_coba_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=6733" title="Seed: Alternative Red Coba" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_red_coba_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=6734" title="Seed: Alternative Red Coba" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_red_coba_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=6735" title="Seed: Alternative Red Coba" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_red_coba_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
  </tr>
  <tr>
    <td><span class="2364463246454236446237489792374238744dsafdf11">Red Coba</span></td>
    <td align="center" width="55"><span class="2364463246454236446237489792374238744dsafdf11">65 - 75</span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1888" title="Synthetic Cokes" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_synthetic_cokes_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=4040" title="Mold Lubricant" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_reagent_gold_i00_0.png" alt="L2InsideL2InsideL2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=6757" title="Seed: Red Coba" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_red_coba_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=6758" title="Seed: Red Coba" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_red_coba_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=6759" title="Seed: Red Coba" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_red_coba_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=6760" title="Seed: Red Coba" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_red_coba_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=6761" title="Seed: Red Coba" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_red_coba_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
  </tr>
  <tr>
    <td><span class="2364463246454236446237489792374238744dsafdf11">Alternative Golden Coba</span></td>
    <td align="center" width="55"><span class="2364463246454236446237489792374238744dsafdf11">68 - 78</span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1887" title="Varnish of Purity" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_potion_clear_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=4041" title="Mold Hardener" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_reagent_blue_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=6747" title="Seed: Alternative Golden Coba" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_golden_coba_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=6748" title="Seed: Alternative Golden Coba" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_golden_coba_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=6749" title="Seed: Alternative Golden Coba" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_golden_coba_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=6750" title="Seed: Alternative Golden Coba" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_golden_coba_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=6751" title="Seed: Alternative Golden Coba" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_golden_coba_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=6752" title="Seed: Alternative Golden Coba" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_golden_coba_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
  </tr>
  <tr>
    <td><span class="2364463246454236446237489792374238744dsafdf11">Golden Coba</span></td>
    <td align="center" width="55"><span class="2364463246454236446237489792374238744dsafdf11">68 - 78</span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1887" title="Varnish of Purity" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_potion_clear_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=4041" title="Mold Hardener" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_reagent_blue_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=6773" title="Seed: Golden Coba" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_golden_coba_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=6774" title="Seed: Golden Coba" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_golden_coba_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=6775" title="Seed: Golden Coba" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_golden_coba_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=6776" title="Seed: Golden Coba" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_golden_coba_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=6777" title="Seed: Golden Coba" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_golden_coba_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=6778" title="Seed: Golden Coba" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_golden_coba_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
  </tr>
  <tr>
    <td><span class="2364463246454236446237489792374238744dsafdf11">Alternative Desert Coba</span></td>
    <td align="center" width="55"><span class="2364463246454236446237489792374238744dsafdf11">71 - 81</span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=4043" title="Asofe" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_gem_blue_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1890" title="Mithril Alloy" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_lump_white_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=6736" title="Seed: Alternative Desert Coba" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_desert_coba_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=6737" title="Seed: Alternative Desert Coba" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_desert_coba_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=6738" title="Seed: Alternative Desert Coba" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_desert_coba_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=6739" title="Seed: Alternative Desert Coba" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_desert_coba_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=6740" title="Seed: Alternative Desert Coba" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_desert_coba_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
  </tr>
  <tr>
    <td><span class="2364463246454236446237489792374238744dsafdf11">Desert Coba</span></td>
    <td align="center" width="55"><span class="2364463246454236446237489792374238744dsafdf11">71 - 81</span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=4043" title="Asofe" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_gem_blue_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1890" title="Mithril Alloy" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_lump_white_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=6762" title="Seed: Desert Coba" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_desert_coba_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=6763" title="Seed: Desert Coba" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_desert_coba_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=6764" title="Seed: Desert Coba" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_desert_coba_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=6765" title="Seed: Desert Coba" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_desert_coba_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=6766" title="Seed: Desert Coba" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_desert_coba_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
  </tr>
  <tr>
    <td><span class="2364463246454236446237489792374238744dsafdf11">Alternative Sea Coba</span></td>
    <td align="center" width="55"><span class="2364463246454236446237489792374238744dsafdf11">74 - 84</span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=4044" title="Thons" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_gem_clear_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=4042" title="Enria" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_gem_red_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=6728" title="Seed: Alternative Sea Coba" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_sea_coba_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=6729" title="Seed: Alternative Sea Coba" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_sea_coba_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=6730" title="Seed: Alternative Sea Coba" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_sea_coba_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
  </tr>
  <tr>
    <td><span class="2364463246454236446237489792374238744dsafdf11">Sea Coba</span></td>
    <td align="center" width="55"><span class="2364463246454236446237489792374238744dsafdf11">74 - 84</span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=4044" title="Thons" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_gem_clear_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=4042" title="Enria" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_gem_red_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=6754" title="Seed: Sea Coba" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_sea_coba_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=6755" title="Seed: Sea Coba" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_sea_coba_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=6756" title="Seed: Sea Coba" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_sea_coba_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
  </tr>
  <tr>
    <td><span class="2364463246454236446237489792374238744dsafdf11">Alternative Twin Coba</span></td>
    <td align="center" width="55"><span class="2364463246454236446237489792374238744dsafdf11">77 - 87</span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1885" title="High Grade Suede" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_pouch_yellow_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1893" title="Oriharukon" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_oriharukon_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=6741" title="Seed: Alternative Twin Coba" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_twin_coba_seed_i00_0.png" alt="L2InsideL2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
  </tr>
  <tr>
    <td><span class="2364463246454236446237489792374238744dsafdf11">Twin Coba</span></td>
    <td align="center" width="55"><span class="2364463246454236446237489792374238744dsafdf11">77 - 87</span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1885" title="High Grade Suede" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_pouch_yellow_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=1893" title="Oriharukon" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_oriharukon_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=6767" title="Seed: Twin Coba" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_twin_coba_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
  </tr>
  <tr>
    <td><span class="2364463246454236446237489792374238744dsafdf11">Alternative Great Coba</span></td>
    <td align="center" width="55"><span class="2364463246454236446237489792374238744dsafdf11">80 - 90</span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5549" title="Metallic Thread" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_skein_gray_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5550" title="Durable Metal Plate" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_squares_wood_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=6727" title="Seed: Alternative Great Coba" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_great_coba_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
  </tr>
  <tr>
    <td><span class="2364463246454236446237489792374238744dsafdf11">Great Coba</span></td>
    <td align="center" width="55"><span class="2364463246454236446237489792374238744dsafdf11">80 - 90</span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5549" title="Metallic Thread" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_skein_gray_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td align="center" width="55"><a href="index.php?file=item&id=5550" title="Durable Metal Plate" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_squares_wood_i00_0.png" alt="L2Inside" border="1" /></a></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td width="55"><span class="2364463246454236446237489792374238744dsafdf12"></span></td>
    <td align="center" width="55"><a href="index.php?file=item&id=6753" title="Seed: Great Coba" class="2364463246454236446237489792374238744dsafdf11"><img src="image/icons/etc_great_coba_seed_i00_0.png" alt="L2Inside" border="1" /></a></td>
  </tr>
  <tr>
    <th><span class="2364463246454236446237489792374238744dsafdf11">Name</span></th>
    <th width="55"><span class="2364463246454236446237489792374238744dsafdf11">Levels</span></th>
    <th width="55"><span class="2364463246454236446237489792374238744dsafdf11">Reward 1</span></th>
    <th width="55"><span class="2364463246454236446237489792374238744dsafdf11">Reward 2</span></th>
    <th width="55"><span class="2364463246454236446237489792374238744dsafdf11">Gludio</span></th>
    <th width="55"><span class="2364463246454236446237489792374238744dsafdf11">Dion</span></th>
    <th width="55"><span class="2364463246454236446237489792374238744dsafdf11">Giran</span></th>
    <th width="55"><span class="2364463246454236446237489792374238744dsafdf11">Oren</span></th>
    <th width="55"><span class="2364463246454236446237489792374238744dsafdf11">Aden</span></th>
    <th width="55"><span class="2364463246454236446237489792374238744dsafdf11">Heine</span></th>
    <th width="55"><span class="2364463246454236446237489792374238744dsafdf11">Goddard</span></th>
    <th width="55"><span class="2364463246454236446237489792374238744dsafdf11">Rune</span></th>
  </tr>
</table>
<? }else{
	include "error.php";
	}
 ?>